# Proyecto API Gateway

Realización de una API en Framework Laravel PHP

## Construcción

Se realiza API con routing segun estandar OpenAPI usando metodos GET, POST, DELETE

## Requisitos de instalación

Es necesario la instalación de composer y laravel para montar el servidor local. Base de datos MYSQL

## Ejemplos de Requests

```
GET api/v1/mascota/get
```

```
GET api/v1/mascota/{id}/get
```

```
POST api/v1/mascota/add
```

## Ejemplo de respuesta

```
{"resultado": {
   "id": 4,
   "dni": "00001003",
   "nombre": "MACHIN",
   "apellido": "JARAMILLO PEREZ",
   "sexo": "Macho",
   "fecha_nacimiento": "2013-01-10",
   "mascota_raza_id": 4,
   "created_at": null,
   "updated_at": null,
   "mascota_raza":    {
      "id": 4,
      "tipo": "Pastor Aleman",
      "descripcion": null,
      "estado": 1,
      "mascota_tipo_id": 1,
      "created_at": null,
      "updated_at": null,
      "mascota_tipo":       {
         "id": 1,
         "tipo": "CANINO",
         "descripcion": null,
         "estado": 1,
         "created_at": null,
         "updated_at": null
      }
   },
   "mascota_salud":    {
      "id": 4,
      "visita": 1,
      "alergia_medicamento": 1,
      "vacunacion": 1,
      "antirrabica": 1,
      "alergia": 1,
      "enfermedad": 0,
      "enfermedad_descripcion": null,
      "fecha_desparacitacion": null,
      "estado": 1,
      "mascota_id": 4,
      "created_at": null,
      "updated_at": null
   }
}}
```

## Caracteristicas principales de API

### Routing:
```
Route::group(['prefix'=> 'v1/mascota'], function(){
    Route::get('{id}/get', ['uses'=>'ControladorMascota@obtenerMascota']);
    Route::get('get', ['uses'=>'ControladorMascota@obtenerMascotas']);
    Route::post('add', ['uses'=>'ControladorMascota@crearMascota']);
    Route::delete('remove', ['uses'=>'ControladorMascota@eliminarMascota']);
});
```

### Validación de parametros
```
$validacionMascota = Validator::make($request->all(), [
                        'nombre' => 'required|min:1',
                        'apellido' => 'required|min:1',
                        'sexo' => 'required|min:1',
                        'fecha_nacimiento' => 'required|min:1',
                        'mascota_raza_id' => 'required|min:1',
            ]);
$validacionMascotaSalud = Validator::make($request->mascotaSalud, [
			'visita' => 'required',
			'alergia_medicamento' => 'required',
			'vacunacion' => 'required',
			'antirrabica' => 'required',
			'alergia' => 'required',
			'enfermedad' => 'required',
			'enfermedad_descripcion' => 'required|min:1',
]);
```

### Mapeo, orquestación y filtrado
```
$mascota_raza = MascotaRaza::where('tipo', $request->mascota_raza_id)->first();
$mascota_raza_id = $mascota_raza->id;
$mascota = new Mascota($request->all());
$mascota->mascota_raza_id = $mascota_raza_id;
$mascota->dni = $this->generarDNI();
$bandera1 = $mascota->save();
$mascotaSalud = new MascotaSalud($request->mascotaSalud);
$mascotaSalud->mascota_id = $mascota->id;
```
	
### Salida o respuesta.
```
return new JsonResponse(['resultado' => $mascota], 201);	
```

## Comandos para instalar

- php artisan migrate
- php artisan db:seed

## Proyecto SOAPUI 

Dentro del proyecto existe un archivo xml llamado: APIGATEWAY.xml, es un proyecto SOAPUI con request y responses de las operaciones

## Authors

Jorge Alejandro Aguirre Gutierrez
alejoved@gmail.com

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details