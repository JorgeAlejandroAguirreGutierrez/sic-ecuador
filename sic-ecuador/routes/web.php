<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', ['uses'=>'usuarios\ControladorUsuarios@vistaInicioSesion']);
Route::get('/index', ['uses'=>'usuarios\ControladorUsuarios@vistaIndex']);
Route::get('/menu', ['uses'=>'usuarios\ControladorUsuarios@vistaMenu']);

Route::group(['prefix'=> 'usuarios'], function(){
    Route::get('lae', ['uses'=>'usuarios\ControladorUsuarios@vistaLeerActualizarEliminar']);
    Route::get('c', ['uses'=>'usuarios\ControladorUsuarios@vistaCrear']);  
    Route::get('mostrarInicioSesion', ['uses'=>'usuarios\ControladorUsuarios@vistaMostrarInicioSesion']);
    Route::post('crearUsuario', ['uses'=>'usuarios\ControladorUsuarios@crearUsuario']);
    Route::post('actualizarUsuario', ['uses'=>'usuarios\ControladorUsuarios@actualizarUsuario']);
    Route::post('activarSesion', ['uses'=>'usuarios\ControladorUsuarios@activarSesion']);
    Route::post('iniciarSesion', ['uses'=>'usuarios\ControladorUsuarios@iniciarSesion']);
    Route::post('cerrarSesion', ['uses'=>'usuarios\ControladorUsuarios@cerrarSesion']);
    Route::post('verificarIniciarSesion', ['uses'=>'usuarios\ControladorUsuarios@verificarIniciarSesion']);
    Route::post('obtenerCodigoUsuario', ['uses'=>'usuarios\ControladorUsuarios@obtenerCodigoUsuario']);
    Route::post('obtenerUsuarios', ['uses'=>'usuarios\ControladorUsuarios@obtenerUsuarios']);
    Route::post('obtenerUsuario', ['uses'=>'usuarios\ControladorUsuarios@obtenerUsuario']);
    Route::post('obtenerVendedores', ['uses'=>'usuarios\ControladorUsuarios@obtenerVendedores']);
    Route::post('obtenerCajeros', ['uses'=>'usuarios\ControladorUsuarios@obtenerCajeros']);
    Route::post('eliminarUsuario', ['uses'=>'usuarios\ControladorUsuarios@eliminarUsuario']);
    Route::post('obtenerEstablecimientoUsuario', ['uses'=>'usuarios\ControladorUsuarios@obtenerEstablecimientoUsuario']);
    Route::post('obtenerPuntoVentaUsuario', ['uses'=>'usuarios\ControladorUsuarios@obtenerPuntoVentaUsuario']);
});

Route::group(['prefix'=> 'clientes'], function(){
    Route::get('le', ['uses'=>'clientes\ControladorClientes@vistaLeerEliminar']);
    Route::get('c', ['uses'=>'clientes\ControladorClientes@vistaCrear']);
    Route::get('a/{id}', ['uses'=>'clientes\ControladorClientes@vistaActualizar']);
    Route::get('laeClientesAuxiliares', ['uses'=>'clientes\ControladorClientes@vistaLeerActualizarEliminarClientesAuxiliares']);
    Route::get('asignarClientesAuxiliares', ['uses'=>'clientes\ControladorClientes@vistaAsignarClientesAuxiliares']);
    Route::post('crearCliente', ['uses'=>'clientes\ControladorClientes@crearCliente']);
    Route::post('obtenerCliente', ['uses'=>'clientes\ControladorClientes@obtenerCliente']);
    Route::post('obtenerClientes', ['uses'=>'clientes\ControladorClientes@obtenerClientes']);
    Route::post('obtenerCodigoCliente', ['uses'=>'clientes\ControladorClientes@obtenerCodigoCliente']);
    Route::post('obtenerClienteIdentificacion', ['uses'=>'clientes\ControladorClientes@obtenerClienteIdentificacion']);
    Route::post('obtenerClientesNoAuxiliares', ['uses'=>'clientes\ControladorClientes@obtenerClientesNoAuxiliares']);
    Route::post('obtenerClientesNoAuxiliaresCF', ['uses'=>'clientes\ControladorClientes@obtenerClientesNoAuxiliaresCF']);
    Route::post('asignarClienteAuxiliarCF', ['uses'=>'clientes\ControladorClientes@asignarClienteAuxiliarCF']);
    Route::post('asignarClienteAuxiliar', ['uses'=>'clientes\ControladorClientes@asignarClienteAuxiliar']);
    Route::post('obtenerClientePrepagoOPospago', ['uses'=>'clientes\ControladorClientes@obtenerClientePrepagoOPospago']);
    Route::post('actualizarCliente', ['uses'=>'clientes\ControladorClientes@actualizarCliente']);
    Route::post('eliminarCliente', ['uses'=>'clientes\ControladorClientes@eliminarCliente']);
});

Route::group(['prefix'=> 'configuraciones'], function(){
    Route::get('lae', ['uses'=>'estructuras\ControladorConfiguraciones@vistaLeerActualizarEliminar']);
    Route::get('c', ['uses'=>'estructuras\ControladorConfiguraciones@vistaCrear']);
    Route::post('crearConfiguracion', ['uses'=>'estructuras\ControladorConfiguraciones@crearConfiguracion']);
    Route::post('obtenerConfiguraciones', ['uses'=>'estructuras\ControladorConfiguraciones@obtenerConfiguraciones']);
    Route::post('obtenerConfiguracion', ['uses'=>'estructuras\ControladorConfiguraciones@obtenerConfiguracion']);
    Route::post('actualizarConfiguracion/{id}', ['uses'=>'estructuras\ControladorConfiguraciones@actualizarConfiguracion']);
    Route::post('eliminarConfiguracion', ['uses'=>'estructuras\ControladorConfiguraciones@eliminarConfiguracion']);
});

Route::group(['prefix'=> 'gruposcontables'], function(){
    Route::get('laeGC', ['uses'=>'clientes\ControladorGruposContables@vistaLeerActualizarEliminarGC']);
    Route::get('laeGS', ['uses'=>'clientes\ControladorGruposContables@vistaLeerActualizarEliminarGS']);
    Route::get('c', ['uses'=>'clientes\ControladorGruposContables@vistaCrear']);
    Route::post('crearGrupoContable', ['uses'=>'clientes\ControladorGruposContables@crearGrupoContable']);
    Route::post('obtenerCodigoGrupoServicio', ['uses'=>'clientes\ControladorGruposContables@obtenerCodigoGrupoServicio']);
    Route::post('obtenerCodigoGrupoCliente', ['uses'=>'clientes\ControladorGruposContables@obtenerCodigoGrupoCliente']);
    Route::post('obtenerGruposContables', ['uses'=>'clientes\ControladorGruposContables@obtenerGruposContables']);
    Route::post('obtenerGruposClientes', ['uses'=>'clientes\ControladorGruposContables@obtenerGruposClientes']);
    Route::post('obtenerGruposServicios', ['uses'=>'clientes\ControladorGruposContables@obtenerGruposServicios']);
    Route::post('obtenerGrupoContable', ['uses'=>'clientes\ControladorGruposContables@obtenerGrupoContable']);
    Route::post('actualizarGrupoContable', ['uses'=>'clientes\ControladorGruposContables@actualizarGrupoContable']);
    Route::post('eliminarGrupoContable', ['uses'=>'clientes\ControladorGruposContables@eliminarGrupoContable']);
});

Route::group(['namespace'=> 'clientes', 'prefix' =>'servicio'], function(){
    Route::get('plazocredito', ['uses'=>'ControladorPlazosCreditos@obtenerPlazosCreditos']);
    Route::get('plazocredito/{id}', ['uses'=>'ControladorPlazosCreditos@obtenerPlazoCredito']);
    Route::get('plazocredito/codigo', ['uses'=>'ControladorPlazosCreditos@obtenerCodigoPlazoCredito']);
    Route::post('plazocredito', ['uses'=>'ControladorPlazosCreditos@crearPlazoCredito']);
    Route::put('plazocredito', ['uses'=>'ControladorPlazosCreditos@actualizarPlazoCredito']);
    Route::delete('plazocredito', ['uses'=>'ControladorPlazosCreditos@eliminarPlazoCredito']);
});

Route::group(['prefix'=> 'transportistas'], function(){
    Route::get('lae', ['uses'=>'clientes\ControladorTransportistas@vistaLeerActualizarEliminar']);
    Route::get('c', ['uses'=>'clientes\ControladorTransportistas@vistaCrear']);
    Route::post('crearTransportista', ['uses'=>'clientes\ControladorTransportistas@crearTransportista']);
    Route::post('obtenerCodigoTransportista', ['uses'=>'clientes\ControladorTransportistas@obtenerCodigoTransportista']);
    Route::post('obtenerTransportista', ['uses'=>'clientes\ControladorTransportistas@obtenerTransportista']);
    Route::post('obtenerTransportistas', ['uses'=>'clientes\ControladorTransportistas@obtenerTransportistas']);
    Route::post('actualizarTransportista', ['uses'=>'clientes\ControladorTransportistas@actualizarTransportista']);
    Route::post('eliminarTransportista', ['uses'=>'clientes\ControladorTransportistas@eliminarTransportista']);
});

Route::group(['prefix'=> 'vehiculostransportes'], function(){
    Route::get('lae', ['uses'=>'clientes\ControladorVehiculosTransportes@vistaLeerActualizarEliminar']);
    Route::get('c', ['uses'=>'clientes\ControladorVehiculosTransportes@vistaCrear']);
    Route::post('crearVehiculoTransporte', ['uses'=>'clientes\ControladorVehiculosTransportes@crearVehiculoTransporte']);
    
    Route::post('obtenerCodigoVehiculoTransporte', ['uses'=>'clientes\ControladorVehiculosTransportes@obtenerCodigoVehiculoTransporte']);
    Route::post('obtenerVehiculoTransporte', ['uses'=>'clientes\ControladorVehiculosTransportes@obtenerVehiculoTransporte']);
    Route::post('obtenerVehiculosTransportes', ['uses'=>'clientes\ControladorVehiculosTransportes@obtenerVehiculosTransportes']);
    Route::post('actualizarVehiculoTransporte', ['uses'=>'clientes\ControladorVehiculosTransportes@actualizarVehiculoTransporte']);
    Route::post('eliminarVehiculoTransporte', ['uses'=>'clientes\ControladorVehiculosTransportes@eliminarVehiculoTransporte']);
});

Route::group(['prefix'=> 'ubicacionesgeo'], function(){
    Route::get('lae', ['uses'=>'clientes\ControladorUbicacionesgeo@vistaLeerActualizarEliminar']);
    Route::get('c', ['uses'=>'clientes\ControladorUbicacionesgeo@vistaCrear']);
    Route::post('crearUbicaciongeo', ['uses'=>'clientes\ControladorUbicacionesgeo@crearUbicaciongeo']);
    Route::post('obtenerCodigoUbicaciongeo', ['uses'=>'clientes\ControladorUbicacionesgeo@obtenerCodigoUbicaciongeo']);
    Route::post('obtenerUbicacionesgeo', ['uses'=>'clientes\ControladorUbicacionesgeo@obtenerUbicacionesgeo']);
    Route::post('obtenerUbicaciongeo', ['uses'=>'clientes\ControladorUbicacionesgeo@obtenerUbicaciongeo']);
    Route::post('obtenerProvincias', ['uses'=>'clientes\ControladorUbicacionesgeo@obtenerProvincias']);
    Route::post('obtenerCantones', ['uses'=>'clientes\ControladorUbicacionesgeo@obtenerCantones']);
    Route::post('obtenerParroquias', ['uses'=>'clientes\ControladorUbicacionesgeo@obtenerParroquias']);
    Route::post('actualizarUbicaciongeo', ['uses'=>'clientes\ControladorUbicacionesgeo@actualizarUbicaciongeo']);
    Route::post('eliminarUbicaciongeo', ['uses'=>'clientes\ControladorUbicacionesgeo@eliminarUbicaciongeo']);
});

Route::group(['prefix'=> 'servicios'], function(){
    Route::get('lae', ['uses'=>'clientes\ControladorServicios@vistaLeerActualizarEliminar']);
    Route::get('c', ['uses'=>'clientes\ControladorServicios@vistaCrear']);
    Route::post('crearServicio', ['uses'=>'clientes\ControladorServicios@crearServicio']);
    Route::post('obtenerCodigoServicio', ['uses'=>'clientes\ControladorServicios@obtenerCodigoServicio']);
    Route::post('obtenerServicios', ['uses'=>'clientes\ControladorServicios@obtenerServicios']);
    Route::post('obtenerServicio', ['uses'=>'clientes\ControladorServicios@obtenerServicio']);
    Route::post('actualizarServicio', ['uses'=>'clientes\ControladorServicios@actualizarServicio']);
    Route::post('eliminarServicio', ['uses'=>'clientes\ControladorServicios@eliminarServicio']);
});

Route::group(['prefix'=> 'retenciones'], function(){
    Route::get('laeIVAs', ['uses'=>'clientes\ControladorRetenciones@vistaLeerActualizarEliminarIVAs']);
    Route::get('laeIRs', ['uses'=>'clientes\ControladorRetenciones@vistaLeerActualizarEliminarIRs']);
    Route::get('c', ['uses'=>'clientes\ControladorRetenciones@vistaCrear']);  
    Route::post('crearRetencion', ['uses'=>'clientes\ControladorRetenciones@crearRetencion']);
    Route::post('obtenerCodigoRetencionIR', ['uses'=>'clientes\ControladorRetenciones@obtenerCodigoRetencionIR']);
    Route::post('obtenerCodigoRetencionIVA', ['uses'=>'clientes\ControladorRetenciones@obtenerCodigoRetencionIVA']);
    Route::post('obtenerRetenciones', ['uses'=>'clientes\ControladorRetenciones@obtenerRetenciones']);
    Route::post('obtenerRetencion', ['uses'=>'clientes\ControladorRetenciones@obtenerRetencion']);
    Route::post('obtenerIVAs', ['uses'=>'clientes\ControladorRetenciones@obtenerIVAs']);
    Route::post('obtenerIRs', ['uses'=>'clientes\ControladorRetenciones@obtenerIRs']);
    Route::post('actualizarRetencion', ['uses'=>'clientes\ControladorRetenciones@actualizarRetencion']);
    Route::post('eliminarRetencion', ['uses'=>'clientes\ControladorRetenciones@eliminarRetencion']);
    Route::post('verificarRetencionIR', ['uses'=>'clientes\ControladorRetenciones@verificarRetencionIR']);
    Route::post('verificarRetencionIVA', ['uses'=>'clientes\ControladorRetenciones@verificarRetencionIVA']);
});

Route::group(['prefix'=> 'impuestos'], function(){
    Route::get('lae', ['uses'=>'clientes\ControladorImpuestos@vistaLeerActualizarEliminar']);
    Route::get('c', ['uses'=>'clientes\ControladorImpuestos@vistaCrear']);  
    Route::post('crearImpuesto', ['uses'=>'clientes\ControladorImpuestos@crearImpuesto']);
    Route::post('obtenerCodigoImpuesto', ['uses'=>'clientes\ControladorImpuestos@obtenerCodigoImpuesto']);
    Route::post('obtenerImpuestos', ['uses'=>'clientes\ControladorImpuestos@obtenerImpuestos']);
    Route::post('obtenerImpuesto', ['uses'=>'clientes\ControladorImpuestos@obtenerImpuesto']);
    Route::post('actualizarImpuesto', ['uses'=>'clientes\ControladorImpuestos@actualizarImpuesto']);
    Route::post('eliminarImpuesto', ['uses'=>'clientes\ControladorImpuestos@eliminarImpuesto']);
});

Route::group(['prefix'=> 'datosadicionales'], function(){
    Route::get('lae', ['uses'=>'clientes\ControladorDatosAdicionales@vistaLeerActualizarEliminar']);
    Route::get('c', ['uses'=>'clientes\ControladorDatosAdicionales@vistaCrear']);  
    Route::post('crearDatoAdicional', ['uses'=>'clientes\ControladorDatosAdicionales@crearDatoAdicional']);
    Route::post('obtenerCodigoDatoAdicional', ['uses'=>'clientes\ControladorDatosAdicionales@obtenerCodigoDatoAdicional']);
    Route::post('obtenerDatosAdicionales', ['uses'=>'clientes\ControladorDatosAdicionales@obtenerDatosAdicionales']);
    Route::post('obtenerDatoAdicional', ['uses'=>'clientes\ControladorDatosAdicionales@obtenerDatoAdicional']);
    Route::post('actualizarDatoAdicional', ['uses'=>'clientes\ControladorDatosAdicionales@actualizarDatoAdicional']);
    Route::post('eliminarDatoAdicional', ['uses'=>'clientes\ControladorDatosAdicionales@eliminarDatoAdicional']);
});

Route::group(['prefix'=> 'tiposcontribuyentes'], function(){
    Route::get('lae', ['uses'=>'clientes\ControladorTiposContribuyentes@vistaLeerActualizarEliminar']);
    Route::get('c', ['uses'=>'clientes\ControladorTiposContribuyentes@vistaCrear']);  
    Route::post('crearTipoContribuyente', ['uses'=>'clientes\ControladorTiposContribuyentes@crearTipoContribuyente']);
    Route::post('obtenerCodigoTipoContribuyente', ['uses'=>'clientes\ControladorTiposContribuyentes@obtenerCodigoTipoContribuyente']);
    Route::post('obtenerTiposContribuyentes', ['uses'=>'clientes\ControladorTiposContribuyentes@obtenerTiposContribuyentes']);
    Route::post('obtenerTipoContribuyente', ['uses'=>'clientes\ControladorTiposContribuyentes@obtenerTipoContribuyente']);
    Route::post('actualizarTipoContribuyente', ['uses'=>'clientes\ControladorTiposContribuyentes@actualizarTipoContribuyente']);
    Route::post('eliminarTipoContribuyente', ['uses'=>'clientes\ControladorTiposContribuyentes@eliminarTipoContribuyente']);
});

Route::group(['prefix'=> 'auxiliarescf'], function(){
    Route::get('lae', ['uses'=>'clientes\ControladorAuxiliaresCF@vistaLeerActualizarEliminar']);
    Route::get('c', ['uses'=>'clientes\ControladorAuxiliaresCF@vistaCrear']);  
    Route::post('crearAuxiliaresCF', ['uses'=>'clientes\ControladorAuxiliaresCF@crearAuxiliaresCF']);
    Route::post('obtenerAuxiliaresCF', ['uses'=>'clientes\ControladorAuxiliaresCF@obtenerAuxiliaresCF']);
    Route::post('obtenerAuxiliarCF', ['uses'=>'clientes\ControladorAuxiliaresCF@obtenerAuxiliarCF']);
    Route::post('actualizarAuxiliarCF', ['uses'=>'clientes\ControladorAuxiliaresCF@actualizarAuxiliarCF']);
    Route::post('eliminarAuxiliarCF', ['uses'=>'clientes\ControladorAuxiliaresCF@eliminarAuxiliarCF']);
});

Route::group(['prefix'=> 'auxiliares'], function(){
    Route::get('lae', ['uses'=>'clientes\ControladorAuxiliares@vistaLeerActualizarEliminar']);
    Route::get('c', ['uses'=>'clientes\ControladorAuxiliares@vistaCrear']);
    Route::post('crearAuxiliar', ['uses'=>'clientes\ControladorAuxiliares@crearAuxiliar']);
    Route::post('obtenerAuxiliar', ['uses'=>'clientes\ControladorAuxiliares@obtenerAuxiliar']);
    Route::post('obtenerAuxiliaresCliente', ['uses'=>'clientes\ControladorAuxiliares@obtenerAuxiliaresCliente']);
    Route::post('eliminarAuxiliar', ['uses'=>'clientes\ControladorAuxiliares@eliminarAuxiliar']);
});

Route::group(['prefix'=> 'facturas'], function(){
    Route::get('c', ['uses'=>'facturas\ControladorFacturas@vistaCrear']);
    Route::get('a/{id}', ['uses'=>'facturas\ControladorFacturas@vistaActualizar']);
    Route::get('le', ['uses'=>'facturas\ControladorFacturas@vistaLeerEliminar']);
    Route::post('obtenerCodigo', ['uses'=>'facturas\ControladorFacturas@obtenerCodigo']);
    Route::post('obtenerCodigoInterno', ['uses'=>'facturas\ControladorFacturas@obtenerCodigoInterno']);
    Route::post('obtenerFecha', ['uses'=>'facturas\ControladorFacturas@obtenerFecha']);
    Route::post('obtenerSucursal', ['uses'=>'facturas\ControladorFacturas@obtenerSucursal']);
    Route::post('crearCabeceraFactura', ['uses'=>'facturas\ControladorFacturas@crearCabeceraFactura']);
    Route::post('crearLineaFactura', ['uses'=>'facturas\ControladorFacturas@crearLineaFactura']);
    Route::post('obtenerCabeceraFactura', ['uses'=>'facturas\ControladorFacturas@obtenerCabeceraFactura']);
    Route::post('obtenerLineaFactura', ['uses'=>'facturas\ControladorFacturas@obtenerLineaFactura']);
    Route::post('obtenerLineasFactura', ['uses'=>'facturas\ControladorFacturas@obtenerLineasFactura']);
    Route::post('cambiarTotalDescuentoPS', ['uses'=>'facturas\ControladorFacturas@cambiarTotalDescuentoPS']);
    Route::post('cambiarPorcentajeDescuentoPS', ['uses'=>'facturas\ControladorFacturas@cambiarPorcentajeDescuentoPS']);
    Route::post('cambiarDescuentoPS', ['uses'=>'facturas\ControladorFacturas@cambiarDescuentoPS']);
    Route::post('cambiarTotalPS', ['uses'=>'facturas\ControladorFacturas@cambiarTotalPS']);
    Route::post('cambiarTotalNetoPS', ['uses'=>'facturas\ControladorFacturas@cambiarTotalNetoPS']);
});

Route::group(['prefix'=> 'empresas'], function(){
    Route::get('c', ['uses'=>'estructuras\ControladorEmpresas@vistaCrear']);
    Route::get('lae', ['uses'=>'estructuras\ControladorEmpresas@vistaLeerActualizarEliminar']);
    Route::post('crearEmpresa', ['uses'=>'estructuras\ControladorEmpresas@crearEmpresa']);
    Route::post('obtenerEmpresa', ['uses'=>'estructuras\ControladorEmpresas@obtenerEmpresa']);
    Route::post('obtenerEmpresas', ['uses'=>'estructuras\ControladorEmpresas@obtenerEmpresas']);
    Route::post('eliminarEmpresa', ['uses'=>'estructuras\ControladorEmpresas@eliminarEmpresa']);
});

Route::group(['prefix'=> 'establecimientos'], function(){
    Route::get('c', ['uses'=>'estructuras\ControladorEstablecimientos@vistaCrear']);
    Route::get('lae', ['uses'=>'estructuras\ControladorEstablecimientos@vistaLeerActualizarEliminar']);
    Route::post('crearEstablecimiento', ['uses'=>'estructuras\ControladorEstablecimientos@crearEstablecimiento']);
    Route::post('obtenerEstablecimiento', ['uses'=>'estructuras\ControladorEstablecimientos@obtenerEstablecimiento']);
    Route::post('obtenerEstablecimientos', ['uses'=>'estructuras\ControladorEstablecimientos@obtenerEstablecimientos']);
    Route::post('obtenerEstablecimientosEmpresa', ['uses'=>'estructuras\ControladorEstablecimientos@obtenerEstablecimientosEmpresa']);
    Route::post('eliminarEstablecimiento', ['uses'=>'estructuras\ControladorEstablecimientos@eliminarEstablecimiento']);
});

Route::group(['prefix'=> 'puntosventas'], function(){
    Route::get('c', ['uses'=>'estructuras\ControladorPuntosVentas@vistaCrear']);
    Route::get('lae', ['uses'=>'estructuras\ControladorPuntosVentas@vistaLeerActualizarEliminar']);
    Route::post('crearPuntoVenta', ['uses'=>'estructuras\ControladorPuntosVentas@crearPuntoVenta']);
    Route::post('obtenerPuntoVenta', ['uses'=>'estructuras\ControladorPuntosVentas@obtenerPuntoVenta']);
    Route::post('obtenerPuntosVentasEstablecimiento', ['uses'=>'estructuras\ControladorPuntosVentas@obtenerPuntosVentasEstablecimiento']);
    Route::post('eliminarPuntoVenta', ['uses'=>'estructuras\ControladorPuntosVentas@eliminarPuntoVenta']);
});

Route::group(['prefix'=> 'bodegas'], function(){
    Route::get('c', ['uses'=>'inventarios\ControladorBodegas@vistaCrear']);
    Route::get('lae', ['uses'=>'inventarios\ControladorBodegas@vistaLeerActualizarEliminar']);
    Route::post('crearBodega', ['uses'=>'inventarios\ControladorBodegas@crearBodega']);
    Route::post('obtenerCodigoBodega', ['uses'=>'inventarios\ControladorBodegas@obtenerCodigoBodega']);
    Route::post('obtenerBodega', ['uses'=>'inventarios\ControladorBodegas@obtenerBodega']);
    Route::post('obtenerBodegas', ['uses'=>'inventarios\ControladorBodegas@obtenerBodegas']);
    Route::post('eliminarBodega', ['uses'=>'inventarios\ControladorBodegas@eliminarBodega']);
    Route::post('obtenerBodegasUsuario', ['uses'=>'inventarios\ControladorBodegas@obtenerBodegasUsuario']);
});

Route::group(['prefix'=> 'productos'], function(){
    Route::get('c', ['uses'=>'inventarios\ControladorProductos@vistaCrear']);
    Route::get('lae', ['uses'=>'inventarios\ControladorProductos@vistaLeerActualizarEliminar']);
    Route::post('crearProducto', ['uses'=>'inventarios\ControladorProductos@crearProducto']);
    Route::post('obtenerCodigoProducto', ['uses'=>'inventarios\ControladorProductos@obtenerCodigoProducto']);
    Route::post('obtenerProducto', ['uses'=>'inventarios\ControladorProductos@obtenerProducto']);
    Route::post('obtenerProductos', ['uses'=>'inventarios\ControladorProductos@obtenerProductos']);
    Route::post('obtenerProductosBodega', ['uses'=>'inventarios\ControladorProductos@obtenerProductosBodega']);
    Route::post('eliminarProducto', ['uses'=>'inventarios\ControladorProductos@eliminarProducto']);
    Route::post('esProducto', ['uses'=>'inventarios\ControladorProductos@esProducto']);
});

Route::group(['prefix'=> 'recaudacionesefectivo'], function(){
    Route::get('crear', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@vistaCrear']);
    Route::post('crearChequeVista', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@crearChequeVista']);
    Route::post('crearChequePosfechado', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@crearChequePosfechado']);
    Route::post('crearEfectivo', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@crearEfectivo']);
    Route::post('crearDeposito', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@crearDeposito']);
    Route::post('crearTransferencia', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@crearTransferencia']);
    Route::post('eliminarCheque', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@eliminarCheque']);
    Route::post('eliminarDepositoTransferencia', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@eliminarDepositoTransferencia']);
    Route::post('eliminarEfectivo', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@eliminarEfectivo']);
    Route::post('obtenerCodigoChequeVista', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@obtenerCodigoChequeVista']);
    Route::post('obtenerCodigoChequePosfechado', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@obtenerCodigoChequePosfechado']);
    Route::post('obtenerCodigoEfectivo', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@obtenerCodigoEfectivo']);
    Route::post('obtenerCodigoDeposito', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@obtenerCodigoDeposito']);
    Route::post('obtenerCodigoTransferencia', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@obtenerCodigoTransferencia']);
    Route::post('obtenerFecha', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@obtenerFecha']);
    Route::post('obtenerCheque', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@obtenerCheque']);
    Route::post('obtenerEfectivo', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@obtenerEfectivo']);
    Route::post('obtenerDepositoTransferencia', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@obtenerDepositoTransferencia']);
    Route::post('obtenerTiposLineas', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@obtenerTiposLineas']);
});

Route::group(['prefix'=> 'recaudacionestarjetas'], function(){
    Route::get('crear', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@vistaCrear']);
    Route::post('crearTarjetaCredito', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@crearTarjetaCredito']);
    Route::post('crearTarjetaDebito', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@crearTarjetaDebito']);
    Route::post('eliminarTarjetaCredito', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@eliminarTarjetaCredito']);
    Route::post('eliminarTarjetaDebito', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@eliminarTarjetaDebito']);
    Route::post('obtenerCodigoTarjetaCredito', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@obtenerCodigoTarjetaCredito']);
    Route::post('obtenerCodigoTarjetaDebito', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@obtenerCodigoTarjetaDebito']);
    Route::post('obtenerFecha', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@obtenerFecha']);
    Route::post('obtenerTarjetaCredito', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@obtenerTarjetaCredito']);
    Route::post('obtenerTarjetaDebito', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@obtenerTarjetaDebito']);
    Route::post('obtenerTiposLineas', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@obtenerTiposLineas']);
    Route::post('actualizarTarjetaCredito/{id}', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@actualizarTarjetaCredito']);
    Route::post('actualizarTarjetaDebito/{id}', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@actualizarTarjetaDebito']);
});

Route::group(['prefix'=> 'recaudacionescomprobantesretenciones'], function(){
    Route::get('crear', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@vistaCrear']);
    Route::get('actualizar', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@vistaActualizar']);
    Route::post('crearComprobanteRetencion', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@crearComprobanteRetencion']);
    Route::post('eliminarComprobanteRetencion', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@eliminarComprobanteRetencion']);
    Route::post('crearItemComprobanteRetencion', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@crearItemComprobanteRetencion']);
    Route::post('eliminarItemComprobanteRetencion', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@eliminarItemComprobanteRetencion']);
    Route::post('obtenerCodigoComprobanteRetencion', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@obtenerCodigoComprobanteRetencion']);
    Route::post('obtenerComprobanteRetencion', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@obtenerComprobanteRetencion']);
    Route::post('obtenerTipo', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@obtenerTipo']);
    Route::post('obtenerBaseImponible', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@obtenerBaseImponible']);
    Route::post('actualizarValorComprobanteRetencion', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@actualizarValorComprobanteRetencion']);
    Route::post('cargarComprobanteRetencion', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@cargarComprobanteRetencion']);
});

Route::group(['prefix'=> 'recaudacionescreditos'], function(){
    Route::get('crear', ['uses'=>'cobros\ControladorRecaudacionesCreditos@vistaCrear']);
    Route::post('crearCredito', ['uses'=>'cobros\ControladorRecaudacionesCreditos@crearCredito']);
    Route::post('eliminarCredito', ['uses'=>'cobros\ControladorRecaudacionesCreditos@eliminarCredito']);
    Route::post('crearItemsCredito', ['uses'=>'cobros\ControladorRecaudacionesCreditos@crearItemsCredito']);
    Route::post('eliminarItemCredito', ['uses'=>'cobros\ControladorRecaudacionesCreditos@eliminarItemCredito']);
    Route::post('obtenerCodigoCredito', ['uses'=>'cobros\ControladorRecaudacionesCreditos@obtenerCodigoCredito']);
    Route::post('obtenerCredito', ['uses'=>'cobros\ControladorRecaudacionesCreditos@obtenerCredito']);
    Route::post('generarCredito', ['uses'=>'cobros\ControladorRecaudacionesCreditos@generarCredito']);
    Route::post('generarAmortizacionCredito', ['uses'=>'cobros\ControladorRecaudacionesCreditos@generarAmortizacionCredito']);
    Route::post('actualizarValorCredito', ['uses'=>'cobros\ControladorRecaudacionesCreditos@actualizarValorCredito']);
    Route::post('obtenerTiempo', ['uses'=>'cobros\ControladorRecaudacionesCreditos@obtenerTiempo']);
});

Route::group(['prefix'=> 'egresosinventarios'], function(){
    Route::get('c', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@vistaCrear']);
    Route::get('a/{id}', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@vistaActualizar']);
    Route::get('le', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@vistaLeerEliminar']);
    Route::post('obtenerCodigo', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@obtenerCodigo']);
    Route::post('obtenerCodigoInterno', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@obtenerCodigoInterno']);
    Route::post('obtenerFecha', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@obtenerFecha']);
    Route::post('crearCabeceraEgresoInventario', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@crearCabeceraEgresoInventario']);
    Route::post('crearLineaEgresoInventario', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@crearLineaEgresoInventario']);
    Route::post('obtenerCabeceraEgresoInventario', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@obtenerCabeceraEgresoInventario']);
    Route::post('obtenerLineaEgresoInventario', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@obtenerLineaEgresoInventario']);
    Route::post('obtenerLineasEgresoInventario', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@obtenerLineasEgresoInventario']);
    Route::post('obtenerConsumoSaldoPrepago', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@obtenerConsumoSaldoPrepago']);
    Route::post('obtenerConsumoSaldoPospago', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@obtenerConsumoSaldoPospago']);
    Route::post('cambiarTotalDescuentoPS', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@cambiarTotalDescuentoPS']);
    Route::post('cambiarPorcentajeDescuentoPS', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@cambiarPorcentajeDescuentoPS']);
    Route::post('cambiarDescuentoPS', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@cambiarDescuentoPS']);
    Route::post('cambiarTotalPS', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@cambiarTotalPS']);
    Route::post('cambiarTotalNetoPS', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@cambiarTotalNetoPS']);
});

Route::group(['prefix'=> 'utilidades'], function(){
    Route::post('obtenerPS', ['uses'=>'facturas\ControladorUtilidades@obtenerPS']);
});