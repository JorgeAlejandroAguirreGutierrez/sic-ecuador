<?php

use Illuminate\Http\Request;
use Illuminate\Http\Response;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=> 'usuarios'], function(){
    Route::get('lae', ['uses'=>'usuarios\ControladorUsuarios@vistaLeerActualizarEliminar']);
    Route::get('c', ['uses'=>'usuarios\ControladorUsuarios@vistaCrear']);  
    Route::get('mostrarInicioSesion', ['uses'=>'usuarios\ControladorUsuarios@vistaMostrarInicioSesion']);
    Route::post('crearUsuario', ['uses'=>'usuarios\ControladorUsuarios@crearUsuario']);
    Route::post('actualizarUsuario', ['uses'=>'usuarios\ControladorUsuarios@actualizarUsuario']);
    Route::post('activarSesion', ['uses'=>'usuarios\ControladorUsuarios@activarSesion']);
    Route::post('iniciarSesion', ['uses'=>'usuarios\ControladorUsuarios@iniciarSesion']);
    Route::post('cerrarSesion', ['uses'=>'usuarios\ControladorUsuarios@cerrarSesion']);
    Route::post('verificarIniciarSesion', ['uses'=>'usuarios\ControladorUsuarios@verificarIniciarSesion']);
    Route::post('obtenerCodigoUsuario', ['uses'=>'usuarios\ControladorUsuarios@obtenerCodigoUsuario']);
    Route::post('obtenerUsuarios', ['uses'=>'usuarios\ControladorUsuarios@obtenerUsuarios']);
    Route::post('obtenerUsuario', ['uses'=>'usuarios\ControladorUsuarios@obtenerUsuario']);
    Route::post('obtenerVendedores', ['uses'=>'usuarios\ControladorUsuarios@obtenerVendedores']);
    Route::post('obtenerCajeros', ['uses'=>'usuarios\ControladorUsuarios@obtenerCajeros']);
    Route::post('eliminarUsuario', ['uses'=>'usuarios\ControladorUsuarios@eliminarUsuario']);
    Route::post('obtenerEstablecimientoUsuario', ['uses'=>'usuarios\ControladorUsuarios@obtenerEstablecimientoUsuario']);
    Route::post('obtenerPuntoVentaUsuario', ['uses'=>'usuarios\ControladorUsuarios@obtenerPuntoVentaUsuario']);
});

Route::group(['namespace'=> 'clientes', 'prefix' =>'sicecuador'], function(){
    Route::post('cliente', ['uses'=>'ControladorCliente@crear']);
    Route::get('cliente/{id}', ['uses'=>'ControladorCliente@obtener']);
    Route::get('cliente', ['uses'=>'ControladorCliente@consultar']);
    Route::put('cliente', ['uses'=>'ControladorCliente@actualizar']);
    Route::delete('cliente/{id}', ['uses'=>'ControladorCliente@eliminar']);
});

Route::group(['prefix'=> 'configuraciones'], function(){
    Route::get('lae', ['uses'=>'estructuras\ControladorConfiguraciones@vistaLeerActualizarEliminar']);
    Route::get('c', ['uses'=>'estructuras\ControladorConfiguraciones@vistaCrear']);
    Route::post('crearConfiguracion', ['uses'=>'estructuras\ControladorConfiguraciones@crearConfiguracion']);
    Route::post('obtenerConfiguraciones', ['uses'=>'estructuras\ControladorConfiguraciones@obtenerConfiguraciones']);
    Route::post('obtenerConfiguracion', ['uses'=>'estructuras\ControladorConfiguraciones@obtenerConfiguracion']);
    Route::post('actualizarConfiguracion/{id}', ['uses'=>'estructuras\ControladorConfiguraciones@actualizarConfiguracion']);
    Route::post('eliminarConfiguracion', ['uses'=>'estructuras\ControladorConfiguraciones@eliminarConfiguracion']);
});

Route::group(['namespace'=> 'clientes', 'prefix' =>'sicecuador'], function(){
    Route::get('grupocliente', ['uses'=>'ControladorGrupoCliente@consultar']);
    Route::get('grupocliente/{id}', ['uses'=>'ControladorGrupoCliente@obtener']);
    Route::post('grupocliente', ['uses'=>'ControladorGrupoCliente@crear']);
    Route::put('grupocliente', ['uses'=>'ControladorGrupoCliente@actualizar']);
    Route::delete('grupocliente/{id}', ['uses'=>'ControladorGrupoCliente@eliminar']);
});

Route::group(['namespace'=> 'clientes', 'prefix' =>'sicecuador'], function(){
    Route::get('genero', ['uses'=>'ControladorGenero@consultar']);
    Route::get('genero/{id}', ['uses'=>'ControladorGenero@obtener']);
    Route::post('genero', ['uses'=>'ControladorGenero@crear']);
    Route::put('genero', ['uses'=>'ControladorGenero@actualizar']);
    Route::delete('genero/{id}', ['uses'=>'ControladorGenero@eliminar']);
});

Route::group(['namespace'=> 'clientes', 'prefix' =>'sicecuador'], function(){
    Route::get('estadocivil', ['uses'=>'ControladorEstadoCivil@consultar']);
    Route::get('estadocivil/{id}', ['uses'=>'ControladorEstadoCivil@obtener']);
    Route::post('estadocivil', ['uses'=>'ControladorEstadoCivil@crear']);
    Route::put('estadocivil', ['uses'=>'ControladorEstadoCivil@actualizar']);
    Route::delete('estadocivil/{id}', ['uses'=>'ControladorEstadoCivil@eliminar']);
});

Route::group(['namespace'=> 'clientes', 'prefix' =>'sicecuador'], function(){
    Route::get('origeningreso', ['uses'=>'ControladorOrigenIngreso@consultar']);
    Route::get('origeningreso/{id}', ['uses'=>'ControladorOrigenIngreso@obtener']);
    Route::post('origeningreso', ['uses'=>'ControladorOrigenIngreso@crear']);
    Route::put('origeningreso', ['uses'=>'ControladorOrigenIngreso@actualizar']);
    Route::delete('origeningreso/{id}', ['uses'=>'ControladorOrigenIngreso@eliminar']);
});

Route::group(['namespace'=> 'clientes', 'prefix' =>'sicecuador'], function(){
    Route::get('categoriacliente', ['uses'=>'ControladorCategoriaCliente@consultar']);
    Route::get('categoriacliente/{id}', ['uses'=>'ControladorCategoriaCliente@obtener']);
    Route::post('categoriacliente', ['uses'=>'ControladorCategoriaCliente@crear']);
    Route::put('categoriacliente', ['uses'=>'ControladorCategoriaCliente@actualizar']);
    Route::delete('categoriacliente/{id}', ['uses'=>'ControladorCategoriaCliente@eliminar']);
});

Route::group(['namespace'=> 'clientes', 'prefix' =>'sicecuador'], function(){
    Route::get('plazocredito', ['uses'=>'ControladorPlazoCredito@consultar']);
    Route::get('plazocredito/{id}', ['uses'=>'ControladorPlazoCredito@obtener']);
    Route::get('plazocredito/plazo', ['uses'=>'ControladorPlazoCredito@obtenerPlazo']);
    Route::post('plazocredito', ['uses'=>'ControladorPlazoCredito@crear']);
    Route::put('plazocredito', ['uses'=>'ControladorPlazoCredito@actualizar']);
    Route::delete('plazocredito/{id}', ['uses'=>'ControladorPlazoCredito@eliminar']);
});

Route::group(['namespace'=> 'clientes', 'prefix' =>'sicecuador'], function(){
    Route::get('formapago', ['uses'=>'ControladorFormaPago@consultar']);
    Route::get('formapago/{id}', ['uses'=>'ControladorFormaPago@obtener']);
    Route::post('formapago', ['uses'=>'ControladorFormaPago@crear']);
    Route::put('formapago', ['uses'=>'ControladorFormaPago@actualizar']);
    Route::delete('formapago/{id}', ['uses'=>'ControladorFormaPago@eliminar']);
});

Route::group(['namespace'=> 'clientes', 'prefix' =>'sicecuador'], function(){
    Route::get('tipopago', ['uses'=>'ControladorTipoPago@consultar']);
    Route::get('tipopago/{id}', ['uses'=>'ControladorTipoPago@obtener']);
    Route::post('tipopago', ['uses'=>'ControladorTipoPago@crear']);
    Route::put('tipopago', ['uses'=>'ControladorTipoPago@actualizar']);
    Route::delete('tipopago/{id}', ['uses'=>'ControladorTipoPago@eliminar']);
});

Route::group(['namespace'=> 'clientes', 'prefix' =>'sicecuador'], function(){
    Route::get('transportista', ['uses'=>'ControladorTransportista@consultar']);
    Route::get('transportista/{id}', ['uses'=>'ControladorTransportista@obtener']);
    Route::post('transportista', ['uses'=>'ControladorTransportista@crear']);
    Route::put('transportista', ['uses'=>'ControladorTransportista@actualizar']);
    Route::delete('transportista', ['uses'=>'ControladorTransportista@actualizarTransportista']);
});

Route::group(['prefix'=> 'vehiculostransportes'], function(){
    Route::post('crearVehiculoTransporte', ['uses'=>'clientes\ControladorVehiculosTransportes@crearVehiculoTransporte']);
    
    Route::post('obtenerCodigoVehiculoTransporte', ['uses'=>'clientes\ControladorVehiculosTransportes@obtenerCodigoVehiculoTransporte']);
    Route::post('obtenerVehiculoTransporte', ['uses'=>'clientes\ControladorVehiculosTransportes@obtenerVehiculoTransporte']);
    Route::post('obtenerVehiculosTransportes', ['uses'=>'clientes\ControladorVehiculosTransportes@obtenerVehiculosTransportes']);
    Route::post('actualizarVehiculoTransporte', ['uses'=>'clientes\ControladorVehiculosTransportes@actualizarVehiculoTransporte']);
    Route::post('eliminarVehiculoTransporte', ['uses'=>'clientes\ControladorVehiculosTransportes@eliminarVehiculoTransporte']);
});

Route::group(['namespace'=> 'clientes', 'prefix' =>'sicecuador'], function(){
    Route::post('ubicacion', ['uses'=>'ControladorUbicaciones@crear']);
    Route::get('ubicacion', ['uses'=>'ControladorUbicaciones@consultar']);
    Route::get('ubicacion/{id}', ['uses'=>'ControladorUbicaciones@obtener']);
    Route::get('ubicacion/provincia', ['uses'=>'ControladorUbicaciones@obtenerProvincias']);
    Route::get('ubicacion/{provincia}/canton', ['uses'=>'ControladorUbicaciones@obtenerCantones']);
    Route::get('ubicacion/{canton}/parroquia', ['uses'=>'ControladorUbicaciones@obtenerParroquias']);
    Route::put('ubicacion', ['uses'=>'ControladorUbicaciones@actualizar']);
    Route::delete('ubicacion/{id}', ['uses'=>'ControladorUbicaciones@eliminar']);
});

Route::group(['prefix'=> 'servicios'], function(){
    Route::get('lae', ['uses'=>'clientes\ControladorServicios@vistaLeerActualizarEliminar']);
    Route::get('c', ['uses'=>'clientes\ControladorServicios@vistaCrear']);
    Route::post('crearServicio', ['uses'=>'clientes\ControladorServicios@crearServicio']);
    Route::post('obtenerCodigoServicio', ['uses'=>'clientes\ControladorServicios@obtenerCodigoServicio']);
    Route::post('obtenerServicios', ['uses'=>'clientes\ControladorServicios@obtenerServicios']);
    Route::post('obtenerServicio', ['uses'=>'clientes\ControladorServicios@obtenerServicio']);
    Route::post('actualizarServicio', ['uses'=>'clientes\ControladorServicios@actualizarServicio']);
    Route::post('eliminarServicio', ['uses'=>'clientes\ControladorServicios@eliminarServicio']);
});

Route::group(['prefix'=> 'retenciones'], function(){
    Route::get('laeIVAs', ['uses'=>'clientes\ControladorRetenciones@vistaLeerActualizarEliminarIVAs']);
    Route::get('laeIRs', ['uses'=>'clientes\ControladorRetenciones@vistaLeerActualizarEliminarIRs']);
    Route::get('c', ['uses'=>'clientes\ControladorRetenciones@vistaCrear']);  
    Route::post('crearRetencion', ['uses'=>'clientes\ControladorRetenciones@crearRetencion']);
    Route::post('obtenerCodigoRetencionIR', ['uses'=>'clientes\ControladorRetenciones@obtenerCodigoRetencionIR']);
    Route::post('obtenerCodigoRetencionIVA', ['uses'=>'clientes\ControladorRetenciones@obtenerCodigoRetencionIVA']);
    Route::post('obtenerRetenciones', ['uses'=>'clientes\ControladorRetenciones@obtenerRetenciones']);
    Route::post('obtenerRetencion', ['uses'=>'clientes\ControladorRetenciones@obtenerRetencion']);
    Route::post('obtenerIVAs', ['uses'=>'clientes\ControladorRetenciones@obtenerIVAs']);
    Route::post('obtenerIRs', ['uses'=>'clientes\ControladorRetenciones@obtenerIRs']);
    Route::post('actualizarRetencion', ['uses'=>'clientes\ControladorRetenciones@actualizarRetencion']);
    Route::post('eliminarRetencion', ['uses'=>'clientes\ControladorRetenciones@eliminarRetencion']);
    Route::post('verificarRetencionIR', ['uses'=>'clientes\ControladorRetenciones@verificarRetencionIR']);
    Route::post('verificarRetencionIVA', ['uses'=>'clientes\ControladorRetenciones@verificarRetencionIVA']);
});

Route::group(['prefix'=> 'impuestos'], function(){
    Route::get('lae', ['uses'=>'clientes\ControladorImpuestos@vistaLeerActualizarEliminar']);
    Route::get('c', ['uses'=>'clientes\ControladorImpuestos@vistaCrear']);  
    Route::post('crearImpuesto', ['uses'=>'clientes\ControladorImpuestos@crearImpuesto']);
    Route::post('obtenerCodigoImpuesto', ['uses'=>'clientes\ControladorImpuestos@obtenerCodigoImpuesto']);
    Route::post('obtenerImpuestos', ['uses'=>'clientes\ControladorImpuestos@obtenerImpuestos']);
    Route::post('obtenerImpuesto', ['uses'=>'clientes\ControladorImpuestos@obtenerImpuesto']);
    Route::post('actualizarImpuesto', ['uses'=>'clientes\ControladorImpuestos@actualizarImpuesto']);
    Route::post('eliminarImpuesto', ['uses'=>'clientes\ControladorImpuestos@eliminarImpuesto']);
});

Route::group(['prefix'=> 'datosadicionales'], function(){
    Route::get('lae', ['uses'=>'clientes\ControladorDatosAdicionales@vistaLeerActualizarEliminar']);
    Route::get('c', ['uses'=>'clientes\ControladorDatosAdicionales@vistaCrear']);  
    Route::post('crearDatoAdicional', ['uses'=>'clientes\ControladorDatosAdicionales@crearDatoAdicional']);
    Route::post('obtenerCodigoDatoAdicional', ['uses'=>'clientes\ControladorDatosAdicionales@obtenerCodigoDatoAdicional']);
    Route::post('obtenerDatosAdicionales', ['uses'=>'clientes\ControladorDatosAdicionales@obtenerDatosAdicionales']);
    Route::post('obtenerDatoAdicional', ['uses'=>'clientes\ControladorDatosAdicionales@obtenerDatoAdicional']);
    Route::post('actualizarDatoAdicional', ['uses'=>'clientes\ControladorDatosAdicionales@actualizarDatoAdicional']);
    Route::post('eliminarDatoAdicional', ['uses'=>'clientes\ControladorDatosAdicionales@eliminarDatoAdicional']);
});

Route::group(['prefix'=> 'tiposcontribuyentes'], function(){
    Route::get('lae', ['uses'=>'clientes\ControladorTiposContribuyentes@vistaLeerActualizarEliminar']);
    Route::get('c', ['uses'=>'clientes\ControladorTiposContribuyentes@vistaCrear']);  
    Route::post('crearTipoContribuyente', ['uses'=>'clientes\ControladorTiposContribuyentes@crearTipoContribuyente']);
    Route::post('obtenerCodigoTipoContribuyente', ['uses'=>'clientes\ControladorTiposContribuyentes@obtenerCodigoTipoContribuyente']);
    Route::post('obtenerTiposContribuyentes', ['uses'=>'clientes\ControladorTiposContribuyentes@obtenerTiposContribuyentes']);
    Route::post('obtenerTipoContribuyente', ['uses'=>'clientes\ControladorTiposContribuyentes@obtenerTipoContribuyente']);
    Route::post('actualizarTipoContribuyente', ['uses'=>'clientes\ControladorTiposContribuyentes@actualizarTipoContribuyente']);
    Route::post('eliminarTipoContribuyente', ['uses'=>'clientes\ControladorTiposContribuyentes@eliminarTipoContribuyente']);
});

Route::group(['prefix'=> 'auxiliarescf'], function(){
    Route::get('lae', ['uses'=>'clientes\ControladorAuxiliaresCF@vistaLeerActualizarEliminar']);
    Route::get('c', ['uses'=>'clientes\ControladorAuxiliaresCF@vistaCrear']);  
    Route::post('crearAuxiliaresCF', ['uses'=>'clientes\ControladorAuxiliaresCF@crearAuxiliaresCF']);
    Route::post('obtenerAuxiliaresCF', ['uses'=>'clientes\ControladorAuxiliaresCF@obtenerAuxiliaresCF']);
    Route::post('obtenerAuxiliarCF', ['uses'=>'clientes\ControladorAuxiliaresCF@obtenerAuxiliarCF']);
    Route::post('actualizarAuxiliarCF', ['uses'=>'clientes\ControladorAuxiliaresCF@actualizarAuxiliarCF']);
    Route::post('eliminarAuxiliarCF', ['uses'=>'clientes\ControladorAuxiliaresCF@eliminarAuxiliarCF']);
});

Route::group(['prefix'=> 'auxiliares'], function(){
    Route::get('lae', ['uses'=>'clientes\ControladorAuxiliares@vistaLeerActualizarEliminar']);
    Route::get('c', ['uses'=>'clientes\ControladorAuxiliares@vistaCrear']);
    Route::post('crearAuxiliar', ['uses'=>'clientes\ControladorAuxiliares@crearAuxiliar']);
    Route::post('obtenerAuxiliar', ['uses'=>'clientes\ControladorAuxiliares@obtenerAuxiliar']);
    Route::post('obtenerAuxiliaresCliente', ['uses'=>'clientes\ControladorAuxiliares@obtenerAuxiliaresCliente']);
    Route::post('eliminarAuxiliar', ['uses'=>'clientes\ControladorAuxiliares@eliminarAuxiliar']);
});

Route::group(['prefix'=> 'facturas'], function(){
    Route::get('c', ['uses'=>'facturas\ControladorFacturas@vistaCrear']);
    Route::get('a/{id}', ['uses'=>'facturas\ControladorFacturas@vistaActualizar']);
    Route::get('le', ['uses'=>'facturas\ControladorFacturas@vistaLeerEliminar']);
    Route::post('obtenerCodigo', ['uses'=>'facturas\ControladorFacturas@obtenerCodigo']);
    Route::post('obtenerCodigoInterno', ['uses'=>'facturas\ControladorFacturas@obtenerCodigoInterno']);
    Route::post('obtenerFecha', ['uses'=>'facturas\ControladorFacturas@obtenerFecha']);
    Route::post('obtenerSucursal', ['uses'=>'facturas\ControladorFacturas@obtenerSucursal']);
    Route::post('crearCabeceraFactura', ['uses'=>'facturas\ControladorFacturas@crearCabeceraFactura']);
    Route::post('crearLineaFactura', ['uses'=>'facturas\ControladorFacturas@crearLineaFactura']);
    Route::post('obtenerCabeceraFactura', ['uses'=>'facturas\ControladorFacturas@obtenerCabeceraFactura']);
    Route::post('obtenerLineaFactura', ['uses'=>'facturas\ControladorFacturas@obtenerLineaFactura']);
    Route::post('obtenerLineasFactura', ['uses'=>'facturas\ControladorFacturas@obtenerLineasFactura']);
    Route::post('cambiarTotalDescuentoPS', ['uses'=>'facturas\ControladorFacturas@cambiarTotalDescuentoPS']);
    Route::post('cambiarPorcentajeDescuentoPS', ['uses'=>'facturas\ControladorFacturas@cambiarPorcentajeDescuentoPS']);
    Route::post('cambiarDescuentoPS', ['uses'=>'facturas\ControladorFacturas@cambiarDescuentoPS']);
    Route::post('cambiarTotalPS', ['uses'=>'facturas\ControladorFacturas@cambiarTotalPS']);
    Route::post('cambiarTotalNetoPS', ['uses'=>'facturas\ControladorFacturas@cambiarTotalNetoPS']);
});

Route::group(['prefix'=> 'empresas'], function(){
    Route::get('c', ['uses'=>'estructuras\ControladorEmpresas@vistaCrear']);
    Route::get('lae', ['uses'=>'estructuras\ControladorEmpresas@vistaLeerActualizarEliminar']);
    Route::post('crearEmpresa', ['uses'=>'estructuras\ControladorEmpresas@crearEmpresa']);
    Route::post('obtenerEmpresa', ['uses'=>'estructuras\ControladorEmpresas@obtenerEmpresa']);
    Route::post('obtenerEmpresas', ['uses'=>'estructuras\ControladorEmpresas@obtenerEmpresas']);
    Route::post('eliminarEmpresa', ['uses'=>'estructuras\ControladorEmpresas@eliminarEmpresa']);
});

Route::group(['prefix'=> 'establecimientos'], function(){
    Route::get('c', ['uses'=>'estructuras\ControladorEstablecimientos@vistaCrear']);
    Route::get('lae', ['uses'=>'estructuras\ControladorEstablecimientos@vistaLeerActualizarEliminar']);
    Route::post('crearEstablecimiento', ['uses'=>'estructuras\ControladorEstablecimientos@crearEstablecimiento']);
    Route::post('obtenerEstablecimiento', ['uses'=>'estructuras\ControladorEstablecimientos@obtenerEstablecimiento']);
    Route::post('obtenerEstablecimientos', ['uses'=>'estructuras\ControladorEstablecimientos@obtenerEstablecimientos']);
    Route::post('obtenerEstablecimientosEmpresa', ['uses'=>'estructuras\ControladorEstablecimientos@obtenerEstablecimientosEmpresa']);
    Route::post('eliminarEstablecimiento', ['uses'=>'estructuras\ControladorEstablecimientos@eliminarEstablecimiento']);
});

Route::group(['prefix'=> 'puntosventas'], function(){
    Route::get('c', ['uses'=>'estructuras\ControladorPuntosVentas@vistaCrear']);
    Route::get('lae', ['uses'=>'estructuras\ControladorPuntosVentas@vistaLeerActualizarEliminar']);
    Route::post('crearPuntoVenta', ['uses'=>'estructuras\ControladorPuntosVentas@crearPuntoVenta']);
    Route::post('obtenerPuntoVenta', ['uses'=>'estructuras\ControladorPuntosVentas@obtenerPuntoVenta']);
    Route::post('obtenerPuntosVentasEstablecimiento', ['uses'=>'estructuras\ControladorPuntosVentas@obtenerPuntosVentasEstablecimiento']);
    Route::post('eliminarPuntoVenta', ['uses'=>'estructuras\ControladorPuntosVentas@eliminarPuntoVenta']);
});

Route::group(['prefix'=> 'bodegas'], function(){
    Route::get('c', ['uses'=>'inventarios\ControladorBodegas@vistaCrear']);
    Route::get('lae', ['uses'=>'inventarios\ControladorBodegas@vistaLeerActualizarEliminar']);
    Route::post('crearBodega', ['uses'=>'inventarios\ControladorBodegas@crearBodega']);
    Route::post('obtenerCodigoBodega', ['uses'=>'inventarios\ControladorBodegas@obtenerCodigoBodega']);
    Route::post('obtenerBodega', ['uses'=>'inventarios\ControladorBodegas@obtenerBodega']);
    Route::post('obtenerBodegas', ['uses'=>'inventarios\ControladorBodegas@obtenerBodegas']);
    Route::post('eliminarBodega', ['uses'=>'inventarios\ControladorBodegas@eliminarBodega']);
    Route::post('obtenerBodegasUsuario', ['uses'=>'inventarios\ControladorBodegas@obtenerBodegasUsuario']);
});

Route::group(['prefix'=> 'productos'], function(){
    Route::get('c', ['uses'=>'inventarios\ControladorProductos@vistaCrear']);
    Route::get('lae', ['uses'=>'inventarios\ControladorProductos@vistaLeerActualizarEliminar']);
    Route::post('crearProducto', ['uses'=>'inventarios\ControladorProductos@crearProducto']);
    Route::post('obtenerCodigoProducto', ['uses'=>'inventarios\ControladorProductos@obtenerCodigoProducto']);
    Route::post('obtenerProducto', ['uses'=>'inventarios\ControladorProductos@obtenerProducto']);
    Route::post('obtenerProductos', ['uses'=>'inventarios\ControladorProductos@obtenerProductos']);
    Route::post('obtenerProductosBodega', ['uses'=>'inventarios\ControladorProductos@obtenerProductosBodega']);
    Route::post('eliminarProducto', ['uses'=>'inventarios\ControladorProductos@eliminarProducto']);
    Route::post('esProducto', ['uses'=>'inventarios\ControladorProductos@esProducto']);
});

Route::group(['prefix'=> 'recaudacionesefectivo'], function(){
    Route::get('crear', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@vistaCrear']);
    Route::post('crearChequeVista', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@crearChequeVista']);
    Route::post('crearChequePosfechado', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@crearChequePosfechado']);
    Route::post('crearEfectivo', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@crearEfectivo']);
    Route::post('crearDeposito', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@crearDeposito']);
    Route::post('crearTransferencia', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@crearTransferencia']);
    Route::post('eliminarCheque', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@eliminarCheque']);
    Route::post('eliminarDepositoTransferencia', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@eliminarDepositoTransferencia']);
    Route::post('eliminarEfectivo', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@eliminarEfectivo']);
    Route::post('obtenerCodigoChequeVista', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@obtenerCodigoChequeVista']);
    Route::post('obtenerCodigoChequePosfechado', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@obtenerCodigoChequePosfechado']);
    Route::post('obtenerCodigoEfectivo', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@obtenerCodigoEfectivo']);
    Route::post('obtenerCodigoDeposito', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@obtenerCodigoDeposito']);
    Route::post('obtenerCodigoTransferencia', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@obtenerCodigoTransferencia']);
    Route::post('obtenerFecha', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@obtenerFecha']);
    Route::post('obtenerCheque', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@obtenerCheque']);
    Route::post('obtenerEfectivo', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@obtenerEfectivo']);
    Route::post('obtenerDepositoTransferencia', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@obtenerDepositoTransferencia']);
    Route::post('obtenerTiposLineas', ['uses'=>'cobros\ControladorRecaudacionesEfectivo@obtenerTiposLineas']);
});

Route::group(['prefix'=> 'recaudacionestarjetas'], function(){
    Route::get('crear', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@vistaCrear']);
    Route::post('crearTarjetaCredito', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@crearTarjetaCredito']);
    Route::post('crearTarjetaDebito', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@crearTarjetaDebito']);
    Route::post('eliminarTarjetaCredito', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@eliminarTarjetaCredito']);
    Route::post('eliminarTarjetaDebito', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@eliminarTarjetaDebito']);
    Route::post('obtenerCodigoTarjetaCredito', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@obtenerCodigoTarjetaCredito']);
    Route::post('obtenerCodigoTarjetaDebito', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@obtenerCodigoTarjetaDebito']);
    Route::post('obtenerFecha', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@obtenerFecha']);
    Route::post('obtenerTarjetaCredito', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@obtenerTarjetaCredito']);
    Route::post('obtenerTarjetaDebito', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@obtenerTarjetaDebito']);
    Route::post('obtenerTiposLineas', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@obtenerTiposLineas']);
    Route::post('actualizarTarjetaCredito/{id}', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@actualizarTarjetaCredito']);
    Route::post('actualizarTarjetaDebito/{id}', ['uses'=>'cobros\ControladorRecaudacionesTarjetas@actualizarTarjetaDebito']);
});

Route::group(['prefix'=> 'recaudacionescomprobantesretenciones'], function(){
    Route::get('crear', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@vistaCrear']);
    Route::get('actualizar', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@vistaActualizar']);
    Route::post('crearComprobanteRetencion', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@crearComprobanteRetencion']);
    Route::post('eliminarComprobanteRetencion', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@eliminarComprobanteRetencion']);
    Route::post('crearItemComprobanteRetencion', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@crearItemComprobanteRetencion']);
    Route::post('eliminarItemComprobanteRetencion', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@eliminarItemComprobanteRetencion']);
    Route::post('obtenerCodigoComprobanteRetencion', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@obtenerCodigoComprobanteRetencion']);
    Route::post('obtenerComprobanteRetencion', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@obtenerComprobanteRetencion']);
    Route::post('obtenerTipo', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@obtenerTipo']);
    Route::post('obtenerBaseImponible', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@obtenerBaseImponible']);
    Route::post('actualizarValorComprobanteRetencion', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@actualizarValorComprobanteRetencion']);
    Route::post('cargarComprobanteRetencion', ['uses'=>'cobros\ControladorRecaudacionesComprobantesRetenciones@cargarComprobanteRetencion']);
});

Route::group(['prefix'=> 'recaudacionescreditos'], function(){
    Route::get('crear', ['uses'=>'cobros\ControladorRecaudacionesCreditos@vistaCrear']);
    Route::post('crearCredito', ['uses'=>'cobros\ControladorRecaudacionesCreditos@crearCredito']);
    Route::post('eliminarCredito', ['uses'=>'cobros\ControladorRecaudacionesCreditos@eliminarCredito']);
    Route::post('crearItemsCredito', ['uses'=>'cobros\ControladorRecaudacionesCreditos@crearItemsCredito']);
    Route::post('eliminarItemCredito', ['uses'=>'cobros\ControladorRecaudacionesCreditos@eliminarItemCredito']);
    Route::post('obtenerCodigoCredito', ['uses'=>'cobros\ControladorRecaudacionesCreditos@obtenerCodigoCredito']);
    Route::post('obtenerCredito', ['uses'=>'cobros\ControladorRecaudacionesCreditos@obtenerCredito']);
    Route::post('generarCredito', ['uses'=>'cobros\ControladorRecaudacionesCreditos@generarCredito']);
    Route::post('generarAmortizacionCredito', ['uses'=>'cobros\ControladorRecaudacionesCreditos@generarAmortizacionCredito']);
    Route::post('actualizarValorCredito', ['uses'=>'cobros\ControladorRecaudacionesCreditos@actualizarValorCredito']);
    Route::post('obtenerTiempo', ['uses'=>'cobros\ControladorRecaudacionesCreditos@obtenerTiempo']);
});

Route::group(['prefix'=> 'egresosinventarios'], function(){
    Route::get('c', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@vistaCrear']);
    Route::get('a/{id}', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@vistaActualizar']);
    Route::get('le', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@vistaLeerEliminar']);
    Route::post('obtenerCodigo', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@obtenerCodigo']);
    Route::post('obtenerCodigoInterno', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@obtenerCodigoInterno']);
    Route::post('obtenerFecha', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@obtenerFecha']);
    Route::post('crearCabeceraEgresoInventario', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@crearCabeceraEgresoInventario']);
    Route::post('crearLineaEgresoInventario', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@crearLineaEgresoInventario']);
    Route::post('obtenerCabeceraEgresoInventario', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@obtenerCabeceraEgresoInventario']);
    Route::post('obtenerLineaEgresoInventario', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@obtenerLineaEgresoInventario']);
    Route::post('obtenerLineasEgresoInventario', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@obtenerLineasEgresoInventario']);
    Route::post('obtenerConsumoSaldoPrepago', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@obtenerConsumoSaldoPrepago']);
    Route::post('obtenerConsumoSaldoPospago', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@obtenerConsumoSaldoPospago']);
    Route::post('cambiarTotalDescuentoPS', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@cambiarTotalDescuentoPS']);
    Route::post('cambiarPorcentajeDescuentoPS', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@cambiarPorcentajeDescuentoPS']);
    Route::post('cambiarDescuentoPS', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@cambiarDescuentoPS']);
    Route::post('cambiarTotalPS', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@cambiarTotalPS']);
    Route::post('cambiarTotalNetoPS', ['uses'=>'egresosInventarios\ControladorEgresosInventarios@cambiarTotalNetoPS']);
});

Route::group(['prefix'=> 'utilidades'], function(){
    Route::post('obtenerPS', ['uses'=>'facturas\ControladorUtilidades@obtenerPS']);
});

Route::options('/{any}', function(){ return ''; })->where('any', '.*');

Route::any('{path}', function() {
    return response()->json([
        ['mensaje' => 'API not found']
    ], 404);
})->where('path', '.*');
