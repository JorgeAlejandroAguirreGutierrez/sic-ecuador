<?php

namespace App\compras;

use Illuminate\Database\Eloquent\Model;

class FacturaCompra extends Model
{
    //
    protected $table='facturas_compras';
    protected $fillable=['codigo', 'numero', 'fecha', 'agente', 'autorizacion', 'devengable', 'base_imponible', 'valor', 'tipo_retencion_id', 'recaudacion_id'];
    
    public function tipo_retencion()
    {
        return $this->belongsTo('App\configuraciones\TipoRetencion');
    }
    
    public function recaudacion()
    {
        return $this->belongsTo('App\recaudaciones\Recaudacion');
    }    
}
