<?php

namespace App\compras;

use Illuminate\Database\Eloquent\Model;

class RetencionVenta extends Model
{
    //
    protected $table='retenciones_compras';
    protected $fillable=['codigo', 'numero', 'fecha', 'estado', 'autorizacion', 'total','factura_compra_id'];
    
    public function Factura()
    {
        return $this->belongsTo('App\comprobantes\FacturaCompra');
    }
}
