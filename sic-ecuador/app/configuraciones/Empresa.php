<?php

namespace App\estructuras;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    //
    protected $table='empresas';
    protected $fillable=['codigo', 'identificacion', 'razon_social', 'logo'];

    public function clientes()
    {
        return $this->hasMany('App\clientes\Cliente', 'empresa_id');
    }    

    public function usuarios()
    {
        return $this->hasMany('App\usuarios\Usuario', 'empresa_id');
    }    

    public function establecimientos()
    {
        return $this->hasMany('App\usuarios\Establecimiento', 'empresa_id');
    }     
}
