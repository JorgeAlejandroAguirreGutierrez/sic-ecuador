<?php

namespace App\configuraciones;

use Illuminate\Database\Eloquent\Model;

class Ubicacion extends Model
{
    //
    protected $table='ubicaciones';
    protected $fillable=['codigo', 'codigo_norma', 'provincia', 'canton', 'parroquia'];
    
    public function direcciones()
    {
        return $this->hasMany('App\clientes\Direccion', 'ubicacion_id');
    }

    public function establecimientos()
    {
        return $this->hasMany('App\usuarios\Establecimiento', 'ubicacion_id');
    }    
    
    public function guias_remisiones()
    {
        return $this->hasMany('App\entregas\guias_remisiones', 'n_ubicacion_id');
    }    
}
