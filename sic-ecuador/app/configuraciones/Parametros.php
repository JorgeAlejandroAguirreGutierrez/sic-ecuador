<?php

namespace App\configuraciones;

use Illuminate\Database\Eloquent\Model;

class Parametro extends Model
{
    //
    protected $table='parametros';
    protected $fillable=['codigo', 'tipo', 'nombre', 'abreviatura', 'tabla'];
}
