<?php

namespace App\configuraciones;

use Illuminate\Database\Eloquent\Model;

class TipoRetencion extends Model
{
    //
    protected $table='tipos_retenciones';
    protected $fillable=['codigo','impuesto_retencion','tipo_retencion','codigo_norma', 'homologacion_f_e', 'descripcion', 'porcentaje'];
    
    public function retenciones()
    {
        return $this->hasMany('App\clientes\Retencion', 'retencion_id');
    }

    
}
