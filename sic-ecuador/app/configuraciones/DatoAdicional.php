<?php

namespace App\clientes;

use Illuminate\Database\Eloquent\Model;

class DatoAdicional extends Model
{
    //ABARCA TABLA: CATEGORIAS CLIENTES, FORMAS PAGOS, SEXOS, ESTADOS CIVILES, ORIGENES INGRESOS
    protected $table='datos_adicionales';
    protected $fillable=['codigo', 'tipo', 'nombre', 'abreviatura'];
    
    public function clientes()
    {
        return $this->hasMany('App\clientes\Cliente');
    }
    
    public function serviciosFacturas()
    {
        return $this->hasMany('App\clientes\ServicioFactura');
    }
}
