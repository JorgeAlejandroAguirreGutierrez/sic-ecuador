<?php

namespace App\recaudaciones;

use Illuminate\Database\Eloquent\Model;

class Compensacion extends Model
{
    //
    protected $table='compensaciones';
    protected $fillable=['codigo', 'comprobante', 'fecha_comprobante', 'origen', 'motivo', 'fecha_vencimiento', 'valor_origen', 'saldo_anterior', 
                        'valor_compensado', 'saldo', 'compensado', 'recaudacion_id', 'tipo_comprobante_id', 'cliente_id'];
    
    public function recaudacion()
    {
        return $this->belongsTo('App\recaudaciones\Recaudacion');
    }
    
    public function tipo_comprobante()
    {
        return $this->belongsTo('App\comprobantes\TipoComprobante');
    }
    
    public function cliente()
    {
        return $this->belongsTo('App\clientes\Cliente');
    }    
}
