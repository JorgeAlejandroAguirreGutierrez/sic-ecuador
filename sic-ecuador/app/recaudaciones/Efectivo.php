<?php

namespace App\cobros;

use Illuminate\Database\Eloquent\Model;

class Efectivo extends Model
{
    //
    protected $table='efectivos';
    protected $fillable=['codigo', 'valor'];
    
    public function recaudaciones()
    {
        return $this->hasOne('App\recaudaciones\Recaudacion', 'efectivo_id');
    }
    
}