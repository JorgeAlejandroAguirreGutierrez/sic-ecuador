<?php

namespace App\recaudaciones;

use Illuminate\Database\Eloquent\Model;

class RetencionDetalle extends Model
{
    //
    protected $table='retenciones_ventas_detalles';
    protected $fillable=['posicion', 'base_imponible', 'valor', 'retencion_venta_id','tipo_retencion_id'];
    
    public function retencionVenta()
    {
        return $this->belongsTo('App\recaudaciones\RetencionVenta', 'retencion_venta_id', 'id');
    }
    
    public function tipoRetencion()
    {
        return $this->belongsTo('App\configuraciones\TipoRetencion', 'tipo_retencion_id', 'id');
    }
}
