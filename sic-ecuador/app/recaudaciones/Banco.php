<?php

namespace App\recaudaciones;

use Illuminate\Database\Eloquent\Model;

class Banco extends Model
{
    //
    protected $table='bancos';
    protected $fillable=['codigo', 'tipo', 'nombre', 'abreviatura'];
   
    public function cheques()
    {
        return $this->hasMany('App\recaudaciones\Cheque', 'banco_id');
    }
    
    public function depositos_transferencias()
    {
        return $this->hasMany('App\recaudaciones\DepositoTransferencia', 'banco_id');
    }
    
    public function tarjetas()
    {
        return $this->hasMany('App\recaudaciones\Tarjeta', 'banco_id');
    }    
}
