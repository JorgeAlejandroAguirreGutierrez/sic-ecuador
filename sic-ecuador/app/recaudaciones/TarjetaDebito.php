<?php

namespace App\recaudaciones;

use Illuminate\Database\Eloquent\Model;

class TarjetaDebito extends Model
{
    //
    protected $table='tarjetas_debitos';
    protected $fillable=['codigo', 'identificacion', 'nombre_titular', 'lote', 'valor', 'tarjeta_id', 'operador_tarjeta_id'];

    public function tarjeta()
    {
        return $this->belongsTo('App\recaudaciones\Tarjeta');
    }
    
    public function operador_tarjeta()
    {
        return $this->belongsTo('App\recaudaciones\OperadorTarjeta');
    }
    
    public function recaudaciones()
    {
        return $this->hasOne('App\recaudaciones\Recaudacion', 'tarjeta_credito_id');
    } 
}
