<?php

namespace App\recaudaciones;

use Illuminate\Database\Eloquent\Model;

class Cheque extends Model
{
    //
    protected $table='cheques';
    protected $fillable=['codigo', 'numero', 'tipo_cheque', 'fecha_cheque', 'fecha_efectivizacion', 'valor', 'recaudacion_id', 'banco_id'];
    
    public function recaudacion()
    {
        return $this->belongsTo('App\recaudaciones\Recaudacion');
    }
    
    public function banco()
    {
        return $this->belongsTo('App\configuraciones\Banco');
    }
}
