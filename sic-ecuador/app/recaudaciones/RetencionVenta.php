<?php

namespace App\recaudaciones;

use Illuminate\Database\Eloquent\Model;

class RetencionCompra extends Model
{
    //
    protected $table='retenciones_ventas';
    protected $fillable=['codigo', 'numero', 'fecha', 'agente', 'autorizacion', 'compensado', 'base_imponible', 'valor', 'tipo_retencion_id', 'recaudacion_id'];
    
    public function tipo_retencion()
    {
        return $this->belongsTo('App\configuraciones\TipoRetencion');
    }
    
    public function recaudacion()
    {
        return $this->belongsTo('App\recaudaciones\Recaudacion');
    }
}
