<?php

namespace App\recaudaciones;

use Illuminate\Database\Eloquent\Model;

class Credito extends Model
{
    protected $table='creditos';
    protected $fillable=['codigo', 'saldo', 'plazo', 'periodicidad', 'modelo_tabla', 'interes_periodo', 'interes_anual', 
                        'primera_cuota', 'vencimiento', 'cuota', 'intereses', 'total', 'estado'];
    
    public function recaudaciones()
    {
        return $this->hasOne('App\recaudaciones\Recaudacion', 'credito_id');
    }
    
    public function creditos_amortizaciones()
    {
        return $this->hasOne('App\recaudaciones\CreditoAmortizacion', 'credito_id');
    }    
}
