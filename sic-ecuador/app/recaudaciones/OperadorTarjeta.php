<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OperadorTarjeta extends Model
{
    //
    protected $table='operadores_tarjetas';
    protected $fillable=['codigo', 'tipo', 'nombre', 'abreviatura'];
   
    public function tarjetas_creditos()
    {
        return $this->hasMany('App\recaudaciones\TarjetaCredito', 'operador_tarjeta_id');
    }
    
    public function tarjetas_debitos()
    {
        return $this->hasMany('App\recaudaciones\TarjetaDebito', 'operador_tarjeta_id');
    }     
}
