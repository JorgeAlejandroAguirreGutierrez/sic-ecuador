<?php

namespace App\recaudaciones;

use Illuminate\Database\Eloquent\Model;

class CreditoAmortizacion extends Model
{
    //
    protected $table='creditos_amortizaciones';
    protected $fillable=['secuencia', 'vencimiento', 'saldo_capital', 'dias', 'capital', 'interes', 'dividendo', 'seguro', 'otro', 'cuota', 
                        'mora', 'fecha_abono', 'abono', 'fecha_pago', 'pago', 'capital_reducido', 'estado', 'credito_id'];
    
    public function credito()
    {
        return $this->belongsTo('App\recaudaciones\Credito');
    }
}
