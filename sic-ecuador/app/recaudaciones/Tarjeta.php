<?php

namespace App\recaudaciones;

use Illuminate\Database\Eloquent\Model;

class Tarjeta extends Model
{
    //
    protected $table='tarjetas';
    protected $fillable=['codigo', 'tipo', 'nombre', 'banco_id'];
   
    public function banco()
    {
        return $this->belongsTo('App\recaudaciones\Banco');
    } 
    
    public function tarjetas_creditos()
    {
        return $this->hasMany('App\recaudaciones\TarjetaCredito', 'tarjeta_id');
    }
    
    public function tarjetas_debitos()
    {
        return $this->hasMany('App\recaudaciones\TarjetaDebito', 'tarjeta_id');
    }      
}
