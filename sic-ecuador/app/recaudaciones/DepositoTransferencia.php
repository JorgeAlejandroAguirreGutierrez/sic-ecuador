<?php

namespace App\recaudaciones;

use Illuminate\Database\Eloquent\Model;

class DepositoTransferencia extends Model
{
    //
    protected $table='depositos_transferencias';
    protected $fillable=['codigo', 'tipo_transaccion', 'numero_transaccion', 'fecha_transaccion', 'valor', 'recaudacion_id', 'banco_id'];
    
    public function recaudacion()
    {
        return $this->belongsTo('App\recaudaciones\Recaudacion');
    }
    
    public function banco()
    {
        return $this->belongsTo('App\configuraciones\Banco');
    }
}
