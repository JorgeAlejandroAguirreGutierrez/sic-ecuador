<?php

namespace App\recaudaciones;

use Illuminate\Database\Eloquent\Model;

class Recaudacion extends Model
{
    //
    protected $table='recaudaciones';
    protected $fillable=['codigo', 'fecha', 'total', 'comentario', 'efectivo', 'total_cheques', 'total_depositos_transferencias', 'total_retenciones_compras',
                        'tarjeta_credito_id', 'tarjeta_debito_id', 'credito_id', 'tipo_comprobante_id', 'comprobante_id', 'sesion_id'];

//    public function efectivo()
//    {
//        return $this->belongsTo('App\recaudaciones\Efectivo');
//    }

    public function tarjeta_credito()
    {
        return $this->belongsTo('App\recaudaciones\TarjetaCredito');
    }

    public function tarjeta_debito()
    {
        return $this->belongsTo('App\recaudaciones\TarjetaDebito');
    }
    
    public function credito()
    {
        return $this->belongsTo('App\recaudaciones\Credito');
    }

    public function tipoComprobante()
    {
        return $this->belongsTo('App\comprobantes\TipoComprobante', 'tipo_comprobante_id', 'id');
    }

    public function comprobanteFactura()
    {
        return $this->belongsTo('App\comprobantes\Factura', 'comprobante_id', 'id');
    }
    
    public function comprobantePedido()
    {
        return $this->belongsTo('App\comprobantes\Pedido', 'comprobante_id', 'id');
    }

    public function sesion()
    {
        return $this->belongsTo('App\usuarios\Usuario');
    }

    public function cheques()
    {
        return $this->hasMany('App\recaudaciones\Cheques', 'recaudacion_id');
    }

    public function depositos_transferencias()
    {
        return $this->hasMany('App\recaudaciones\DepositoTransferencia', 'recaudacion_id');
    }

    public function retenciones_compras()
    {
        return $this->hasMany('App\recaudaciones\RetencionCompra', 'recaudacion_id');
    }
    
    public function asientos()
    {
        return $this->hasMany('App\contabilizacion\Asiento', 'recaudacion_id');
    }    
}
