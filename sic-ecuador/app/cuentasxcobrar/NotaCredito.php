<?php

namespace App\cuentasxcobrar;

use Illuminate\Database\Eloquent\Model;

class NotaCredito extends Model
{
    //
    protected $table='notas_creditos';
    protected $fillable=['codigo', 'numero', 'fecha', 'agente', 'autorizacion', 'devengable', 'base_imponible', 'valor', 'tipo_retencion_id', 'recaudacion_id'];
    
    public function tipo_retencion()
    {
        return $this->belongsTo('App\configuraciones\TipoRetencion');
    }
    
    public function recaudacion()
    {
        return $this->belongsTo('App\recaudaciones\Recaudacion');
    }    
}
