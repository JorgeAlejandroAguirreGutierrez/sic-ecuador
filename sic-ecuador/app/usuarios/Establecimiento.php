<?php

namespace App\usuarios;

use Illuminate\Database\Eloquent\Model;

class Establecimiento extends Model
{
    //
    protected $table='establecimientos';
    protected $fillable=['codigo', 'direccion', 'empresa_id', 'ubicacion_id'];
    
    public function empresa()
    {
        return $this->belongsTo('App\configuraciones\Empresa');
    }  
    
    public function ubicacion()
    {
        return $this->belongsTo('App\configuraciones\Ubicacion');
    }

    public function puntosVenta()
    {
        return $this->hasMany('App\usuarios\PuntoVenta', 'establecimiento_id');
    }  
}
