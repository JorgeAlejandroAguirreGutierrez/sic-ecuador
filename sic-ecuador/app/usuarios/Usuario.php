<?php

namespace App\usuarios;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    //
    protected $table='usuarios';
    protected $fillable=['codigo', 'nombre', 'correo', 'contrasena',  'identificacion', 'avatar', 'activo', 'empresa_id', 'perfil_id'];
    
    public function empresa()
    {
        return $this->belongsTo('App\configuraciones\Empresa', 'empresa_id', 'id'); // No hace falta especificar id
    }
    
    public function perfil()
    {
        return $this->belongsTo('App\usuarios\Perfil', 'perfil_id', 'id');
    }
    
    public function sesiones()
    {
        return $this->hasMany('App\usuarios\Sesion', 'usuario_id');
    }       
    
    public function establecimientos()
    {
        return $this->hasMany('App\usuarios\Establecimiento', 'usuario_id');
    }
    
}
