<?php

namespace App\usuarios;

use Illuminate\Database\Eloquent\Model;

class PuntoVenta extends Model
{
    //
    protected $table='puntos_ventas';
    protected $fillable=['codigo' ,'descripcion', 'establecimiento_id'];
   
    public function establecimiento()
    {
        return $this->belongsTo('App\usuarios\Establecimiento');
    }

    public function sesiones()
    {
        return $this->hasMany('App\usuarios\Sesion', 'punto_venta_id');
    }     
}
