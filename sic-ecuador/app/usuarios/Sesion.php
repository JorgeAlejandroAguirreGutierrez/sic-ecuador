<?php

namespace App\usuarios;

use Illuminate\Database\Eloquent\Model;

class Sesion extends Model
{
    //
    protected $table='sesiones';
    protected $fillable=['codigo', 'estado', 'fecha_ape', 'fecha_cie', 'nombre_pc', 'sesion_ip', 'usuario_id', 'punto_venta_id'];

    public function usuario()
    {
        return $this->belongsTo('App\usuarios\Usuario');
    }
    
    public function punto_venta()
    {
        return $this->belongsTo('App\usuarios\PuntoVenta');
    }

    public function facturas()
    {
        return $this->hasMany('App\comprobantes\Factura', 'sesion_id');
    } 

    public function egresos()
    {
        return $this->hasMany('App\comprobantes\Egreso', 'sesion_id');
    } 

    public function pedidos()
    {
        return $this->hasMany('App\comprobantes\Pedido', 'vendedor_id');
    } 

    public function proformas()
    {
        return $this->hasMany('App\comprobantes\Proforma', 'vendedor_id');
    } 

    public function recaudaciones()
    {
        return $this->hasMany('App\recaudaciones\Recaudacion', 'sesion_id');
    } 
    
    public function guias_remisiones()
    {
        return $this->hasMany('App\entregas\guias_remisiones', 'sesion_id');
    }    
}
