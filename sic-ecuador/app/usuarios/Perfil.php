<?php

namespace App\usuarios;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    //
    protected $table='perfiles';
    protected $fillable=['codigo', 'descripcion','abreviatura'];
    
    public function usuarios()
    {
        return $this->hasMany('App\usuarios\Usuario', 'perfil_id');
    }
    
    public function permisos()
    {
        return $this->hasMany('App\usuarios\Usuario', 'perfil_id');    
    }
}