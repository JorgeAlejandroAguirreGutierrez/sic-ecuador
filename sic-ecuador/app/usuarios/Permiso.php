<?php

namespace App\usuarios;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    //
    protected $table='permisos';
    protected $fillable=['codigo', 'modulo','operacion', 'habilitado', 'perfil_id'];

    public function perfil()
    {
        return $this->belongsTo('App\usuarios\Perfil');
    }
}
