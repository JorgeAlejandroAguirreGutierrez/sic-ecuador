<?php

namespace App\entregas;

use Illuminate\Database\Eloquent\Model;

class GuiaRemision extends Model
{
    //
    protected $table='guias_remisiones';
    protected $fillable=['codigo', 'numero', 'fecha', 'direccion_cliente', 'n_direccion', 'n_telefono', 'n_celular', 'n_correo', 'n_referencia', 'n_logitudgeo', 'n_latitudgeo',
                        'estado', 'n_ubicacion_id', 'cliente_id', 'tipo_comprobante_id', 'comprobante_id', 'transportista_id', 'vehiculo_transporte_id', 'sesion_id'];

    public function nuevaUbicacion()
    {
        return $this->belongsTo('App\configuraciones\Ubicacion', 'n_ubicacion_id', 'id');
    }

    public function cliente()
    {
        return $this->belongsTo('App\clientes\Cliente');
    }

    public function tipo_comprobante()
    {
        return $this->belongsTo('App\comprobantes\TipoComprobante');
    }

    public function comprobanteFactura()
    {
        return $this->belongsTo('App\comprobantes\Factura', 'comprobante_id', 'id');
    }

    public function comprobanteEgreso()
    {
        return $this->belongsTo('App\comprobantes\Egreso', 'comprobante_id', 'id');
    }
    
    public function transportista()
    {
        return $this->belongsTo('App\entregas\Transportista');
    }

    public function vehiculo_transporte()
    {
        return $this->belongsTo('App\entregas\VehiculoTransporte');
    }
    
    public function sesion()
    {
        return $this->belongsTo('App\usuarios\Usuario');
    }

    public function kardexs()
    {
        return $this->hasMany('App\inventarios\Kardex', 'kardexs_id');
    } 
    
    public function asientos()
    {
        return $this->hasMany('App\contabilizacion\Asiento', 'guias_remisiones_id');
    }       
}
