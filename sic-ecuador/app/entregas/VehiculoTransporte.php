<?php

namespace App\entregas;

use Illuminate\Database\Eloquent\Model;

class VehiculoTransporte extends Model
{
    //
    protected $table='vehiculos_transportes';
    protected $fillable=['codigo', 'placa', 'numero', 'marca', 'modelo', 'anio', 'cilindraje', 'clase', 'color', 'fabricacion', 'activo'];

    public function guias_remisiones()
    {
        return $this->hasMany('App\entregas\guias_remisiones', 'vehiculo_transporte_id');
    }    
}
