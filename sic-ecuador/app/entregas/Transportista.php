<?php

namespace App\entregas;

use Illuminate\Database\Eloquent\Model;

class Transportista extends Model
{
    //
    protected $table='transportistas';
    protected $fillable=['codigo', 'nombre', 'identificacion', 'vehiculo_propio'];

    public function guias_remisiones()
    {
        return $this->hasMany('App\entregas\guias_remisiones', 'transportista_id');
    }    
}
