<?php

namespace App\inventarios;

use Illuminate\Database\Eloquent\Model;

class Bodega extends Model
{
    //
    protected $table='bodegas';
    protected $fillable=['codigo', 'punto_venta_id'];
    
    public function puntoVenta()
    {
        return $this->belongsTo('App\estructuras\PuntoVenta', 'punto_venta_id', 'id');
    }
}
