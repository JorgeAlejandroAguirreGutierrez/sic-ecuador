<?php

namespace App\clientes;

use Illuminate\Database\Eloquent\Model;

class Impuesto extends Model
{
    //
    protected $table='impuestos';
    protected $fillable=['codigo','codigo_norma', 'porcentaje'];
}
