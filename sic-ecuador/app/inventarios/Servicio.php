<?php

namespace App\clientes;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $table='servicios';
    protected $fillable=['codigo', 'nombre', 'precio1','precio2','precio3','precio4', 'activo', 'grupo_contable_id', 'impuesto_id', 'tipo_id'];
    
    
    public function grupo_contable()
    {
        return $this->belongsTo('App\clientes\GrupoContable', 'grupo_contable_id', 'id');
    }
    
    public function impuesto()
    {
        return $this->belongsTo('App\clientes\Impuesto', 'impuesto_id', 'id');
    }
    
    public function tipo()
    {
        return $this->belongsTo('App\clientes\DatoAdicional', 'tipo_id', 'id');
    }
}
