<?php

namespace App\inventarios;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    //
    protected $table='productos';
    protected $fillable=['codigo', 'nombre', 'cantidad', 'precio', 'consignacion', 'costo_compra', 'costo_kardex','medida_id', 'tipo_id','impuesto_id', 'bodega_id'];
            
    public function medida()
    {
        return $this->belongsTo('App\clientes\DatoAdicional', 'medida_id', 'id');
    }
    
    public function tipo()
    {
        return $this->belongsTo('App\clientes\DatoAdicional', 'tipo_id', 'id');
    }
    
    public function impuesto()
    {
        return $this->belongsTo('App\clientes\Impuesto', 'impuesto_id', 'id');
    }
    
    public function bodega()
    {
        return $this->belongsTo('App\inventarios\Bodega', 'bodega_id', 'id');
    }
    
}
