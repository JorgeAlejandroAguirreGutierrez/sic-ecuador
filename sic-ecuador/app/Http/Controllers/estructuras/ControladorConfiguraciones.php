<?php

namespace App\Http\Controllers\estructuras;

use Illuminate\Http\Request;
use App\estructuras\Configuracion;

class ControladorConfiguraciones extends Controller {

    //
    public function vistaLeerActualizarEliminar() {
        $configuraciones = Configuracion::all();
        return view('estructuras/LAEConfiguraciones', ['configuraciones' => $configuraciones]);
    }

    public function vistaCrear() {
        return view('estructuras/CConfiguraciones');
    }

    public function crear(Request $request) {
        try {
            $configuracion = new Configuracion($request->all());
            $bandera = $configuracion->save();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    public function obtenerConfiguraciones(Request $request) {
        try {
            $configuraciones = Configuracion::where('tipo', $request->tipo)->get();
            return ['resultado'=>$configuraciones];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerConfiguracion(Request $request) {
        try {
            $configuracion = Configuracion::find($request->id);
            return ['resultado'=>$configuracion];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function modificarConfiguracion(Request $request) {
        try {
            $configuracion = Configuracion::find($request->id);
            $configuracion->tabla = $request->tabla;
            $configuracion->tipo = $request->tipo;
            $configuracion->codigo = $request->codigo;
            $bandera = $configuracion->save();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    public function eliminarConfiguracion(Request $request) {
        try {
            $configuracion = Configuracion::find($request->id);
            $bandera = $configuracion->delete();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

}
