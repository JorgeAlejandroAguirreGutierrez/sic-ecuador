<?php

namespace App\Http\Controllers\estructuras;

use Illuminate\Http\Request;
use App\estructuras\Establecimiento;

class ControladorEstablecimientos extends Controller {

    //
    public function vistaLeerActualizarEliminar() {
        $establecimientos = Establecimiento::all();
        return view('estructuras/LAEEstablecimientos', ['establecimientos' => $establecimientos]);
    }

    public function vistaCrear() {
        return view('estructuras/CEstablecimientos');
    }

    public function crear(Request $request) {
        try {
            $establecimiento = new Establecimiento($request->all());
            $bandera = $establecimiento->save();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerEstablecimientos() {
        try {
            $establecimientos = Establecimiento::all();
            return ['resultado'=>$establecimientos];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerEstablecimiento(Request $request) {
        try {
            $establecimiento = Establecimiento::find($request->id);
            $establecimiento->empresa;
            $establecimiento->ubicaciongeo;
            return ['resultado'=>$establecimiento];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function eliminarEstablecimiento(Request $request) {
        try {
            $establecimiento = Establecimiento::find($request->id);
            $bandera = $establecimiento->delete();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    public function obtenerEstablecimientosEmpresa(Request $request) {
        try {
            $establecimientos = Establecimiento::where('empresa_id', $request->empresa_id)->get();
            return ['resultado'=>$establecimientos];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

}
