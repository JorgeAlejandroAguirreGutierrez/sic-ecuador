<?php

namespace App\Http\Controllers\estructuras;

use Illuminate\Http\Request;
use App\estructuras\PuntoVenta;

class ControladorPuntosVentas extends Controller
{
    //
    public function vistaLeerActualizarEliminar() {
        $puntosVentas= PuntoVenta::all();
        return view('estructuras/LAEPuntosVentas', ['puntosVentas' => $puntosVentas]);
    }
    
    public function vistaCrear()
    {
        return view('estructuras/CPuntosVentas');
    }
    
    public function crear(Request $request){
        try {
            $puntoVenta=new PuntoVenta($request->all());
            $bandera=$puntoVenta->save();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    public function obtenerPuntoVenta(Request $request){
        try {
        $puntoVenta = PuntoVenta::find($request->id);
        return ['resultado'=>$puntoVenta];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    public function eliminarPuntoVenta(Request $request) {
        try {
            $puntoVenta = PuntoVenta::find($request->id);
            $bandera = $puntoVenta->delete();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    public function obtenerPuntosVentasEstablecimiento(Request $request){
        try {
            $puntosVentas=PuntoVenta::where('establecimiento_id', $request->establecimiento_id)->get();
            return ['resultado'=>$puntosVentas];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
}
