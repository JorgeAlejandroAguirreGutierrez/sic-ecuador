<?php

namespace App\Http\Controllers\estructuras;

use Illuminate\Http\Request;
use App\estructuras\Empresa;

class ControladorEmpresas extends Controller {

    public function vistaLeerActualizarEliminar() {
        $empresas = Empresa::all();
        return view('estructuras/LAEEmpresas', ['empresas' => $empresas]);
    }

    public function vistaCrear() {
        return view('estructuras/CEmpresas');
    }

    public function crear(Request $request) {
        try {
            $empresa = new Empresa($request->all());
            $bandera = $empresa->save();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerEmpresas() {
        try {
            $empresas = Empresa::all();
            return ['resultado'=>$empresas];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerEmpresa(Request $request) {
        try {
            $empresa = Empresa::find($request->id);
            return ['resultado'=>$empresa, 'resultado'=>null, 'mensaje'=>'Exito al obtener la empresa'];
        } catch (Exception $e) {
            return ['resultado'=>null,'resultado'=>null, 'mensaje'=>'Error al obtener la empresa'];
        }
    }

    public function eliminarEmpresa(Request $request) {
        try {
            $empresa = Empresa::find($request->id);
            $bandera = $empresa->delete();
            return ['respuesta'=>true, 'resultado'=>$bandera, 'mensaje'=>'Exito al eliminar la empresa'];
        } catch (Exception $e) {
            return ['respuesta'=>false, 'resultado'=>null, 'mensaje'=>'Error al eliminar la empresa'];
        }
    }

}
