<?php

namespace App\Http\Controllers\cobros;

use Illuminate\Http\Request;
use App\cobros\Cheque;
use App\cobros\DepositoTransferencia;
use App\cobros\Efectivo;
use App\cobros\TarjetaCredito;
use App\cobros\TarjetaDebito;
use App\cobros\ComprobanteRetencion;
use App\cobros\Credito;
use App\facturas\CabeceraFactura;
use App\facturas\LineaFactura;
use App\clientes\DatoAdicional;
use App\estructuras\Configuracion;

class ControladorRecaudacionesEfectivo extends Controller {

    public function vistaCrear(Request $request) {
        $cabeceraFactura = CabeceraFactura::find($request->id);
        $tipo = DatoAdicional::where('nombre', 'CHEQUE A LA VISTA')->get();
        $chequesVista = Cheque::where('cabecera_factura_id', $cabeceraFactura->id)->where('tipo_id', $tipo[0]->id)->get();
        $tipo = DatoAdicional::where('nombre', 'CHEQUE POSFECHADO')->get();
        $chequesPosfechado = Cheque::where('cabecera_factura_id', $cabeceraFactura->id)->where('tipo_id', $tipo[0]->id)->get();
        $tipo = DatoAdicional::where('nombre', 'DEPOSITO')->get();
        $depositos = DepositoTransferencia::where('cabecera_factura_id', $cabeceraFactura->id)->where('tipo_id', $tipo[0]->id)->get();
        $tipo = DatoAdicional::where('nombre', 'TRANSFERENCIA')->get();
        $transferencias = DepositoTransferencia::where('cabecera_factura_id', $cabeceraFactura->id)->where('tipo_id', $tipo[0]->id)->get();
        $efectivos = Efectivo::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        $recaudacionesEfectivo = ['CHEQUE A LA VISTA' => $chequesVista, 'CHEQUE POSFECHADO' => $chequesPosfechado, 'DEPOSITO' => $depositos, 'TRANSFERENCIA' => $transferencias, 'EFECTIVO' => $efectivos];
        $recaudo = $this->obtenerRecaudos($cabeceraFactura);
        return view('cobros/recaudacionesEfectivo', ['cabeceraFactura' => $cabeceraFactura, 'recaudacionesEfectivo' => $recaudacionesEfectivo, 'recaudo' => $recaudo]);
    }

    public function obtenerTiposLineas(Request $request) {
        try {
            $lineasFacturas = LineaFactura::where('cabecera_factura_id', $request->cabecera_factura_id)->get();
            $productos = '';
            $servicios = '';
            for ($i = 0; $i < count($lineasFacturas); $i++) {
                if ($lineasFacturas[$i]->producto_id != null && $productos == '') {
                    $productos = 'productos';
                }
                if ($lineasFacturas[$i]->servicio_id != null && $servicios == '') {
                    $servicios = 'servicios';
                }
            }
            return ['resultado'=>array('servicios' => $servicios, 'productos' => $productos)];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerCodigoChequeVista() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'cheques_vistas')->where('tipo', 'CREAR')->get();
            $conteoCheques = Cheque::count();
            $conteoCheques++;
            $rellenoConteo = str_pad($conteoCheques, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 0, 2);
            return ['resultado'=>$configuracion[0]->codigo . $año . $mes . $rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerCodigoChequePosfechado() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'cheques_posfechados')->where('tipo', 'CREAR')->get();
            $conteoCheques = Cheque::count();
            $conteoCheques++;
            $rellenoConteo = str_pad($conteoCheques, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 3, 2);
            return ['resultado'=>$configuracion[0]->codigo . $año . $mes . $rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function crearChequeVista(Request $request) {
        try {
            $cabeceraFactura = CabeceraFactura::find($request->cabecera_factura_id);
            $cheque = new Cheque($request->all());
            $cheque->valor=round($cheque->valor, 2);
            $recaudo = $this->obtenerRecaudos($cabeceraFactura);
            if ($recaudo + $cheque->valor <= $cabeceraFactura->total) {
                $tipo = DatoAdicional::where('nombre', 'CHEQUE A LA VISTA')->get();
                $cheque->tipo_id = $tipo[0]->id;
                $bandera = $cheque->save();
                return ['resultado'=>$bandera];
            } else {
                $bandera = false;
                return ['resultado'=>$bandera];
            }
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    public function crearChequePosfechado(Request $request) {
        try {
            $cabeceraFactura = CabeceraFactura::find($request->cabecera_factura_id);
            $cheque = new Cheque($request->all());
            $recaudo = $this->obtenerRecaudos($cabeceraFactura);
            if ($recaudo + $cheque->valor <= $cabeceraFactura->total) {
                $tipo = DatoAdicional::where('nombre', 'CHEQUE POSFECHADO')->get();
                $cheque->tipo_id = $tipo[0]->id;
                $bandera = $cheque->save();
                return ['resultado'=>$bandera];
            } else {
                $bandera = false;
                return ['resultado'=>$bandera];
            }
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    public function obtenerCodigoEfectivo() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'efectivos')->where('tipo', 'CREAR')->get();
            $conteoEfectivos = Efectivo::count();
            $conteoEfectivos++;
            $rellenoConteo = str_pad($conteoEfectivos, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 3, 2);
            return ['resultado'=>$configuracion[0]->codigo . $año . $mes . $rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function crearEfectivo(Request $request) {
        try {
            $cabeceraFactura = CabeceraFactura::find($request->cabecera_factura_id);
            $recaudo = $this->obtenerRecaudos($cabeceraFactura);
            $efectivo = new Efectivo($request->all());
            if ($recaudo + $efectivo->valor <= $cabeceraFactura->total) {
                $bandera = $efectivo->save();
                return ['resultado'=>$bandera];
            } else {
                $bandera = false;
                return ['resultado'=>$bandera];
            }
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    public function obtenerFecha(Request $request) {
        try {
            $cabeceraFactura = CabeceraFactura::find($request->cabecera_factura_id);
            $fecha = $cabeceraFactura->fecha;
            return ['resultado'=>$fecha];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerCodigoDeposito() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'depositos')->where('tipo', 'CREAR')->get();
            $conteoDepositosTransferencias = DepositoTransferencia::count();
            $conteoDepositosTransferencias++;
            $rellenoConteo = str_pad($conteoDepositosTransferencias, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 3, 2);
            return ['resultado'=>$configuracion[0]->codigo . $año . $mes . $rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerCodigoTransferencia() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'transferencias')->where('tipo', 'CREAR')->get();
            $conteoDepositosTransferencias = DepositoTransferencia::count();
            $conteoDepositosTransferencias++;
            $rellenoConteo = str_pad($conteoDepositosTransferencias, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 3, 2);
            return ['resultado'=>$configuracion[0]->codigo . $año . $mes . $rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function crearDeposito(Request $request) {
        try {
            $cabeceraFactura = CabeceraFactura::find($request->cabecera_factura_id);
            $recaudo = $this->obtenerRecaudos($cabeceraFactura);
            $deposito = new DepositoTransferencia($request->all());
            if ($recaudo + $deposito->valor <= $cabeceraFactura->total) {
                $tipo = DatoAdicional::where('nombre', 'DEPOSITO')->get();
                $deposito->tipo_id = $tipo[0]->id;
                $bandera = $deposito->save();
                return ['resultado'=>$bandera];
            } else {
                $bandera = false;
                return ['resultado'=>$bandera];
            }
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    public function crearTransferencia(Request $request) {
        try {
            $cabeceraFactura = CabeceraFactura::find($request->cabecera_factura_id);
            $recaudo = $this->obtenerRecaudos($cabeceraFactura);
            $transferencia = new DepositoTransferencia($request->all());
            if ($recaudo + $transferencia->valor <= $cabeceraFactura->total) {
                $tipo = DatoAdicional::where('nombre', 'TRANSFERENCIA')->get();
                $transferencia->tipo_id = $tipo[0]->id;
                $bandera = $transferencia->save();
                return ['resultado'=>$bandera];
            } else {
                $bandera = false;
                return ['resultado'=>$bandera];
            }
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    public function eliminarCheque(Request $request) {
        try {
            $cheque = Cheque::find($request->id);
            $bandera = $cheque->delete();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    public function eliminarDepositoTransferencia(Request $request) {
        try {
            $depositoTransferencia = DepositoTransferencia::find($request->id);
            $bandera = $depositoTransferencia->delete();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    public function eliminarEfectivo(Request $request) {
        try {
            $efectivo = Efectivo::find($request->id);
            $bandera = $efectivo->delete();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    public function obtenerCheque(Request $request) {
        try {
            $cheque = Cheque::find($request->id);
            $cheque->banco;
            return ['resultado'=>$cheque];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerDepositoTransferencia(Request $request) {
        try {
            $depositoTransferencia = DepositoTransferencia::find($request->id);
            $depositoTransferencia->banco;
            return ['resultado'=>$depositoTransferencia];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerEfectivo(Request $request) {
        try {
            $efectivo = Efectivo::find($request->id);
            return ['resultado'=>$efectivo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    private function obtenerRecaudos($cabeceraFactura) {
        $tipo = DatoAdicional::where('nombre', 'CHEQUE A LA VISTA')->get();
        $chequesVista = Cheque::where('cabecera_factura_id', $cabeceraFactura->id)->where('tipo_id', $tipo[0]->id)->get();
        $tipo = DatoAdicional::where('nombre', 'CHEQUE POSFECHADO')->get();
        $chequesPosfechado = Cheque::where('cabecera_factura_id', $cabeceraFactura->id)->where('tipo_id', $tipo[0]->id)->get();
        $tipo = DatoAdicional::where('nombre', 'DEPOSITO')->get();
        $depositos = DepositoTransferencia::where('cabecera_factura_id', $cabeceraFactura->id)->where('tipo_id', $tipo[0]->id)->get();
        $tipo = DatoAdicional::where('nombre', 'TRANSFERENCIA')->get();
        $transferencias = DepositoTransferencia::where('cabecera_factura_id', $cabeceraFactura->id)->where('tipo_id', $tipo[0]->id)->get();
        $efectivos = Efectivo::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        $recaudo = 0;
        for ($i = 0; $i < count($chequesVista); $i++) {
            $recaudo = $recaudo + $chequesVista[$i]->valor;
        }
        for ($i = 0; $i < count($chequesPosfechado); $i++) {
            $recaudo = $recaudo + $chequesPosfechado[$i]->valor;
        }
        for ($i = 0; $i < count($depositos); $i++) {
            $recaudo = $recaudo + $depositos[$i]->valor;
        }
        for ($i = 0; $i < count($transferencias); $i++) {
            $recaudo = $recaudo + $transferencias[$i]->valor;
        }
        for ($i = 0; $i < count($efectivos); $i++) {
            $recaudo = $recaudo + $efectivos[$i]->valor;
        }
        $recaudacionesTarjetasCreditos = TarjetaCredito::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        for ($i = 0; $i < count($recaudacionesTarjetasCreditos); $i++) {
            $recaudo = $recaudo + $recaudacionesTarjetasCreditos[$i]->valor;
        }
        $recaudacionesTarjetasDebitos = TarjetaDebito::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        for ($i = 0; $i < count($recaudacionesTarjetasDebitos); $i++) {
            $recaudo = $recaudo + $recaudacionesTarjetasDebitos[$i]->valor;
        }
        $recaudacionesComprobantesRetenciones = ComprobanteRetencion::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        for ($i = 0; $i < count($recaudacionesComprobantesRetenciones); $i++) {
            $recaudo = $recaudo + $recaudacionesComprobantesRetenciones[$i]->valor;
        }
        $recaudacionesCreditos=Credito::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        for ($i = 0; $i < count($recaudacionesCreditos); $i++) {
            $recaudo = $recaudo + $recaudacionesCreditos[$i]->valor+$recaudacionesCreditos[$i]->abono;
        }
        if ($recaudo>$cabeceraFactura->total) {
            $recaudo=$cabeceraFactura->total;
            return ['resultado'=>$recaudo];
        } else {
            return ['resultado'=>$recaudo];
        }
    }

}
