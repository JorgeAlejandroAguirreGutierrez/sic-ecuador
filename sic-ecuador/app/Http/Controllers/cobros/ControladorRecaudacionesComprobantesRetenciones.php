<?php

namespace App\Http\Controllers\cobros;

use Illuminate\Http\Request;
use App\facturas\CabeceraFactura;
use App\clientes\DatoAdicional;
use App\clientes\Retencion;
use App\estructuras\Configuracion;
use App\cobros\ComprobanteRetencion;
use App\cobros\ItemComprobanteRetencion;
use App\cobros\Cheque;
use App\cobros\DepositoTransferencia;
use App\cobros\Efectivo;
use App\cobros\TarjetaCredito;
use App\cobros\TarjetaDebito;
use App\cobros\Credito;

class ControladorRecaudacionesComprobantesRetenciones extends Controller {

    public function vistaCrear(Request $request) {
        $cabeceraFactura = CabeceraFactura::find($request->id);
        $recaudo = $this->obtenerRecaudos($cabeceraFactura);
        $comprobantesRetenciones = ComprobanteRetencion::where('cabecera_factura_id', $cabeceraFactura->id)->get();

        $tipo = DatoAdicional::where('tipo', 'TIPO COMPROBANTE RETENCION')->first();
        $anoRetencion = $this->obtenerAno($cabeceraFactura);
        $cabeceraFacturaRetencion = $this->serieCabeceraFactura($cabeceraFactura);
        $serieEstablecimientoRetencion = $this->serieEstablecimiento($cabeceraFactura);
        $seriePuntoVentaRetencion = $this->seriePuntoVenta($cabeceraFactura);
        return view('cobros/recaudacionesComprobantesRetenciones', ['cabeceraFactura' => $cabeceraFactura, 'comprobantesRetenciones' => $comprobantesRetenciones, 'recaudo' => $recaudo, 'tipo' => $tipo, 'anoRetencion' => $anoRetencion,
            'cabeceraFacturaRetencion' => $cabeceraFacturaRetencion, 'serieEstablecimientoRetencion' => $serieEstablecimientoRetencion, 'seriePuntoVentaRetencion' => $seriePuntoVentaRetencion]);
    }
    
    public function vistaActualizar(Request $request) {
        $comprobanteRetencion = ComprobanteRetencion::find($request->id);
        $cabeceraFactura = CabeceraFactura::find($request->cabecera_factura_id);
        $recaudo = $this->obtenerRecaudos($cabeceraFactura);
        $anoRetencion=$this->obtenerAno($cabeceraFactura);
        $serieEstablecimientoRetencion = $this->serieEstablecimiento($cabeceraFactura);
        $seriePuntoVentaRetencion = $this->seriePuntoVenta($cabeceraFactura);
        $cabeceraFacturaRetencion = $this->serieCabeceraFactura($cabeceraFactura);
        $itemsComprobanteRetencion= ItemComprobanteRetencion::where('comprobante_retencion_id',$comprobanteRetencion->id);
        return view('cobros/recaudacionesComprobantesRetenciones', ['cabeceraFactura' => $cabeceraFactura, 'comprobanteRetencion' => $comprobanteRetencion, 'itemsComprobanteRetencion'=>$itemsComprobanteRetencion,'recaudo' => $recaudo,
            'cabeceraFacturaRetencion' => $cabeceraFacturaRetencion, 'anoRetencion'=>$anoRetencion,'serieEstablecimientoRetencion' => $serieEstablecimientoRetencion, 'seriePuntoVentaRetencion' => $seriePuntoVentaRetencion]);
    }

    public function obtenerCodigoComprobanteRetencion() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'comprobantes_retenciones')->where('tipo', 'CREAR')->get();
            $conteoComprobantesRetenciones = ComprobanteRetencion::count();
            $conteoComprobantesRetenciones++;
            $rellenoConteo = str_pad($conteoComprobantesRetenciones, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 0, 2);
            return ['resultado'=>$configuracion[0]->codigo . $año . $mes . $rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerTipo() {
        try {
            $datoAdicional = DatoAdicional::where('tipo', 'TIPOS COMPROBANTES')->get();
            return ['resultado'=>$datoAdicional];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function cargarComprobanteRetencion(Request $request) {
        $comprobanteRetencion = array();
        $nombre = $request->archivo->getClientOriginalName();
        $ruta = $request->archivo->storeAs('xml', $nombre);
        if ($ruta != null) {
            $ruta = storage_path() . '\\app\\xml\\' . $nombre;
            $rawXML = file_get_contents($ruta);
            $rawXML = str_replace('<![CDATA[<?xml version="1.0" encoding="UTF-8"?><comprobanteRetencion id="comprobante" version="1.0.0">', "", $rawXML);
            $rawXML = str_replace('</comprobanteRetencion>]]>', "", $rawXML);
            $rawXML = str_replace('<comprobante>', "", $rawXML);
            $rawXML = str_replace('</comprobante>', "", $rawXML);
            file_put_contents($ruta, $rawXML);
            $comprobante = $this->leerComprobanteRetencion($ruta);
            $items = $this->leerItemsComprobanteRetencion($ruta);
            array_push($comprobanteRetencion, $comprobante);
            array_push($comprobanteRetencion, $items);
            return ['resultado'=>$comprobanteRetencion];
        } else {
            return ['resultado'=>null];
        }
    }

    private function leerComprobanteRetencion($ruta) {
        try {
            $xml = simplexml_load_file($ruta);
            $establecimiento = (string) $xml->infoTributaria->estab;
            $puntoventa = (string) $xml->infoTributaria->ptoEmi;
            $secuencial = (string) $xml->infoTributaria->secuencial;
            $comprobanteRetencion = $establecimiento . '-' . $puntoventa . '-' . $secuencial;
            $fechaEmision = (string) $xml->infoCompRetencion->fechaEmision;
            $autorizacion = (string) $xml->infoTributaria->claveAcceso;
            $comprobante = new ComprobanteRetencion();
            $comprobante->comprobante_retencion = $comprobanteRetencion;
            $comprobante->fecha_emision = $fechaEmision;
            $comprobante->autorizacion = $autorizacion;
            $comprobante->valor = 0;
            return ['resultado'=>$comprobante];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    private function leerItemsComprobanteRetencion($ruta) {
        try {
            $items = array();
            $xml = simplexml_load_file($ruta);
            foreach ($xml->impuestos->impuesto as $impuesto) {
                $codigoRetencion = (string) $impuesto->codigoRetencion;
                $baseImponible = (double) $impuesto->baseImponible;
                $valor = (double) $impuesto->valorRetenido;
                $retencion = Retencion::where('codigo_norma', $codigoRetencion)->orWhere('homologacion_f_e', $codigoRetencion)->first();
                $retencion_id = $retencion->id;
                $item = new ItemComprobanteRetencion();
                $item->retencion_id = $retencion_id;
                $item->retencion;
                $item->base_imponible = $baseImponible;
                $item->valor = $valor;
                array_push($items, $item);
            }
            return ['resultado'=>$items];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerAno($cabeceraFactura) {
        try {
            $fecha = strtotime($cabeceraFactura->fecha);
            $ano=date("Y", $fecha);
            return ['resultado'=>$ano];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function serieCabeceraFactura($cabeceraFactura) {
        try {
            $codigoInterno = $cabeceraFactura->codigo_interno;
            $serieCabeceraFactura = substr($codigoInterno, 13);
            return ['resultado'=>$serieCabeceraFactura];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function serieEstablecimiento($cabeceraFactura) {
        try {
            $codigoInterno = $cabeceraFactura->codigo_interno;
            $serieEstablecimiento = substr($codigoInterno, 3, 3);
            return ['resultado'=>$serieEstablecimiento];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function seriePuntoVenta($cabeceraFactura) {
        try {
            $codigoInterno = $cabeceraFactura->codigo_interno;
            $seriePuntoVenta = substr($codigoInterno, 6, 3);
            return ['resultado'=>$seriePuntoVenta];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerBaseImponible(Request $request) {
        try {
            $cabeceraFactura = CabeceraFactura::find($request->cabecera_factura_id);
            $retencion = Retencion::find($request->retencion_id);
            if (strpos($retencion->codigo, 'IR') !== false) {
                return ['resultado'=>$cabeceraFactura->subtotal];
            } else {
                return ['resultado'=>$cabeceraFactura->importe_iva];
            }
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function crearComprobanteRetencion(Request $request) {
        try {
            $comprobanteRetencion = new ComprobanteRetencion($request->all());
            $recaudos = $this->obtenerRecaudos($comprobanteRetencion->cabecera_factura_id);
            if ($comprobanteRetencion->valor <= $recaudos) {
                $bandera = $comprobanteRetencion->save();
                if ($bandera) {
                    return ['resultado'=>$comprobanteRetencion];
                } else {
                    return ['resultado'=>null];
                }
            } else {
                return ['resultado'=>null];
            }
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function crearItemComprobanteRetencion(Request $request) {
        try {
            $itemComprobanteRetencion = new ItemComprobanteRetencion($request->all());
            $bandera = $itemComprobanteRetencion->save();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    public function eliminarComprobanteRetencion(Request $request) {
        try {
            $comprobanteRetencion = ComprobanteRetencion::find($request->id);
            $bandera = $comprobanteRetencion->delete();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    public function eliminarItemComprobanteRetencion(Request $request) {
        try {
            $itemComprobanteRetencion = ItemComprobanteRetencion::find($request->id);
            $bandera = $itemComprobanteRetencion->delete();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    public function obtenerComprobanteRetencion(Request $request) {
        try {
            $comprobanteRetencion = ComprobanteRetencion::find($request->id);
            return ['resultado'=>$comprobanteRetencion];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerItemComprobanteRetencion(Request $request) {
        try {
            $itemComprobanteRetencion = ItemComprobanteRetencion::find($request->id);
            return ['resultado'=>$itemComprobanteRetencion];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function actualizarValorComprobanteRetencion(Request $request) {
        try {
            $comprobanteRetencion = ComprobanteRetencion::find($request->comprobante_retencion_id);
            $itemsComprobanteRetencion = ItemComprobanteRetencion::where('comprobante_retencion_id', $request->comprobante_retencion_id)->get();
            $valor = 0;
            for ($i = 0; $i < count($itemsComprobanteRetencion); $i++) {
                $valor = $itemsComprobanteRetencion[$i]->valor + $valor;
            }
            $comprobanteRetencion->valor = $valor;
            $bandera = $comprobanteRetencion->save();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    private function obtenerRecaudos($cabeceraFactura) {
        $tipo = DatoAdicional::where('nombre', 'CHEQUE A LA VISTA')->get();
        $chequesVista = Cheque::where('cabecera_factura_id', $cabeceraFactura->id)->where('tipo_id', $tipo[0]->id)->get();
        $tipo = DatoAdicional::where('nombre', 'CHEQUE POSFECHADO')->get();
        $chequesPosfechado = Cheque::where('cabecera_factura_id', $cabeceraFactura->id)->where('tipo_id', $tipo[0]->id)->get();
        $tipo = DatoAdicional::where('nombre', 'DEPOSITO')->get();
        $depositos = DepositoTransferencia::where('cabecera_factura_id', $cabeceraFactura->id)->where('tipo_id', $tipo[0]->id)->get();
        $tipo = DatoAdicional::where('nombre', 'TRANSFERENCIA')->get();
        $transferencias = DepositoTransferencia::where('cabecera_factura_id', $cabeceraFactura->id)->where('tipo_id', $tipo[0]->id)->get();
        $efectivos = Efectivo::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        $recaudo = 0;
        for ($i = 0; $i < count($chequesVista); $i++) {
            $recaudo = $recaudo + $chequesVista[$i]->valor;
        }
        for ($i = 0; $i < count($chequesPosfechado); $i++) {
            $recaudo = $recaudo + $chequesPosfechado[$i]->valor;
        }
        for ($i = 0; $i < count($depositos); $i++) {
            $recaudo = $recaudo + $depositos[$i]->valor;
        }
        for ($i = 0; $i < count($transferencias); $i++) {
            $recaudo = $recaudo + $transferencias[$i]->valor;
        }
        for ($i = 0; $i < count($efectivos); $i++) {
            $recaudo = $recaudo + $efectivos[$i]->valor;
        }
        $recaudacionesTarjetasCreditos = TarjetaCredito::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        for ($i = 0; $i < count($recaudacionesTarjetasCreditos); $i++) {
            $recaudo = $recaudo + $recaudacionesTarjetasCreditos[$i]->valor;
        }
        $recaudacionesTarjetasDebitos = TarjetaDebito::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        for ($i = 0; $i < count($recaudacionesTarjetasDebitos); $i++) {
            $recaudo = $recaudo + $recaudacionesTarjetasDebitos[$i]->valor;
        }
        $recaudacionesComprobantesRetenciones = ComprobanteRetencion::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        for ($i = 0; $i < count($recaudacionesComprobantesRetenciones); $i++) {
            $recaudo = $recaudo + $recaudacionesComprobantesRetenciones[$i]->valor;
        }
        $recaudacionesCreditos=Credito::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        for ($i = 0; $i < count($recaudacionesCreditos); $i++) {
            $recaudo = $recaudo + $recaudacionesCreditos[$i]->valor+$recaudacionesCreditos[$i]->abono;
        }
        if ($recaudo>$cabeceraFactura->total) {
            $recaudo=$cabeceraFactura->total;
            return ['resultado'=>$recaudo];
        } else {
            return ['resultado'=>$recaudo];
        }
    }

}
