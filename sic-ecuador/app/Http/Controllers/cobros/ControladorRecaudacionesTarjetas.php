<?php

namespace App\Http\Controllers\cobros;

use Illuminate\Http\Request;
use App\cobros\TarjetaCredito;
use App\cobros\TarjetaDebito;
use App\cobros\Cheque;
use App\cobros\DepositoTransferencia;
use App\cobros\Efectivo;
use App\cobros\ComprobanteRetencion;
use App\cobros\Credito;
use App\facturas\CabeceraFactura;
use App\facturas\LineaFactura;
use App\estructuras\Configuracion;
use App\clientes\DatoAdicional;

class ControladorRecaudacionesTarjetas extends Controller {

    //
    public function vistaCrear(Request $request) {
        $cabeceraFactura = CabeceraFactura::find($request->id);
        $tarjetasCreditos = TarjetaCredito::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        $tarjetasDebitos = TarjetaDebito::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        $recaudacionesTarjetas = ['TARJETA CREDITO' => $tarjetasCreditos, 'TARJETA DEBITO' => $tarjetasDebitos];
        $recaudo = $this->obtenerRecaudos($cabeceraFactura);

        return view('cobros/recaudacionesTarjetas', ['cabeceraFactura' => $cabeceraFactura, 'recaudacionesTarjetas' => $recaudacionesTarjetas, 'recaudo' => $recaudo]);
    }

    public function obtenerTiposLineas(Request $request) {
        try {
            $lineasFacturas = LineaFactura::where('cabecera_factura_id', $request->cabecera_factura_id)->get();
            $productos = '';
            $servicios = '';
            for ($i = 0; $i < count($lineasFacturas); $i++) {
                if ($lineasFacturas[$i]->producto_id != null && $productos == '') {
                    $productos = 'productos';
                }
                if ($lineasFacturas[$i]->servicio_id != null && $servicios == '') {
                    $servicios = 'servicios';
                }
            }
            return ['resultado'=>array('servicios' => $servicios, 'productos' => $productos)];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerCodigoTarjetaCredito() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'tarjetas_creditos')->where('tipo', 'CREAR')->get();
            $conteoTarjetasCreditos = TarjetaCredito::count();
            $conteoTarjetasCreditos++;
            $rellenoConteo = str_pad($conteoTarjetasCreditos, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 0, 2);
            return ['resultado'=>$configuracion[0]->codigo . $año . $mes . $rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerCodigoTarjetaDebito() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'tarjetas_debitos')->where('tipo', 'CREAR')->get();
            $conteoTarjetasDebitos = TarjetaDebito::count();
            $conteoTarjetasDebitos++;
            $rellenoConteo = str_pad($conteoTarjetasDebitos, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 0, 2);
            return ['resultado'=>$configuracion[0]->codigo . $año . $mes . $rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function crearTarjetaCredito(Request $request) {
        try {
            $cabeceraFactura = CabeceraFactura::find($request->cabecera_factura_id);
            $recaudo = $this->obtenerRecaudos($cabeceraFactura);
            $tarjetaCredito = new TarjetaCredito($request->all());
            if ($recaudo + $tarjetaCredito->valor <= $cabeceraFactura->total) {
                $bandera = $tarjetaCredito->save();
                return ['resultado'=>$bandera];
            } else {
                $bandera = false;
                return ['resultado'=>$bandera];
            }
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    public function crearTarjetaDebito(Request $request) {
        try {
            $cabeceraFactura = CabeceraFactura::find($request->cabecera_factura_id);
            $recaudo = $this->obtenerRecaudos($cabeceraFactura);
            $tarjetaDebito = new TarjetaDebito($request->all());
            if ($recaudo + $tarjetaDebito->valor <= $cabeceraFactura->total) {
                $bandera = $tarjetaDebito->save();
                return ['resultado'=>$bandera];
            } else {
                $bandera = false;
                return ['resultado'=>$bandera];
            }
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    public function obtenerFecha(Request $request) {
        try {
            $cabeceraFactura = CabeceraFactura::find($request->cabecera_factura_id);
            $fecha = $cabeceraFactura->fecha;
            return ['resultado'=>$fecha];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function eliminarTarjetaCredito(Request $request) {
        try {
            $tarjetaCredito = TarjetaCredito::find($request->id);
            $bandera = $tarjetaCredito->delete();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    public function eliminarTarjetaDebito(Request $request) {
        try {
            $tarjetaDebito = TarjetaDebito::find($request->id);
            $bandera = $tarjetaDebito->delete();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    public function obtenerTarjetaCredito(Request $request) {
        try {
            $tarjetaCredito = TarjetaCredito::find($request->id);
            $tarjetaCredito->tarjeta;
            $tarjetaCredito->ifi;
            $tarjetaCredito->operador;
            $tarjetaCredito->formaCobro;
            return ['resultado'=>$tarjetaCredito];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerTarjetaDebito(Request $request) {
        try {
            $tarjetaDebito = TarjetaDebito::find($request->id);
            $tarjetaDebito->tarjeta;
            $tarjetaDebito->ifi;
            $tarjetaDebito->operador;
            return ['resultado'=>$tarjetaDebito];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function modificarTarjetaCredito(Request $request) {
        try {
            $tarjetaCredito = TarjetaCredito::find($request->id);
            $tarjetaCredito->boucher = $request->boucher;
            $tarjetaCredito->lote = $request->lote;
            $tarjetaCredito->fecha = $request->fecha;
            $tarjetaCredito->comentario = $request->comentario;
            $tarjetaCredito->valor = $request->valor;
            return ['resultado'=>$tarjetaCredito];
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    private function obtenerRecaudos($cabeceraFactura) {
        $tipo = DatoAdicional::where('nombre', 'CHEQUE A LA VISTA')->get();
        $chequesVista = Cheque::where('cabecera_factura_id', $cabeceraFactura->id)->where('tipo_id', $tipo[0]->id)->get();
        $tipo = DatoAdicional::where('nombre', 'CHEQUE POSFECHADO')->get();
        $chequesPosfechado = Cheque::where('cabecera_factura_id', $cabeceraFactura->id)->where('tipo_id', $tipo[0]->id)->get();
        $tipo = DatoAdicional::where('nombre', 'DEPOSITO')->get();
        $depositos = DepositoTransferencia::where('cabecera_factura_id', $cabeceraFactura->id)->where('tipo_id', $tipo[0]->id)->get();
        $tipo = DatoAdicional::where('nombre', 'TRANSFERENCIA')->get();
        $transferencias = DepositoTransferencia::where('cabecera_factura_id', $cabeceraFactura->id)->where('tipo_id', $tipo[0]->id)->get();
        $efectivos = Efectivo::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        $recaudo = 0;
        for ($i = 0; $i < count($chequesVista); $i++) {
            $recaudo = $recaudo + $chequesVista[$i]->valor;
        }
        for ($i = 0; $i < count($chequesPosfechado); $i++) {
            $recaudo = $recaudo + $chequesPosfechado[$i]->valor;
        }
        for ($i = 0; $i < count($depositos); $i++) {
            $recaudo = $recaudo + $depositos[$i]->valor;
        }
        for ($i = 0; $i < count($transferencias); $i++) {
            $recaudo = $recaudo + $transferencias[$i]->valor;
        }
        for ($i = 0; $i < count($efectivos); $i++) {
            $recaudo = $recaudo + $efectivos[$i]->valor;
        }
        $recaudacionesTarjetasCreditos = TarjetaCredito::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        for ($i = 0; $i < count($recaudacionesTarjetasCreditos); $i++) {
            $recaudo = $recaudo + $recaudacionesTarjetasCreditos[$i]->valor;
        }
        $recaudacionesTarjetasDebitos = TarjetaDebito::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        for ($i = 0; $i < count($recaudacionesTarjetasDebitos); $i++) {
            $recaudo = $recaudo + $recaudacionesTarjetasDebitos[$i]->valor;
        }
        $recaudacionesComprobantesRetenciones = ComprobanteRetencion::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        for ($i = 0; $i < count($recaudacionesComprobantesRetenciones); $i++) {
            $recaudo = $recaudo + $recaudacionesComprobantesRetenciones[$i]->valor;
        }
        $recaudacionesCreditos=Credito::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        for ($i = 0; $i < count($recaudacionesCreditos); $i++) {
            $recaudo = $recaudo + $recaudacionesCreditos[$i]->valor+$recaudacionesCreditos[$i]->abono;
        }
        if ($recaudo>$cabeceraFactura->total) {
            $recaudo=$cabeceraFactura->total;
            return ['resultado'=>$recaudo];
        } else {
            return ['resultado'=>$recaudo];
        }
    }

}
