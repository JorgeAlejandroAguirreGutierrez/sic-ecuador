<?php

namespace App\Http\Controllers\cobros;

use Illuminate\Http\Request;
use App\cobros\Credito;
use App\cobros\ItemCredito;
use App\cobros\ItemAmortizacionCredito;
use App\facturas\CabeceraFactura;
use App\estructuras\Configuracion;
use App\clientes\DatoAdicional;
use App\cobros\Cheque;
use App\cobros\DepositoTransferencia;
use App\cobros\Efectivo;
use App\cobros\TarjetaCredito;
use App\cobros\TarjetaDebito;
use App\cobros\ComprobanteRetencion;

class ControladorRecaudacionesCreditos extends Controller {

    public function vistaCrear(Request $request) {
        $cabeceraFactura = CabeceraFactura::find($request->id);
        $recaudo = $this->obtenerRecaudos($cabeceraFactura);
        $creditos = Credito::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        return view('cobros/recaudacionesCreditos', ['cabeceraFactura' => $cabeceraFactura, 'creditos' => $creditos, 'recaudo' => $recaudo]);
    }

    public function obtenerCodigoCredito() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'creditos')->where('tipo', 'CREAR')->get();
            $conteoCreditos = Credito::count();
            $conteoCreditos++;
            $rellenoConteo = str_pad($conteoCreditos, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 0, 2);
            return ['resultado'=>$configuracion[0]->codigo . $año . $mes . $rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerCredito(Request $request) {
        try {
            $credito = Credito::find($request->id);
            return ['resultado'=>$credito];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function crearCredito(Request $request) {
        try {
            $cabeceraFactura = CabeceraFactura::find($request->cabecera_factura_id);
            $credito = new Credito($request->all());
            $recaudo = $this->obtenerRecaudos($cabeceraFactura);
            if ($recaudo <= $cabeceraFactura->total) {
                $bandera = $credito->save();
                if ($bandera) {
                    return ['resultado'=>$credito];
                } else {
                    return ['resultado'=>null];
                }
            } else {
                return ['resultado'=>null];
            }
        } catch (Exception $e) {
            return null;
        }
    }

    public function eliminarCredito(Request $request) {
        try {
            $credito = Credito::find($request->id);
            $bandera = $credito->delete();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return false;
        }
    }

    public function crearItemsCredito(Request $request) {
        try {
            $credito=Credito::find($request->credito_id);
            $cabecera_factura = CabeceraFactura::find($credito->cabecera_factura_id);
            $fecha = $cabecera_factura->fecha;
            $saldo = $credito->saldo;
            $cuotas = $credito->cuotas;
            $porcentaje_interes = $credito->porcentaje_interes;
            $credito_id = $request->credito_id;
            $interes = $porcentaje_interes / 100;
            $valor = round(((($interes * pow((1 + $interes), $cuotas)) * $saldo) / (((pow((1 + $interes), $cuotas)) - 1))), 2);
            $vencimiento = date("Y-m-d", strtotime($fecha . "+ 1 month"));
            $saldo_inicial = $saldo;
            $bandera = true;
            for ($i = 1; $i <= $cuotas && $bandera; $i++) {
                $itemCredito = new ItemCredito();
                $secuencia = $i;
                $concepto = 'CUOTA ' . $i . '/' . $cuotas . ' A FACTURA: ' . $cabecera_factura->codigo_interno;
                $pago_interes = round(($porcentaje_interes / 100) * $saldo_inicial, 2);
                $pago_capital = round($valor - $pago_interes, 2);
                $saldo_final = round($saldo_inicial - $pago_capital, 2);
                $itemCredito->secuencia = $secuencia;
                $itemCredito->saldo_inicial = $saldo_inicial;
                $itemCredito->vencimiento = $vencimiento;
                $itemCredito->concepto = $concepto;
                $itemCredito->valor = $valor;
                $itemCredito->pago_capital = $pago_capital;
                $itemCredito->pago_interes = $pago_interes;
                $itemCredito->saldo_final = $saldo_final;
                $itemCredito->credito_id = $credito_id;
                $bandera = $itemCredito->save();
                $vencimiento = date("Y-m-d", strtotime($vencimiento . "+ 1 month"));
                $saldo_inicial = $saldo_final;
            }
            if (!$bandera) {
                $credito->delete();
            }
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    public function eliminarItemCredito(Request $request) {
        try {
            $itemCredito = ItemCredito::find($request->id);
            $bandera = $itemCredito->delete();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    public function generarCredito(Request $request) {
        try {
            $forma_tiempo = DatoAdicional::find($request->forma_tiempo_id);
            if ($forma_tiempo->nombre == 'MES') {
                $itemsCredito = Array();
                $cabecera_factura = CabeceraFactura::find($request->cabecera_factura_id);
                $fecha = $cabecera_factura->fecha;
                $tiempo = $request->tiempo;
                $saldo = $request->saldo;
                $cuotas = $request->cuotas;
                $periodos = $cuotas * $tiempo;
                $porcentaje_interes = $request->porcentaje_interes;
                $interes = $porcentaje_interes / 100;
                $valor = round(((($interes * pow((1 + $interes), $cuotas)) * $saldo) / (((pow((1 + $interes), $cuotas)) - 1))), 2);
                $valor_amortizado = round(((($interes * pow((1 + $interes), $periodos)) * $saldo) / (((pow((1 + $interes), $periodos)) - 1))), 2);
                $vencimiento = date("Y-m-d", strtotime($fecha . "+ " . $tiempo . " days"));
                $total_valor=0;
                for ($i = 1; $i <= $cuotas; $i++) {
                    $itemCredito = new ItemCredito();
                    $secuencia = $i;
                    $concepto = 'CUOTA ' . $i . '/' . $cuotas . ' A FACTURA: ' . $cabecera_factura->codigo_interno;
                    $itemCredito->secuencia = $secuencia;
                    $itemCredito->vencimiento = $vencimiento;
                    $itemCredito->concepto = $concepto;                    
                    $itemCredito->valor = $valor;                   
                    array_push($itemsCredito, $itemCredito);
                    $total_valor += $valor;
                    $vencimiento = date("Y-m-d", strtotime($vencimiento . "+ " . $tiempo . " days"));                    
                }
                $comparacion=$total_valor-$saldo;
                if ($comparacion!=$valor_amortizado) {
                    $restante=$comparacion-$valor;
                    $itemCredito=array_pop($itemsCredito);
                    $itemCredito->valor = $itemCredito->valor-$restante;
                    $total_valor=$total_valor-$restante;
                    array_push($itemsCredito, $itemCredito);
                }
                $respuesta = array('itemsCredito' => $itemsCredito, 'total_valor' => $total_valor);
                return ['resultado'=>$respuesta];
            } else {
                $itemsCredito = Array();
                $cabecera_factura = CabeceraFactura::find($request->cabecera_factura_id);
                $fecha = $cabecera_factura->fecha;
                $tiempo = $request->tiempo;
                $saldo = $request->saldo;
                $cuotas = $request->cuotas;
                $porcentaje_interes = $request->porcentaje_interes;
                $periodos = $cuotas * $tiempo;
                $periodos_anio = 360;               
                $tasa_anual_porcentaje = $periodos_anio * 0.1;                
                $tasa_periodo_porcentaje = ($tasa_anual_porcentaje / $periodos_anio);                
                $interes = $tasa_periodo_porcentaje / 100;
                $valor = round(((($interes * pow((1 + $interes), $periodos)) * $saldo) / (((pow((1 + $interes), $periodos)) - 1))), 2);
                $valor_amortizado = round(((($interes * pow((1 + $interes), $periodos)) * $saldo) / (((pow((1 + $interes), $periodos)) - 1))), 2);
                $vencimiento = date("Y-m-d", strtotime($fecha . "+ " . $tiempo . " days"));                
                $total_valor = 0;
                for ($i = 1; $i <= $cuotas; $i++) {
                    $itemCredito = new ItemCredito();
                    $secuencia = $i;
                    $concepto = 'CUOTA ' . $i . '/' . $cuotas . ' A FACTURA: ' . $cabecera_factura->codigo_interno;
                    $itemCredito->secuencia = $secuencia;
                    $itemCredito->vencimiento = $vencimiento;
                    $itemCredito->concepto = $concepto;
                    $itemCredito->valor = $valor*$tiempo;
                    array_push($itemsCredito, $itemCredito);
                    $total_valor += $valor*$tiempo;
                    $vencimiento = date("Y-m-d", strtotime($vencimiento . "+ " . $tiempo . " days"));
                }
                $comparacion=$total_valor-$saldo;
                if ($comparacion!=$valor_amortizado) {
                    $restante=$comparacion-$valor;
                    $itemCredito=array_pop($itemsCredito);
                    $itemCredito->valor = $itemCredito->valor-$restante;
                    $total_valor=$total_valor-$restante;
                    array_push($itemsCredito, $itemCredito);
                }
                $respuesta = array('itemsCredito' => $itemsCredito, 'total_valor' => $total_valor);
                return ['resultado'=>$respuesta];
            }
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function generarAmortizacionCredito(Request $request) {
        try {
            $forma_tiempo = DatoAdicional::find($request->forma_tiempo_id);
            if ($forma_tiempo->nombre == 'MES') {
                $itemsCredito = Array();
                $cabecera_factura = CabeceraFactura::find($request->cabecera_factura_id);
                $fecha = $cabecera_factura->fecha;
                $tiempo = $request->tiempo;
                $saldo = $request->saldo;
                $cuotas = $request->cuotas;
                $periodos=$cuotas*$tiempo;
                $porcentaje_interes = $request->porcentaje_interes;
                $interes = $porcentaje_interes / 100;
                $valor = round(((($interes * pow((1 + $interes), $cuotas)) * $saldo) / (((pow((1 + $interes), $cuotas)) - 1))), 2);
                $valor_amortizado = round(((($interes * pow((1 + $interes), $periodos)) * $saldo) / (((pow((1 + $interes), $periodos)) - 1))), 2);
                $saldo_inicial = $saldo;
                $total_pago_capital = 0;
                $total_pago_interes = 0;
                $total_valor = 0;
                for ($i = 1; $i <= $cuotas; $i++) {
                    $itemCredito = new ItemAmortizacionCredito();
                    $secuencia = $i;
                    $pago_interes = round(($porcentaje_interes / 100) * $saldo_inicial, 2);
                    $pago_capital = round($valor - $pago_interes, 2);
                    $saldo_final = round($saldo_inicial - $pago_capital, 2);
                    $itemCredito->secuencia = $secuencia;
                    $itemCredito->saldo_inicial = $saldo_inicial;
                    $itemCredito->valor = $valor;
                    $itemCredito->pago_interes = $pago_interes;
                    $itemCredito->pago_capital = $pago_capital;
                    $itemCredito->saldo_final = $saldo_final;
                    array_push($itemsCredito, $itemCredito);
                    $total_pago_capital += $pago_capital;
                    $total_pago_interes += $pago_interes;
                    $total_valor += $valor;
                    $saldo_inicial = $saldo_final;
                }
                $comparacion=$total_valor-$saldo;
                if ($comparacion!=$valor_amortizado) {
                    $restante=$comparacion-$valor;
                    $itemCredito=array_pop($itemsCredito);
                    $itemCredito->valor = $itemCredito->valor-$restante;
                    $itemCredito->pago_capital = $itemCredito->pago_capital-$restante;
                    $itemCredito->saldo_final = 0;
                    $total_pago_capital=$total_pago_capital-$restante;
                    $total_valor=$total_valor-$restante;
                    array_push($itemsCredito, $itemCredito);
                }
                $respuesta = array('itemsCredito' => $itemsCredito, 'total_pago_capital' => $total_pago_capital,
                    'total_pago_interes' => $total_pago_interes, 'total_valor' => $total_valor);
                return ['resultado'=>$respuesta];
            } else {
                $itemsCredito = Array();
                $cabecera_factura = CabeceraFactura::find($request->cabecera_factura_id);
                $fecha = $cabecera_factura->fecha;
                $tiempo = $request->tiempo;
                $saldo = $request->saldo;
                $cuotas = $request->cuotas;
                $porcentaje_interes = $request->porcentaje_interes;
                $periodos = $cuotas * $tiempo;
                $periodos_anio = 360;
                $tasa_anual_porcentaje = $periodos_anio * 0.1;
                $tasa_periodo_porcentaje = ($tasa_anual_porcentaje / $periodos_anio);
                $interes = $tasa_periodo_porcentaje / 100;
                $valor = round(((($interes * pow((1 + $interes), $periodos)) * $saldo) / (((pow((1 + $interes), $periodos)) - 1))), 2);
                                  //0.001               0.001       15           800                  0.001  
                $valor_amortizado = round(((($interes * pow((1 + $interes), $periodos)) * $saldo) / (((pow((1 + $interes), $periodos)) - 1))), 2);
                $saldo_inicial = $saldo;
                $total_pago_capital = 0;
                $total_pago_interes = 0;
                $total_valor = 0;
                for ($i = 1; $i <= $periodos; $i++) {
                    $itemCredito = new ItemAmortizacionCredito();
                    $secuencia = $i;
                    $pago_interes = round(($tasa_periodo_porcentaje / 100) * $saldo_inicial, 2);
                    $pago_capital = round($valor - $pago_interes, 2);
                    $saldo_final = round($saldo_inicial - $pago_capital, 2);
                    $itemCredito->secuencia = $secuencia;
                    $itemCredito->saldo_inicial = $saldo_inicial;
                    $itemCredito->valor = $valor;
                    $itemCredito->pago_interes = $pago_interes;
                    $itemCredito->pago_capital = $pago_capital;
                    $itemCredito->saldo_final = $saldo_final;
                    array_push($itemsCredito, $itemCredito);
                    $total_pago_capital += $pago_capital;
                    $total_pago_interes += $pago_interes;
                    $total_valor += $valor;
                    $saldo_inicial = $saldo_final;
                }
                $comparacion=$total_valor-$saldo;
                if ($comparacion!=$valor_amortizado) {
                    $restante=$comparacion-$valor;
                    $itemCredito=array_pop($itemsCredito);
                    $itemCredito->valor = $itemCredito->valor-$restante;
                    $itemCredito->pago_capital = $itemCredito->pago_capital-$restante;                    
                    $itemCredito->saldo_final = 0;
                    $total_pago_capital=$total_pago_capital-$restante;
                    $total_valor=$total_valor-$restante;
                    array_push($itemsCredito, $itemCredito);
                }
                $respuesta = array('itemsCredito' => $itemsCredito, 'total_pago_capital' => $total_pago_capital,
                    'total_pago_interes' => $total_pago_interes, 'total_valor' => $total_valor);
                return ['resultado'=>$respuesta];
            }
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function actualizarValorCredito(Request $request) {
        try {
            $credito = Credito::find($request->credito_id);
            $itemsCredito = ItemCredito::where('credito_id', $credito->id)->get();
            $valor = 0;
            for ($i = 0; $i < count($itemsCredito); $i++) {
                $valor = $itemsCredito[$i]->valor + $valor;
            }
            $credito->valor = $valor;
            $bandera = $credito->save();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }

    public function obtenerTiempo(Request $request) {
        try {
            $forma_tiempo = DatoAdicional::find($request->forma_tiempo_id);
            if ($forma_tiempo->nombre == 'DIA') {
                return ['resultado'=>0];
            } else {
                return ['resultado'=>30];
            }
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    private function obtenerRecaudos($cabeceraFactura) {
        $tipo = DatoAdicional::where('nombre', 'CHEQUE A LA VISTA')->get();
        $chequesVista = Cheque::where('cabecera_factura_id', $cabeceraFactura->id)->where('tipo_id', $tipo[0]->id)->get();
        $tipo = DatoAdicional::where('nombre', 'CHEQUE POSFECHADO')->get();
        $chequesPosfechado = Cheque::where('cabecera_factura_id', $cabeceraFactura->id)->where('tipo_id', $tipo[0]->id)->get();
        $tipo = DatoAdicional::where('nombre', 'DEPOSITO')->get();
        $depositos = DepositoTransferencia::where('cabecera_factura_id', $cabeceraFactura->id)->where('tipo_id', $tipo[0]->id)->get();
        $tipo = DatoAdicional::where('nombre', 'TRANSFERENCIA')->get();
        $transferencias = DepositoTransferencia::where('cabecera_factura_id', $cabeceraFactura->id)->where('tipo_id', $tipo[0]->id)->get();
        $efectivos = Efectivo::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        $recaudo = 0;
        for ($i = 0; $i < count($chequesVista); $i++) {
            $recaudo = $recaudo + $chequesVista[$i]->valor;
        }
        for ($i = 0; $i < count($chequesPosfechado); $i++) {
            $recaudo = $recaudo + $chequesPosfechado[$i]->valor;
        }
        for ($i = 0; $i < count($depositos); $i++) {
            $recaudo = $recaudo + $depositos[$i]->valor;
        }
        for ($i = 0; $i < count($transferencias); $i++) {
            $recaudo = $recaudo + $transferencias[$i]->valor;
        }
        for ($i = 0; $i < count($efectivos); $i++) {
            $recaudo = $recaudo + $efectivos[$i]->valor;
        }
        $recaudacionesTarjetasCreditos = TarjetaCredito::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        for ($i = 0; $i < count($recaudacionesTarjetasCreditos); $i++) {
            $recaudo = $recaudo + $recaudacionesTarjetasCreditos[$i]->valor;
        }
        $recaudacionesTarjetasDebitos = TarjetaDebito::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        for ($i = 0; $i < count($recaudacionesTarjetasDebitos); $i++) {
            $recaudo = $recaudo + $recaudacionesTarjetasDebitos[$i]->valor;
        }
        $recaudacionesComprobantesRetenciones = ComprobanteRetencion::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        for ($i = 0; $i < count($recaudacionesComprobantesRetenciones); $i++) {
            $recaudo = $recaudo + $recaudacionesComprobantesRetenciones[$i]->valor;
        }
        $recaudacionesCreditos = Credito::where('cabecera_factura_id', $cabeceraFactura->id)->get();
        for ($i = 0; $i < count($recaudacionesCreditos); $i++) {
            $recaudo = $recaudo + $recaudacionesCreditos[$i]->valor + $recaudacionesCreditos[$i]->abono;
        }
        if ($recaudo > $cabeceraFactura->total) {
            $recaudo = $cabeceraFactura->total;
            return ['resultado'=>$recaudo];
        } else {
            return ['resultado'=>$recaudo];
        }
    }

}
