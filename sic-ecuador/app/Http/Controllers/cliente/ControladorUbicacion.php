<?php

namespace App\Http\Controllers\clientes;

use Illuminate\Http\Request;
use App\cliente\Ubicacion;
use App\configuracion\Configuracion;
use Validator;

/**
 * Controlador para administrar la ubicación geográfica de un cliente en el sistema
 * 
 * Contiene un conjuto de métodos que permiten administrar la ubicación geográfica de los
 * clientes en el sistema. Ej. Provincia, Cantón, Parroquia, etc.
 * 
 * @copyright (c) 2019, SICE WEB
 */
class ControladorUbicacion extends Controller {

    private static $headers = [
        'Content-Type' => 'application/json',
        'Access-Control-Allow-Origin' => '*', 'Access-Control-Allow-Headers' => 'Origin, Content-Type, Accept',
        'Access-Control-Allow-Methods' => 'GET,HEAD,POST,PUT,DELETE,OPTIONS'
    ];

    /**
     * Nivel funcional: Proceso para crear nuevas ubicaciones geográficas en el sistema
     * 
     * Nivel técnico: Crea una instancia del modelo "Ubicacion", relaciona todos los campos 
     * de la tabla "ubicaciones" con los datos de la nueva ubicacion geográfica recibida por
     * parámetro y guarda en la BD a traves del modelo "Ubicacion".
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el nuevo transportista con los campos de la tabla "ubicaciones"
     * @return Boolean Devuelve True si se creó la nueva ubicación geográfica en la tabla "ubicaciones"
     * @since versión 1.0
     * @version 1.0
     */
    public function crear(Request $request) {
        try {
            $validacion = Validator::make($request->all(), [
                'provincia' => 'required|min:1',
                'canton' => 'required|min:1',
                'parroquia'=> 'required|min:1',
            ]);
            if ($validacion->fails()) {
                return response()->json(['error' => 'Bad Request', 'bandera' => false, 'mensaje' => 'Error en validacion de parametros'], 400)
                ->withHeaders(self::$headers);
            }
            $ubicacion = new Ubicacion($request->all());
            $ubicacion->codigo = $this->obtenerCodigo();
            $bandera = $ubicacion->save();
            return response()->json(['resultado' => $ubicacion, 'bandera' => $bandera, 'mensaje' => 'Se ha creado la ubicacion'], 201)
                ->withHeaders(self::$headers);
        } catch (Exception | QueryException $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Obtiene el código secuencial para crear una nueva ubicación geográfica en el sistema
     * 
     * Nivel técnico: Obtiene el código secuencial para crear ua nueva ubicación geográfica, contando el total
     * de registros de la tabla "ubicaciones", incrementado en 1 y concatenando con la letra "UG" 
     * obtenida del Modelo Configuración.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_encode Devuelve el código interno para la nueva ubicación geográfica.
     * @since versión 1.0
     * @version 1.0
     */
    public function obtenerCodigo() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'ubicaciones')->where('tipo', 'CREAR')->get();
            $conteoubicaciones = Ubicacion::count();
            $conteoubicaciones++;
            $rellenoConteo = str_pad($conteoubicaciones, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 0, 2);
            return $configuracion[0]->codigo . $año . $mes . $rellenoConteo;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Nivel funcional: Obtiene todas las ubicaciones geográficas creadas en el sistema
     * 
     * Nivel técnico: Obtiene todos los registros de la tabla "ubicaciones", a través
     * del Modelo "Ubicacion".
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_enconde Devuelve en la variable $ubicaciones todos los registros de 
     * la tabla "ubicaciones", sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function consultar() {
        try {
            $ubicaciones = Ubicacion::all();
            return response()->json(['resultado' => $ubicaciones, 'bandera' =>true, 'mensaje' => 'Se ha consultado las ubicaciones'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception | QueryException $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Obtiene una ubicación geográfica específica según el id que recibe
     * 
     * Nivel técnico: Obtiene a través del Modelo "Ubicacion" el registro de una ubicación geográfica
     * específica según el id recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) de la ubicación geográfica a buscar en la tabla
     * @return json_enconde Devuelve en la variable $ubicacion los datos de una ubicación geográfica específica, 
     * obtenida de la tabla "ubicaciones" según el id recibido por parámetro, sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function obtener(Request $request) {
        try {
            $ubicacion = Ubicacion::findOrFail($request->id);
            return response()->json(['resultado' => $ubicacion, 'bandera' => true, 'mensaje' => 'Se ha obtenido la ubicacion'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro la ubicacion'], 404)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Proceso para actualizar una ubicación geográfica registrada en el sistema
     * 
     * Nivel técnico: Actualizacion en la tabla "ubicaciones" el registro que corresponda
     * al número interno (id) recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) de la ubicación geográfica a modificar
     * @return Boolean Devuelve True si se modificó la ubicación geográfica en la tabla "ubicaciones", sino false.
     * @since versión 1.0
     * @version 1.0
     */
    public function actualizar(Request $request) {
        try {
            $ubicacion = Ubicacion::findOrFail($request->id);
            $bandera=$ubicacion->update($request->all());
            return response()->json(['resultado' => $ubicacion, 'bandera' => $bandera, 'mensaje' => 'Se ha actualizado la ubicacion'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro la ubicacion'], 404)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Proceso para eliminar una ubicación geográfica registrada en el sistema
     * 
     * Nivel técnico: Borra de la tabla "ubicaciones" el registro que corresponda
     * al número interno (id) recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) de la ubicación geográfica a eliminar de la tabla.
     * @return Boolean Devuelve True si se eliminó el registro en la tabla "ubicaciones", sino false.
     * @since versión 1.0
     * @version 1.0
     */
    public function eliminar(Request $request) {
        try {
            $ubicacion = Ubicacion::findOrFail($request->id);
            $bandera = $ubicacion->delete();
            return response()->json(['resultado' => $ubicacion, 'bandera' => $bandera, 'mensaje' => 'Se ha elimnado la ubicacion'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro la ubicacion'], 404)
                ->withHeaders(self::$headers);
        }
    }

    public function obtenerProvincias() {
        try {
            $provincias = Ubicacion::distinct()->get(['provincia']);
            return response()->json(['resultado' => $provincias, 'bandera' => true, 'mensaje' => 'Se ha obtenido las provincias'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }

    public function obtenerCantones(Request $request) {
        try {
            $cantones = Ubicacion::distinct()->where('provincia', $request->provincia)->get(['canton']);
            return response()->json(['resultado' => $cantones, 'bandera' => true, 'mensaje' => 'Se ha obtenido los cantones'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }

    public function obtenerParroquias(Request $request) {
        try {
            $parroquias = Ubicacion::where('canton', $request->canton)->get();
            return response()->json(['resultado' => $parroquias, 'bandera' => true, 'mensaje' => 'Se ha obntenido las parroquias'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }

}
