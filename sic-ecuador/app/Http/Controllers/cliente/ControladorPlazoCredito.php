<?php

namespace App\Http\Controllers\clientes;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Database\QueryException;
use App\cliente\PlazoCredito;
use App\estructura\Configuracion;
use Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;  

/**
 * Controlador para administrar la cofiguración de los tiempos de plazo de crédito
 * 
 * Contiene un conjuto de métodos que permiten administrar la configuración de los tiempos
 * de plazo de crédito. Ej. 30, 45, 60, etc.
 * 
 * @copyright (c) 2019, SICE WEB
 */
class ControladorPlazoCredito extends Controller
{

    private static $headers = [
        'Content-Type' => 'application/json',
        'Access-Control-Allow-Origin' => '*', 'Access-Control-Allow-Headers' => 'Origin, Content-Type, Accept',
        'Access-Control-Allow-Methods' => 'GET,HEAD,POST,PUT,DELETE,OPTIONS'
    ];

    /**
     * Nivel funcional: Proceso para crear nuevos Plazo de crédito en la BD
     * 
     * Nivel técnico: Crea una instancia del modelo PlazoCredito, relaciona todos los campos 
     * de la tabla "plazos_creditos" con los datos del nuevo plazo de crédito recibido por parámetro 
     * y guarda en la BD a traves del modelo PlazoCredito.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el nuevo plazo según los campos de la tabla "plazos_creditos"
     * @return Boolean Envía un mensaje True si se guardó el nuevo plazo en la tabla "plazos_creditos"
     * @since versión 1.0
     * @version 1.0
     */
    public function crear(Request $request)
    {
        try {
            $validacion = Validator::make($request->all(), [
                'plazo' => 'required|integer'
            ]);
            if ($validacion->fails()) {
                return response()->json(['error' => 'Bad Request', 'bandera' => false, 'mensaje' => 'Error en validacion de parametros'], 400)
                ->withHeaders(self::$headers);
            }
            $plazoCredito = new PlazoCredito($request->all());
            $plazoCredito->codigo = $this->obtenerCodigo();
            $bandera = $plazoCredito->save();
            return response()->json(['resultado' => $plazoCredito, 'bandera' => $bandera, 'mensaje' => 'Se ha creado el plazo de credito'], 201)
                ->withHeaders(self::$headers);
        } catch (Exception | QueryException $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => $bandera, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Obtiene el código secuencial para crear un nuevo plazo de crédito
     * 
     * Nivel técnico: Obtiene el código secuencial para crear un nuevo plazo de crédito, 
     * contando el total de registros de la tabla "plazos_creditos", incrementado en 1 
     * y concatenando con la letra "PC" obtenida del Modelo Configuración.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_encode Devuelve el código del nuevo plazo de crédito
     * @since versión 1.0
     * @version 1.0
     */
    public function obtenerCodigo(){
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'plazos_creditos')->where('tipo', 'CREAR')->get();
            $conteoPlazosCreditos = PlazoCredito::count();
            $conteoPlazosCreditos++;
            $rellenoConteo = str_pad($conteoPlazosCreditos, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 0, 2);
            return $configuracion[0]->codigo . $año . $mes . $rellenoConteo;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Nivel funcional: Obtiene todos los plazos de crédito creados
     * 
     * Nivel técnico: Obtiene todos los registros de la tabla "plazos_creditos", a través
     * del Modelo PlazoCredito.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_enconde Devuelve en la variable $plazosCreditos todos los plazos de crédito,
     * sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function consultar()
    {
        try {
            $plazosCreditos = PlazoCredito::all();
            return response()->json(['resultado' => $plazosCreditos, 'bandera' => true, 'mensaje' => 'Se ha obtenido el plazo de credito'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Obtiene un plazo de crédito específico según el id que recibe
     * 
     * Nivel técnico: Obtiene a través del Modelo PlazoCredito el registro de un plazo de
     * crédito según el id recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del plazo de crédito a buscar en la tabla
     * @return json_enconde Devuelve en la variable $plazoCredito el plazo de crédito específico 
     * obtenido de la tabla "plazos_creditos" según el id recibido por parámetro, 
     * sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function obtener(Request $request)
    {
        try {
            $plazoCredito = PlazoCredito::findOrFail($request->id);
            return response()->json(['resultado' => $plazoCredito, 'bandera' => true, 'mensaje' => 'Se ha obtenido el plazo de credito'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el plazo de credito'], 404)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Proceso para modificar un plazo de crédito en el sistema
     * 
     * Nivel técnico: Actualizacion en la tabla "plazos_creditos" el registro que corresponda
     * al número interno (id) enviado por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del plazo de crédito a buscar en la tabla
     * @return Boolean Envía un mensaje True si se modificó el plazo de crédito en la tabla "plazos_creditos"
     * @since versión 1.0
     * @version 1.0
     */
    public function actualizar(Request $request)
    {
        try {
            $plazoCredito = PlazoCredito::findOrFail($request->id);
            $bandera = $plazoCredito->update($request->all());
            return response()->json(['resultado' => $plazoCredito, 'bandera' => $bandera, 'mensaje' => 'Se ha actualizado el plazo de credito'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(), 'bandera' => false, 'mensaje' => 'No se encontro el plazo de credito'], 404)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Proceso para eliminar un plazo de crédito en el sistema
     * 
     * Nivel técnico: Borra en la tabla "plazos_creditos" el registro que corresponda
     * al número interno (id) enviado por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del plazo de crédito a eliminar de la tabla
     * @return Boolean Envía un mensaje True si se eliminó el registro de plazo de crédito 
     * en la tabla "plazos_creditos"
     * @since versión 1.0
     * @version 1.0
     */
    public function eliminar(Request $request)
    {
        try {
            
            $plazoCredito = PlazoCredito::findOrFail($request->id);
            $bandera = $plazoCredito->delete();
            return response()->json(['resultado' => $plazoCredito, 'bandera' => $bandera, 'mensaje' => 'Se ha eliminado el plazo de credito'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(), 'bandera' => false, 'mensaje' => 'No se encontro el plazo de credito'], 404)
                ->withHeaders(self::$headers);
        }
    }
}
