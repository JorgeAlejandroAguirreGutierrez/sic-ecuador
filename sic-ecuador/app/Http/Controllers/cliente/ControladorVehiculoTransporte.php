<?php

namespace App\Http\Controllers\clientes;

use Illuminate\Http\Request;
use App\cliente\VehiculoTransporte;
use App\estructura\Configuracion;

/**
 * Controlador para administrar los vehículos para transporte en el sistema
 * 
 * Contiene un conjuto de métodos que permiten administrar los vehículos para transporte
 * en el sistema. Ej. Modelo, Placa, Marca, etc.
 * 
 * @copyright (c) 2019, SICE WEB
 */
class ControladorVehiculoTransporte extends Controller
{
    
    /**
     * Nivel funcional: Proceso para crear nuevos vehículos para transporte en el sistema
     * 
     * Nivel técnico: Crea una instancia del modelo "VehiculoTransporte", relaciona todos los campos 
     * de la tabla "vehiculos_transportes" con los datos del nuevo vehículo recibido por
     * parámetro y guarda en la BD a traves del modelo "VehiculoTransporte".
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el nuevo vehículo con los campos de la tabla "vehiculos_transportes"
     * @return Boolean Devuelve True si se creó el nuevo vehículo en la tabla "vehiculos_transportes"
     * @since versión 1.0
     * @version 1.0
     */ 
    public function crear(Request $request)
    {
        try {
            $validacion = Validator::make($request->all(), [
                'modelo' => 'required|integer',
                'placa' => 'required|min:1',
                'marca'=> 'required|min:1',
                'cilindraje'=> 'required|integer',
                'clase'=> 'required|min:1',
                'color'=> 'required|min:1',
                'fabricacion'=> 'required|integer',
            ]);
            if ($validacion->fails()) {
                return response()->json(['error' => 'Bad Request', 'bandera' => false, 'mensaje' => 'Error en validacion de parametros'], 400)
                ->withHeaders(self::$headers);
            }
            $vehiculoTransporte=new VehiculoTransporte($request->all());
            $bandera=$vehiculoTransporte->save();
            return response()->json(['resultado' => $vehiculoTransporte, 'bandera' => $bandera, 'mensaje' => 'Se ha creado el vehiculo de transporte'], 201)
                    ->withHeaders(self::$headers);
        } catch (Exception | QueryException $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Obtiene el código secuencial para crear un nuevo vehículo en el sistema
     * 
     * Nivel técnico: Obtiene el código secuencial para crear un nuevo vehículo, contando el total
     * de registros de la tabla "vehiculos_transportes", incrementado en 1 y concatenando con la letra "VH" 
     * obtenida del Modelo Configuración.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_encode Devuelve el código interno del nuevo vehículo para transporte.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function obtenerCodigo()
    {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla','vehiculos_transportes')->where('tipo', 'CREAR')->get();
            $conteoVehiculosTransportes= VehiculoTransporte::count();
            $conteoVehiculosTransportes++;
            $rellenoConteo= str_pad($conteoVehiculosTransportes,6, '0',STR_PAD_LEFT);
            $fecha=date("m.d.y");
            $año= substr($fecha, 6,2);
            $mes= substr($fecha, 0,2);
            return ['resultado'=>$configuracion[0]->codigo.$año.$mes.$rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Obtiene todos los vehículos para transporte creados en el sistema
     * 
     * Nivel técnico: Obtiene los registros de la tabla "vehiculos_transportes", a través
     * del Modelo "VehiculoTransporte".
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_enconde Devuelve en la variable $vehiculosTransportes todos los registros de 
     * la tabla "vehiculos_transportes", sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function obtener(Request $request) {
        try {
            $vehiculoTransporte = VehiculoTransporte::findOrFail($request->id);
            return response()->json(['resultado' => $vehiculoTransporte, 'bandera' => true, 'mensaje' => 'Se ha obtenido el vehiculo de transporte'], 200)
                    ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el transportista'], 404)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Obtiene un vehículo para transporte específico según el id que recibe
     * 
     * Nivel técnico: Obtiene a través del Modelo "vehiculoTransporte" el registro de un vehículo
     * específico según el id recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del vehículo a buscar en la tabla
     * @return json_enconde Devuelve en la variable $vehiculoTransporte los datos de un vehículo específico, 
     * obtenido de la tabla "vehiculos_transportes" según el id recibido por parámetro, sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function consultar() {
        try {
            $vehiculosTransportes = VehiculoTransporte::all();
            return response()->json(['resultado' => $vehiculosTransportes, 'bandera' =>true, 'mensaje' => 'Se ha consultado los vehiculos de transporte'], 201)
                ->withHeaders(self::$headers);
        } catch (Exception | QueryException $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Proceso para actualizar un vehículo para transporte registrado en el sistema
     * 
     * Nivel técnico: Actualizacion en la tabla "vehiculos_transportes" el registro que corresponda
     * al número interno (id) recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del vehículo a modificar
     * @return Boolean Devuelve True si se modificó el vehículo en la tabla "vehiculos_transportes", sino false.
     * @since versión 1.0
     * @version 1.0
     */
    public function actualizar(Request $request) {
        try {
            $vehiculoTransporte = VehiculoTransporte::findOrFail($request->id);
            $vehiculoTransporte->codigo = $request->codigo;
            $vehiculoTransporte->modelo = $request->modelo;
            $vehiculoTransporte->placa = $request->placa;
            $vehiculoTransporte->marca = $request->marca;
            $vehiculoTransporte->cilindraje = $request->cilindraje;
            $vehiculoTransporte->clase = $request->clase;
            $vehiculoTransporte->color = $request->color;
            $vehiculoTransporte->fabricacion = $request->fabricacion;
            $vehiculoTransporte->activo = $request->activo;
            $bandera = $vehiculoTransporte->save();
            return response()->json(['resultado' => $vehiculoTransporte, 'bandera' => $bandera, 'mensaje' => 'Se ha actualizado el transportista'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el vehiculo de transporte'], 404)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Proceso para eliminar un vehículo para transporte registrado en el sistema
     * 
     * Nivel técnico: Borra de la tabla "vehiculos_transportes" el registro que corresponda
     * al número interno (id) recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del vehículo a eliminar de la tabla.
     * @return Boolean Devuelve True si se eliminó el registro en la tabla "vehiculos_transportes", sino false.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function eliminar(Request $request) {
        try {
            $vehiculoTransporte = VehiculoTransporte::find($request->id);
            $bandera = $vehiculoTransporte->delete();
            return response()->json(['resultado' => $vehiculoTransporte, 'bandera' => $bandera, 'mensaje' => 'Se ha elimnado el vehiculo de transporte'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el vehiculo de transporte'], 404)
                ->withHeaders(self::$headers);
        }
    }
}
