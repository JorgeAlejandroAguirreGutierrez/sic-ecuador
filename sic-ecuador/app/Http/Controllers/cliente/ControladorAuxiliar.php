<?php

namespace App\Http\Controllers\cliente;

use Illuminate\Http\Request;
use App\clientes\Auxiliar;
use App\cliente\Cliente;

/**
 * Controlador para administrar los clientes auxiliares
 * 
 * Contiene un conjuto de métodos que permiten manejar los clientes auxiliares.
 * Los clientes auxiliares son aquellos que requieren factura con el nombre o ruc 
 * del cliente principal pero con otros datos como dirección y/o telefono diferente, 
 * debido a que tienen diferentes sucursales y cada una devenga de su presupuesto el pago.
 * 
 * @copyright (c) 2019, SICE WEB
 */

class ControladorAuxiliares extends Controller {
    /**
     * Nivel funcional: Proceso para crear nuevos clientes auxiliares en el sistema
     * 
     * Nivel técnico: Crea una instancia del modelo "Auxiliar", relaciona todos los campos 
     * de la tabla "Auxiliares" con los datos del nuevo cliente auxiliar recibido por parámetro 
     * y guarda en la BD a traves del modelo "Auxiliar".
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el nuevo cliente auxiliar según los campos de la tabla "Auxiliares"
     * @return Boolean devuelve True si se creó el nuevo cliente auxiliar en la tabla "Auxiliares"
     * @since versión 1.0
     * @version 1.0
     */
    public function crear(Request $request)
    {
        try {
            $auxiliar=new Auxiliar($request->all());
            $bandera=$auxiliar->save();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Proceso para obtener la ubicación de un cliente auxiliar registrado en el sistema
     * 
     * Nivel técnico: Obtiene a traves de la función ubicaciongeo() del modelo Cliente la ubicación 
     * geográfica de un clientes auxiliares especifico, según el id recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del cliente auxiliar
     * @return True devuelve en la variable $auxiliar la ubicación del cliente auxiliar específico,
     * obtenidos de la tabla "ubicacionesgeo", según el id recibido por parámetro.
     * @since versión 1.0
     * @version 1.0
     */  
    public function obtenerAuxiliar(Request $request)
    {
        try {
            $auxiliar = Auxiliar::find($request->id);
            $auxiliar->ubicaciongeo;
            return ['resultado'=> $auxiliar];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Obtiene los clientes auxiliares de un cliente específico
     * 
     * Nivel técnico: Obtiene los clientes auxiliares de la tabla "auxiliares" relacionados al
     * cliente principal.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el numero interno (cliente_id) del cliente principal.
     * @return True Devuelve los clientes auxiliares relacionados al cliente principal, obtenidos de la tabla "auxiliares".
     * @since versión 1.0
     * @version 1.0
     */     
    public function obtenerAuxiliaresCliente(Request $request){
        try {
            $auxiliares=Auxiliar::where('cliente_id', $request->cliente_id)->get();
            return ['resultado'=> $auxiliares];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Proceso para eliminar un cliente auxiliar registrado en el sistema
     * 
     * Nivel técnico: Borra de la tabla "auxiliares" todos los clientes auxiliares relacionados
     * al cliente principal a traves del modelo "Auxiliar".
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe los datos del cliente principal para buscar en tabla "auxiliares"
     * @return Boolean Devuelve True si se eliminó los registros de clientes auxiliares en la tabla "auxiliares"
     * @since versión 1.0
     * @version 1.0
     */  
    public function eliminarAuxiliar(Request $request) {
        try {
            $auxiliar = Auxiliar::find($request->id);
            $bandera = $auxiliar->delete();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    

}
