<?php

namespace App\Http\Controllers\clientes;

use Illuminate\Http\Request;
use App\cliente\Impuesto;
use App\estructura\Configuracion;

/**
 * Controlador para administrar la configuración de impuestos
 * 
 * Contiene un conjuto de métodos que permiten administrar la configuración de impuestos
 * Los Impuestos son el porcentaje de incremento calculado sobre la base de un valos,
 * a la cual se denomina base imponible. Ej. IVA, ICE, etc.
 * 
 * @copyright (c) 2019, SICE WEB
 */
class ControladorImpuesto extends Controller {
    /**
     * Nivel funcional: Obtiene el código secuencial para crear un nuevo impuesto
     * 
     * Nivel técnico: Obtiene el código secuencial para crear un nuevo impuesto, 
     * contando el total de registros de la tabla "Impuestos", incrementado en 1 
     * y concatenando con la letra "IMP" obtenida del Modelo Configuración.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_encode Devuelve el código del nuevo impuesto
     * @since versión 1.0
     * @version 1.0
     */
    public function obtenerCodigo() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'impuestos')->where('tipo', 'CREAR')->get();
            $conteoImpuestos = Impuesto::count();
            $conteoImpuestos++;
            $rellenoConteo = str_pad($conteoImpuestos, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 0, 2);
            return ['resultado'=>$configuracion[0]->codigo . $año . $mes . $rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    /**
     * Nivel funcional: Proceso para crear nuevos impuestos en el sistema
     * 
     * Nivel técnico: Crea una instancia del modelo Impuesto, relaciona todos los campos 
     * de la tabla "impuestos" con los datos del nuevo impuesto recibido por parámetro 
     * y guarda en la BD a traves del modelo Impuesto.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el nuevo impuesto según los campos de la tabla "impuestos"
     * @return Boolean Envía un mensaje True si se guardó el nuevo impuesto en la tabla "impuestos"
     * @since versión 1.0
     * @version 1.0
     */
    public function crear(Request $request) {
        try {
            $validacion = Validator::make($request->all(), [
                'codigo_norma' => 'required|min:1',
                'porcentaje' => 'required|double'
            ]);
            if ($validacion->fails()) {
                return response()->json(['error' => 'Bad Request', 'bandera' => false, 'mensaje' => 'Error en validacion de parametros'], 400)
                ->withHeaders(self::$headers);
            }
            $impuesto = new Impuesto($request->all());
            $impuesto->porcentaje = round($impuesto->porcentaje, 2);
            $bandera = $impuesto->save();
            return response()->json(['resultado' => $impuesto, 'bandera' => $bandera, 'mensaje' => 'Se ha creado el impuesto'], 201)
                ->withHeaders(self::$headers);
        } catch (Exception | QueryException $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Obtiene todos los impuestos creados
     * 
     * Nivel técnico: Obtiene todos los registros del Modelo Impuesto.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_enconde Devuelve en la variable $impuestos todos los impuestos,
     * sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function consultar() {
        try {
            $impuestos = Impuesto::all();
            return response()->json(['resultado' => $impuestos, 'bandera' => true, 'mensaje' => 'Se ha obtenido los impuestos'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Obtiene un impuesto específico según el id que recibe
     * 
     * Nivel técnico: Obtiene a través del Modelo Impuesto el registro de un impuesto
     * según el id recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno del impuesto a buscar en la tabla
     * @return json_enconde Devuelve en la variable $impuesto el impuesto específico 
     * obtenido de la tabla "impuestos" según el id recibido por parámetro, 
     * sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function obtener(Request $request) {
        try {
            $impuesto = Impuesto::findOrFail($request->id);
            return response()->json(['resultado' => $impuesto, 'bandera' => true, 'mensaje' => 'Se ha obtenido el impuesto'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el impuesto'], 404)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Proceso para actualizar un tipo de impuesto en la DB
     * 
     * Nivel técnico: Actualizacion en la tabla "impuestos" el registro que corresponda
     * al número interno (id) enviado por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno del impuesto a buscar en la tabla
     * @return Boolean Envía un mensaje True si se modificó el impuesto en la tabla "impuestos"
     * @since versión 1.0
     * @version 1.0
     */
    public function actualizar(Request $request) {
        try {
            $impuesto = Impuesto::findOrFail($request->id);
            $bandera=$impuesto->update($request->all());
            return response()->json(['resultado' => $impuesto, 'bandera' => $bandera, 'mensaje' => 'Se ha actualizado el impuesto'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el impuesto'], 404)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Proceso para eliminar un tipo de impuesto en la DB
     * 
     * Nivel técnico: Borra en la tabla "impuestos" el registro que corresponda
     * al número interno (id) enviado por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno del impuesto a eliminar de la tabla
     * @return Boolean Envía un mensaje True si se eliminó el impuesto en la tabla "impuestos"
     * @since versión 1.0
     * @version 1.0
     */
    public function eliminar(Request $request) {
        try {
            $impuesto = Impuesto::find($request->id);
            $bandera = $impuesto->delete();
            return response()->json(['resultado' => $impuesto, 'bandera' => $bandera, 'mensaje' => 'Se ha eliminado la retencion'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(), 'bandera' => false, 'mensaje' => 'No se encontro la retencion'], 404)
                ->withHeaders(self::$headers);
        }
    }

}
