<?php

namespace App\Http\Controllers\cliente;

use Illuminate\Http\Request;
use App\cliente\Transportista;
use App\estructura\Configuracion;

/**
 * Controlador para administrar la cofiguración de los transportistas en el sistema
 * 
 * Contiene un conjuto de métodos que permiten administrar la configuración de los transportistas
 * en el sistema. Ej. Jua Perez, 0601234567, etc.
 * 
 * @copyright (c) 2019, SICE WEB
 */
class ControladorTransportista extends Controller {

    /**
     * Nivel funcional: Muestra el listado de todos los transportistas definidos en el sistema
     * 
     * Nivel técnico: Obtiene a traves del modelo "Transportista", el listado de todos los transportistas
     * registrados en la BD y envía estos a la vista "LAETransportistas" a través de la variable $transportistas.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return View Invoca a la vista "LAETransportistas" y pasa la variable $transpostistas.
     * @since versión 1.0
     * @version 1.0
     */
    //public function vistaLeerActualizarEliminar() {
    //    $transportistas = Transportista::all();
    //    return view('clientes/LAETransportistas', ['transportistas' => $transportistas]);
    //}
    
    /**
     * Nivel funcional: Presenta la pantalla para crear transportistas en el sistema
     * 
     * Nivel técnico: Invoca a la vista "clientes/CTrasportistas"
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return View Invoca a la vista "CTransportistas"
     * @since versión 1.0
     * @version 1.0
     */ 
    //public function vistaCrear() {
    //    return view('clientes/CTransportistas');
    //}
    
    /**
     * Nivel funcional: Proceso para crear nuevos transportistas en el sistema
     * 
     * Nivel técnico: Crea una instancia del modelo "Transportista", relaciona todos los campos 
     * de la tabla "transportistas" con los datos del nuevo transportista recibido por
     * parámetro y guarda en la BD a traves del modelo "Transportista".
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el nuevo transportista con los campos de la tabla "transportistas"
     * @return Boolean Devuelve True si se creó el nuevo transportista en la tabla "transportistas"
     * @since versión 1.0
     * @version 1.0
     */ 
    public function crear(Request $request) {
        try {
            $validacion = Validator::make($request->all(), [
                'nombre' => 'required|min:1',
                'identificacion' => 'required|min:1',
                'vehiculo_propio'=> 'required|min:1',
            ]);
            if ($validacion->fails()) {
                return response()->json(['error' => 'Bad Request', 'bandera' => false, 'mensaje' => 'Error en validacion de parametros'], 400)
                ->withHeaders(self::$headers);
            }
            $transportista = new Transportista($request->all());
            $bandera = $transportista->save();
            return response()->json(['resultado' => $transportista, 'bandera' => $bandera, 'mensaje' => 'Se ha creado el tipo de contribuyente'], 201)
                ->withHeaders(self::$headers);
        } catch (Exception | QueryException $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Obtiene el código secuencial para crear un nuevo transportista en el sistema
     * 
     * Nivel técnico: Obtiene el código secuencial para crear un nuevo transportista, contando el total
     * de registros de la tabla "transportistas", incrementado en 1 y concatenando con la letra "TR" 
     * obtenida del Modelo Configuración.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_encode Devuelve el código para el nuevo transportista.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function obtenerCodigo() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'transportistas')->where('tipo', 'CREAR')->get();
            $conteoTransportistas = Transportista::count();
            $conteoTransportistas++;
            $rellenoConteo = str_pad($conteoTransportistas, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 0, 2);
            return ['resultado'=>$configuracion[0]->codigo . $año . $mes . $rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Obtiene un transportista específico según el id que recibe
     * 
     * Nivel técnico: Obtiene a través del Modelo "Transportista" el registro de un transportista
     * específico según el id recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del transportista a buscar en la tabla
     * @return json_enconde Devuelve en la variable $transportista los datos de un transportista específico, 
     * obtenido de la tabla "transportistas" según el id recibido por parámetro, sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function obtener(Request $request) {
        try {
            $transportista = Transportista::findOrFail($request->id);
            return response()->json(['resultado' => $transportista, 'bandera' => true, 'mensaje' => 'Se ha obtenido los transportistas'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el transportista'], 404)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Obtiene todos los transportistas creados en el sistema
     * 
     * Nivel técnico: Obtiene los registros de la tabla "transportistas", a través
     * del Modelo "Transportista".
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_enconde Devuelve en la variable $transportistas todos los registros de 
     * la tabla "transportistas", sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function consultar() {
        try {
            $transportistas = Transportista::all();
            return response()->json(['resultado' => $transportistas, 'bandera' =>true, 'mensaje' => 'Se ha consultado los transportistas'], 201)
                ->withHeaders(self::$headers);
        } catch (Exception | QueryException $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Proceso para actualizar un transportista registrado en el sistema
     * 
     * Nivel técnico: Actualizacion en la tabla "transportistas" el registro que corresponda
     * al número interno (id) recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del transportista a modificar
     * @return Boolean Devuelve True si se modificó el transportista en la tabla "transportistas", sino false.
     * @since versión 1.0
     * @version 1.0
     */
    public function actualizar(Request $request) {
        try {
        $transportista = Transportista::findOrFail($request->id);
        $transportista->codigo = $request->codigo;
        $transportista->nombre = $request->nombre;
        $transportista->identificacion = $request->identificacion;
        $bandera = $transportista->save();
        return response()->json(['resultado' => $transportista, 'bandera' => $bandera, 'mensaje' => 'Se ha actualizado el transportista'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el transportista'], 404)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Proceso para eliminar un transportista registrado en el sistema
     * 
     * Nivel técnico: Borra de la tabla "transportistas" el registro que corresponda
     * al número interno (id) recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del transportista a eliminar de la tabla.
     * @return Boolean Devuelve True si se eliminó el registro en la tabla "transportistas", sino false.
     * @since versión 1.0
     * @version 1.0
     */
    public function eliminar(Request $request) {
        try {
            $transportista = Transportista::findOrFail($request->id);
            $bandera = $transportista->delete();
            return response()->json(['resultado' => $transportista, 'bandera' => $bandera, 'mensaje' => 'Se ha eliminado el transportista'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el transportista'], 404)
                ->withHeaders(self::$headers);
        }
    }

}
