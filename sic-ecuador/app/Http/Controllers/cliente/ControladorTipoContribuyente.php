<?php

namespace App\Http\Controllers\clientes;

use Illuminate\Http\Request;
use App\cliente\TipoContribuyente;
use App\estructura\Configuracion;

/**
 * Controlador para administrar la cofiguración de los tipos de contribuyentes en el sistema
 * 
 * Contiene un conjuto de métodos que permiten administrar la configuración de los tipos de
 * contribuyentes y los subtipos para el sistema. Ej. Persona natural, Persona Jurídica, etc.
 * 
 * @copyright (c) 2019, SICE WEB
 */
class ControladorTipoContribuyente extends Controller {

    /**
     * Nivel funcional: Proceso para crear nuevos tipos de contribuyentes en el sistema
     * 
     * Nivel técnico: Crea una instancia del modelo "TipoContribuyente", relaciona todos los campos 
     * de la tabla "tipos_contribuyentes" con los datos del nuevo tipo de contribuyente recibido por
     * parámetro y guarda en la BD a traves del modelo "TipoContribuyente".
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el nuevo tipo de contribuyente con los campos de la tabla "tipos_contribuyentes"
     * @return Boolean Envía un mensaje True si se guardó el nuevo tipo de contribuyente en la tabla "tipos_contribuyentes"
     * @since versión 1.0
     * @version 1.0
     */ 
    public function crear(Request $request) {
        try {
            $validacion = Validator::make($request->all(), [
                'tipo' => 'required|min:1',
                'subtipo' => 'required|min:1',
                'especial'=> 'required|boolean',
            ]);
            if ($validacion->fails()) {
                return response()->json(['error' => 'Bad Request', 'bandera' => false, 'mensaje' => 'Error en validacion de parametros'], 400)
                ->withHeaders(self::$headers);
            }
            $tipoContribuyente = new TipoContribuyente($request->all());
            $bandera = $tipoContribuyente->save();
            return response()->json(['resultado' => $tipoContribuyente, 'bandera' => $bandera, 'mensaje' => 'Se ha creado el tipo de contribuyente'], 201)
                ->withHeaders(self::$headers);
        } catch (Exception | QueryException $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Obtiene el código secuencial para crear un nuevo tipo de contribuyente en el sistema
     * 
     * Nivel técnico: Obtiene el código secuencial para crear un nuevo tipo de contribuyente, contando el total
     * de registros de la tabla "tipos_contribuyentes", incrementado en 1 y concatenando con la letra "TC" 
     * obtenida del Modelo Configuración.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_encode Devuelve el nuevo código para el tipo y subtipo de contribuyente
     * @since versión 1.0
     * @version 1.0
     */ 
    public function obtenerCodigo() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'tipos_contribuyentes')->where('tipo', 'CREAR')->get();
            $conteoTiposContribuyentes = TipoContribuyente::count();
            $conteoTiposContribuyentes++;
            $rellenoConteo = str_pad($conteoTiposContribuyentes, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 0, 2);
            return ['resultado'=>$configuracion[0]->codigo . $año . $mes . $rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Obtiene los tipos de contribuyentes creados en el sistema
     * 
     * Nivel técnico: Obtiene los registros de la tabla "tipos_contribuyentes", a través
     * del Modelo "TipoContribuyente", según el campo "tipo" recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el campo "tipo" del contribuyente a buscar en la tabla
     * @return json_enconde Devuelve en la variable $tiposContribuyentes todos los registros de 
     * la tabla "tipos_contribuyentes" que corespondan al tipo recibido por parámetro, sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function consultar() {
        try {
            $tiposContribuyentes = TipoContribuyente::all();
            return response()->json(['resultado' => $tiposContribuyentes, 'bandera' =>true, 'mensaje' => 'Se ha consultado los tipos de contribuyentes'], 201)
                ->withHeaders(self::$headers);
        } catch (Exception | QueryException $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Obtiene un tipo de contribuyente específico según el id que recibe
     * 
     * Nivel técnico: Obtiene a través del Modelo "TipoContribuyente" el registro de un tipo
     * de contribuyente específico según el id recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del tipo de contribuyente a buscar en la tabla
     * @return json_enconde Devuelve en la variable $tipoContribuyente el tipo de contribuyente específico 
     * obtenido de la tabla "tipos_contribuyentes" según el id recibido por parámetro, sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function obtener(Request $request) {
        try {
            $tipoContribuyente = TipoContribuyente::findOrFail($request->id);
            return response()->json(['resultado' => $tipoContribuyente, 'bandera' => true, 'mensaje' => 'Se ha obtenido el tipo de contribuyente'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el dato adicional'], 404)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Proceso para actualizar un tipo de contribuyente registrado en el sistema
     * 
     * Nivel técnico: Actualizacion en la tabla "tipos_contribuyentes" el registro que corresponda
     * al número interno (id) recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del tipo de contribuyente a buscar en la tabla
     * @return Boolean Envía un mensaje True si se modificó el tipo de contribuyente en la tabla "tipos_contribuyentes"
     * @since versión 1.0
     * @version 1.0
     */
    public function actualizar(Request $request) {
        try {
            $tipoContribuyente = TipoContribuyente::findOrFail($request->id);
            $tipoContribuyente->codigo = $request->codigo;
            $tipoContribuyente->tipo = $request->tipo;
            $tipoContribuyente->subtipo = $request->subtipo;
            $tipoContribuyente->especial = $request->especial;
            $bandera = $tipoContribuyente->save();
            return response()->json(['resultado' => $tipoContribuyente, 'bandera' => $bandera, 'mensaje' => 'Se ha actualizado el tipo de contribuyente'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el tipo de contribuyente'], 404)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Proceso para eliminar un tipo de contribuyente registrado en el sistema
     * 
     * Nivel técnico: Borra de la tabla "tipos_contribuyentes" el registro que corresponda
     * al número interno (id) recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del tipo de contribuyente a eliminar de la tabla.
     * @return Boolean Envía un mensaje True si se eliminó el registro en la tabla "tipos_contribuyentes", sino false.
     * @since versión 1.0
     * @version 1.0
     */
    public function eliminar(Request $request) {
        try {
            $tipoContribuyente = TipoContribuyente::findOrFail($request->id);
            $bandera = $tipoContribuyente->delete();
            return response()->json(['resultado' => $tipoContribuyente, 'bandera' => $bandera, 'mensaje' => 'Se ha actualizado el tipo de contribuyente'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el tipo de contribuyente'], 404)
                ->withHeaders(self::$headers);
        }
    }

}
