<?php

namespace App\Http\Controllers\cliente;

use Illuminate\Http\Request;
use App\cliente\Cliente;
use App\cliente\Telefono;
use App\cliente\Celular;
use App\cliente\Correo;
use App\cliente\GrupoContable;
use App\cliente\TipoContribuyente;
use App\cliente\Ubicaciongeo;
use App\cliente\DatoAdicional;
use App\cliente\PlazoCredito;
use App\cliente\Retencion;
use App\estructura\Configuracion;
use App\cliente\AuxiliarCF;

/**
 * Controlador para manejar los Clientes
 * 
 * Contiene un conjuto de métodos que permiten manejar los clientes.
 * Los clientes son aquellas personas que requieren factura o alguna acción
 * dentro del sistema.
 * 
 * @copyright (c) 2019, SICE WEB
 */
class ControladorCliente extends Controller
{
    private static $headers = [
        'Content-Type' => 'application/json',
        'Access-Control-Allow-Origin' => '*', 'Access-Control-Allow-Headers' => 'Origin, Content-Type, Accept',
        'Access-Control-Allow-Methods' => 'GET,HEAD,POST,PUT,DELETE,OPTIONS'
    ];

    /**
     * Nivel funcional: Proceso para crear nuevos clientes en la BD
     * 
     * Nivel técnico: Crea una instancia del modelo Cliente, relaciona todos los campos 
     * de la tabla "Clientes" con los datos del cliente recibido por parámetro 
     * y guarda en la BD a traves del modelo Cliente.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe los datos del cliente según los campos de la tabla Clientes
     * @return Boolean Envía un mensaje True si se guardó el cliente en la tabla "Clientes"
     * @since versión 1.0
     * @version 1.0
     */
    public function crear(Request $request)
    {
        try {
            error_log(print_r($request->all(), true), 3, "test_log.log");
            $cliente = new Cliente($request->all());
            $bandera = $cliente->save();
            $telefonos = array();
            for ($i = 0; $i < count($request->telefonos); $i++) {
                $telefono = new Telefono();
                $telefono->codigo = $telefono->obtenerCodigoTelefono();
                $telefono->numero = $request->telefonos[$i];
                $telefono->cliente_id = $cliente->id;
                array_push($telefonos, $telefono);
            }
            $celulares = array();
            for ($i = 0; $i < count($request->celulares); $i++) {
                $celular = new Celular();
                $celular->codigo = $celular->obtenerCodigoCelular();
                $celular->numero = $request->celulares[$i];
                $celular->cliente_id = $cliente->id;
                array_push($celulares, $celular);
            }
            $correos = array();
            for ($i = 0; $i < count($request->correos); $i++) {
                $correo = new Correo();
                $correo->codigo = $correo->obtenerCodigoCorreo();
                $correo->email = $request->correos[$i];
                $correo->cliente_id = $cliente->id;
                array_push($correos, $correo);
            }
            $cliente->telefonos()->saveMany($telefonos);
            $cliente->celulares()->saveMany($celulares);
            $cliente->correos()->saveMany($correos);
            return response()->json(['resultado' => $cliente, 'bandera' => $bandera, 'mensaje' => 'Se ha creado el cliente'], 201)
                ->withHeaders(self::$headers);
        } catch (Exception | QueryException $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => $bandera, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }

    function obtener(Request $request)
    {
        try {
            $cliente = Cliente::find($request->id);
            $cliente->ubicaciongeo;
            if ($cliente->cliente_prepago) {
                $cliente->cliente_prepago = 'SI';
            } else {
                $cliente->cliente_prepago = 'NO';
            }
            if ($cliente->cliente_pospago) {
                $cliente->cliente_pospago = 'SI';
            } else {
                $cliente->cliente_pospago = 'NO';
            }
            return response()->json(['resultado' => $cliente, 'bandera' => true, 'mensaje' => 'Se ha obtenido el cliente'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(), 'bandera' => false, 'mensaje' => 'No se encontro el cliente'], 404)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Proceso para obtener de la BD todos los clientes
     * 
     * Nivel técnico: Obtiene de la tabla "Clientes" todos los clientes a través 
     * del modelo Cliente.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_encode Devuelve todos los clientes de la tabla Clientes o sino null.
     * @since versión 1.0
     * @version 1.0
     */
    function consultar()
    {
        try {
            $clientes = Cliente::all();
            return response()->json(['resultado' => $clientes, 'bandera' => true, 'mensaje' => 'Se ha consultado los clientes'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception | QueryException $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Obtiene el código secuencial para crear un cliente
     * 
     * Nivel técnico: Obtiene el código secuencial para un nuevo cliente, contando el total
     * de clientes, incrementado en 1 y concatenando con la letra correspondiente
     * obtenida del Modelo Configuración.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_encode Devuelve el código del nuevo cliente
     * @since versión 1.0
     * @version 1.0
     */
    public function obtenerCodigo()
    {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'clientes')->where('tipo', 'CREAR')->get();
            $conteoClientes = Cliente::count();
            $conteoClientes++;
            $rellenoConteo = str_pad($conteoClientes, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 0, 2);
            return ['resultado' => $configuracion[0]->codigo . $año . $mes . $rellenoConteo];
        } catch (Exception $e) {
            return ['resultado' => null];
        }
    }

    /**
     * Nivel funcional: Obtiene los Clientes No Auxiliares Consumidor Final
     * 
     * Nivel técnico: Función no utilizada
     *
     * @since versión 1.0
     * @version 1.0
     */
    public function obtenerClientesNoAuxiliaresCF()
    {
        try {
            $clientesNoAuxiliaresCF = Cliente::whereNotIn('id', function ($query) {
                $query->select('cliente_id')->from(with(new AuxiliarCF)->getTable());
            })->where('identificacion', '!=', '9999999999999')->get();
            return response()->json(['resultado' => $clientesNoAuxiliaresCF, 'bandera' => true, 'mensaje' => 'Se ha obtenido los clientes no auxiliares de consumo final'], 201)
                ->withHeaders(self::$headers);
        } catch (Exception | QueryException $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Obtiene los Clientes que no tienen Clientes Auxiliares
     * 
     * Nivel técnico: Obtiene los clientes que no tienen Clientes Auxiliares (Auxiliar=0),
     * utilizando el modelo cliente.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_enconde Devuelve los clientes no auxiliares en la variable $clientesNoAuxiliares 
     * sino devuelve null
     * @since versión 1.0
     * @version 1.0
     */
    public function obtenerClientesNoAuxiliares()
    {
        try {
            $clientesNoAuxiliares = Cliente::where('auxiliar', 0)->get();
            return response()->json(['resultado' => $clientesNoAuxiliares, 'bandera' => true, 'mensaje' => 'Se ha obtenido los clientes no auxiliares'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception | QueryException $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Asigna un Clientes como cliente con Clientes Auxiliares
     * 
     * Nivel técnico: Cambia en la BD el valor del campo auxiliar de 0 a 1, para
     * identificarlo como cliente con datos Auxiliares.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe los datos del cliente al que se le asignará como cliente Auxiliar 
     * @return json_enconde Devuelve los True si se guardó los cambios en la BD. 
     * @since versión 1.0
     * @version 1.0
     */
    public function asignarClienteAuxiliar(Request $request)
    {
        try {
            $clienteAuxiliar = Cliente::findOrFail($request->id);
            $clienteAuxiliar->auxiliar = 1;
            $bandera = $clienteAuxiliar->save();
            return response()->json(['resultado' => $clienteAuxiliar, 'bandera' => $bandera, 'mensaje' => 'Se ha asignado el cliente auxiliar'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception | QueryException $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(), 'bandera' => false, 'mensaje' => 'No se encontro el cliente'], 404)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Obtiene un Cliente por identificación
     * 
     * Nivel técnico: Devuelve los datos del cliente buscando por identificación
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe la identificación del cliente que se buscará en la tabla "Clientes" 
     * @return json_enconde Devuelve el cliente encontrado por identificación sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function obtenerIdentificacion(Request $request)
    {
        try {
            $cliente = DatoAdicional::where('identificacion', $request->identificacion)->get();
            return response()->json(['resultado' => $cliente, 'bandera' => true, 'mensaje' => 'Se ha asignado la identificacion del cliente'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception | QueryException $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Obtiene un Cliente por identificación
     * 
     * Nivel técnico: Devuelve los datos del cliente buscando por identificación
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el cliente con su identificación y lo busca en la tabla "Clientes" 
     * @return json_enconde Devuelve true si el cliente es prepago y false si es pospago, sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function obtenerPrepagoOPospago(Request $request)
    {
        try {
            $cliente = Cliente::findOrFail($request->id);
            if ($cliente->cliente_prepago) {
                return response()->json(['resultado' => $cliente, 'bandera' => true, 'mensaje' => 'Se ha obtenido el cliente prepago'], 200)
                    ->withHeaders(self::$headers);
            } else if ($cliente->cliente_pospago) {
                return response()->json(['resultado' => $cliente, 'bandera' => true, 'mensaje' => 'Se ha obtenido el cliente pospago'], 200)
                    ->withHeaders(self::$headers);
            } else {
                return response()->json(['resultado' => null, 'bandera' => true, 'mensaje' => 'No se ha obtenido el cliente prepago o pospago'], 404)
                    ->withHeaders(self::$headers);
            }
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(), 'bandera' => false, 'mensaje' => 'No se encontro el cliente'], 404)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Proceso para modificar un cliente registrado en el sistema
     * 
     * Nivel técnico: Modifica en la tabla "clientes" el registro que corresponda
     * al número interno (id) recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del cliente a buscar en la tabla
     * @return Boolean Envía un mensaje True si se modificó el cliente en la tabla "clientes"
     * @since versión 1.0
     * @version 1.0
     */
    public function actualizar(Request $request)
    {
        try {
            $cliente = Cliente::findOrFail($request->id);
            $bandera = $cliente->update($request->all());
            return response()->json(['resultado' => $cliente, 'bandera' => $bandera, 'mensaje' => 'Se ha actualizado el cliente'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(), 'bandera' => false, 'mensaje' => 'No se encontro el cliente'], 404)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Proceso para eliminar un cliente registrado en el sistema
     * 
     * Nivel técnico: Borra de la tabla "clientes" el registro que corresponda
     * al número interno (id) recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del cliente a eliminar de la tabla "clientes"
     * @return Boolean Envía un mensaje True si se eliminó el registro en la tabla "clientes", sino null.
     * @since versión 1.0
     * @version 1.0
     */
    function eliminar(Request $request)
    {
        try {
            $cliente = Cliente::findOrFail($request->id);
            $bandera = $cliente->delete();
            return response()->json(['resultado' => $cliente, 'bandera' => $bandera, 'mensaje' => 'Se ha eliminado el cliente'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(), 'bandera' => false, 'mensaje' => 'No se encontro el cliente'], 404)
                ->withHeaders(self::$headers);
        }
    }
}
