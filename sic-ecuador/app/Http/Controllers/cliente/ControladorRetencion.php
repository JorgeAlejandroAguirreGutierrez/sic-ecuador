<?php

namespace App\Http\Controllers\clientes;

use Illuminate\Http\Request;
use App\cliente\Retencion;
use App\estructura\Configuracion;

/**
 * Controlador para administrar la cofiguración de las retenciones
 * 
 * Contiene un conjuto de métodos que permiten administrar la configuración de las retenciones.
 * Ej. Retenciones del IVA, retenciones a la fuente o renta, etc.
 * 
 * @copyright (c) 2019, SICE WEB
 */
class ControladorRetencion extends Controller {

    private static $headers = [
        'Content-Type' => 'application/json',
        'Access-Control-Allow-Origin' => '*', 'Access-Control-Allow-Headers' => 'Origin, Content-Type, Accept',
        'Access-Control-Allow-Methods' => 'GET,HEAD,POST,PUT,DELETE,OPTIONS'
    ];
    
    /**
     * Nivel funcional: Obtiene el código secuencial para crear una nueva retención en la Renta
     * 
     * Nivel técnico: Obtiene el código secuencial para crear un nuevo porcentaje de retención 
     * en el impuesto a la renta, contando el total de registros de la tabla "retenciones", 
     * incrementado en 1 y concatenando con la letra "RETEIR" obtenida del Modelo Configuración.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_encode Devuelve el código del nuevo porcentaje de retención en la renta
     * @since versión 1.0
     * @version 1.0
     */ 
    public function obtenerCodigoRetencionIR() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'retenciones')->where('tipo', 'CREARIR')->get();
            $conteoRetenciones = Retencion::count();
            $conteoRetenciones++;
            $rellenoConteo = str_pad($conteoRetenciones, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes= substr($fecha, 0,2);
            return ['resultado'=>$configuracion[0]->codigo . $año . $mes . $rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Obtiene el código secuencial para crear una nueva retención en el IVA
     * 
     * Nivel técnico: Obtiene el código secuencial para crear un nuevo porcentaje de retención 
     * en el impuesto al valor agregado (IVA), contando el total de registros de la tabla "retenciones", 
     * incrementado en 1 y concatenando con la letra "RETEIVA" obtenida del Modelo Configuración.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_encode Devuelve el código del nuevo porcentaje de retención en el IVA
     * @since versión 1.0
     * @version 1.0
     */
    public function obtenerCodigoRetencionIVA() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'retenciones')->where('tipo', 'CREARIVA')->get();
            $conteoRetenciones = Retencion::count();
            $conteoRetenciones++;
            $rellenoConteo = str_pad($conteoRetenciones, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 3, 2);
            return ['resultado'=>$configuracion[0]->codigo . $año . $mes . $rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Proceso para crear nuevas retenciones en el sistema
     * 
     * Nivel técnico: Crea una instancia del modelo "Retencion", relaciona todos los campos 
     * de la tabla "retenciones" con los datos del nuevo porcentaje de retención recibido por parámetro 
     * y guarda en la BD a traves del modelo "Retencion".
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el nuevo porcentaje de retención según los campos de la tabla "retenciones"
     * @return Boolean Envía un mensaje True si se guardó el nuevo porcentaje de retención en la tabla "retenciones"
     * @since versión 1.0
     * @version 1.0
     */ 
    public function crear(Request $request) {
        try {
            $validacion = Validator::make($request->all(), [
                'cliente_id' => 'required|integer',
                'tipo_retencion_id' => 'required|integer'
            ]);
            if ($validacion->fails()) {
                return response()->json(['error' => 'Bad Request', 'bandera' => false, 'mensaje' => 'Error en validacion de parametros'], 400)
                ->withHeaders(self::$headers);
            }
            $retencion = new Retencion($request->all());
            $bandera = $retencion->save();
            return response()->json(['resultado' => $retencion, 'bandera' => $bandera, 'mensaje' => 'Se ha creado la retencion'], 201)
                ->withHeaders(self::$headers);
        } catch (Exception | QueryException $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => $bandera, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Obtiene todos los porcentajes de retención creados en el sistema
     * 
     * Nivel técnico: Obtiene todos los registros de la tabla "retenciones", a través
     * del Modelo Retencion.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_enconde Devuelve en la variable $retenciones todos los registros de la
     * tabla "retenciones", sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function consultar() {
        try {
            $retenciones = Retencion::all();
            return response()->json(['resultado' => $retenciones, 'bandera' => true, 'mensaje' => 'Se ha obtenido las retenciones'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Obtiene los porcentajes de retención del IVA creados en el sistema
     * 
     * Nivel técnico: Obtiene los registros de la tabla "retenciones" que contengan el
     * código "IVA", a través del Modelo Retencion.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_enconde Devuelve en la variable $retenciones los registros de la tabla "retenciones"
     * que contengan el código "IVA", sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function obtenerIVAs() {
        try {
            $ivas = Retencion::where('codigo', 'LIKE', '%IVA%')->get();
            return response()->json(['resultado' => $ivas, 'bandera' => true, 'mensaje' => 'Se ha obtenido IVAs'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Obtiene los porcentajes de retención en la Renta creados en el sistema
     * 
     * Nivel técnico: Obtiene los registros de la tabla "retenciones" que contengan el
     * código "IR", a través del Modelo Retencion.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_enconde Devuelve en la variable $retenciones los registros de la tabla "retenciones"
     * que contengan el código "IR", sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function obtenerIRs() {
        try {
            $irs = Retencion::where('codigo', 'LIKE', '%IR%')->get();
            return response()->json(['resultado' => $irs, 'bandera' => true, 'mensaje' => 'Se ha obtenido IRs'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Obtiene un porcentaje de retención específico según el id que recibe
     * 
     * Nivel técnico: Obtiene a través del Modelo Retencion el registro de un porcentaje de
     * retención específico según el id recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del porcentaje de retención a buscar en la tabla
     * @return json_enconde Devuelve en la variable $retencion el porcentaje de retención específico 
     * obtenido de la tabla "retenciones" según el id recibido por parámetro, sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function obtener(Request $request) {
        try {
            $retencion = Retencion::findOrFail($request->id);
            return response()->json(['resultado' => $retencion, 'bandera' => true, 'mensaje' => 'Se ha obtenido el plazo de credito'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro la retencion'], 404)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Proceso para actualizar un porcentaje de retención en el sistema
     * 
     * Nivel técnico: Actualizacion en la tabla "retenciones" el registro que corresponda
     * al número interno (id) enviado por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del porcentaje de retención a buscar en la tabla
     * @return Boolean Envía un mensaje True si se modificó la retención en la tabla "retenciones"
     * @since versión 1.0
     * @version 1.0
     */
    public function actualizar(Request $request) {
        try {
            $retencion = Retencion::findOrFail($request->id);
            $retencion->codigo = $request->codigo;
            $retencion->descripcion = $request->descripcion;
            $retencion->porcentaje = $request->porcentaje;
            $retencion->porcentaje = round($retencion->porcentaje, 2);
            $bandera = $retencion->save();
            return response()->json(['resultado' => $retencion, 'bandera' => $bandera, 'mensaje' => 'Se ha actualizado el plazo de credito'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(), 'bandera' => false, 'mensaje' => 'No se encontro la retencion'], 404)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Proceso para eliminar un porcentaje de retención en el sistema
     * 
     * Nivel técnico: Borra de la tabla "retenciones" el registro que corresponda
     * al número interno (id) enviado por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del registro a eliminar de la tabla
     * @return Boolean Envía un mensaje True si se eliminó el registro de retención
     * en la tabla "retenciones"
     * @since versión 1.0
     * @version 1.0
     */ 
    public function eliminar(Request $request) {
        try {
            $retencion = Retencion::findOrFail($request->id);
            $bandera = $retencion->delete();
            return response()->json(['resultado' => $retencion, 'bandera' => $bandera, 'mensaje' => 'Se ha eliminado el plazo de credito'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(), 'bandera' => false, 'mensaje' => 'No se encontro la retencion'], 404)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Verifica si el código de retención corresponde al tipo Impuesto Renta (IR)
     * 
     * Nivel técnico: Verifica si el código de Retencion contiene la palabra "IR" a través de la 
     * función "strpos" que devuelve la posición del inicio del substring o false si no lo encuentra.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) de la retención a verificar en la tabla
     * @return json_enconde Devuelve true si el código de retención es de tipo impuesto renta (IR)
     * sino devuelve false.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function verificarRetencionIR(Request $request){
        $retencion= Retencion::find($request->retencion_id);
        if (strpos($retencion->codigo, 'IR') !== false){
            return response()->json(['resultado' => true, 'bandera' => true, 'mensaje' => 'Se verifico retencion IR'], 200)
                ->withHeaders(self::$headers);
        }
        else{
            return response()->json(['resultado' => false, 'bandera' => true, 'mensaje' => 'Se verifico retencion IR'], 200)
                ->withHeaders(self::$headers);
        }
    }
    
     /**
     * Nivel funcional: Verifica si el código de retención corresponde al tipo Impuesto al Valor Agregado (IVA)
     * 
     * Nivel técnico: Verifica si el código de Retencion contiene la palabra "IVA" a través de la 
     * función "strpos" que devuelve la posición del inicio del substring o false si no lo encuentra.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) de la retención a verificar en la tabla
     * @return json_enconde Devuelve true si el código de retención es de tipo impuesto al valor agregado (IVA)
     * sino devuelve false.
     * @since versión 1.0
     * @version 1.0
     */
    public function verificarRetencionIVA(Request $request){
        $retencion= Retencion::find($request->retencion_id);
        if (strpos($retencion->codigo, 'IVA') !== false){
            return response()->json(['resultado' => true, 'bandera' => true, 'mensaje' => 'Se verifico retencion IVA'], 200)
                ->withHeaders(self::$headers);
        }
        else{
            return response()->json(['resultado' => false, 'bandera' => true, 'mensaje' => 'Se verifico retencion IVA'], 200)
                ->withHeaders(self::$headers);
        }
    }
}
