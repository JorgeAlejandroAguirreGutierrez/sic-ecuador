<?php

namespace App\Http\Controllers\cliente;

use Illuminate\Http\Request;
use App\clientes\AuxiliarCF;
use App\cliente\Cliente;

class ControladorAuxiliaresCF extends Controller {

    public function crear(Request $request) {
        $clienteAuxiliarCF = new AuxiliarCF($request->all());
        $bandera = $clienteAuxiliarCF->save();
        return ['resultado'=>$bandera];
    }

    public function obtenerAuxiliaresCF() {
        $clientesAuxiliaresCF = AuxiliarCF::all();
        return ['resultado'=>$clientesAuxiliaresCF];
    }

    public function obtenerAuxiliarCF(Request $request) {
        $clienteAuxiliarCF = AuxiliarCF::find($request->id);
        return ['resultado'=>$clienteAuxiliarCF];
    }

    public function modificarAuxiliarCF(Request $request) {
        $clienteAuxiliarCF = AuxiliarCF::find($request->id);
        $clienteAuxiliarCF->cliente_id = $request->cliente_id;
        $bandera = $clienteAuxiliarCF->save();
        return ['resultado'=>$bandera];
    }

    public function eliminarAuxiliarCF(Request $request) {
        $clienteAuxiliarCF = AuxiliarCF::find($request->id);
        $bandera = $clienteAuxiliarCF->delete();
        return ['resultado'=>$bandera];
    }

}
