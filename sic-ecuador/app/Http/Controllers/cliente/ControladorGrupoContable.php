<?php

namespace App\Http\Controllers\cliente;

use Illuminate\Http\Request;
use App\cliente\GrupoContable;
use App\estructura\Configuracion;

/**
 * Controlador para administrar los Grupos Contables de Clientes y Servicios
 * 
 * Contiene un conjuto de métodos que permiten manejar los grupos contables.
 * Los grupos contables son parámetros para la identificación y correcta contabilización
 * de los registros del sistema. Ej. Clientes Nacionales, Honorarios, servicios, etc.
 * 
 * @copyright (c) 2019, SICE WEB
 */
class ControladorGrupoContable extends Controller {

    /**
     * Nivel funcional: Obtiene el código secuencial para crear un grupo contable para servicios
     * 
     * Nivel técnico: Obtiene el código secuencial para un nuevo grupo contable de servicios, 
     * contando el total de registros de la tabla GrupoContable que corresponden a Servicios,
     * incrementado en 1 y concatenando con la letra "GS" obtenida del Modelo Configuración.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_encode Devuelve el código del nuevo grupo contable para servicios
     * @since versión 1.0
     * @version 1.0
     */    
    public function obtenerCodigoGrupoServicio() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'grupos_contables')->where('tipo', 'CREARGS')->get();
            $conteoGruposServicios = GrupoContable::count();
            $conteoGruposServicios++;
            $rellenoConteo = str_pad($conteoGruposServicios, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 0, 2);
            return ['resultado'=>$configuracion[0]->codigo . $año . $mes . $rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Obtiene el código secuencial para crear un grupo contable de cleintes
     * 
     * Nivel técnico: Obtiene el código secuencial para un nuevo grupo contable de clientes, 
     * contando el total de registros de la tabla GrupoContable que corresponden a Clientes,
     * incrementado en 1 y concatenando con la letra "GC" obtenida del Modelo Configuración.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_encode Devuelve el código del nuevo grupo contable de clientes
     * @since versión 1.0
     * @version 1.0
     */ 
    public function obtenerCodigoGrupoCliente() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'grupos_contables')->where('tipo', 'CREARGC')->get();
            $conteoDatosAdicionales = GrupoContable::count();
            $conteoDatosAdicionales++;
            $rellenoConteo = str_pad($conteoDatosAdicionales, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 3, 2);
            return ['resultado'=>$configuracion[0]->codigo . $año . $mes . $rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Proceso para crear nuevos grupos contables en la BD
     * 
     * Nivel técnico: Crea una instancia del modelo GrupoContable, relaciona todos los campos 
     * de la tabla "grupos_contables" con los grupos contables recibido por parámetro 
     * y guarda en la BD a través del modelo GrupoContable.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe los grupos contables según los campos de la tabla "grupos_contables"
     * @return Boolean Envía un mensaje True si se guardó el grupo contable en la tabla "grupos_contables"
     * @since versión 1.0
     * @version 1.0
     */ 
    public function crear(Request $request) {
        try {
            $validacion = Validator::make($request->all(), [
                'grupo' => 'required|min:1',
                'subgrupos' => 'required|min:1'
            ]);
            if ($validacion->fails()) {
                return response()->json(['error' => 'Bad Request', 'bandera' => false, 'mensaje' => 'Error en validacion de parametros'], 400)
                ->withHeaders(self::$headers);
            }
            $grupoContable = new GrupoContable($request->all());
            $bandera = $grupoContable->save();
            return response()->json(['resultado' => $grupoContable, 'bandera' => $bandera, 'mensaje' => 'Se ha creado el grupo contable'], 201)
                ->withHeaders(self::$headers);
        } catch (Exception | QueryException $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Obtiene todos los grupos contables sin distinción de subgrupo
     * 
     * Nivel técnico: Obtiene todos los registros del Modelo GrupoContrable.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_enconde Devuelve en la variable $gruposContables todos los grupos contables,
     * sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function consultar() {
        try {
            $gruposContables = GrupoContable::all();
            return response()->json(['resultado' => $gruposContables, 'bandera' => true, 'mensaje' => 'Se ha obtenido los grupos contables'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Obtiene los grupos contables asignados para Servicios
     * 
     * Nivel técnico: Obtiene los registros del Modelo GrupoContrable que corresponden
     * al subgrupo Servicios (GS).
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_enconde Devuelve en la variable $gruposContables los grupos contables del
     * subgrupo servicios, sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function obtenerGruposServicios() {
        try {
            $gruposContables = GrupoContable::where('codigo', 'LIKE', '%GS%')->get();
            return response()->json(['resultado' => $gruposContables, 'bandera' => true, 'mensaje' => 'Se ha obtenido los grupos de servicios'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Obtiene los grupos contables asignados para Clientes
     * 
     * Nivel técnico: Obtiene los registros del Modelo GrupoContrable que corresponden
     * al subgrupo Clientes (GC).
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_enconde Devuelve en la variable $gruposContables los grupos contables del
     * subgrupo clientes, sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */   
    public function obtenerGruposClientes() {
        try {
            $gruposContables = GrupoContable::where('codigo', 'LIKE', '%GC%')->get();
            return ['resultado'=>$gruposContables];
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Obtiene un grupo contable específico según el id requerido
     * 
     * Nivel técnico: Obtiene el registros del grupo contable según el id requerido
     * por parámetro, obtenido a través del Modelo GrupoContable.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno del grupo contable a buscar en la tabla
     * @return json_enconde Devuelve en la variable $grupoContable el grupo contable específico 
     * que concuerda con el id de la tabla "grupos_contables" solicitado por parámetro, 
     * sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function obtener(Request $request) {
        try {
            $grupoContable = GrupoContable::findOrFail($request->id);
            return response()->json(['resultado' => $grupoContable, 'bandera' => true, 'mensaje' => 'Se ha obtenido el grupo contable'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el grupo contable'], 404)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Proceso para modificar los grupos contables en la DB
     * 
     * Nivel técnico: Actualizacion en la tabla "grupos_contables" el registro que corresponda
     * al número interno (id) enviado por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno del grupo contable a buscar en la tabla
     * @return Boolean Envía un mensaje True si se modificó el grupo contable en la tabla "grupos_contables"
     * @since versión 1.0
     * @version 1.0
     */
    public function actualizar(Request $request) {
        try {
            $grupoContable = GrupoContable::findOrFail($request->id);
            $bandera=$grupoContable->update($request->all());
            return response()->json(['resultado' => $grupoContable, 'bandera' => $bandera, 'mensaje' => 'Se ha actualizado el grupo contable'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el grupo contable'], 404)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Proceso para eliminar los grupos contables en la DB
     * 
     * Nivel técnico: Borra en la tabla "grupos_contables" el registro que corresponda
     * al número interno (id) enviado por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno del grupo contable a eliminar de la tabla
     * @return Boolean Envía un mensaje True si se eliminó el grupo contable en la tabla "grupos_contables"
     * @since versión 1.0
     * @version 1.0
     */  
    public function eliminar(Request $request) {
        try {
            $grupoContable = GrupoContable::findOrFail($request->id);
            $bandera = $grupoContable->delete();
            return response()->json(['resultado' => $grupoContable, 'bandera' => $bandera, 'mensaje' => 'Se ha eliminado el grupo contable'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el grupo contable'], 404)
                ->withHeaders(self::$headers);
        }
    }

}
