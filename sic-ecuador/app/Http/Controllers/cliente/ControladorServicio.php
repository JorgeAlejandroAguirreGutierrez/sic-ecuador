<?php

namespace App\Http\Controllers\clientes;

use Illuminate\Http\Request;
use App\cliente\Servicio;
use App\estructura\Configuracion;

/**
 * Controlador para administrar la cofiguración de los servicios en el sistema
 * 
 * Contiene un conjuto de métodos que permiten administrar la configuración de los servicios
 * para el sistema. Ej. Carga Panela, limpieza, empaque, etc.
 * 
 * @copyright (c) 2019, SICE WEB
 */
class ControladorServicio extends Controller {

    /**
     * Nivel funcional: Proceso para crear nuevos servicios en el sistema
     * 
     * Nivel técnico: Crea una instancia del modelo "Servicio", relaciona todos los campos 
     * de la tabla "servicios" con los datos del nuevo servicio recibido por parámetro 
     * y guarda en la BD a traves del modelo "Servicio".
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el nuevo servicio según los campos de la tabla "servicios"
     * @return Boolean Envía un mensaje True si se guardó el nuevo servicios en la tabla "servicios"
     * @since versión 1.0
     * @version 1.0
     */ 
    public function crear(Request $request) {
        try {
            $validacion = Validator::make($request->all(), [
                'nombre' => 'required|min:1',
                'consignacion' => 'required|boolean',
                'costo'=> 'required|costo',
            ]);
            if ($validacion->fails()) {
                return response()->json(['error' => 'Bad Request', 'bandera' => false, 'mensaje' => 'Error en validacion de parametros'], 400)
                ->withHeaders(self::$headers);
            }
            $servicio = new Servicio($request->all());
            $bandera = $servicio->save();
            return response()->json(['resultado' => $servicio, 'bandera' => $bandera, 'mensaje' => 'Se ha creado el servicio'], 201)
                ->withHeaders(self::$headers);
        } catch (Exception | QueryException $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Obtiene el código secuencial para crear un nuevo servicio en el sistema
     * 
     * Nivel técnico: Obtiene el código secuencial para crear un nuevo servicio, contando el total
     * de registros de la tabla "servicios", incrementado en 1 y concatenando con la letra "S" 
     * obtenida del Modelo Configuración.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_encode Devuelve el nuevo código de servicio
     * @since versión 1.0
     * @version 1.0
     */ 
    public function obtenerCodigoServicio() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'servicios')->where('tipo', 'CREAR')->get();
            $conteoServicios = Servicio::count();
            $conteoServicios++;
            $rellenoConteo = str_pad($conteoServicios, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 0, 2);
            return ['resultado'=>$configuracion[0]->codigo . $año . $mes . $rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Obtiene un serivio específico según el id que recibe
     * 
     * Nivel técnico: Obtiene a través del Modelo "Servicio" el registro de un servicio
     * específico según el id recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del servicio a buscar en la tabla
     * @return json_enconde Devuelve en la variable $servicio el servicio específico 
     * obtenido de la tabla "servicio" según el id recibido por parámetro, sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function obtener(Request $request) {
        try {
            $servicio = Servicio::findOrFail($request->id);
            $servicio->grupo_contable;
            $servicio->impuesto;
            $servicio->tipo;
            return response()->json(['resultado' => $servicio, 'bandera' => true, 'mensaje' => 'Se ha obtenido el servicio'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el grupo contable'], 404)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Obtiene todos los servicios creados en el sistema
     * 
     * Nivel técnico: Obtiene todos los registros de la tabla "servicios", a través
     * del Modelo "Servicio".
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_enconde Devuelve en la variable $servicios todos los registros de 
     * la tabla "servicios", sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function consultar() {
        try {
            $servicios = Servicio::all();
            return response()->json(['resultado' => $servicios, 'bandera' => true, 'mensaje' => 'Se ha consultado los servicios'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }
    
    /**
     * Nivel funcional: Proceso para actualizar un servicio registrado en el sistema
     * 
     * Nivel técnico: Actualizacion en la tabla "servicios" el registro que corresponda
     * al número interno (id) recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del servicio a buscar en la tabla
     * @return Boolean Envía un mensaje True si se modificó el servicio en la tabla "servicios"
     * @since versión 1.0
     * @version 1.0
     */
    public function actualizar(Request $request) {
        try {
            $servicio = Servicio::findOrFail($request->id);
            $servicio->update($request->all());
            $bandera = $servicio->save();
            return response()->json(['resultado' => $servicio, 'bandera' => $bandera, 'mensaje' => 'Se ha actualizado el servicio'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el servicio'], 404)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Proceso para eliminar un servicio registrado en el sistema
     * 
     * Nivel técnico: Borra de la tabla "servicios" el registro que corresponda
     * al número interno (id) enviado por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del servicio a eliminar de la tabla "servicios"
     * @return Boolean Envía un mensaje True si se eliminó el registro en la tabla "servicios", sino null.
     * @since versión 1.0
     * @version 1.0
     */
    public function eliminar(Request $request) {
        try {
            $servicio = Servicio::findOrFail($request->id);
            $bandera = $servicio->delete();
            return response()->json(['resultado' => $servicio, 'bandera' => $bandera, 'mensaje' => 'Se ha eliminado el servicio'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el servicio'], 404)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Verifica si el código de servicio es de tipo servicio (S).
     * 
     * Nivel técnico: Verifica si el código de servicio contiene la letra "S" a través de la 
     * función "strpos" que devuelve la posición del inicio del substring o false si no lo encuentra.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del servicio a verificar en la tabla
     * @return json_enconde Devuelve true si el código de servicio es de tipo servicio (S)
     * sino devuelve false.
     * @since versión 1.0
     * @version 1.0
     */
    public function esServicio(Request $request) {
        if (strpos($request->codigo, 'S') !== false) {
            return response()->json(['resultado' => true, 'bandera' => true, 'mensaje' => 'Se ha validado el servicio'], 200)
                ->withHeaders(self::$headers);
        } else {
            return response()->json(['resultado' => false, 'bandera' => true, 'mensaje' => 'Se ha validado el servicio'], 200)
                ->withHeaders(self::$headers);
        }
    }

}
