<?php

namespace App\Http\Controllers\cliente;

use Illuminate\Http\Request;
use App\cliente\GrupoCliente;
use App\configuraciones\Configuracion;
use Validator;

class ControladorGrupoCliente extends Controller
{
    private static $headers = [
        'Content-Type' => 'application/json',
        'Access-Control-Allow-Origin' => '*', 'Access-Control-Allow-Headers' => 'Origin, Content-Type, Accept',
        'Access-Control-Allow-Methods' => 'GET,HEAD,POST,PUT,DELETE,OPTIONS'
    ];

    /**
     * Nivel funcional: Proceso para crear nuevos Plazo de crédito en la BD
     * 
     * Nivel técnico: Crea una instancia del modelo GrupoCliente, relaciona todos los campos 
     * de la tabla "plazos_creditos" con los datos del nuevo plazo de crédito recibido por parámetro 
     * y guarda en la BD a traves del modelo GrupoCliente.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el nuevo plazo según los campos de la tabla "plazos_creditos"
     * @return Boolean Envía un mensaje True si se guardó el nuevo plazo en la tabla "plazos_creditos"
     * @since versión 1.0
     * @version 1.0
     */
    public function crear(Request $request)
    {
        try {
            $validacion = Validator::make($request->all(), [
                'denominacion' => 'required|min:1',
                'numero_cuenta' => 'required|min:1',
                'nombre_cuenta' => 'required|min:1'
            ]);
            if ($validacion->fails()) {
                return response()->json(['error' => 'Bad Request', 'bandera' => false, 'mensaje' => 'Error en validacion de parametros'], 400)
                ->withHeaders(self::$headers);
            }
            $grupoCliente = new GrupoCliente($request->all());
            $grupoCliente->codigo = $this->obtenerCodigo();
            $bandera = $grupoCliente->save();
            return response()->json(['resultado' => $grupoCliente, 'bandera' => $bandera, 'mensaje' => 'Se ha creado el grupo de cliente'], 201)
                ->withHeaders(self::$headers);
        } catch (Exception | QueryException $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => $bandera, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Obtiene el código secuencial para crear un nuevo plazo de crédito
     * 
     * Nivel técnico: Obtiene el código secuencial para crear un nuevo plazo de crédito, 
     * contando el total de registros de la tabla "plazos_creditos", incrementado en 1 
     * y concatenando con la letra "PC" obtenida del Modelo Configuración.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_encode Devuelve el código del nuevo plazo de crédito
     * @since versión 1.0
     * @version 1.0
     */
    public function obtenerCodigo(){
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'grupos_clientes')->where('tipo', 'CREAR')->get();
            $conteo = GrupoCliente::count();
            $conteo++;
            $rellenoConteo = str_pad($conteo, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 0, 2);
            return $configuracion[0]->codigo . $año . $mes . $rellenoConteo;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Nivel funcional: Obtiene todos los plazos de crédito creados
     * 
     * Nivel técnico: Obtiene todos los registros de la tabla "plazos_creditos", a través
     * del Modelo GrupoCliente.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_enconde Devuelve en la variable $gruposClientes todos los plazos de crédito,
     * sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function consultar()
    {
        try {
            $gruposClientes = GrupoCliente::all();
            return response()->json(['resultado' => $gruposClientes, 'bandera' => true, 'mensaje' => 'Se ha consultado los grupos de clientes'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Obtiene un plazo de crédito específico según el id que recibe
     * 
     * Nivel técnico: Obtiene a través del Modelo GrupoCliente el registro de un plazo de
     * crédito según el id recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del plazo de crédito a buscar en la tabla
     * @return json_enconde Devuelve en la variable $GrupoCliente el plazo de crédito específico 
     * obtenido de la tabla "plazos_creditos" según el id recibido por parámetro, 
     * sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function obtener(Request $request)
    {
        try {
            $GrupoCliente = GrupoCliente::findOrFail($request->id);
            return response()->json(['resultado' => $GrupoCliente, 'bandera' => true, 'mensaje' => 'Se ha obtenido el grupo de cliente'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el grupo de cliente'], 404)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Proceso para modificar un plazo de crédito en el sistema
     * 
     * Nivel técnico: Actualizacion en la tabla "plazos_creditos" el registro que corresponda
     * al número interno (id) enviado por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del plazo de crédito a buscar en la tabla
     * @return Boolean Envía un mensaje True si se modificó el plazo de crédito en la tabla "plazos_creditos"
     * @since versión 1.0
     * @version 1.0
     */
    public function actualizar(Request $request)
    {
        try {
            $GrupoCliente = GrupoCliente::findOrFail($request->id);
            $bandera = $GrupoCliente->update($request->all());
            return response()->json(['resultado' => $GrupoCliente, 'bandera' => $bandera, 'mensaje' => 'Se ha actualizado el grupo de cliente'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(), 'bandera' => false, 'mensaje' => 'No se encontro el grupo de cliente'], 404)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Proceso para eliminar un plazo de crédito en el sistema
     * 
     * Nivel técnico: Borra en la tabla "plazos_creditos" el registro que corresponda
     * al número interno (id) enviado por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del plazo de crédito a eliminar de la tabla
     * @return Boolean Envía un mensaje True si se eliminó el registro de plazo de crédito 
     * en la tabla "plazos_creditos"
     * @since versión 1.0
     * @version 1.0
     */
    public function eliminar(Request $request)
    {
        try {
            
            $GrupoCliente = GrupoCliente::findOrFail($request->id);
            $bandera = $GrupoCliente->delete();
            return response()->json(['resultado' => $GrupoCliente, 'bandera' => $bandera, 'mensaje' => 'Se ha eliminado el grupo de cliente'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(), 'bandera' => false, 'mensaje' => 'No se encontro el grupo de cliente'], 404)
                ->withHeaders(self::$headers);
        }
    }
}
