<?php

namespace App\Http\Controllers\cliente;

use Illuminate\Http\Request;
use App\cliente\DatoAdicional;
use App\estructura\Configuracion;

/**
 * Controlador para administrar los Datos Adicionales
 * 
 * Contiene un conjuto de métodos que permiten manejar los datos adicionales.
 * Los datos adicionales son las diferentes opciones que pueden ser desplegadas
 * en los cuadros de selección de las vistas del sistema. Ej. Estado civil, Forma de pago, etc.
 * 
 * @copyright (c) 2019, SICE WEB
 */
class ControladorDatoAdicional extends Controller {

    /**
     * Nivel funcional: Obtiene el código secuencial para crear un dato adicional
     * 
     * Nivel técnico: Obtiene el código secuencial para un nuevo dato adicional, contando el total
     * de registros en la tabla Datos_Adicionales, incrementado en 1 y concatenando con la letra 
     * correspondiente obtenida del Modelo Configuración.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_encode Devuelve el código del nuevo cliente
     * @since versión 1.0
     * @version 1.0
     */
    public function obtenerCodigoDatoAdicional() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'datos_adicionales')->where('tipo', 'CREAR')->get();
            $conteoDatosAdicionales = DatoAdicional::count();
            $conteoDatosAdicionales++;
            $rellenoConteo = str_pad($conteoDatosAdicionales, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 0, 2);
            return $configuracion[0]->codigo . $año . $mes . $rellenoConteo;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Nivel funcional: Proceso para crear nuevos datos Adicionales en la BD
     * 
     * Nivel técnico: Crea una instancia del modelo DatoAdicional, relaciona todos los campos 
     * de la tabla "datos_adicionales" con los datos adicionales recibido por parámetro 
     * y guarda en la BD a traves del modelo Cliente.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe los datos adicionales según los campos de la tabla datos_adicionales
     * @return Boolean Envía un mensaje True si se guardó el dato adicional en la tabla "datos_adicioneles"
     * @since versión 1.0
     * @version 1.0
     */
    public function crear(Request $request) {
        try {
            $validacion = Validator::make($request->all(), [
                'tipo' => 'required|min:1',
                'nombre' => 'required|min:1',
                'abreviatura'=> 'required|min:1',
            ]);
            if ($validacion->fails()) {
                return response()->json(['error' => 'Bad Request', 'bandera' => false, 'mensaje' => 'Error en validacion de parametros'], 400)
                ->withHeaders(self::$headers);
            }
            $datoAdicional = new DatoAdicional($request->all());
            $bandera = $datoAdicional->save();
            return response()->json(['resultado' => $datoAdicional, 'bandera' => $bandera, 'mensaje' => 'Se ha creado el dato adicional'], 201)
                ->withHeaders(self::$headers);
            } catch (Exception | QueryException $e) {
                return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                    ->withHeaders(self::$headers);
            }
    }

    /**
     * Nivel funcional: Obtiene todos los datos adicionales según el tipo requerido
     * 
     * Nivel técnico: Obtiene todos los registros de Modelo DatoAdicional que correspondan
     * al tipo enviado por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el tipo de dato adicional a buscar en la tabla
     * @return json_enconde Devuelve en la variable $datosAdicionales los datos adicionales 
     * que concuerdan con el campo tipo de la tabla "dato_adicional" solicitado por parámetro 
     * sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function obtenerTipo(Request $request) {
        try {
            $datosAdicionales = DatoAdicional::where('tipo', $request->tipo)->get();
            return response()->json(['resultado' => $datosAdicionales, 'bandera' =>true, 'mensaje' => 'Se ha creado el dato adicional'], 201)
                ->withHeaders(self::$headers);
        } catch (Exception | QueryException $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Obtiene un dato adicional específico según el id requerido
     * 
     * Nivel técnico: Obtiene el registros del dato adicional según el id requerido
     * por parámetro, obtenido a través del Modelo DatoAdicional.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno del dato adicional a buscar en la tabla
     * @return json_enconde Devuelve en la variable $datosAdicionales el dato adicional específico 
     * que concuerda con el id de la tabla "dato_adicional" solicitado por parámetro, 
     * sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function obtener(Request $request) {
        try {
            $datoAdicional = DatoAdicional::findOrFail($request->id);
            return response()->json(['resultado' => $datoAdicional, 'bandera' => true, 'mensaje' => 'Se ha obtenido el dato adicional'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el dato adicional'], 404)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Proceso para modificar los datos adicionales en la DB
     * 
     * Nivel técnico: Actualizacion en la tabla "dato_adicional" el registro que corresponda
     * al número interno (id) enviado por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno del dato adicional a buscar en la tabla
     * @return Boolean Envía un mensaje True si se modificó los datos adicionales en la tabla "dato_adicional"
     * @since versión 1.0
     * @version 1.0
     */
    public function actualizar(Request $request) {
        try {
            $datoAdicional = DatoAdicional::findOrFail($request->id);
            $datoAdicional->codigo = $request->codigo;
            $datoAdicional->tipo = $request->tipo;
            $datoAdicional->nombre = $request->nombre;
            $datoAdicional->abreviatura = $request->abreviatura;
            $bandera = $datoAdicional->save();
            return response()->json(['resultado' => $datoAdicional, 'bandera' => $bandera, 'mensaje' => 'Se ha actualizado el dato adicional'], 200)
                    ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el dato adicional'], 404)
                ->withHeaders(self::$headers);
        }
    }

    /**
     * Nivel funcional: Proceso para eliminar los datos adicionales en la DB
     * 
     * Nivel técnico: Borra en la tabla "dato_adicional" el registro que corresponda
     * al número interno (id) enviado por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno del dato adicional a buscar en la tabla
     * @return Boolean Envía un mensaje True si se eliminó el dato adicional en la tabla "dato_adicional"
     * @since versión 1.0
     * @version 1.0
     */
    public function eliminar(Request $request) {
        try {
            $datoAdicional = DatoAdicional::findOrFail($request->id);
            $bandera = $datoAdicional->delete();
            return response()->json(['resultado' => $datoAdicional, 'bandera' => $bandera, 'mensaje' => 'Se ha eliminado el dato adicional'], 200)
                    ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el dato adicional'], 404)
                ->withHeaders(self::$headers);
        }
    }

}
