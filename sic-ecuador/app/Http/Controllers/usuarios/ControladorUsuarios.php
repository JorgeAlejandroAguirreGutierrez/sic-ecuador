<?php

namespace App\Http\Controllers\usuarios;

use Illuminate\Http\Request;
use App\usuarios\Usuario;
use App\configuraciones\DatoAdicional;
use App\configuraciones\Configuracion;

class ControladorUsuarios extends Controller {

    //public function vistaLeerActualizarEliminar() {
    //    $usuarios = Usuario::all();
    //    return view('usuarios/LAEUsuarios', ['usuarios' => $usuarios]);
    //}

    //public function vistaCrear() {
    //    return view('usuarios/CUsuarios');
    //}

    //public function vistaInicioSesion() {
    //    return view('usuarios/InicioSesion');
    //}
    
    //public function vistaIndex() {
    //    return view('index');
    //}
    
    //public function vistaMenu() {
    //    return view('usuarios/Menu');
    //}

    public function obtenerCodigoUsuario() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'usuarios')->where('tipo', 'CREAR')->get();
            $conteoUsuarios = Usuario::count();
            $conteoUsuarios++;
            $rellenoConteo = str_pad($conteoUsuarios, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 0, 2);
            return ['respuesta'=>true,'resultado'=>$configuracion[0]->codigo . $año . $mes . $rellenoConteo, 'mensaje'=>'Exito al obtener el codigo de usuario'];
        } catch (Exception $e) {
            return ['respuesta'=>true,'resultado'=>null, 'mensaje'=>'Error al obtener el codigo de usuario'];
        }
    }

    public function crear(Request $request) {
        try {
            $usuario = new Usuario($request->all());
            $bandera = $usuario->save();
            return response()->json(['resultado' => $usuario, 'bandera' => $bandera, 'mensaje' => 'Se ha creado el usuario'], 201)
                ->withHeaders(self::$headers);
        } catch (Exception | QueryException $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => $bandera, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }
    
    public function actualizar(Request $request) {
        try {
            $usuario = Usuario::findOrFail($request->id);
            $bandera=$usuario->update($request->all());
            return response()->json(['resultado' => $usuario, 'bandera' => $bandera, 'mensaje' => 'Se ha actualizado el usuario'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro la retencion'], 404)
                ->withHeaders(self::$headers);
        }
    }

    public function obtenerVendedores() {
        try {
            $datoAdicional = DatoAdicional::where('tipo', 'TIPOS USUARIOS')->where('nombre', 'VENDEDOR')->firstOrFail();
            $tipo_id = $datoAdicional->id;
            $vendedores = Usuario::where('tipo_id', $tipo_id)->get();
            return response()->json(['resultado' => $vendedores, 'bandera' => true, 'mensaje' => 'Se ha obtenido los vendedores'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el vendedor'], 404)
                ->withHeaders(self::$headers);
        }
    }

    public function obtenerCajeros() {
        try {
            $datoAdicional = DatoAdicional::where('tipo', 'TIPOS USUARIOS')->where('nombre', 'CAJERO')->firstOrFail();
            $tipo_id = $datoAdicional->id;
            $cajeros = Usuario::where('tipo_id', $tipo_id)->get();
            return response()->json(['resultado' => $cajeros, 'bandera' => true, 'mensaje' => 'Se ha obtenido los cajeros'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        } catch (ModelNotFoundException $m) {
            return response()->json(['error' => $m->getMessage(),'bandera' => false, 'mensaje' => 'No se encontro el tipo cajero'], 404)
                ->withHeaders(self::$headers);
        }
    }
    
    public function consultar() {
        try {
            $usuarios = Usuario::all();
            return response()->json(['resultado' => $usuarios, 'bandera' => true, 'mensaje' => 'Se ha consultado los usuarios'], 200)
                ->withHeaders(self::$headers);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'bandera' => false, 'mensaje' => 'Error interno en el servidor'], 500)
                ->withHeaders(self::$headers);
        }
    }

    public function obtenerUsuario(Request $request) {
        try {
            $usuario = Usuario::findOrFail($request->id);
            return ['respuesta'=>true, 'resultado'=>$usuario, 'mensaje'=>'Exito al obtener el usuario'];
        } catch (Exception $e) {
            return ['respuesta'=>false, 'resultado'=>false, 'mensaje'=>'Error al obtener el usuario'];
        }
    }

    public function eliminarUsuario(Request $request) {
        try {
            $usuario = Usuario::findOrFail($request->id);
            $bandera = $usuario->delete();
            return ['respuesta'=>true, 'resultado'=>$bandera, 'mensaje'=>'Exito eliminar usuario'];
        } catch (Exception $e) {
            return ['respuesta'=>false, 'resultado'=>false, 'mensaje'=>'Error al eliminar el usuario'];
        }
    }

    public function verificarIniciarSesion() {
        session_start();
        if (isset($_SESSION['usuario'])) {
            return ['respuesta'=>true, 'resultado'=>true, 'mensaje'=>'Exito al verificar el inicio de sesion'];
        } else {
            return ['respuesta'=>false, 'resultado'=>false, 'mensaje'=>'Error al verificar el inicio de sesion'];
        }
    }

    public function iniciarSesion(Request $request) {
        try {
            $usuario = Usuario::where('identificacion', $request->identificacion)->where('contrasena', $request->contrasena)->where('activo', true)->firstOrFail();
                session_start();
                $_SESSION['usuario'] = $usuario[0];
                $link = null;
                if ($_SESSION['usuario']->tipo->nombre == 'VENDEDOR') {
                    $link = 'menu';
                } else if ($_SESSION['usuario']->tipo->nombre == 'CAJERO') {
                    $link = 'menu';
                }
                return ['respuesta'=>true, 'resultado'=>$link, 'mensaje'=>'Exito al iniciar sesion'];
        } catch (Exception $e) {
            return ['respuesta'=>false, 'resultado'=>null, 'mensaje'=>'Exito al iniciar sesion'];
        }
    }

    public function cerrarSesion() {
        $link = null;
        session_start();
        if (session_destroy()) {
            $link = '/';
        }
        return ['respuesta'=>true, 'resultado'=>$link, 'mensaje'=>'Exito al cerrar sesion'];
    }

    public function activarSesion(Request $request) {
        try {
            if ($request->contrasena == $request->repetir_contrasena) {
                $usuario = Usuario::where('identificacion', $request->identificacion)->where('contrasena', $request->contrasena)->where('correo', $request->correo)->first();
                if ($usuario == null) {
                    return ['respuesta'=>false, 'resultado'=>null, 'mensaje'=>'Error al activar la sesion'];
                } else {
                    $usuario->activo = true;
                    $usuario->save();
                    return ['respuesta'=>true, 'resultado'=>$usuario, 'mensaje'=>'Exito al activar la sesion'];
                }
            } else {
                return ['respuesta'=>false, 'resultado'=>null, 'mensaje'=>'Exito al activar la sesion'];
            }
        } catch (Exception $e) {
            return ['respuesta'=>false, 'resultado'=>null, 'mensaje'=>'Error al activar la sesion'];
        }
    }
    
    public function obtenerEstablecimientoUsuario() {
        try {
            session_start();
            $usuario=$_SESSION['usuario'];
            $establecimiento=$usuario->puntoVenta->establecimiento;
            return ['respuesta'=>true, 'resultado'=>$establecimiento, 'mensaje'=>'Exito al obtener el establecimiento de usuario'];
        } catch (Exception $e) {
            return ['respuesta'=>false, 'resultado'=>null, 'mensaje'=>'Error al obtener el establecimiento de usuario'];
        }
    }
    
    public function obtenerPuntoVentaUsuario() {
        try {
            session_start();
            $usuario=$_SESSION['usuario'];
            $puntoVenta=$usuario->puntoVenta;
            return ['respuesta'=>true, 'resultado'=>$puntoVenta, 'mensaje'=>'Exito al obtener el punto de venta de usuario'];
        } catch (Exception $e) {
            return ['respuesta'=>false, 'resultado'=>null, 'mensaje'=>'Error al obtener el punto de venta de usuario'];
        }
    } 

}
