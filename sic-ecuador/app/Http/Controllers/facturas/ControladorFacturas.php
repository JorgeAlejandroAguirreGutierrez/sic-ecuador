<?php

namespace App\Http\Controllers\facturas;

use Illuminate\Http\Request;
use App\facturas\CabeceraFactura;
use App\facturas\LineaFactura;
use App\estructuras\Configuracion;
use App\estructuras\Establecimiento;
use App\estructuras\PuntoVenta;
use App\clientes\Cliente;
use App\clientes\Auxiliar;
use App\inventarios\Bodega;
use App\clientes\Transportista;
use App\clientes\VehiculoTransporte;
use App\usuarios\Usuario;
use App\inventarios\Producto;
use App\clientes\Servicio;

/**
 * Controlador para administrar las facturas en el sistema
 * 
 * Contiene un conjunto de métodos que permiten administrar las facturas de los
 * clientes en el sistema.
 * 
 * @copyright (c) 2019, SICE WEB
 */
class ControladorFacturas extends Controller {

    /**
     * Nivel funcional: Presenta la pantalla para crear nuevas facturas en el sistema
     * 
     * Nivel técnico: Invoca a la vista "facturas/crearFacturas"
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return View Invoca a la vista "crearFacturas"
     * @since versión 1.0
     * @version 1.0
     */  
    public function vistaCrear() {
        $cabeceraFactura=new CabeceraFactura();
        //$cabecera->guia_remision = new Guiaremision(); //No implementada
        $cabeceraFactura->punto_venta=new PuntoVenta();
        //$cabecera->punto_venta_gr=new PuntoVenta(); //Que es gr
        $cabeceraFactura->cliente=new cliente();
        $cabeceraFactura->auxiliar=new Auxiliar();
        $cabeceraFactura->bodega=new Bodega();
        $cabeceraFactura->transportista=new Transportista();
        $cabeceraFactura->vehiculo_transporte=new VehiculoTransporte();
        //$cabecera->vendedor=new Vendedor(); //No implementada
        $cabeceraFactura->cajero=new Usuario(); //El cajero es el usuario?
        $lineasFactura=new LineaFactura();
        $lineasFactura->producto=new Producto();
        $lineasFactura->servicio=new Servicio();
        return view('facturas/CAFacturas', ['cabeceraFactura' => $cabeceraFactura, 'lineasFactura' => $lineasFactura]);
    }
    
    /**
     * Nivel funcional: Presenta la pantalla para modificar la cabecera de una factura ingresada en el sistema
     * 
     * Nivel técnico: Obtiene a traves del modelo "CabeceraFactura", una factura específica 
     * ingresada en la BD y envía esta factura a la vista "facturas/modificarFacturas" a través 
     * de las variables $cabecerasFacturas y $lineasFactura.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) de la factura a buscar en las tablas
     * "cabeceras_facturas" y "lineas_facturas".
     * @return View Invoca a la vista "modificarFacturas" y pasa las variables $cabeceraFactura y $lineasFactura.
     * @since versión 1.0
     * @version 1.0
     */
    public function vistaActualizar(Request $request)
    {
        $cabeceraFactura=CabeceraFactura::find($request->id);
        $lineasFactura=LineaFactura::where('cabecera_factura_id', $request->id)->get();
        return view('facturas/CAFacturas', ['cabeceraFactura' => $cabeceraFactura, 'lineasFactura' => $lineasFactura]);
    }
    
    /**
     * Nivel funcional: Muestra el listado de todas las facturas registradas en el sistema
     * 
     * Nivel técnico: Obtiene a traves del modelo "CabeceraFactura", el listado de todas las facturas 
     * registradas en la BD y envía estos datos a la vista "facturas/mostrarFacturas" a través 
     * de la variable $cabecerasFacturas.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return View Invoca a la vista "mostrarFacturas" y pasa la variable $cabecerasFacturas.
     * @since versión 1.0
     * @version 1.0
     */
    public function vistaLeerEliminar() {
        $cabecerasFacturas = CabeceraFactura::all();
        return view('facturas/LEFacturas', ['cabecerasFacturas' => $cabecerasFacturas]);
    }
    
    /**
     * Nivel funcional: Obtiene el código secuencial para crear una nueva factura en el sistema
     * 
     * Nivel técnico: Obtiene el código secuencial para crear una nueva factura, contando el total
     * de registros de la tabla "cabeceras_factura", incrementado en 1 y concatenando el codigo de
     * establecimiento y punto de venta obtenidas por parametro con la letra "FAC" obtenida del Modelo Configuración.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Return $cabeceraFactura Recibe la cabecera de la factura, de la cual se tomará el codigo de 
     * establecimiento y punto de venta para concatenarlo al nuevo código interno de la factura.
     * @return json_encode Devuelve el código interno para la nueva factura.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function obtenerCodigoInterno($cabeceraFactura) {
        date_default_timezone_set('America/Bogota');
        $configuracion = Configuracion::where('tabla', 'cabeceras_facturas')->where('tipo', 'CREAR')->get();
        $conteoFacturas = CabeceraFactura::count();
        $conteoFacturas++;
        $rellenoConteo = str_pad($conteoFacturas, 6, '0', STR_PAD_LEFT);
        $fecha = date("m.d.y");
        $año = substr($fecha, 6, 2);
        $mes = substr($fecha, 0, 2);
        $establecimiento = $cabeceraFactura->puntoVenta->establecimiento;
        $puntoVenta = $cabeceraFactura->puntoVenta;
        return ['resultado'=>$configuracion[0]->codigo . $establecimiento->codigo . $puntoVenta->codigo . $año . $mes . $rellenoConteo];
    }
    
    /**
     * Nivel funcional: Devuelve la ciudad y fecha para la cabecera de la factura
     * 
     * Nivel técnico: Obtiene la ciudad y la fecha en la que inició sesión el usuario del sistema.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_encode Devuelve la ciudad y la fecha en la que el usuario inició sesión.
     * @since versión 1.0
     * @version 1.0
     */  
    public function obtenerFecha() {
        date_default_timezone_set('America/Bogota');
        $fecha = date("m.d.y");
        $año = substr($fecha, 6, 2);
        $mes = substr($fecha, 0, 2);
        $dia = substr($fecha, 3, 2);
        session_start();
        $establecimiento = $_SESSION['usuario']->puntoVenta->establecimiento;
        return ['resultado'=>$establecimiento->ubicaciongeo->canton . ', ' . $dia . '/' . $mes . '/' . $año];
    }
    
    /**
     * Nivel funcional: Obtiene la fecha actual para la cabecera de la factura
     * 
     * Nivel técnico: Obtiene la fecha actual en formato año, mes y día para la cabecera de la factura.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_encode Devuelve en la variable $fecha la fecha actual en formato año, mes y día.
     * @since versión 1.0
     * @version 1.0
     */   
    public function obtenerFechaCabeceraFactura(){
        try {
            date_default_timezone_set('America/Bogota');
            $fecha = date("Y-m-d");
            return ['resultado'=>$fecha];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Devuelve el siguiente código de factura en un punto de venta
     * 
     * Nivel técnico: A través del Modelo "CabecaraFactura", cuenta todos los registros de 
     * la tabla "cabeceras_facturas" que correspondan al punto de venta recibido por parametro,
     * incrementa en 1 de devuelve el nuevo código de factura. 
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Return $cabeceraFactura Recibe la cabecera de la factura que contiene el punto de venta a buscar en la tabla.
     * @return json_enconde Devuelve en la variable $rellenoConteo el siguiente codigo de factura
     * según el punto de venta recibido por parámetro.
     * @since versión 1.0
     * @version 1.0
     */
    public function obtenerCodigo($cabeceraFactura) {
        try {
            $conteoFacturas = CabeceraFactura::where('punto_venta_id', $cabeceraFactura->punto_venta_id)->count();
            $conteoFacturas++;
            $rellenoConteo = str_pad($conteoFacturas, 15, '0', STR_PAD_LEFT);
            return ['resultado'=>$rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Devuelve el código de establecimiento o sucursal
     * 
     * Nivel técnico: Obtiene el código del establecimiento que corresponda al punto de venta 
     * con el que el usuario inició sesión y lo concatena con el texto "CD-".
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_enconde Devuelve el código del establecimiento en que se inició sesión.
     * @since versión 1.0
     * @version 1.0
     */  
    public function obtenerSucursal() {
        try {
            session_start();
            $establecimiento = $_SESSION['usuario']->puntoVenta->establecimiento;
            return ['resultado'=>'CD-' . $establecimiento->codigo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Obtiene el código secuencial para crear una nueva guia de remisión en el sistema
     * 
     * Nivel técnico: Obtiene el código secuencial para crear una nueva guia de remisión, contando el total
     * de registros de la tabla "cabeceras_factura", incrementado en 1 y concatenando el codigo de
     * establecimiento y punto de venta obtenidas por parametro con la letra "GR" obtenida del Modelo "Configuración".
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $cabeceraFactura Recibe la cabecera de la factura, de la cual se tomará el codigo de 
     * establecimiento y punto de venta para concatenarlo al nuevo código para la guia de remisión.
     * @return json_encode Devuelve el código interno para la nueva guia de remisión.
     * @since versión 1.0
     * @version 1.0
     */
    public function obtenerGuiaRemision($cabeceraFactura) {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'guias_remisiones')->where('tipo', 'CREAR')->get();
            $conteoFacturas = CabeceraFactura::count();
            $conteoFacturas++;
            $rellenoConteo = str_pad($conteoFacturas, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 0, 2);
            $establecimiento = $cabeceraFactura->puntoVentaGR->establecimiento;
            $puntoVenta = $cabeceraFactura->puntoVentaGR;
            return ['resultado'=>$configuracion[0]->codigo . $establecimiento->codigo . $puntoVenta->codigo . $año . $mes . $rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Crea o actualiza la cabecera de una factura
     * 
     * Nivel técnico: Verifica si existe el código de factura recibido por parámetro, 
     * si es nulo crea la cabecera de la factura, sino permite modificar la cabecera existente.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe la cabecera de la factura
     * @return json_enconde Devuelve la cabecera de la factura creada o modificada.
     * @since versión 1.0
     * @version 1.0
     */
    public function crearCabeceraFactura(Request $request) {
        try {
            session_start();
            if ($request->codigo == null) {
                $cabeceraFactura = new CabeceraFactura($request->all());
                $cabeceraFactura->punto_venta_id = $_SESSION['usuario']->punto_venta_id;
                $cabeceraFactura->codigo = $this->obtenerCodigo($cabeceraFactura);
                $cabeceraFactura->codigo_interno = $this->obtenerCodigoInterno($cabeceraFactura);
                $cabeceraFactura->fecha= $this->obtenerFechaCabeceraFactura();            
                $cabeceraFactura->save();
                return ['resultado'=>$cabeceraFactura];
            } else {
                $cabeceraFactura = CabeceraFactura::where('codigo_interno',$request->codigo_interno)->get();
                $cabeceraFactura->fecha= $this->obtenerFechaCabeceraFactura();
                $cabeceraFactura[0]->auxiliar = $request->auxiliar;
                $cabeceraFactura[0]->auxiliar_id = $request->auxiliar_id;
                $cabeceraFactura[0]->descarga_inventario = $request->descarga_inventario;
                $cabeceraFactura[0]->guia_remision = $request->guia_remision;
                $cabeceraFactura[0]->factura_anulada = $request->factura_anulada;
                $cabeceraFactura[0]->punto_venta_id = $request->punto_venta_id;
                $cabeceraFactura[0]->cliente_id = $request->cliente_id;
                $cabeceraFactura[0]->bodega_id = $request->bodega_id;
                $cabeceraFactura[0]->transportista_id = $request->transportista_id;
                $cabeceraFactura[0]->vehiculo_transporte_id = $request->vehiculo_transporte_id;
                $cabeceraFactura[0]->vendedor_id = $request->vendedor_id;
                $cabeceraFactura[0]->cajero_id = $request->cajero_id;
                $cabeceraFactura[0]->save();
                return ['resultado'=>$cabeceraFactura[0]];
            }
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Crea o actualiza la linea de la factura
     * 
     * Nivel técnico: Verifica si existe lineas de factura previas y guarda la nueva linea de la factura
     * recibida por parámetro en la sigueinte posición.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe la cabecera de la factura con si id interno.
     * @return json_enconde Devuelve true si se guardo la linea de la factura.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function crearLineaFactura(Request $request) {
        try {
            $lineaFactura = LineaFactura::where('linea', $request->linea)->where('cabecera_factura_id', $request->cabecera_factura_id)->get();
            if (count($lineaFactura) == 0) {
                $lineaFactura = new LineaFactura($request->all());
                $bandera = $lineaFactura->save();
                return ['resultado'=>$bandera];
            }
            else{
                $lineaFactura[0]->descarga_inventario=$request->descarga_inventario;
                $lineaFactura[0]->cantidad=$request->cantidad;
                $lineaFactura[0]->descuento=$request->descuento;
                $lineaFactura[0]->total=$request->total;
                $lineaFactura[0]->producto_id=$request->producto_id;
                $lineaFactura[0]->servicio_id=$request->servicio_id;
                $bandera =$lineaFactura[0]->save();
                return ['resultado'=>$bandera];
            }
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Obtiene una cabecera de factura específica según el id de la factura que recibe
     * 
     * Nivel técnico: Obtiene a través del Modelo "CabeceraFactura" la cabecera de una factura
     * específica según el id de factura recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) de la factura a buscar la cabecera en la tabla
     * @return json_enconde Devuelve en la variable $cabecerafactura los datos de una cabecera de una factura
     * específica, obtenida de la tabla "cabeceras_facturas" según el id de factura recibido por parámetro, sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function obtenerCabeceraFactura(Request $request) {
        try {
            $cabeceraFactura = CabeceraFactura::find($request->id);
            $cabeceraFactura->puntoVenta;
            $cabeceraFactura->vendedor;
            $cabeceraFactura->cajero;
            return ['resultado'=>$cabeceraFactura];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Obtiene una linea de una factura específica según el id de linea que recibe
     * 
     * Nivel técnico: Obtiene a través del Modelo "LineaFactura" una la linea de una factura
     * específica, según el id de linea recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) de la linea de la factura a buscar en la tabla
     * @return json_enconde Devuelve en la variable $lineaFactura los datos de una linea de una factura
     * específica, obtenida de la tabla "lineas_facturas" según el id de linea recibido por parámetro, sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function obtenerLineaFactura(Request $request) {
        try {
            $lineaFactura = LineaFactura::find($request->id);
            return ['resultado'=>$lineaFactura];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Devuelve todas las lineas de una factura específica según el id que recibe
     * 
     * Nivel técnico: Obtiene a través del Modelo "LineaFactura" todas las lineas que correspondan a
     * una factura específica, según el id de cabecera recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) de la cabecera de la factura a buscar en la tabla
     * @return json_enconde Devuelve en la variable $lineasFactura todas las lineas de una factura
     * específica, obtenida de la tabla "lineas_facturas" según el id de cabecera recibido por parámetro, sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function obtenerLineasFactura(Request $request) {
        try {
            $lineasFactura = LineaFactura::where('cabecera_factura_id',$request->cabecera_factura_id);
            return ['resultado'=>$lineasFactura];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /** REVISAR:
     * Nivel funcional: Proceso para calcular los nuevos valores de la factura, cuando se realiza un 
     * descuento en el valor total de la factura.
     * 
     * Nivel técnico: Recibe el registro con el nuevo valor de descuento y aplica este descuento a cada
     * uno de los productos individuales.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el registro con el valor total ha descuentar en la factura
     * @return json_enconde Devuelve en la variable $resultado el nuevo valor aplicado el descuento, sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */  
    public function cambiarTotalDescuentoPS(Request $request) {
        try {
            $cantidad = $request->cantidad;
            $totalDescuento = $request->total_descuento;
            $totalCantidad = $request->total_cantidad;
            $resultado = $cantidad * $totalDescuento / $totalCantidad;
            return ['resultado'=>$resultado];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /** REVISAR:
     * Nivel funcional: Proceso para calcular los nuevos valores de la factura, cuando se realiza un 
     * descuento en porcentaje al valor total de la factura.
     * 
     * Nivel técnico: Recibe el registro con el nuevo porcentaje de descuento y aplica este porcentaje a cada
     * uno de los productos individuales.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el registro con el nuevo porcentaje total de descuento
     * @return json_enconde Devuelve en la variable $resultado el nuevo precio de los productos
     * aplicando el porcentaje de descuento recibido por parámetro, sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function cambiarPorcentajeDescuentoPS(Request $request) {
        try {
            $precioPS = $request->precio_ps;
            $descuentoPS = $request->descuento_ps;
            $cantidad = $request->cantidad;
            $precio = $precioPS * $cantidad;
            $resultado = ($descuentoPS * 100) / $precio;
            return ['resultado'=>$resultado];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /** REVISAR:
     * Nivel funcional: Proceso para calcular los nuevos valores de la linea de factura, cuando se realiza un 
     * descuento en porcentaje al valor individual por cada linea de la factura.
     * 
     * Nivel técnico: Recibe el registro con el nuevo porcentaje de descuento y aplica este porcentaje a la 
     * linea que contiene un producto específico.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el registro con el nuevo porcentaje individual de descuento
     * @return json_enconde Devuelve en la variable $resultado el nuevo precio de el producto específico
     * aplicando el porcentaje de descuento recibido por parámetro, sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function cambiarDescuentoPS(Request $request) {
        try {
            $precioPS = $request->precio_ps;
            $cantidad = $request->cantidad;
            $porcentajeDescuentoPS = $request->porcentaje_descuento;
            $precio = $precioPS * $cantidad;
            $resultado = ($porcentajeDescuentoPS * 100) / $precio;
            return ['resultado'=>$resultado];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /** REVISAR:
     * Nivel funcional: Proceso para calcular los nuevos valores de la factura, cuando se realiza un 
     * descuento en el valor individual de un producto en la factura.
     * 
     * Nivel técnico: Recibe el registro con el nuevo valor de descuento y aplica este descuento al
     * productos específico.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el registro con el valor ha descontar del producto individual.
     * @return json_enconde Devuelve en la variable $resultado el nuevo valor del producto individual 
     * una vez aplicado el descuento, sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function cambiarTotalPS(Request $request) {
        try {
            $cantidad = $request->cantidad;
            $precioPS = $request->precio_ps;
            $total = $cantidad * $precioPS;
            $resultado = round($total, 2);
            return ['resultado'=>$resultado];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /** REVISAR:
     * Nivel funcional: Proceso para calcular el nuevo porcentaje de descuento, cuando se realiza un 
     * cambio en el valor neto de un producto en la factura.
     * 
     * Nivel técnico: Recibe el registro con el nuevo valor neto, el valor neto es el resultado de
     * multiplicar la cantidad por el precio menos el descuento, realiza los cálculos para determinar
     * el porcentaje que corresponde el descuento aplicado a un producto específico.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el registro con el nuevo valor neto del producto individual.
     * @return json_enconde Devuelve en la variable $resultado el nuevo valor neto del producto individual 
     * una vez aplicado el descuento, sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function cambiarTotalNetoPS(Request $request) {
        try {
            $cantidad = $request->cantidad;
            $precioPS = $request->precio_ps;
            $descuentoPS = $request->descuento_ps;
            $totalNeto = ($cantidad * $precioPS) - $descuentoPS;
            $resultado = round($totalNeto, 2);
            return ['resultado'=>$resultado];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    /**
     * Nivel funcional: Proceso para eliminar una factura registrada en el sistema
     * 
     * Nivel técnico: Elimina de la tabla "cabeceras_facturas" y "lineas_facturas" en caso de existir,
     * los registros que correspondan al número interno (id) recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) de la factura a eliminar de las tablas
     * "cabeceras_factura" y "lineas_facturas".
     * @return Boolean Envía un mensaje True si se eliminó el registro de la factura en la BD, false 
     * si no encontró el registro y null si hubo error al eliminar.
     * @since versión 1.0
     * @version 1.0
     */    
    function eliminar(Request $request) {
        try {
        $cabeceraFactura=CabeceraFactura::find($request->id);
        $lineasFactura=LineaFactura::where('cabecera_factura_id', $request->id)->get();            
        if ((count($cabeceraFactura) == 0) and (count($lineasFactura) == 0)) {
            $bandera=false;
        } elseif ((count($lineasFactura) == 0)) {
                $bandera=$cabeceraFactura->delete();
            } else {
                $lineasFactura->delete();
                $bandera=$cabeceraFactura->delete(); // Consideración: no puede haber lineas sin cabecera
            }
        return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }    
}
