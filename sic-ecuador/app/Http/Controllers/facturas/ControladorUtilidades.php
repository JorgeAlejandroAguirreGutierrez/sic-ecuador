<?php

namespace App\Http\Controllers\facturas;

use Illuminate\Http\Request;
use App\clientes\Servicio;
use App\inventarios\Producto;

class ControladorUtilidades extends Controller
{
    //
    public function obtenerPS(Request $request)
    {
        if (strpos($request->codigo, 'P') !== false)
        {
            $producto= Producto::find($request->id);
            $producto->impuesto;
            $producto->tipo;
            return ['resultado'=>$producto];
        }
        else
        {
            $servicio= Servicio::find($request->id);
            $servicio->impuesto;
            $servicio->tipo;
            return ['resultado'=>$servicio];
        }
    }
}
