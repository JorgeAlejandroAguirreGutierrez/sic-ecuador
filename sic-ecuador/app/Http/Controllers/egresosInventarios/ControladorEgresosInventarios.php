<?php

namespace App\Http\Controllers\egresosInventarios;

use Illuminate\Http\Request;
use App\egresosInventarios\CabeceraEgresoInventario;
use App\egresosInventarios\LineaEgresoInventario;
use App\estructuras\Configuracion;
use App\estructuras\Establecimiento;
use App\clientes\Cliente;

class ControladorEgresosInventarios extends Controller {

    public function vistaCrearActualizar(Request $request) {
        $cabeceraEgresoInventario = CabeceraEgresoInventario::find($request->id);
        $lineasEgresoInventario = LineaEgresoinventario::where('cabecera_egreso_inventario_id', $request->id)->get();
        return view('facturas/CUEgresosInventarios', ['cabeceraEgresoInventario' => $cabeceraEgresoInventario, 'lineasEgresoInventario' => $lineasEgresoInventario]);
    }

    public function vistaLeerEliminar() {
        $cabecerasEgresosInventarios = CabeceraEgresoInventario::all();
        return view('egresosInventarios/LEEgresosInventarios', ['cabecerasEgresosInventarios' => $cabecerasEgresosInventarios]);
    }

    public function obtenerCodigoInterno($cabeceraEgresoInventario) {
        date_default_timezone_set('America/Bogota');
        $configuracion = Configuracion::where('tabla', 'cabeceras_egresos_inventarios')->where('tipo', 'CREAR')->get();
        $conteoEgresosInventarios = CabeceraEgresoInventario::count();
        $conteoEgresosInventarios++;
        $rellenoConteo = str_pad($conteoEgresosInventarios, 6, '0', STR_PAD_LEFT);
        $fecha = date("m.d.y");
        $año = substr($fecha, 6, 2);
        $mes = substr($fecha, 0, 2);
        $establecimiento = $cabeceraEgresoInventario->puntoVenta->establecimiento;
        $puntoVenta = $cabeceraEgresoInventario->puntoVenta;
        return ['resultado'=>$configuracion[0]->codigo . $establecimiento->codigo . $puntoVenta->codigo . $año . $mes . $rellenoConteo];
    }

    public function obtenerFecha() {
        try {
            date_default_timezone_set('America/Bogota');
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 0, 2);
            $dia = substr($fecha, 3, 2);
            session_start();
            $establecimiento = $_SESSION['usuario']->puntoVenta->establecimiento;
            return ['resultado'=>$establecimiento->ubicaciongeo->canton . ', ' . $dia . '/' . $mes . '/' . $año];
        } catch (Exception $e) {
           return ['resultado'=>null];
        }
    }

    public function obtenerFechaEgresoInventario() {
        try {
            date_default_timezone_set('America/Bogota');
            $fecha = date("Y-m-d");
            return ['resultado'=>$fecha];
        } catch (Exception $e) {
           return ['resultado'=>null];
        }
    }

    public function obtenerCodigo($cabeceraEgresoInventario) {
        try {
            $conteoEgresosInventarios = CabeceraEgresoInventario::where('punto_venta_id', $cabeceraEgresoInventario->punto_venta_id)->count();
            $conteoEgresosInventarios++;
            $rellenoConteo = str_pad($conteoEgresosInventarios, 15, '0', STR_PAD_LEFT);
            return ['resultado'=>$rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerGuiaRemision($cabeceraEgresoInventario) {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'guias_remisiones')->where('tipo', 'CREAR')->get();
            $conteoEgresosInventarios = CabeceraEgresoInventario::count();
            $conteoEgresosInventarios++;
            $rellenoConteo = str_pad($conteoEgresosInventarios, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 0, 2);
            $establecimiento = $cabeceraEgresoInventario->puntoVentaGR->establecimiento;
            $puntoVenta = $cabeceraEgresoInventario->puntoVentaGR;
            return ['resultado'=>$configuracion[0]->codigo . $establecimiento->codigo . $puntoVenta->codigo . $año . $mes . $rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function crearCabeceraEgresoInventario(Request $request) {
        try {
            session_start();
            if ($request->codigo == null) {
                $cabeceraEgresoInventario = new CabeceraEgresoInventario($request->all());
                $cabeceraEgresoInventario->punto_venta_id = $_SESSION['usuario']->punto_venta_id;
                $cabeceraEgresoInventario->codigo = $this->obtenerCodigo($cabeceraEgresoInventario);
                $cabeceraEgresoInventario->codigo_interno = $this->obtenerCodigoInterno($cabeceraEgresoInventario);
                $cabeceraEgresoInventario->fecha = $this->obtenerFechaEgresoInventario();
                $cabeceraEgresoInventario->save();
                return ['resultado'=>$cabeceraEgresoInventario];
            } else {
                $cabeceraEgresoInventario = CabeceraEgresoInventario::where('codigo_interno', $request->codigo_interno)->first();
                $cabeceraEgresoInventario->fecha = $this->obtenerFechaCabeceraFactura();
                $cabeceraEgresoInventario->auxiliar = $request->auxiliar;
                $cabeceraEgresoInventario->auxiliar_id = $request->auxiliar_id;
                $cabeceraEgresoInventario->guia_remision = $request->guia_remision;
                $cabeceraEgresoInventario->egreso_inventario_anulada = $request->egreso_inventario_anulada;
                $cabeceraEgresoInventario->punto_venta_id = $request->punto_venta_id;
                $cabeceraEgresoInventario->cliente_id = $request->cliente_id;
                $cabeceraEgresoInventario->bodega_id = $request->bodega_id;
                $cabeceraEgresoInventario->transportista_id = $request->transportista_id;
                $cabeceraEgresoInventario->vehiculo_transporte_id = $request->vehiculo_transporte_id;
                $cabeceraEgresoInventario->vendedor_id = $request->vendedor_id;
                $cabeceraEgresoInventario->cajero_id = $request->cajero_id;
                $cabeceraEgresoInventario->save();
                return ['resultado'=>$cabeceraEgresoInventario];
            }
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function crearLineaEgresoInventario(Request $request) {
        try {
            $lineaEgresoInventario = LineaEgresoInventario::where('linea', $request->linea)->where('cabecera_egreso_inventario_id', $request->cabecera_egreso_inventario_id)->first();
            if ($lineaEgresoInventario == null) {
                $lineaEgresoInventario = new LineaEgresoInventario($request->all());
                $bandera = $lineaEgresoInventario->save();
                ['resultado'=>$bandera];
            } else {
                $lineaEgresoInventario->cantidad = $request->cantidad;
                $lineaEgresoInventario->descuento = $request->descuento;
                $lineaEgresoInventario->total = $request->total;
                $lineaEgresoInventario->producto_id = $request->producto_id;
                $lineaEgresoInventario->servicio_id = $request->servicio_id;
                $bandera = $lineaEgresoInventario->save();
                return ['resultado'=>$bandera];
            }
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerCabeceraEgresoInventario(Request $request) {
        try {
            $cabeceraEgresoInventario = CabeceraEgresoInventario::find($request->id);
            $cabeceraEgresoInventario->puntoVenta;
            $cabeceraEgresoInventario->vendedor;
            $cabeceraEgresoInventario->cajero;
            return ['resultado'=>$cabeceraEgresoInventario];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerLineaEgresoInventario(Request $request) {
        try {
            $lineaEgresoInventario = LineaEgresoInventario::find($request->id);
            echo ['resultado'=>$lineaEgresoInventario];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerLineasEgresoInventario(Request $request) {
        try {
            $lineasEgresoInventario = LineaEgresoInventario::where('cabecera_egreso_inventario_id', $request->cabecera_egreso_inventario_id);
            return ['resultado'=>$lineasEgresoInventario];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerConsumoSaldoPrepago(Request $request) {
        try {
            $cliente = Cliente::find($request->id);
            $consumo = 0;
            $saldo = 0;
            $resultado = array('consumo' => $consumo, 'saldo' => $saldo);
            return ['resultado'=>$resultado];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function obtenerConsumoSaldoPospago(Request $request) {
        try {
            $cliente = Cliente::find($request->id);
            $consumo = 0;
            $saldo = 0;
            $resultado = array('consumo' => $consumo, 'saldo' => $saldo);
            return ['resultado'=>$resultado];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function cambiarTotalDescuentoPS(Request $request) {
        try {
            $cantidad = $request->cantidad;
            $totalDescuento = $request->total_descuento;
            $totalCantidad = $request->total_cantidad;
            $resultado = $cantidad * $totalDescuento / $totalCantidad;
            return ['resultado'=>$resultado];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function cambiarPorcentajeDescuentoPS(Request $request) {
        try {
            $precioPS = $request->precio_ps;
            $descuentoPS = $request->descuento_ps;
            $cantidad = $request->cantidad;
            $precio = $precioPS * $cantidad;
            $resultado = ($descuentoPS * 100) / $precio;
            return ['resultado'=>$resultado];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function cambiarDescuentoPS(Request $request) {
        try {
            $precioPS = $request->precio_ps;
            $cantidad = $request->cantidad;
            $porcentajeDescuentoPS = $request->porcentaje_descuento;
            $precio = $precioPS * $cantidad;
            $resultado = ($porcentajeDescuentoPS * 100) / $precio;
            return ['resultado'=>$resultado];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function cambiarTotalPS(Request $request) {
        try {
            $cantidad = $request->cantidad;
            $precioPS = $request->precio_ps;
            $total = $cantidad * $precioPS;
            $resultado = round($total, 2);
            return ['resultado'=>$resultado];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

    public function cambiarTotalNetoPS(Request $request) {
        try {
            $cantidad = $request->cantidad;
            $precioPS = $request->precio_ps;
            $descuentoPS = $request->descuento_ps;
            $totalNeto = ($cantidad * $precioPS) - $descuentoPS;
            $resultado = round($totalNeto, 2);
            return ['resultado'=>$resultado];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

}
