<?php

namespace App\Http\Controllers\inventarios;

use Illuminate\Http\Request;
use App\inventarios\Bodega;
use App\estructuras\Configuracion;

/**
 * Controlador para administrar las bodegas en el sistema
 * 
 * Contiene un conjuto de métodos que permiten administrar las bodegas, asigna un 
 * código a cada bodega de forma secuencial en el sistema. Ej. B1903000004, etc.
 * 
 * @copyright (c) 2019, SICE WEB
 */
class ControladorBodegas extends Controller {
    
    /**
     * Nivel funcional: Muestra el listado de todos las bodegas definidas en el sistema
     * 
     * Nivel técnico: Obtiene a traves del modelo "Bodega", el listado de todas las bodegas
     * registradas en la BD y envía estos a la vista "mostrarBodegas" a través de la variable $bodegas.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return View Invoca a la vista "mostrarBodegas" y pasa la variable $bodegas.
     * @since versión 1.0
     * @version 1.0
     */
    public function vistaLeerActualizarEliminar() {
        $bodegas = Bodega::all();
        return view('inventarios/LAEBodegas', ['bodegas' => $bodegas]);
    }
    
    /**
     * Nivel funcional: Presenta la pantalla para crear Bodegas
     * 
     * Nivel técnico: Invoca a la vista "inventarios/crearBodegas"
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return View Invoca a la vista "crearBodegas"
     * @since versión 1.0
     * @version 1.0
     */
    public function vistaCrear() {
        return view('inventarios/CBodegas');
    }

    /**
     * Nivel funcional: Proceso para crear nuevas bodegas en el sistema
     * 
     * Nivel técnico: Crea una instancia del modelo "Bodega", relaciona todos los campos 
     * de la tabla "bodegas" con los datos de la nueva bodega recibida por parámetro 
     * y guarda en la BD a traves del modelo "Bodega".
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe la nueva bodega que contiene los campos de la tabla "bodegas"
     * @return Boolean Envía un mensaje True si se guardó la nueva bodega en la tabla "bodegas"
     * @since versión 1.0
     * @version 1.0
     */ 
    public function crear(Request $request) {
        try {
            $bodega = new Bodega($request->all());
            $bandera = $bodega->save();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }
    
    /**
     * Nivel funcional: Obtiene el nuevo código secuencial para crear una bodega en el sistema
     * 
     * Nivel técnico: Devuelve el código secuencial para crear una nueva bodega en el sistema,
     * contando el total de registros de la tabla "bodegas", incrementado en 1 y concatenando
     * con la letra "B" obtenida del Modelo "Configuracion".
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_encode Devuelve el nuevo código de la bodega a crear.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function obtenerCodigoBodega() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'bodegas')->where('tipo', 'CREAR')->get();
            $conteoBodegas = Bodega::count();
            $conteoBodegas++;
            $rellenoConteo = str_pad($conteoBodegas, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 0, 2);
            return ['resultado'=>$configuracion[0]->codigo . $año . $mes . $rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Obtiene todas las bodega creadas en el sistema
     * 
     * Nivel técnico: Devuelve todos los registros de la tabla "bodegas", a través
     * del Modelo "Bodega".
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_enconde Devuelve en la variable $bodegas todos los registros de la
     * tabla "bodegas", sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function obtenerBodegas() {
        try {
            $bodegas = Bodega::all();
            return ['resultado'=>$bodegas];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Obtiene una bodega específica según el id que recibe
     * 
     * Nivel técnico: Obtiene a través del Modelo "Bodega" el registro de una bodega
     * específica según el id recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) de la bodega a buscar en la tabla
     * @return json_enconde Devuelve en la variable $bodega los datos de una bodega específica, 
     * obtenida de la tabla "bodegas" según el id recibido por parámetro, sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function obtenerBodega(Request $request) {
        try {
            $bodega = Bodega::find($request->id);
            return ['resultado'=>$bodega];
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }
    
    /**
     * Nivel funcional: Proceso para eliminar una bodega específica en el sistema
     * 
     * Nivel técnico: Borra de la tabla "bodegas" el registro que corresponda
     * al número interno (id) enviado por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del registro a eliminar de la tabla
     * @return Boolean Envía un mensaje True si se eliminó el registro de la tabla "bodegas".
     * @since versión 1.0
     * @version 1.0
     */ 
    public function eliminarBodega(Request $request) {
        try {
            $bodega = Bodega::find($request->id);
            $bandera = $bodega->delete();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }
    
    /**
     * Nivel funcional: Obtiene la bodega asignada al usuario que inició sesión en el sistema
     * 
     * Nivel técnico: Devuelve todos los registros de la tabla "bodegas", a través
     * del Modelo "Bodega".
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_enconde Devuelve en la variable $bodegas todos los registros de la
     * tabla "bodegas", sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function obtenerBodegasUsuario() {
        try {
            session_start();
            $puntoVenta=$_SESSION['usuario']->puntoVenta;
            $bodegas = Bodega::where('punto_venta_id', $puntoVenta->id)->get();
            return ['resultado'=>$bodegas];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }

}
