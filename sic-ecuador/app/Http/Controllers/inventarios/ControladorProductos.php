<?php

namespace App\Http\Controllers\inventarios;

use Illuminate\Http\Request;
use App\inventarios\Producto;
use App\estructuras\Configuracion;

/**
 * Controlador para administrar la cofiguración de los productos en el sistema
 * 
 * Contiene un conjuto de métodos que permiten administrar la configuración de los productos
 * para el sistema. Ej. Arroz, cantidad, precio, medida, bodega, etc.
 * 
 * @copyright (c) 2019, SICE WEB
 */
class ControladorProductos extends Controller {
    
    /**
     * Nivel funcional: Muestra el listado de todos los productos definidos en el sistema
     * 
     * Nivel técnico: Obtiene a traves del modelo "Producto", el listado de todos los productos
     * registradas en la BD y envía estos a la vista "mostrarProductos" a través de la variable $productos.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return View Invoca a la vista "inventarios/mostrarProductos" y pasa la variable $productos.
     * @since versión 1.0
     * @version 1.0
     */
    public function vistaMostrar() {
        $productos = Producto::all();
        return view('inventarios/LAEProductos', ['productos' => $productos]);
    }
    
    /**
     * Nivel funcional: Presenta la pantalla para crear Productos
     * 
     * Nivel técnico: Invoca a la vista "inventarios/crearProductos"
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return View Invoca a la vista "crearProductos"
     * @since versión 1.0
     * @version 1.0
     */ 
    public function vistaCrear() {
        return view('inventarios/CProductos');
    }
    
    /**
     * Nivel funcional: Proceso para crear nuevos productos en el sistema
     * 
     * Nivel técnico: Crea una instancia del modelo "Producto", relaciona todos los campos 
     * de la tabla "productos" con los datos del nuevo producto recibido por parámetro 
     * y guarda en la BD a traves del modelo "Producto".
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el nuevo producto que contiene los campos de la tabla "productos"
     * @return Boolean devuelve un mensaje True si se guardó el nuevo producto en la tabla "productos", sino false.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function crear(Request $request) {
        try {
            $producto = new Producto($request->all());
            $bandera = $producto->save();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }
    
    /**
     * Nivel funcional: Obtiene el código secuencial para crear un nuevo producto en el sistema
     * 
     * Nivel técnico: Devuelve el código secuencial para crear un nuevo producto, contando el total
     * de registros de la tabla "productos", incrementado en 1 y concatenando con las letras "PRO" 
     * obtenida del Modelo "Configuración".
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_encode Devuelve el nuevo código de producto
     * @since versión 1.0
     * @version 1.0
     */ 
    public function obtenerCodigoProducto() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'productos')->where('tipo', 'CREAR')->get();
            $conteoProductos = Producto::count();
            $conteoProductos++;
            $rellenoConteo = str_pad($conteoProductos, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 0, 2);
            return ['resultado'=>$configuracion[0]->codigo . $año . $mes . $rellenoConteo];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Obtiene todos los productos creados en el sistema
     * 
     * Nivel técnico: Devuelve todos los registros de la tabla "productos", a través
     * del Modelo "Producto".
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param ()
     * @return json_enconde Devuelve en la variable $productos todos los registros de 
     * la tabla "productos", sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */     
    public function obtenerProductos() {
        try {
            $productos = Producto::all();
            return $productos;
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Obtiene un producto específico según el id que recibe
     * 
     * Nivel técnico: Obtiene a través del Modelo "Producto" el registro de un producto
     * específico según el id recibido por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del producto a buscar en la tabla
     * @return json_enconde Devuelve en la variable $producto el registro del producto específico 
     * obtenido de la tabla "productos" según el id recibido por parámetro, sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function obtenerProducto(Request $request) {
        try {
            $producto = Producto::find($request->id);
            $producto->medida;
            $producto->tipo;
            $producto->impuesto;
            return ['resultado'=>$producto];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Proceso para eliminar un producto registrado en el sistema
     * 
     * Nivel técnico: Borra de la tabla "productos" el registro que corresponda
     * al número interno (id) enviado por parámetro.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del producto a eliminar de la tabla "productos"
     * @return Boolean Devuelve un mensaje True si se eliminó el registro en la tabla "productos", sino false.
     * @since versión 1.0
     * @version 1.0
     */ 
    public function eliminarProducto(Request $request) {
        try {
            $producto = Producto::find($request->id);
            $bandera = $producto->delete();
            return ['resultado'=>$bandera];
        } catch (Exception $e) {
            return ['resultado'=>false];
        }
    }
    
    /**
     * Nivel funcional: Obtiene los productos asignados a una bodega específica
     * 
     * Nivel técnico: Devuelve todos los registros de la tabla "productos", que fueron 
     * asignados a una bodega específica, a través del Modelo "Producto".
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) de la bodega a buscar de la tabla "productos"
     * @return json_enconde Devuelve en la variable $productoss todos los registros de la
     * tabla "productos" que corresponden a una bodega específica, sino devuelve null.
     * @since versión 1.0
     * @version 1.0
     */
    public function obtenerProductosBodega(Request $request) {
        try {
            $productos = Producto::where('bodega_id', $request->bodega_id)->get();
            return ['resultado'=>$productos];
        } catch (Exception $e) {
            return ['resultado'=>null];
        }
    }
    
    /**
     * Nivel funcional: Verifica si el código de producto es de tipo producto (P).
     * 
     * Nivel técnico: Verifica si el código de producto contiene la letra "P" a través de la 
     * función "strpos" que devuelve la posición del inicio del substring o false si no lo encuentra.
     * 
     * @author Alejandro Aguirre <alejoved@gmail.com>; Jorge Hidalgo <gato_sta@hotmail.com>
     * @param Request $request Recibe el número interno (id) del producto a verificar su código
     * @return json_enconde Devuelve true si el código de producto es de tipo producto (P)
     * sino devuelve false.
     * @since versión 1.0
     * @version 1.0
     */
    public function esProducto(Request $request) {
        if (strpos($request->codigo, 'P') !== false) {
            return ['resultado'=>true];
        } else {
            return ['resultado'=>false];
        }
    }

}
