<?php

namespace App\cliente;

use Illuminate\Database\Eloquent\Model;
use App\estructuras\Configuracion;

class Telefono extends Model
{
    //
    protected $table='telefonos';
    protected $fillable=['codigo', 'numero', 'cliente_id'];
    
    public function cliente()
    {
        return $this->belongsTo('App\clientes\Cliente', 'cliente_id', 'id');
    }
    
    public function obtenerCodigoTelefono() {
        try {
            date_default_timezone_set('America/Bogota');
            $configuracion = Configuracion::where('tabla', 'telefonos')->where('tipo', 'CREAR')->get();
            $conteo = Telefono::count();
            $conteo++;
            $rellenoConteo = str_pad($conteo, 6, '0', STR_PAD_LEFT);
            $fecha = date("m.d.y");
            $año = substr($fecha, 6, 2);
            $mes = substr($fecha, 0, 2);
            return $configuracion[0]->codigo . $año . $mes . $rellenoConteo;
        } catch (Exception $e) {
            return null;
        }
    }
}
