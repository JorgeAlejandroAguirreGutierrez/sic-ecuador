<?php

namespace App\cliente;

use Illuminate\Database\Eloquent\Model;

class PlazoCredito extends Model
{
    protected $table='plazos_creditos';
    protected $fillable=['codigo', 'plazo'];
    
    
    public function plazosCreditos()
    {
        return $this->hasMany('App\clientes\Financiamiento', 'plazo_credito_id');
    }
}
