<?php

namespace App\cliente;

use Illuminate\Database\Eloquent\Model;

class CategoriaCliente extends Model
{
    //
    protected $table='categoria_clientes';
    protected $fillable=['codigo', 'categoria', 'abreviatura'];

    public function categoriaClientes()
    {
        return $this->hasMany('App\clientes\Cliente', 'categoria_cliente_id');
    }
    
}
