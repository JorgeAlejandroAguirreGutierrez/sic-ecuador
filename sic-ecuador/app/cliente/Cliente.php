<?php

namespace App\cliente;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    //
    protected $table='clientes';
    protected $fillable=['codigo', 'identificacion', 'razon_social', 'estado', 'eliminado', 'empresa_id',
        'grupo_contable_id', 'tipo_contribuyente_id', 'direccion_id', 'financiamiento_id', 'genero_id', 
        'estado_civil_id','categoria_cliente_id', 'origen_ingreso_id' ];

    public function empresa()
    {
        return $this->belongsTo('App\configuraciones\Empresa');
    }

    public function grupoCliente()
    {
        return $this->belongsTo('App\clientes\GrupoCliente');
    }
    
    public function tipoContribuyente()
    {
        return $this->belongsTo('App\clientes\TipoContribuyente');
    }
    
    public function direccion()
    {
        return $this->belongsTo('App\clientes\Direccion');
    }
    
    public function financiamiento()
    {
        return $this->belongsTo('App\clientes\Financiamiento');
    }
    
    public function genero()
    {
        return $this->belongsTo('App\clientes\Genero');
    }
    
    public function estadoCivil()
    {
        return $this->belongsTo('App\clientes\EstadoCivil');
    }
    
    public function categoriaCliente()
    {
        return $this->belongsTo('App\clientes\CategoriaCliente');
    }
    
    public function origenIngreso()
    {
        return $this->belongsTo('App\clientes\OrigenIngreso');
    }
 
    public function auxiliares()
    {
        return $this->hasMany('App\clientes\Auxiliar', 'cliente_id');
    }
        
    public function telefonos()
    {
        return $this->hasMany('App\clientes\Telefono', 'cliente_id');
    }

    public function celulares()
    {
        return $this->hasMany('App\clientes\Celular','cliente_id');
    }

    public function correos()
    {
        return $this->hasMany('App\clientes\Correo','cliente_id');
    }  

    public function retenciones()
    {
        return $this->hasMany('App\clientes\Retencion', 'cliente_id');
    }

    public function facturas()
    {
        return $this->hasMany('App\comprobantes\Factura', 'cliente_id');
    }

    public function clientes_facturas()
    {
        return $this->hasMany('App\comprobantes\Factura', 'cliente_factura_id');
    }    

    public function egresos()
    {
        return $this->hasMany('App\comprobantes\Egreso', 'cliente_id');
    }

    public function pedidos()
    {
        return $this->hasMany('App\comprobantes\Pedido', 'cliente_id');
    }

    public function guias_remisiones()
    {
        return $this->hasMany('App\entregas\guias_remisiones', 'cliente_id');
    }    
}
