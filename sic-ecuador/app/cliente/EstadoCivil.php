<?php

namespace App\cliente;

use Illuminate\Database\Eloquent\Model;

class EstadoCivil extends Model
{
    //
    protected $table='estados_civiles';
    protected $fillable=['codigo', 'estado_civil', 'abreviatura'];

    public function estadosCiviles()
    {
        return $this->hasMany('App\clientes\Cliente', 'estado_civil_id');
    }
       
}
