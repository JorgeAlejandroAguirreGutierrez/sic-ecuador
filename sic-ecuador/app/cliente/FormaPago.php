<?php

namespace App\cliente;

use Illuminate\Database\Eloquent\Model;

class FormaPago extends Model
{
    //
    protected $table='formas_pagos';
    protected $fillable=['codigo', 'descripcion', 'abreviatura'];

 
    public function tiposPagos()
    {
        return $this->hasMany('App\clientes\Financiamiento', 'forma_pago_id');
    }
        
}
