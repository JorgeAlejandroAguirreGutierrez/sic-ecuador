<?php

namespace App\cliente;

use Illuminate\Database\Eloquent\Model;

class Financiamiento extends Model
{
    //
    protected $table='financiamientos';
    protected $fillable=['codigo', 'monto', 'tipo_pago_id', 'forma_pago_id', 'plazo_credito_id'];

    public function tipoPago()
    {
        return $this->belongsTo('App\clientes\TipoPago');
    }

    public function formaPago()
    {
        return $this->belongsTo('App\clientes\FormaPago');
    }

    public function plazoCredito()
    {
        return $this->belongsTo('App\clientes\PlazoCredito');
    }
    
    public function clientes()
    {
        return $this->hasOne('App\clientes\Cliente', 'financiamiento_id');
    }  
        
}
