<?php

namespace App\cliente;

use Illuminate\Database\Eloquent\Model;

class OrigenIngreso extends Model
{
    //
    protected $table='origen_ingresos';
    protected $fillable=['codigo', 'origen_ingreso', 'abreviatura'];

    public function categoriaClientes()
    {
        return $this->hasMany('App\clientes\Cliente', 'origen_ingreso_id');
    }
    
}
