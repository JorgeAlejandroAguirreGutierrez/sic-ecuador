<?php

namespace App\cliente;

use Illuminate\Database\Eloquent\Model;

class TipoContribuyente extends Model
{
    //
    protected $table='tipos_contribuyentes';
    protected $fillable=['codigo', 'tipo', 'subtipo', 'especial'];
    
    public function clientes()
    {
        return $this->hasMany('App\clientes\Cliente');
    }
}
