<?php

namespace App\cliente;

use Illuminate\Database\Eloquent\Model;

class TipoPago extends Model
{
    //
    protected $table='tipos_pagos';
    protected $fillable=['codigo', 'descripcion', 'abreviatura'];

 
    public function tiposPagos()
    {
        return $this->hasMany('App\clientes\Financiamiento', 'tipo_pago_id');
    }
    
}
