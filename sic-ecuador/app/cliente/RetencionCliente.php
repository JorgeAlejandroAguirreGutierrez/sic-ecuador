<?php

namespace App\cliente;

use Illuminate\Database\Eloquent\Model;

class RetencionCliente extends Model
{
    //
    protected $table='retenciones_clientes';
    protected $fillable=['codigo','cliente_id','tipo_retencion_id'];
    
    public function cliente()
    {
        return $this->belongsTo('App\clientes\Cliente');
    }
    
    public function tipoRetencion()
    {
        return $this->belongsTo('App\configuraciones\TipoRetencion');
    }
}
