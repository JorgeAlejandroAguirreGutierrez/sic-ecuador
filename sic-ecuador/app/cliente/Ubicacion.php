<?php

namespace App\cliente;

use Illuminate\Database\Eloquent\Model;

class Ubicacion extends Model
{
    //
    protected $table='ubicaciones';
    protected $fillable=['codigo', 'codigo_norma', 'provincia', 'canton', 'parroquia'];
    
    public function clientes()
    {
        return $this->hasMany('App\clientes\Ubicaciongeo');
    }
    
}
