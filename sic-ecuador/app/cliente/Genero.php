<?php

namespace App\cliente;

use Illuminate\Database\Eloquent\Model;

class Genero extends Model
{
    //
    protected $table='generos';
    protected $fillable=['codigo', 'sexo', 'abreviatura'];

    public function generos()
    {
        return $this->hasMany('App\clientes\Cliente', 'genero_id');
    }
    
}
