<?php

namespace App\cliente;

use Illuminate\Database\Eloquent\Model;

class GrupoCliente extends Model
{
    //
    protected $table='grupos_clientes';
    protected $fillable=['codigo', 'denominacion', 'numero_cuenta', 'nombre_cuenta', 'cliente_relacionado'];
    
    public function clientes()
    {
        return $this->hasMany('App\clientes\Cliente', 'grupo_cliente_id');
    }
    
}
