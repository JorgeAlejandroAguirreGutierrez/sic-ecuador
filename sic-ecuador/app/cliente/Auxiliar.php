<?php

namespace App\cliente;

use Illuminate\Database\Eloquent\Model;

class Auxiliar extends Model
{
    //
    protected $table='auxiliares';
    protected $fillable=['codigo','razon_social','estado', 'eliminado', 'cliente_id', 'direccion_id'];
    
    public function cliente()
    {
        return $this->belongsTo('App\clientes\Cliente');
    }
    
    public function direccion()
    {
        return $this->belongsTo('App\clientes\Direccion');
    }

    public function telefonos()
    {
        return $this->hasMany('App\clientes\Telefono', 'cliente_id');
    }

    public function celulares()
    {
        return $this->hasMany('App\clientes\Celular','cliente_id');
    }

    public function correos()
    {
        return $this->hasMany('App\clientes\Correo','cliente_id');
    }  
    
}
