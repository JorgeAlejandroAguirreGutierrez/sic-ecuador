<?php

namespace App\cliente;

use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{
    //
    protected $table='direcciones';
    protected $fillable=['codigo', 'direccion', 'latitudgeo', 'longitudgeo','ubicacion_id'];

    public function ubicacion()
    {
        return $this->belongsTo('App\configuraciones\Ubicacion');
    }
    
    public function clientes()
    {
        return $this->hasOne('App\clientes\Cliente', 'direccion_id');
    }  
    
    public function auxiliares()
    {
        return $this->hasOne('App\clientes\Cliente', 'direccion_id');
    } 
    
}
