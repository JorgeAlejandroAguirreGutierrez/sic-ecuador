<?php

namespace App\comprobantes;

use Illuminate\Database\Eloquent\Model;

class Proforma extends Model
{
    //
    protected $table='proformas';
    protected $fillable=['codigo', 'numero_interno', 'fecha', 'fecha_caducidad', 'estado', 'subtotal', 'subdescuento', 'base_iva', 'base_0', 'importe_iva','total', 
                        'descuento_porcentaje', 'descuento', 'comentario', 'cliente_id', 'vendedor_id'];
    
    public function cliente()
    {
        return $this->belongsTo('App\clientes\Cliente', 'cliente_id', 'id');
    }
    
    public function vendedor()
    {
        return $this->belongsTo('App\usuarios\Usuario', 'vendedor_id', 'id');
    }

    public function comprobantesDetalles()
    {
        return $this->hasMany('App\comprobantes\ComprobanteDetalle', 'comprobante_id');
    }
   
}
