<?php

namespace App\comprobantes;

use Illuminate\Database\Eloquent\Model;

class TipoProducto extends Model
{
    //
    protected $table='tipos_productos';
    protected $fillable=['codigo', 'descripcion', 'nombre_tabla'];
    
    public function comprobantesDetalles()
    {
        return $this->hasMany('App\comprobantes\ComprobanteDetalle', 'tipo_producto_id');
    }     
}
