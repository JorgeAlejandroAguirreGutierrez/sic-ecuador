<?php

namespace App\comprobantes;

use Illuminate\Database\Eloquent\Model;

class ComprobanteDetalle extends Model
{
    //
    protected $table='comprobantes_detalles';
    protected $fillable=['posicion','entregado', 'cantidad', 'descuento_porcentaje', 'descuento', 'total', 
                        'tipo_comprobante_id', 'comprobante_id', 'tipo_producto_id', 'producto_id'];
    
    public function tipoComprobante()
    {
        return $this->belongsTo('App\comprobantes\TipoComprobante', 'tipo_comprobante_id', 'id');
    }
    
    public function comprobanteFactura()
    {
        return $this->belongsTo('App\comprobantes\Factura', 'comprobante_id', 'id');
    }
    
    public function comprobanteProforma()
    {
        return $this->belongsTo('App\comprobantes\Proforma', 'comprobante_id', 'id');
    }

    public function comprobantePedido()
    {
        return $this->belongsTo('App\comprobantes\Pedido', 'comprobante_id', 'id');
    }

    public function comprobanteEgreso()
    {
        return $this->belongsTo('App\comprobantes\Egreso', 'comprobante_id', 'id');
    }
    
    public function tipoProducto()
    {
        return $this->belongsTo('App\comprobantes\TipoProducto', 'tipo_producto_id', 'id');
    }

    public function productoMercaderia()
    {
        return $this->belongsTo('App\inventarios\Mercaderia', 'producto_id', 'id');
    }
    
    public function productoServicio()
    {
        return $this->belongsTo('App\inventarios\Servicio', 'producto_id', 'id');
    }

    public function productoActivoFijo()
    {
        return $this->belongsTo('App\inventarios\ActivoFijo', 'producto_id', 'id');
    }    
}
