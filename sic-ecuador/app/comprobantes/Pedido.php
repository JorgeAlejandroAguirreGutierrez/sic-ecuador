<?php

namespace App\comprobantes;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    //
    protected $table='pedidos';
    protected $fillable=['codigo', 'numero_interno', 'fecha', 'fecha_entrega', 'estado', 'subtotal', 'subdescuento', 'base_iva', 'base_0', 'importe_iva','total', 
                        'descuento_porcentaje', 'descuento', 'comentario', 'cliente_id', 'vendedor_id'];
    
    public function cliente()
    {
        return $this->belongsTo('App\clientes\Cliente', 'cliente_id', 'id');
    }
    
    public function vendedor()
    {
        return $this->belongsTo('App\usuarios\Usuario', 'vendedor_id', 'id');
    }

    public function comprobantesDetalles()
    {
        return $this->hasMany('App\comprobantes\ComprobanteDetalle', 'comprobante_id');
    }
    
    public function recaudaciones()
    {
        return $this->hasMany('App\recaudaciones\Recaudacion', 'comprobante_id');
    }    
}
