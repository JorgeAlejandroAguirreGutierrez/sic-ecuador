<?php

namespace App\comprobantes;

use Illuminate\Database\Eloquent\Model;

class TipoComprobante extends Model
{
    //
    protected $table='tipos_comprobantes';
    protected $fillable=['codigo', 'descripcion', 'nombre_tabla'];
    
    public function comprobantesDetalles()
    {
        return $this->hasMany('App\comprobantes\ComprobanteDetalle', 'tipo_comprobante_id');
    }
    
    public function guias_remisiones()
    {
        return $this->hasMany('App\entregas\guias_remisiones', 'tipo_comprobante_id');
    }    
}
