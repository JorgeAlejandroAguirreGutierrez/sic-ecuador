<?php

namespace App\comprobantes;

use Illuminate\Database\Eloquent\Model;

class Egreso extends Model
{
    //
    protected $table='egresos';
    protected $fillable=['codigo', 'numero_interno', 'fecha', 'fecha_entrega', 'estado', 'subtotal', 'subdescuento', 'base_iva', 'base_0', 'importe_iva','total', 
                        'descuento_porcentaje', 'descuento', 'abono', 'comentario', 'cliente_id', 'sesion_id', 'vendedor_id'];
    
    public function cliente()
    {
        return $this->belongsTo('App\clientes\Cliente', 'cliente_id', 'id');
    }
    
    public function sesion()
    {
        return $this->belongsTo('App\usuarios\Sesion', 'sesion_id', 'id');
    }
    
    public function vendedor()
    {
        return $this->belongsTo('App\usuarios\Usuario', 'vendedor_id', 'id');
    }
    
    public function guias_remisiones()
    {
        return $this->hasMany('App\entregas\guias_remisiones', 'comprobante_id');
    }
    
    public function asientos()
    {
        return $this->hasMany('App\contabilizaciones\Asiento', 'comprobante_id');
    }    

    public function comprobantesDetalles()
    {
        return $this->hasMany('App\comprobantes\ComprobanteDetalle', 'comprobante_id');
    }
}
