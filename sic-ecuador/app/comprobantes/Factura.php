<?php

namespace App\comprobantes;

use Illuminate\Database\Eloquent\Model;

class CabeceraFactura extends Model
{
    //
    protected $table='facturas';
    protected $fillable=['codigo', 'numero', 'fecha','estado', 'subtotal', 'subdescuento', 'base_iva', 'base_0', 'importe_iva','total', 
                        'descuento_porcentaje', 'descuento', 'comentario', 'cliente_id', 'cliente_factura_id', 'auxiliar_id', 'sesion_id', 'vendedor_id'];
    
    public function cliente()
    {
        return $this->belongsTo('App\clientes\Cliente', 'cliente_id', 'id');
    }
    
    public function cliente_factura()
    {
        return $this->belongsTo('App\clientes\Cliente', 'cliente_factura_id', 'id');
    }

    public function auxiliar()
    {
        return $this->belongsTo('App\clientes\Auxiliar', 'auxiliar_id', 'id');
    }
    
    public function sesion()
    {
        return $this->belongsTo('App\usuarios\Sesion', 'sesion_id', 'id');
    }
    
    public function vendedor()
    {
        return $this->belongsTo('App\usuarios\Usuario', 'vendedor_id', 'id');
    }

    public function comprobantesDetalles()
    {
        return $this->hasMany('App\comprobantes\ComprobanteDetalle', 'comprobante_id');
    }
    
    public function retencionesVentas()
    {
        return $this->hasMany('App\comprobantes\RetencionVenta', 'factura_id');
    }
    
    public function guias_remisiones()
    {
        return $this->hasMany('App\entregas\guias_remisiones', 'comprobante_id');
    }
    
    public function recaudaciones()
    {
        return $this->hasMany('App\recaudaciones\Recaudacion', 'comprobante_id');
    }
}


