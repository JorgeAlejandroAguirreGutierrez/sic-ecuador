/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/*
 * COMBOBOX DE SELECCION DEL CLIENTE POR NOMBRE O RUC
 * CODIGO DEL CLIENTE, NOMBRE DEL CLIENTE, IDENTIFICACION NO MODIFICACION
 * DIRECCION, PROVINCIA, CANTON, Y PARROQUIA SI SON MODIFICABLES, AUTOMATICAMENTE SE MODIFICA EN CLIENTES
 */
$(function ()
{
    var cliente_id = null;
    $(document).ready(function ()
    {
        obtenerClientes();
    });

    //<editor-fold defaultstate="collapsed" desc="Evento que permite crear un auxiliar de consumo final">
    $("#crearAuxiliarCF").on("click", function ()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), cliente_id: cliente_id};
        var resultado = data.resultado;
        if (resultado) {
            window.location.href = "/clientes/mostrar";
        } else {
            alert('ERROR AL CREAR UN AUXILIAR DE CONSUMO FINAL');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener todos los cliente">
    function obtenerClientes()
    {
        $('#razon_socialB').editableSelect();
        $('#identificacionB').editableSelect();
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerClientesNoAuxiliaresCF');
        if (resultado != null) {
            for (var i = 0; i < resultado.length; i++)
            {
                $('#razon_socialB').editableSelect('add', function () {
                    $(this).val(resultado[i].id);
                    $(this).text(resultado[i].razon_social);
                });

                $('#identificacionB').editableSelect('add', function () {
                    $(this).val(resultado[i].id);
                    $(this).text(resultado[i].identificacion);
                });
            }
        } else {
            alert('ERROR AL OBTENER LOS CLIENTES QUE NO SON DE CONSUMO FINAL');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener el cliente">
    function obtenerCliente(cliente_id)
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: cliente_id};
        var resultado = peticion(data, obtenerCliente);
        if (resultado != null) {
            $('#auxiliarCF').empty();
            var tr = $('<tr>');
            var td1 = $('<td>', {text: resultado.codigo});
            var td2 = $('<td>', {text: resultado.razon_social});
            var td3 = $('<td>', {text: resultado.direccion});
            var td4 = $('<td>', {text: resultado.direccion});
            var td5 = $('<td>', {text: resultado.ubicaciongeo.provincia});
            var td6 = $('<td>', {text: resultado.ubicaciongeo.canton});
            var td7 = $('<td>', {text: resultado.ubicaciongeo.parroquia});
            tr.append(td1);
            tr.append(td2);
            tr.append(td3);
            tr.append(td4);
            tr.append(td5);
            tr.append(td6);
            tr.append(td7);
            $('#auxiliarCF').append(tr);
        } else {
            alert('ERROR AL OBTENER EL CLIENTE');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento de cambio de razon social para obtener el cliente">
    $('#razon_socialB').editableSelect().on('select.editable-select', function (e, li) {
        var id = li.val();
        cliente_id = id;
        obtenerCliente(cliente_id);
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento de cambio de identificacion para obtener el cliente">
    $('#identificacionB').editableSelect().on('select.editable-select', function (e, li) {
        var id = li.val();
        cliente_id = id;
        obtenerCliente(cliente_id);
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/clientes/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});


