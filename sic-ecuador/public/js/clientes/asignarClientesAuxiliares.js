/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function ()
{
    var cliente_id = null;
    $(document).ready(function ()
    {
        obtenerClientes();
    });

    //<editor-fold defaultstate="collapsed" desc="Evento para la asignacion de un cliente auxiliar">
    $("#asignarClienteAuxiliar").on("click", function ()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: cliente_id};
        var resultado = peticion(data, 'asignarClienteAuxiliar');
        if (resultado) {
            alert('ASIGNADO EL CLIENTE AUXILIAR');
        } else {
            alert('ERROR AL ASIGNAR EL CLIENTE AUXILIAR');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener todos los clientes">
    function obtenerClientes()
    {
        $('#razon_socialB').editableSelect();
        $('#identificacionB').editableSelect();
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerClientesNoAuxiliares');
        if (resultado != null) {
            for (var i = 0; i < resultado.length; i++)
            {
                $('#razon_socialB').editableSelect('add', function () {
                    $(this).val(resultado[i].id);
                    $(this).text(resultado[i].razon_social);
                });

                $('#identificacionB').editableSelect('add', function () {
                    $(this).val(resultado[i].id);
                    $(this).text(resultado[i].identificacion);
                });
            }
        } else {
            alert('ERROR AL OBTENER LOS CLIENTES NO AUXILIARES');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener el cliente">
    function obtenerCliente(cliente_id)
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: cliente_id};
        var resultado = peticion(data, 'obtenerCliente');
        if (resultado != null) {
            $('#clienteAuxiliar').empty();
            var tr = $('<tr>');
            var td1 = $('<td>', {text: resultado.codigo});
            var td2 = $('<td>', {text: resultado.razon_social});
            var td3 = $('<td>', {text: resultado.direccion});
            var td4 = $('<td>', {text: resultado.direccion});
            var td5 = $('<td>', {text: resultado.ubicaciongeo.provincia});
            var td6 = $('<td>', {text: resultado.ubicaciongeo.canton});
            var td7 = $('<td>', {text: resultado.ubicaciongeo.parroquia});
            tr.append(td1);
            tr.append(td2);
            tr.append(td3);
            tr.append(td4);
            tr.append(td5);
            tr.append(td6);
            tr.append(td7);
            $('#clienteAuxiliar').append(tr);
        } else {
            alert('ERROR AL OBTENER EL CLIENTE');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento al cambiar la razon social para obtener el cliente correspondiente">
    $('#razon_socialB').editableSelect().on('select.editable-select', function (e, li) {
        var id = li.val();
        cliente_id = id;
        obtenerCliente(cliente_id);
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento al cambiar la identificacion para obtener el cliente correspondiente">
    $('#identificacionB').editableSelect().on('select.editable-select', function (e, li) {
        var id = li.val();
        cliente_id = id;
        obtenerCliente(cliente_id);
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion) {
        var respuesta;
        $.ajax({
            type: "POST",
            url: '/clientes/' + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});