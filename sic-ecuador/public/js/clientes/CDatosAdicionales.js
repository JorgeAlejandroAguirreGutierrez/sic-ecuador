/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function ()
{
    $(document).ready(function ()
    {
        //obtenerCodigoDatoAdicional();
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener el codigo para el dato adicional">
    function obtenerCodigoDatoAdicional()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCodigoDatoAdicional');
        if (resultado != null) {
            $('#codigo').val(resultado);
        } else {
            alert('ERROR AL OBTENER EL CODIGO PARA EL DATO ADICIONAL');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para crear el dato adicional">
    $("#crearDatoAdicional").on("click", function ()
    {
        var codigo = $('#codigo').val().toUpperCase();
        var tipo = $('#tipo').val().toUpperCase();
        var nombre = $('#nombre').val().toUpperCase();
        var abreviatura = $('#abreviatura').val().toUpperCase();
        if (nombre === '') {
            $('#nombre').addClass('is-invalid');
            $('#invalido_nombre').text('Error en el campo');
        }
        if (abreviatura === '') {
            $('#abreviatura').addClass('is-invalid');
            $('#invalido_abreviatura').text('Error en el campo');
        }
        if (nombre !== '' && abreviatura !== '') {
            var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, tipo: tipo,
                nombre: nombre, abreviatura: abreviatura};
            var resultado = peticion(data, 'crearDatoAdicional');
            if (resultado) {
                window.location.href = "/datosadicionales/crear";
            } else {
                alert('ERROR AL CREAR EL DATO ADICIONAL');
            }
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion) {
        var respuesta;
        $.ajax({
            type: "POST",
            url: '/datosadicionales/' + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});
