/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function ()
{
    $(document).ready(function ()
    {
        obtenerTiposUsuarios();
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion pra obtener tipos de usuarios">
    function obtenerTiposUsuarios()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), tipo: 'USUARIO'};
        var resultado = peticion(data, 'obtenerDatosAdicionales', 'datosadicionales');
        for (var i = 0; i < resultado.length; i++){
            var opcion = $('<option>', {value: resultado[i].id, text: resultado[i].codigo});
            $('#tipo').append(opcion);
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para crear un usuario">
    $("#crearUsuario").on("click", function ()
    {
        var codigo = $('#codigo').val().toUpperCase();
        var nombre = $('#nombre').val().toUpperCase();
        var contrasena = $('#contrasena').val();
        var identificacion = $('#identificacion').val();
        var tipo = $('select[name=tipo]').val();

        if (nombre === '') {
            $('#nombre').addClass('is-invalid');
            $('#invalido_nombre').text('Error en el campo');
        }
        if (contrasena === '') {
            $('#contrasena').addClass('is-invalid');
            $('#invalido_contrasena').text('Error en el campo');
        }
        if (identificacion === '') {
            $('#identificacion').addClass('is-invalid');
            $('#invalido_identificacion').text('Error en el campo');
        }

        if (nombre !== '' && contrasena !== '' && identificacion !== '') {
            var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo,
                nombre: nombre, contrasena: contrasena, identificacion: identificacion, tipo: tipo};
            var resultado = peticion(data, 'crearUsuario');
            if (resultado) {
                $('#mensaje').addClass('alert-success');
                $('#mensaje').html('Se ha creado un usuario correctamente');
                $('#respuesta').modal();
                limpiarFormulario('formulario');
            } else {
                $('#mensaje').addClass('alert-danger');
                $('#mensaje').html('Error al crear un usuario');
                $('#respuesta').modal();
            }
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/usuarios/')
    {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});

