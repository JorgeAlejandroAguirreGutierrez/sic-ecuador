/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function ()
{

    $(document).ready(function ()
    {
        obtenerCodigoGrupoContable();
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener el codigo de grupo de cliente o de grupo de servicio">
    function obtenerCodigoGrupoContable()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCodigoGrupoCliente');
        if (resultado != null) {
            var codigo = $('select[name=codigo]');
            codigo.append($('<option>', {text: resultado, value: resultado}));
        } else {
            alert('ERROR AL OBTENER EL CODIGO PARA EL GRUPO DE CLIENTE');
        }
        var resultado = peticion(data, 'obtenerCodigoGrupoServicio');
        if (resultado != null) {
            var codigo = $('select[name=codigo]');
            codigo.append($('<option>', {text: resultado, value: resultado}));
        } else {
            alert('ERROR AL OBTENER EL CODIGO PARA EL GRUPO DE CSERVICIO')
        }
    }
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Evento para crear el grupo contable">
    $("#crearGrupoContable").on("click", function ()
    {
        var codigo = $('select[name=codigo]').val();
        var denominacion = $('#denominacion').val().toUpperCase();
        var numero_cuenta = $('#numero_cuenta').val().toUpperCase();
        var nombre_cuenta = $('#nombre_cuenta').val().toUpperCase();
        var cliente_relacionado = $('#cliente_relacionado').prop('checked');
        if (cliente_relacionado) {
            cliente_relacionado = 1;
        } else {
            cliente_relacionado = 0;
        }
        if (denominacion === '') {
            $('#denominacion').addClass('is-invalid');
            $('#invalido_denominacion').text('Error en el campo denominacion');
        }
        if (numero_cuenta === '') {
            $('#numero_cuenta').addClass('is-invalid');
            $('#invalido_numero_cuenta').text('Error en el campo numero de cuenta');
        }
        if (nombre_cuenta === '') {
            $('#nombre_cuenta').addClass('is-invalid');
            $('#invalido_nombre_cuenta').text('Error en el campo nombre de cuenta');
        }
        if (denominacion !== '' && numero_cuenta !== '' && nombre_cuenta !== '') {
            var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, denominacion: denominacion,
                numero_cuenta: numero_cuenta, nombre_cuenta: nombre_cuenta, cliente_relacionado: cliente_relacionado};
            var resultado = peticion(data, 'crearGrupoContable');
            if (resultado) {
                $('#mensaje').addClass('alert-success');
                $('#mensaje').html('Se ha creado el grupo contable correctamente');
                $('#respuesta').modal();
                $('#formulario').reset();
            } else {
                $('#mensaje').addClass('alert-danger');
                $('#mensaje').html('Error al crear un grupo contable');
                $('#respuesta').modal();
            }
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion) {
        var respuesta;
        $.ajax({
            type: "POST",
            url: '/gruposcontables/' + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});

