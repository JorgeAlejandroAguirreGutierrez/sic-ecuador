/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    $(document).ready(function ()
    {
        obtenerCodigoTipoContribuyente();
    });

    //<editor-fold defaultstate="collapsed" desc="Evento para obtener el tipo de contribuyente">
    function obtenerCodigoTipoContribuyente(){
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCodigoTipoContribuyente');
        if (resultado != null) {
            $('#codigo').val(resultado);
        } else {
            alert('ERROR AL OBTENER EL CODIGO DE TIPO DE CONTRIBUYENTE');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para crear el tipo de contribuyente">
    $("#crearTipoContribuyente").on("click", function () {
        var codigo = $('#codigo').val().toUpperCase();
        var tipo = $('#tipo').val().toUpperCase();
        var subtipo = $('#subtipo').val().toUpperCase();
        var especial = $('#especial').prop('checked');
        if (especial) {
            especial = 1;
        } else {
            especial = 0;
        }

        if (tipo === '') {
            $('#tipo').addClass('is-invalid');
            $('[name=invalido]').text('Error en el campo de tipod e contribuyente');
        }
        if (subtipo === '') {
            $('#subtipo').addClass('is-invalid');
            $('#invalido_subtipo').text('Error en el campo del subtipo de contribuyente');
        }
        if (tipo !== '' && subtipo !== '')
        {
            var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, tipo: tipo,
                subtipo: subtipo, especial:especial};
            var resultado = peticion(data, 'crearTipoContribuyente');
            if (resultado) {
                $('#mensaje').addClass('alert-success');
                $('#mensaje').html('Se ha creado el tipo de contribuyente correctamente');
                $('#respuesta').modal();
                limpiarFormulario('formulario');
            } else {
                $('#mensaje').addClass('alert-danger');
                $('#mensaje').html('Error al crear un tipo de contribuyente');
                $('#respuesta').modal();
            }
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta='/tiposcontribuyentes/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});


