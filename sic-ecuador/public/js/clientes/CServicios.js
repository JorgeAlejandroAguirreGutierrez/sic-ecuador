/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 * DESPUES DE ACTIVO
 * PRECOBRADO INPUT DE CHECK POR DEFECTO DESACTIVADO
 */

$(function ()
{
    $(document).ready(function ()
    {
        obtenerCodigoServicio();
        obtenerGruposServicios();
        obtenerImpuestos();
        obtenerTiposServicios();
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener un codigo de servicio">
    function obtenerCodigoServicio()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCodigoServicio');
        if (resultado != null) {
            $('#codigo').val(resultado);
        } else {
            alert('ERROR AL OBTENER EL CODIGO DEL SERVICIO');
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener los tipos de servicio">
    function obtenerTiposServicios() {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), tipo: 'TIPOS PS'};
        var resultado = peticion(data, 'obtenerDatosAdicionales', '/datosadicionales/');
        if (resultado != null) {
            var tipo_servicio_id = $('#tipo_id');
            for (var i = 0; i < resultado.length; i++)
            {
                var texto = resultado[i].nombre + " - " + resultado[i].abreviatura;
                var opcion = $('<option>', {text: texto, value: resultado[i].id});
                tipo_servicio_id.append(opcion);
            }
        } else {
            alert('ERROR AL OBTENER LOS TIPOS DE SERVICIOS');
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener grupos de servicios">
    function obtenerGruposServicios()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerGruposServicios', '/gruposcontables/');
        if (resultado != null) {
            var grupo_contable_id = $('#grupo_contable_id');
            for (var i = 0; i < resultado.length; i++) {
                var texto = resultado[i].denominacion + " - " + resultado[i].numero_cuenta;
                var opcion = $('<option>', {text: texto, value: resultado[i].id});
                grupo_contable_id.append(opcion);
            }
        } else {
            alert('ERROR AL OBTENER LOS GRUPOS DE SERVICIOS');
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener retenciones">
    function obtenerImpuestos()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerImpuestos', '/impuestos/');
        var impuesto_id = $('#impuesto_id');
        for (var i = 0; i < resultado.length; i++)
        {
            var opcion = $('<option>', {text: resultado[i].codigo_norma, value: resultado[i].id});
            opcion.data("atributos", {porcentaje: resultado[i].porcentaje});
            impuesto_id.append(opcion);
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para crear servicios">
    $("#crearServicio").on("click", function () {
        var codigo = $('#codigo').val().toUpperCase();
        var nombre = $('#nombre').val().toUpperCase();
        var grupo_contable_id = $('select[name=grupo_contable_id]').val();
        var impuesto_id = $('select[name=impuesto_id]').val();
        var tipo_id = $('select[name=tipo_id]').val();
        var activo = $('#activo').prop('checked');
        if (activo) {
            activo = 1;
        } else {
            activo = 0;
        }
        var precio1 = $('#precio1').val();
        var precio2 = $('#precio2').val();
        var precio3 = $('#precio3').val();
        var precio4 = $('#precio4').val();

        if (nombre === '') {
            $('#nombre').addClass('is-invalid');
            $('[name=invalido]').text('Error en el campo');
        }
        if (precio1 < 0) {
            $('#precio1').addClass('is-invalid');
            $('#precio1').text('Error en el campo del primer precio');
        }
        if (precio2 < 0) {
            $('#precio2').addClass('is-invalid');
            $('#precio2').text('Error en el campo del segundo precio');
        }
        if (precio3 < 0) {
            $('#precio3').addClass('is-invalid');
            $('#precio3').text('Error en el campo del tercer precio');
        }
        if (precio4 < 0) {
            $('#precio4').addClass('is-invalid');
            $('#precio4').text('Error en el campo del cuarto precio');
        }
        if (nombre !== '' && precio1 >= 0 && precio2 >= 0 && precio2 >= 0 && precio3 >= 0 && precio4 >= 0) {
            var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo,
                nombre: nombre, precio1: precio1, precio2: precio2, precio3: precio3,
                precio4: precio4, activo: activo, grupo_contable_id: grupo_contable_id, impuesto_id: impuesto_id,
                tipo_id: tipo_id};
            var resultado = peticion(data, 'crearServicio');
            alert(resultado);
            if (resultado) {
                $('#mensaje').addClass('alert-success');
                $('#mensaje').html('Se ha creado el servicio correctamente');
                $('#respuesta').modal();
                limpiarFormulario('formulario');
            } else {
                $('#mensaje').addClass('alert-danger');
                $('#mensaje').html('Error al crear un servicio');
                $('#respuesta').modal();
            }
        }
    });
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para cambios en el campo de precios">
    $("#precio1").on("change", function ()
    {
        var precio1 = $('#precio1').val();
        var impuesto_id = $("#impuesto_id option:selected");
        var atributos = impuesto_id.data("atributos");
        var iva = precio1 * atributos.porcentaje / 100;
        $("#iva1").val(iva);
        var suma = parseInt(precio1) + parseInt(iva);
        $("#total1").val(suma);

    });

    $("#precio2").on("change", function ()
    {
        var precio2 = $('#precio2').val();
        var impuesto_id = $("#impuesto_id option:selected");
        var atributos = impuesto_id.data("atributos");
        var iva = precio2 * atributos.porcentaje / 100;
        $("#iva2").val(iva);
        var suma = parseInt(precio2) + parseInt(iva);
        $("#total2").val(suma);

    });

    $("#precio3").on("change", function ()
    {
        var precio3 = $('#precio3').val();
        var impuesto_id = $("#impuesto_id option:selected");
        var atributos = impuesto_id.data("atributos");
        var iva = precio3 * atributos.porcentaje / 100;
        $("#iva3").val(iva);
        var suma = parseInt(precio3) + parseInt(iva);
        $("#total3").val(suma);

    });

    $("#precio4").on("change", function ()
    {
        var precio4 = $('#precio4').val();
        var impuesto_id = $("#impuesto_id option:selected");
        var atributos = impuesto_id.data("atributos");
        var iva = precio4 * atributos.porcentaje / 100;
        $("#iva4").val(iva);
        var suma = parseInt(precio4) + parseInt(iva);
        $("#total4").val(suma);

    });
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/servicios/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});
