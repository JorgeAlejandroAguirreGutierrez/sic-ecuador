/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function ()
{
    //<editor-fold defaultstate="collapsed" desc="Evento para obtener los datos para modificar el transportista">
    $('[name=modificarTransportista]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerTransportista');
        if (resultado != null) {
            $('#idM').val(resultado.id);
            $('#codigoM').val(resultado.codigo);
            $('#nombreM').val(resultado.nombre);
            $('#rucM').val(resultado.identificacion);
        } else {
            alert('ERROR AL OBTENER EL TRANSPORTISTA');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para obtener los datos para modificar transportista">
    $('[name=eliminarTransportista]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerTransportista');
        if (resultado != null) {
            $('#idE').val(resultado.id);
            $('#codigoE').val(resultado.codigo);
            $('#nombreE').val(resultado.nombre);
            $('#identificacionE').val(resultado.identificacion);
        } else {
            alert('ERROR AL OBTENER EL TRANSPORTISTA');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para modificar el transportista">
    $('#cModificarTransportista').on("click", function ()
    {
        var id = $('#idM').val();
        var codigo = $('#codigoM').val().toUpperCase();
        var nombre = $('#nombreM').val().toUpperCase();
        var ruc = $('#rucM').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id, codigo: codigo, nombre: nombre, ruc: ruc};
        var resultado = peticion(data, 'modificarTransportista');
        if (resultado) {
            window.location.href = "/transportistas/mostrar";
        } else {
            alert('ERROR AL MODIFICAR EL TRANSPORTISTA');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para eliminar transportista">
    $('#cEliminarTransportista').on("click", function ()
    {
        var id = $('#idE').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarTransportista');
        if (resultado != null) {
            window.location.href = "/transportistas/mostrar";
        } else {
            alert('ERROR AL ELIMINAR EL TRANSPORTISTA');
        }

    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Function peticion de datos">
    function peticion(data, funcion)
    {
        var respuesta;
        $.ajax({
            type: "POST",
            url: '/transportistas/' + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});
