/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    var telefonos = 1;
    var correos = 1;
    var celulares = 1;

    $(document).ready(function ()
    {
        obtenerUbicaciones();
    });

    var ubicaciones = null;
    var provincia_seleccionada = null;
    var canton_seleccionada = null;
    var parroquia_seleccionada = null;

    var cantones_anteriores = null;
    var parroquias_anteriores = null;

    //<editor-fold defaultstate="collapsed" desc="Evento para crear un auxiliar">
    $('#crearAuxiliar').on("click", function (){
        var razon_social = $('#razon_social').val().toUpperCase();
        var direccion = $('#direccion').val().toUpperCase();
        var telefono = "";
        var correo = "";
        var celular = "";
        for (var i = 0; i < telefonos; i++) {
            telefono = $('#telefono' + i).val() + " - " + telefono;
        }
        for (var i = 0; i < correos; i++) {
            correo = $('#correo' + i).val().toUpperCase() + " - " + correo;
        }
        for (var i = 0; i < celulares; i++) {
            celular = $('#celular' + i).val().toUpperCase() + " - " + celular;
        }
        var cliente_id = $('#cliente_id').val();
        var ubicaciongeo_id = obtenerUbicaciongeo_id(provincia_seleccionada, canton_seleccionada, parroquia_seleccionada);
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), razon_social: razon_social, direccion: direccion, correo: correo, telefono: telefono, celular: celular, ubicaciongeo_id: ubicaciongeo_id, cliente_id: cliente_id};
        var resultado = peticion(data, 'crear');
        if (resultado) {
            window.location.href = "/auxiliares/mostrar?id=" + cliente_id;
        } else {
            alert("NO Registrado correctamente");
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para obtener un auxiliar para la eliminacion">
    $('[name=eliminarAuxiliar]').on("click", function () {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerAuxiliar');
        if (resultado != null) {
            $('#idE').val(resultado.id);
            $('#razon_socialE').val(resultado.razon_social);
            $('#direccionE').val(resultado.direccion);
            $('#provinciaE').val(resultado.ubicaciongeo.provincia);
            $('#cantonE').val(resultado.ubicaciongeo.canton);
            $('#parroquiaE').val(resultado.ubicaciongeo.parroquia);
        } else {
            alert('ERROR AL OBTENER EL AUXILIAR');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para eliminar un auxiliar">
    $('#cEliminarAuxiliar').on("click", function () {
        var cliente_id = $('#cliente_id').val();
        var id = $('#idE').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarAuxiliar')
        if (resultado) {
            window.location.href = "/auxiliares/mostrar?id=" + cliente_id;
        } else {
            alert("ERROR");
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener las ubicaciones">
    function obtenerUbicaciones() {
        $('#provincia').editableSelect();
        $('#canton').editableSelect();
        $('#parroquia').editableSelect();
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerUbicaciones', '/ubicacionesgeo/');
        if (resultado != null) {
            ubicaciones = resultado;
            var flags = [], provincias = [], l = ubicaciones.length, i;
            for (i = 0; i < l; i++) {
                if (flags[ubicaciones[i].provincia])
                    continue;
                flags[ubicaciones[i].provincia] = true;
                provincias.push(ubicaciones[i].provincia);
            }
            for (var i = 0; i < provincias.length; i++)
            {
                $('#provincia').editableSelect('add', function () {
                    $(this).val(i);
                    $(this).text(provincias[i]);
                });
            }
        } else {
            alert('ERROR AL OBTENER LAS UBICACIONES');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Eventos para obtener la provincia, el canton y la parroquia">
    $('#provincia').editableSelect().on('select.editable-select', function (e, li) {
        var provincia = li.text();
        provincia_seleccionada = provincia;
        var cantones = [];
        for (var i = 0; i < ubicaciones.length; i++) {
            if (ubicaciones[i].provincia === provincia) {
                cantones.push(ubicaciones[i].canton);
            }
        }

        var flags = [], cantonesD = [], l = cantones.length;
        for (var i = 0; i < l; i++) {
            if (flags[cantones[i]])
                continue;
            flags[cantones[i]] = true;
            cantonesD.push(cantones[i]);

        }
        for (var i = 0; i < cantones_anteriores; i++)
        {
            $('#canton').editableSelect('remove', i);
        }
        for (var i = 0; i < cantonesD.length; i++) {
            $('#canton').editableSelect('add', function ()
            {
                $(this).val(i);
                $(this).text(cantonesD[i]);
            });
        }
        cantones_anteriores = cantonesD.length;
    });

    $("#canton").editableSelect().on('select.editable-select', function (e, li) {
        var provincia = provincia_seleccionada;
        var canton = li.text();
        canton_seleccionada = canton;
        var parroquias = [];
        for (var i = 0; i < ubicaciones.length; i++) {
            if (ubicaciones[i].provincia === provincia && ubicaciones[i].canton === canton) {
                parroquias.push(ubicaciones[i].parroquia);
            }
        }
        var flags = [], parroquiasD = [], l = parroquias.length;
        for (var i = 0; i < l; i++) {
            if (flags[parroquias[i]])
                continue;
            flags[parroquias[i]] = true;
            parroquiasD.push(parroquias[i]);

        }
        for (var i = 0; i < parroquias_anteriores; i++)
        {
            $('#parroquia').editableSelect('remove', i);
        }
        for (var i = 0; i < parroquiasD.length; i++) {
            $('#parroquia').editableSelect('add', function () {
                $(this).val(i);
                $(this).text(parroquiasD[i]);
            });
        }
        parroquias_anteriores = parroquiasD.length;
    });

    $('#parroquia').editableSelect().on('select.editable-select', function (e, li) {
        var parroquia = li.text();
        parroquia_seleccionada = parroquia;
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener el ID de una ubicacion">
    function obtenerUbicaciongeo_id(provincia, canton, parroquia)
    {
        for (var i = 0; i < ubicaciones.length; i++)
        {
            if (ubicaciones[i].provincia === provincia && ubicaciones[i].canton === canton && ubicaciones[i].parroquia === parroquia)
            {
                return ubicaciones[i].id;
            }
        }
        return -1;
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para ingresar mas telefonos, correos, celulares, ">
    $('#masTelefono').on("click", function () {
        telefonos++;
        var input = $('<input>', {type: 'tel', id: 'telefono' + telefonos, name: 'telefono', class: 'form-control'});
        var td = $('#telefonos');
        td.append(input);
    });

    $('#masCorreo').on("click", function () {
        correos++;
        var input = $('<input>', {type: 'email', id: 'correo' + correos, name: 'correo', class: 'form-control'});
        var td = $('#correos');
        td.append(input);
    });

    $('#masCelular').on("click", function () {
        celulares++;
        var input = $('<input>', {type: 'email', id: 'celular' + celulares, name: 'celular', class: 'form-control'});
        var td = $('#celulares');
        td.append(input);
    });

    $('#telefonos').on("change", '[name=telefono]', function () {
        if (!($(this).val().length >= 6 && $(this).val().length <= 9)) {
            alert("Error en el telefono");
        }
    });

    $('#celulares').on("change", '[name=celular]', function () {
        if (!($(this).val().length == 10)) {
            alert("Error en el celular");
        }
    });

    $('#correos').on("change", '[name=correo]', function () {
        if (!($(this).val().indexOf('@') > 0)) {
            alert("Error en el correo");
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/auxiliares/')
    {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});

