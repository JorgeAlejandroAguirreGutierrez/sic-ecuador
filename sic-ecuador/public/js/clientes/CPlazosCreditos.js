/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function ()
{
    $(document).ready(function ()
    {
        //obtenerCodigoPlazoCredito();
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener un codigo de plazo de credito">
    function obtenerCodigoPlazoCredito()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCodigoPlazoCredito');
        if (resultado != null) {
            $('#codigo').val(resultado);
        } else {
            alert('ERROR AL OBTENER EL CODIGO PARA EL PLAZO DE CREDITO')
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para crear un plazo de credito">
    $("#crearPlazoCredito").on("click", function ()
    {
        var plazo = $('#plazo').val();
        if (plazo == 0)
        {
            $('#plazo').addClass('is-invalid');
            $('#invalido_plazo').text('Error en el campo de numero de plazo');
        } else
        {
            var data = {'_token': $('meta[name=csrf-token]').attr('content'),plazo: plazo};
            var resultado = peticion(data, 'crearPlazoCredito');
            if (resultado) {
                $('#mensaje').addClass('alert-success');
                $('#mensaje').html('Se ha creado el plazo de credito correctamente');
                $('#respuesta').modal();
                limpiarFormulario('formulario');
            } else {
                $('#mensaje').addClass('alert-danger');
                $('#mensaje').html('Error al crear un plazo de credito');
                $('#respuesta').modal();
            }
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion) {
        var respuesta;
        $.ajax({
            type: "POST",
            url: '/plazoscreditos/' + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>

    function limpiarFormulario(formulario) {
        jQuery("#" + formulario).find(':input').each(function () {
            switch (this.type) {
                case 'password':
                case 'text':
                case 'textarea':
                case 'file':
                case 'select-one':
                case 'select-multiple':
                case 'date':
                case 'number':
                case 'tel':
                case 'email':
                    jQuery(this).val('');
                    break;
                case 'checkbox':
                case 'radio':
                    this.checked = false;
                    break;
            }
        });
    }
});
