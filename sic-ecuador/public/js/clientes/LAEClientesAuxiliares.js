/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    $(document).ready(function ()
    {
        obtenerUbicaciones();
    });

    var ubicaciones = null;
    var provincia_seleccionada = null;
    var canton_seleccionada = null;
    var parroquia_seleccionada = null;

    var cantones_anteriores = null;
    var parroquias_anteriores = null;

    //<editor-fold defaultstate="collapsed" desc="Evento para crear un auxiliar">
    $('#crearAuxiliar').on("click", function ()
    {
        var cliente_id = $('#cliente_id').val();
        var nombre = $('#nombre').val();
        var identificacion = $('#identificacion');
        var direccion = $('#direccion');
        var ubicaciongeo_id = obtenerUbicaciongeo_id(provincia_seleccionada, canton_seleccionada, parroquia_seleccionada);
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), nombre: nombre, identificacion: identificacion, direccion: direccion, ubicaciongeo_id: ubicaciongeo_id, cliente_id: cliente_id};
        var resultado = peticion(data, 'crear');
        if (resultado){
            alert("Registrado correctamente");
        } else{
            alert("NO Registrado correctamente");
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener las ubicaciones para el auxiliar">
    function obtenerUbicaciones()
    {
        $('#provincia').editableSelect();
        $('#canton').editableSelect();
        $('#parroquia').editableSelect();
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerUbicaciones', '/ubicacionesgeo/');
        if (resultado != null) {
            ubicaciones = resultado;
            var flags = [], provincias = [], l = ubicaciones.length, i;
            for (i = 0; i < l; i++) {
                if (flags[ubicaciones[i].provincia])
                    continue;
                flags[ubicaciones[i].provincia] = true;
                provincias.push(ubicaciones[i].provincia);
            }
            for (var i = 0; i < provincias.length; i++)
            {
                $('#provincia').editableSelect('add', function () {
                    $(this).val(i);
                    $(this).text(provincias[i]);
                });
            }
        } else {
            alert('ERROR AL OBTENER UBICACIONES');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener la provincia y los siguientes cantones">
    $('#provincia').editableSelect().on('select.editable-select', function (e, li) {
        var provincia = li.text();
        provincia_seleccionada = provincia;
        var cantones = [];
        for (var i = 0; i < ubicaciones.length; i++) {
            if (ubicaciones[i].provincia === provincia) {
                cantones.push(ubicaciones[i].canton);
            }
        }

        var flags = [], cantonesD = [], l = cantones.length;
        for (var i = 0; i < l; i++) {
            if (flags[cantones[i]])
                continue;
            flags[cantones[i]] = true;
            cantonesD.push(cantones[i]);

        }
        for (var i = 0; i < cantones_anteriores; i++)
        {
            $('#canton').editableSelect('remove', i);
        }
        for (var i = 0; i < cantonesD.length; i++) {
            $('#canton').editableSelect('add', function ()
            {
                $(this).val(i);
                $(this).text(cantonesD[i]);
            });
        }
        cantones_anteriores = cantonesD.length;
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener el canton y las siguientes parroquias">
    $("#canton").editableSelect().on('select.editable-select', function (e, li) {
        var provincia = provincia_seleccionada;
        var canton = li.text();
        canton_seleccionada = canton;
        var parroquias = [];
        for (var i = 0; i < ubicaciones.length; i++) {
            if (ubicaciones[i].provincia === provincia && ubicaciones[i].canton === canton) {
                parroquias.push(ubicaciones[i].parroquia);
            }
        }
        var flags = [], parroquiasD = [], l = parroquias.length;
        for (var i = 0; i < l; i++) {
            if (flags[parroquias[i]])
                continue;
            flags[parroquias[i]] = true;
            parroquiasD.push(parroquias[i]);

        }
        for (var i = 0; i < parroquias_anteriores; i++)
        {
            $('#parroquia').editableSelect('remove', i);
        }
        for (var i = 0; i < parroquiasD.length; i++) {
            $('#parroquia').editableSelect('add', function () {
                $(this).val(i);
                $(this).text(parroquiasD[i]);
            });
        }
        parroquias_anteriores = parroquiasD.length;
    });
    // </editor-fold>

    $('#parroquia').editableSelect().on('select.editable-select', function (e, li) {
        var parroquia = li.text();
        parroquia_seleccionada = parroquia;
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener el ID de una ubicacion">
    function obtenerUbicaciongeo_id(provincia, canton, parroquia)
    {
        for (var i = 0; i < ubicaciones.length; i++)
        {
            if (ubicaciones[i].provincia === provincia && ubicaciones[i].canton === canton && ubicaciones[i].parroquia === parroquia)
            {
                return ubicaciones[i].id;
            }
        }
        return -1;
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/auxiliares/')
    {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});
