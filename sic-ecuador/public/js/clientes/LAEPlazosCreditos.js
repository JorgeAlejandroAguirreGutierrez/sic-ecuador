/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    //<editor-fold defaultstate="collapsed" desc="Evento para obtener un plazo de credito para modificacion">
    $('[name=modificarPlazoCredito]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerPlazoCredito');
        $('#idM').val(resultado.id);
        $('#codigoM').val(resultado.codigo);
        $('#plazoM').val(resultado.plazo);
    });
    // </editor-fold>
      
    //<editor-fold defaultstate="collapsed" desc="Evento para obtener un plazo de credito para eliminacion ">
    $('[name=eliminarPlazoCredito]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerPlazoCredito');
        $('#idE').val(resultado.id);
        $('#codigoE').val(resultado.codigo);
        $('#plazoE').val(resultado.plazo);
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para modificar un plazo de credito">
    $('#cModificarPlazoCredito').on("click", function ()
    {
        var id = $('#idM').val();
        var codigo = $('#codigoM').val().toUpperCase();
        var plazo = $('#plazoM').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id, codigo: codigo, plazo: plazo};
        var resultado = peticion(data, 'modificarPlazoCredito');
        if (resultado) {
            window.location.href = "/plazoscreditos/mostrar";
        } else {
            alert('ERROR AL MODIFICAR EL PLAZO DE CREDITO');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para eliminar un plazo de credito">
    $('#cEliminarPlazoCredito').on("click", function ()
    {
        var id = $('#idE').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarPlazoCredito');
        if (resultado) {
            window.location.href = "/plazoscreditos/mostrar";
        } else {
            alert('ERROR AL ELIMINAR EL PLAZO DE CREDITO');
        }

    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion)
    {
        var respuesta;
        $.ajax({
            type: "POST",
            url: '/plazoscreditos/' + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                respuesta = data;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});

