/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function ()
{

    $(document).ready(function ()
    {
        obtenerCodigoRetencion();
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener el codigo de retencion">
    function obtenerCodigoRetencion()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCodigoRetencionIR');
        if (resultado != null) {
            var codigo = $('select[name=codigo]');
            codigo.append($('<option>', {text: resultado, value: resultado}));
        } else {
            alert('ERROR AL OBTENER EL CODIGO DE RETENCION IR');
        }
        var resultado = peticion(data, 'obtenerCodigoRetencionIVA');
        if (resultado != null) {
            var codigo = $('select[name=codigo]');
            codigo.append($('<option>', {text: resultado, value: resultado}));
        } else {
            alert('ERROR AL OBTENER EL CODIGO DE RETENCION IVA');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para crear una retencion">
    $("#crearRetencion").on("click", function ()
    {
        var codigo = $('#codigo').val().toUpperCase();
        var codigo_norma = $('#codigo_norma').val().toUpperCase();
        var descripcion = $('#descripcion').val().toUpperCase();
        var porcentaje = $('#porcentaje').val();
        if (codigo_norma === '')
        {
            $('#codigo_norma').addClass('is-invalid');
            $('#invalido_codigo_norma').text('Error en el codigo de norma');
        }
        if (descripcion === '')
        {
            $('#descripcion').addClass('is-invalid');
            $('#invalido_descripcion').text('Error en el de descripcion de la retencion');
        }
        if (porcentaje == 0) {
            $('#plazo').addClass('is-invalid');
            $('#invalido_porcentaje').text('Error en el campo del porcentaje de retencion');
        }
        if (descripcion !== '' && porcentaje != 0) {
            var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, codigo_norma:codigo_norma, descripcion: descripcion, porcentaje: porcentaje};
            var resultado = peticion(data, 'crearRetencion');
            if (resultado) {
                $('#mensaje').addClass('alert-success');
                $('#mensaje').html('Se ha creado la retencion correctamente');
                $('#respuesta').modal();
                limpiarFormulario('formulario');
            } else {
                $('#mensaje').addClass('alert-danger');
                $('#mensaje').html('Error al crear una retencion');
                $('#respuesta').modal();
            }
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta='/retenciones/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});