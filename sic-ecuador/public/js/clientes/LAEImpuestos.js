/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    //<editor-fold defaultstate="collapsed" desc="Evento para obtener datos para la modificacion de un impuesto">
    $('[name=modificarImpuesto]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerImpuesto');
        if (resultado != null) {
            $('#idM').val(resultado.id);
            $('#codigoM').val(resultado.codigo);
            $('#codigo_normaM').val(resultado.codigo_norma);
            $('#porcentajeM').val(resultado.porcentaje);
        } else {
            alert('ERROR ALL OBTENER EL IMPUESTO')
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para obtener datos para la eliminacion de un impuesto">
    $('[name=eliminarImpuesto]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerImpuesto');
        if (resultado != null) {
            $('#idE').val(resultado.id);
            $('#codigoE').val(resultado.codigo);
            $('#codigo_normaE').val(resultado.codigo_norma);
            $('#porcentajeE').val(resultado.porcentaje);
        } else {
            alert('ERROR AL OBTENER EL IMPUESTO');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para modificar un impuesto">
    $('#cModificarImpuesto').on("click", function ()
    {
        var id = $('#idM').val();
        var codigo = $('#codigoM').val().toUpperCase();
        var codigo_norma = $('#codigo_normaM').val().toUpperCase();
        var porcentaje = $('#porcentajeM').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id, codigo: codigo, codigo_norma: codigo_norma, porcentaje: porcentaje};
        var resultado = peticion(data, 'modificarImpuesto');
        if (resultado) {
            window.location.href = "/impuestos/mostrar";
        } else {
            alert('ERROR AL MODIFICAR UN IMPUESTO');
        }

    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para eliminar un impuesto">
    $('#cEliminarImpuesto').on("click", function ()
    {
        var id = $('#idE').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarImpuesto');
        if (resultado) {
            window.location.href = "/impuestos/mostrar";
        } else {
            alert("ERROR");
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion)
    {
        var respuesta;
        $.ajax({
            type: "POST",
            url: '/impuestos/' + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});