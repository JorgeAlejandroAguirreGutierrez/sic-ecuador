/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function ()
{
    //<editor-fold defaultstate="collapsed" desc="Evento para obtener el grupo contable para modificacion">
    $('[name=modificarGrupoContable]').on("click", function ()
    {
        var id = $(this).attr('id');
        var tipoGrupoContable = $('#tipoGrupoContable').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerGrupoContable');
        if (resultado != null) {
            $('#idM').val(resultado.id);
            $('#codigoM').val(resultado.codigo);
            $('#denominacionM').val(resultado.denominacion);
            $('#numero_cuentaM').val(resultado.numero_cuenta);
            $('#nombre_cuentaM').val(resultado.nombre_cuenta);
            if (tipoGrupoContable === 'GC')
            {
                $('#cliente_relacionadoM').prop('checked', resultado.cliente_relacionado);
            }
        } else {
            alert('ERROR AL OBTENER EL GRUPO CONTABLE');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para obtener el grupo contable para eliminacion">
    $('[name=eliminarGrupoContable]').on("click", function ()
    {
        var id = $(this).attr('id');
        var tipoGrupoContable = $('#tipoGrupoContable').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerGrupoContable');
        if (resultado != null) {
            $('#idE').val(resultado.id);
            $('#codigoE').val(resultado.codigo);
            $('#denominacionE').val(resultado.denominacion);
            $('#numero_cuentaE').val(resultado.numero_cuenta);
            $('#nombre_cuentaE').val(resultado.nombre_cuenta);
            if (tipoGrupoContable === 'GC')
            {
                $('#cliente_relacionadoE').prop('checked', resultado.cliente_relacionado);
            }

        } else {
            alert('ERROR AL OBTENER EL GRUPO CONTABLE');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para modificar el grupo contable">
    $('#cModificarGrupoContable').on("click", function ()
    {
        var id = $('#idM').val();
        var codigo = $('#codigoM').val().toUpperCase();
        var denominacion = $('#denominacionM').val().toUpperCase();
        var numero_cuenta = $('#numero_cuentaM').val();
        var nombre_cuenta = $('#nombre_cuentaM').val();
        var tipoGrupoContable = $('#tipoGrupoContable').val();
        var cliente_relacionado = false;
        if (tipoGrupoContable === 'GC') {
            cliente_relacionado = $('#cliente_relacionadoM').prop('checked');
        }
        if (cliente_relacionado) {
            cliente_relacionado = 1;
        } else {
            cliente_relacionado = 0;
        }
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id, codigo: codigo, denominacion: denominacion, numero_cuenta: numero_cuenta, nombre_cuenta: nombre_cuenta, cliente_relacionado: cliente_relacionado};
        var resultado = peticion(data, 'modificarGrupoContable');
        if (resultado) {
            window.location.href = "/gruposcontables/mostrar";
        } else {
            alert('ERROR AL ELIMINAR UN GRUPO CONTABLE');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para eliminar el grupo contable">
    $('#cEliminarGrupoContable').on("click", function ()
    {
        var id = $('#idE').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarGrupoContable');
        if (resultado) {
            window.location.href = "/gruposcontables/mostrar";
        } else {
            alert('ERROR AL ELIMINAR EL GRUPO CONTABLE');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion)
    {
        var respuesta;
        $.ajax({
            type: "POST",
            url: '/gruposcontables/' + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});
