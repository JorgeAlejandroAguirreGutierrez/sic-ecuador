/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    //<editor-fold defaultstate="collapsed" desc="Evento para obtener un auxiliar CF para eliminacion">
    $('[name=eliminarAuxiliarCF]').on("click", function (){
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerClienteAuxiliarCF');
        if (resultado != null) {
            $('#idE').val(resultado.id);
            $('#codigoE').val(resultado.cliente.codigo);
            $('#razon_socialE').val(resultado.cliente.razon_social);
            $('#direccionE').val(resultado.cliente.direccion);
            $('#provinciaE').val(resultado.ubicaciongeo.provincia);
            $('#cantonE').val(resultado.ubicaciongeo.canton);
            $('#parroquiaE').val(resultado.ubicaicongeo.parroquia);
        } else {
            alert('ERROR AL OBTENER EL AUXILIAR DE CONSUMIDOR FINAL')
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para elimianr un auxiliar CF">
    $('#cEliminarAuxiliarCF').on("click", function ()
    {
        var id = $('#idE').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarAuxiliarCF');
        if (resultado) {
            window.location.href = "/auxiliarescf/mostrar";
        } else {
            alert('ERROR AL ELIMINAR UN AUXILIAR DE CONSUMIDOR FINAL');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion)
    {
        var respuesta;
        $.ajax({
            type: "POST",
            url: '/auxiliarescf/' + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});

