/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    $(document).ready(function ()
    {
        //obtenerCodigoImpuesto();
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener el codigo de un impuesto">
    function obtenerCodigoImpuesto()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCodigoImpuesto');
        if (resultado != null) {
            $('#codigo').val(resultado);
        } else {
            alert('ERROR AL OBTENER EL CODIGO PARA UN IMPUESTO');
        } 
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para crear un impuesto">
    $("#crearImpuesto").on("click", function ()
    {
        var codigo = $('#codigo').val().toUpperCase();
        var codigo_norma = $('#codigo_norma').val().toUpperCase();
        var porcentaje = $('#porcentaje').val();
        if (codigo_norma === '') {
            $('#descripcion').addClass('is-invalid');
            $('#invalido_descripcion').text('Error en el de descripcion de la retencion');
        }
        if (codigo_norma !== '') {
            var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, codigo_norma: codigo_norma, porcentaje: porcentaje};
            var resultado = peticion(data, 'crearImpuesto');
            if (resultado) {
                $('#mensaje').addClass('alert-success');
                $('#mensaje').html('Se ha creado el impuesto correctamente');
                $('#respuesta').modal();
                limpiarFormulario('formulario');
            } else {
                $('#mensaje').addClass('alert-danger');
                $('#mensaje').html('Error al crear un impuesto');
                $('#respuesta').modal();
            }
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion) {
        var respuesta;
        $.ajax({
            type: "POST",
            url: '/impuestos/' + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});