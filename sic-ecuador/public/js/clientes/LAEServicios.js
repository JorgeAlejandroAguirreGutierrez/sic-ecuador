/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    //<editor-fold defaultstate="collapsed" desc="Evento para obtener datos para modificar un servicio">
    $('[name=modificarServicio]').on("click", function ()
    {
        //LLAMADO A GRUPOS CONTABLES
        var gruposContables = null;
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerGruposServicios', '/gruposcontables/');
        if (resultado != null) {
            gruposContables = resultado;
            //LLAMADOS A IMPUESTOS
            var impuestos = null;
            data = {'_token': $('meta[name=csrf-token]').attr('content')};
            var resultado = peticion(data, 'obtenerImpuestos', '/impuestos/');
            if (resultado != null) {
                impuestos = resultado;
                //LLAMADOS A TIPOS DE SERVICIOS
                var tipos = null;
                data = {'_token': $('meta[name=csrf-token]').attr('content')};
                var resultado = peticion(data, 'obtenerDatosAdicionales', '/datosadicionales/');
                if (resultado != null) {
                    tipos = resultado;
                    var id = $(this).attr('id');
                    data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
                    var resultado = peticion(data, 'obtenerServicio');
                    if (resultado != null) {
                        $('#idM').val(resultado.id);
                        $('#codigoM').val(resultado.codigo);
                        $('#nombreM').val(resultado.nombre);
                        $('#precio1M').val(resultado.precio1);
                        $('#precio2M').val(resultado.precio2);
                        $('#precio3M').val(resultado.precio3);
                        $('#precio4M').val(resultado.precio4);
                        if (gruposContables !== null && impuestos !== null && tipos !== null){
                            var grupo_contable_idM = $('#grupo_contable_idM').empty();
                            var texto = resultado.grupo_contable.denominacion;
                            var opcion = $('<option>', {text: texto, value: resultado.grupo_contable.id});
                            grupo_contable_idM.append(opcion);
                            for (var i = 0; i < gruposContables.length; i++){
                                if (gruposContables[i].id !== resultado.grupo_contable.id){
                                    texto = gruposContables[i].denominacion;
                                    opcion = $('<option>', {text: texto, value: gruposContables[i].id});
                                    grupo_contable_idM.append(opcion);
                                }
                            }
                            var impuesto_idM = $('#impuesto_idM').empty();
                            opcion = $('<option>', {text: resultado.impuesto.codigo_norma, value: resultado.impuesto.id});
                            opcion.data("atributos", {porcentaje: resultado.impuesto.porcentaje});
                            impuesto_idM.append(opcion);
                            for (var i = 0; i < impuestos.length; i++){
                                if (impuestos[i].id !== resultado.impuesto.id){
                                    texto = impuestos[i].codigo_norma;
                                    opcion = $('<option>', {text: texto, value: impuestos[i].id});
                                    opcion.data("atributos", {porcentaje: impuestos[i].porcentaje});
                                    impuesto_idM.append(opcion);
                                }
                            }
                            var tipo_idM = $('#tipo_idM').empty();
                            opcion = $('<option>', {text: resultado.tipo.abreviatura, value: resultado.tipo.id});
                            tipo_idM.append(opcion);
                            for (var i = 0; i < tipos.length; i++){
                                if (tipos[i].id !== resultado.tipo.id){
                                    texto = tipos[i].abreviatura;
                                    opcion = $('<option>', {text: texto, value: tipos[i].id});
                                    tipo_idM.append(opcion);
                                }
                            }
                            var precio1 = $('#precio1M').val();
                            var porcentaje = resultado.impuesto.porcentaje;
                            var iva = precio1 * porcentaje / 100;
                            $("#iva1M").val(iva);
                            var suma = parseInt(precio1) + parseInt(iva);
                            $("#total1M").val(suma);
                            var precio2 = $('#precio2M').val();
                            iva = precio2 * porcentaje / 100;
                            $("#iva2M").val(iva);
                            suma = parseInt(precio2) + parseInt(iva);
                            $("#total2M").val(suma);
                            var precio3 = $('#precio3M').val();
                            iva = precio3 * porcentaje / 100;
                            $("#iva3M").val(iva);
                            suma = parseInt(precio3) + parseInt(iva);
                            $("#total3M").val(suma);
                            var precio4 = $('#precio4M').val();
                            iva = precio4 * porcentaje / 100;
                            $("#iva4M").val(iva);
                            suma = parseInt(precio4) + parseInt(iva);
                            $("#total4M").val(suma);
                        } else {
                            alert("ERROR AL CARGAR DATOS");
                        }
                    } else {
                        alert('ERROR AL OBTENER EL SERVICIO');
                    }
                } else {
                    alert('ERROR AL OBTENER LOS DATOS ADICIONALES');
                }
            } else {
                alert('ERROR AL OBTENER LOS IMPUESTOS');
            }
        } else {
            alert('ERROR AL OBTENER LOS GRUPOS DE SERVICIOS');
        }
    });
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Evento para obtener los datos del servicio para eliminacion">
    $('[name=eliminarServicio]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerServicio');
        if (resultado != null) {
            $('#idE').val(resultado.id);
            $('#codigoE').val(resultado.codigo);
            $('#nombreE').val(resultado.nombre);
            $('#grupo_contable_idE').val(resultado.grupo_contable.denominacion);
            $('#impuesto_idE').val(resultado.impuesto.codigo_norma);
            $('#tipo_idE').val(resultado.tipo.abreviatura);
            $('#precio1E').val(resultado.precio1);
            $('#precio2E').val(resultado.precio2);
            $('#precio3E').val(resultado.precio3);
            $('#precio4E').val(resultado.precio4);
        } else {
            alert('ERROR AL OBTENER EL SERVICIO');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para modificar un servicio">
    $('#cModificarServicio').on("click", function ()
    {
        var id = $('#idM').val();
        var codigo = $('#codigoM').val().toUpperCase();
        var nombre = $('#nombreM').val().toUpperCase();
        var grupo_contable_id = $('select[name=grupo_contable_idM]').val();
        var impuesto_id = $('select[name=impuesto_idM]').val();
        var tipo_id = $('select[name=tipo_idM]').val();
        var activo = $('#activoM').prop('checked');
        if (activo) {
            activo = 1;
        } else {
            activo = 0;
        }
        var precio1 = $('#precio1M').val();
        var precio2 = $('#precio2M').val();
        var precio3 = $('#precio3M').val();
        var precio4 = $('#precio4M').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id, codigo: codigo,
            nombre: nombre, precio1: precio1, precio2: precio2, precio3: precio3,
            precio4: precio4, activo: activo, grupo_contable_id: grupo_contable_id, impuesto_id: impuesto_id,
            tipo_id: tipo_id};
        var resultado = peticion(data, 'modificarServicio');
        if (resultado) {
            window.location.href = "/servicios/mostrar";
        } else {
            alert('ERROR AL MODIFICAR EL SERVICIO DE FACTURA');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para eliminar un servicio">
    $('#cEliminarServicio').on("click", function ()
    {
        var id = $('#idE').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarServicio');
        if (resultado) {
            window.location.href = "/servicios/mostrar";
        } else {
            alert('ERROR AL ELIMINAR EL SERVICIO');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para modificar precios">
    $("#precio1M").on("change", function ()
    {
        var precio1 = $('#precio1M').val();
        var impuesto_id = $("#impuesto_idM option:selected");
        var atributos = impuesto_id.data("atributos");
        var iva = precio1 * atributos.porcentaje / 100;
        $("#iva1M").val(iva);
        var suma = parseInt(precio1) + parseInt(iva);
        $("#total1M").val(suma);

    });

    $("#precio2M").on("change", function ()
    {
        var precio2 = $('#precio2M').val();
        var impuesto_id = $("#impuesto_idM option:selected");
        var atributos = impuesto_id.data("atributos");
        var iva = precio2 * atributos.porcentaje / 100;
        $("#iva2M").val(iva);
        var suma = parseInt(precio2) + parseInt(iva);
        $("#total2M").val(suma);

    });

    $("#precio3M").on("change", function ()
    {
        var precio3 = $('#precio3M').val();
        var impuesto_id = $("#impuesto_idM option:selected");
        var atributos = impuesto_id.data("atributos");
        var iva = precio3 * atributos.porcentaje / 100;
        $("#iva3M").val(iva);
        var suma = parseInt(precio3) + parseInt(iva);
        $("#total3M").val(suma);

    });

    $("#precio4M").on("change", function ()
    {
        var precio4 = $('#precio4M').val();
        var impuesto_id = $("#impuesto_idM option:selected");
        var atributos = impuesto_id.data("atributos");
        var iva = precio4 * atributos.porcentaje / 100;
        $("#iva4M").val(iva);
        var suma = parseInt(precio4) + parseInt(iva);
        $("#total4M").val(suma);

    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para modificar impuestos">
    $("#impuesto_idM").on("change", function ()
    {
        var impuesto_id = $("#impuesto_idM option:selected");
        var atributos = impuesto_id.data("atributos");
        var porcentaje = atributos.porcentaje;
        var precio1 = $('#precio1M').val();
        var iva = precio1 * porcentaje / 100;
        $("#iva1M").val(iva);
        var suma = parseInt(precio1) + parseInt(iva);
        $("#total1M").val(suma);
        var precio2 = $('#precio2M').val();
        iva = precio2 * porcentaje / 100;
        $("#iva2M").val(iva);
        suma = parseInt(precio2) + parseInt(iva);
        $("#total2M").val(suma);
        var precio3 = $('#precio3M').val();
        iva = precio3 * porcentaje / 100;
        $("#iva3M").val(iva);
        suma = parseInt(precio3) + parseInt(iva);
        $("#total3M").val(suma);
        var precio4 = $('#precio4M').val();
        iva = precio4 * porcentaje / 100;
        $("#iva4M").val(iva);
        suma = parseInt(precio4) + parseInt(iva);
        $("#total4M").val(suma);
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Function peticion de datos">
    function peticion(data, funcion, ruta= '/servicios/')
    {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});