/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function ()
{

    $(document).ready(function ()
    {
        obtenerCodigoTransportista();
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener el codigo de un transportista">
    function obtenerCodigoTransportista()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCodigoTransportista');
        if (resultado != null) {
            $('#codigo').val(resultado);
        } else {
            alert('ERROR AL OBTENER EL CODIGO PARA TRANSPORTISTA');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para crear un transportista">
    $("#crearTransportista").on("click", function () {
        var codigo = $('#codigo').val().toUpperCase();
        var nombre = $('#nombre').val().toUpperCase();
        var identificacion = $('#identificacion').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, nombre: nombre, identificacion: identificacion};
        var resultado = peticion(data, 'crearTransportista');
        if (resultado) {
            $('#mensaje').addClass('alert-success');
            $('#mensaje').html('Se ha creado un transportista correctamente');
            $('#respuesta').modal();
            limpiarFormulario('formulario');
        } else {
            $('#mensaje').addClass('alert-danger');
            $('#mensaje').html('Error al crear un transportista');
            $('#respuesta').modal();
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/transportistas/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});
