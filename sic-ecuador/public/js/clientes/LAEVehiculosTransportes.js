/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    //<editor-fold defaultstate="collapsed" desc="Evento para obtener un vehiculo de transporte">
    $('[name=modificarVehiculoTransporte]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerVehiculoTransporte');
        if (resultado != null) {
            $('#idM').val(resultado.id);
            $('#codigoM').val(resultado.codigo);
            $('#modeloM').val(resultado.modelo);
            $('#placaM').val(resultado.placa);
            $('#marcaM').val(resultado.marca);
            $('#cilindrajeM').val(resultado.cilindraje);
            $('#claseM').val(resultado.clase);
            $('#colorM').val(resultado.color);
            $('#fabricacionM').val(resultado.fabricacion);
            $('#activoM').prop('checked', resultado.activo);
        } else {
            alert('ERROR AL OBTENER EL VEHICULO');
        }

    });

    $('[name=eliminarVehiculoTransporte]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerVehiculoTransporte');
        if (resultado != null) {
            $('#idE').val(resultado.id);
            $('#codigoE').val(resultado.codigo);
            $('#modeloE').val(resultado.modelo);
            $('#placaE').val(resultado.placa);
            $('#marcaE').val(resultado.marca);
            $('#cilindrajeE').val(resultado.cilindraje);
            $('#claseE').val(resultado.clase);
            $('#colorE').val(resultado.color);
            $('#fabricacionE').val(resultado.fabricacion);
            $('#activoE').prop('checked', resultado.activo);
        } else {
            alert('ERROR AL OBTENER EL VEHICULO DE TRANSPORTE');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para modificar un vehiculo de transporte">
    $('#cModificarVehiculoTransporte').on("click", function ()
    {
        var id = $('#idM').val();
        var codigo = $('#codigoM').val().toUpperCase();
        var modelo = $('#modeloM').val();
        var placa = $('#placaM').val().toUpperCase();
        var marca = $('#marcaM').val().toUpperCase();
        var cilindraje = $('#cilindrajeM').val();
        var clase = $('#claseM').val();
        var color = $('#colorM').val().toUpperCase();
        var fabricacion = $('#fabricacionM').val();
        var activo = $('#activoM').prop('checked');
        if (activo) {
            activo = 1;
        } else {
            activo = 0;
        }
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id, codigo: codigo, modelo: modelo,
            placa: placa, marca: marca, cilindraje: cilindraje, clase: clase, color: color, fabricacion: fabricacion, activo: activo};
        var resultado = peticion(data, 'modificarVehiculoTransporte');
        if (resultado) {
            window.location.href = "/vehiculostransportes/mostrar";
        } else {
            alert('ERROR AL MODIFICAR EL VEHICULO DE TRANSPORTE');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para eliminar un vehiculo de transporte">
    $('#cEliminarVehiculoTransporte').on("click", function ()
    {
        var id = $('#idE').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarVehiculoTransporte');
        if (resultado) {
            window.location.href = "/vehiculostransportes/mostrar";
        } else {
            alert('ERROR AL ELIMINAR EL VEHICULO');
        }

    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Function peticion de datos">
    function peticion(data, funcion)
    {
        var respuesta;
        $.ajax({
            type: "POST",
            url: '/vehiculostransportes/' + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});

