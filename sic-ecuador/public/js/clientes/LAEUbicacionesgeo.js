/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    //<editor-fold defaultstate="collapsed" desc="Evento para obtener datos para modificacion y eliminacion">
    $('[name=modificarUbicaciongeo]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerUbicaciongeo');
        if (resultado != null) {
            $('#idM').val(resultado.id);
            $('#codigoM').val(resultado.codigo);
            $('#codigo_normaM').val(resultado.codigo_norma);
            $('#provinciaM').val(resultado.provincia);
            $('#cantonM').val(resultado.canton);
            $('#parroquiaM').val(resultado.parroquia);
        } else {
            alert('ERROR AL OBTENER LA UBICACION');
        }
    });

    $('[name=eliminarUbicaciongeo]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerUbicaciongeo');
        if (resultado != null) {
            $('#idE').val(resultado.id);
            $('#codigoE').val(resultado.codigo);
            $('#codigo_normaE').val(resultado.codigo_norma);
            $('#provinciaE').val(resultado.provincia);
            $('#cantonE').val(resultado.canton);
            $('#parroquiaE').val(resultado.parroquia);
        } else {
            alert('ERROR AL OBTENER LA UBICACION');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para modificar una ubicacion">
    $('#cModificarUbicaciongeo').on("click", function ()
    {
        var id = $('#idM').val();
        var codigo = $('#codigoM').val().toUpperCase();
        var codigo_norma = $('#codigo_norma').val();
        var provincia = $('#provinciaM').val().toUpperCase();
        var canton = $('#cantonM').val().toUpperCase();
        var parroquia = $('#parroquiaM').val().toUpperCase();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id, codigo: codigo, codigo_norma: codigo_norma,
            provincia: provincia, canton: canton, parroquia: parroquia};
        var resultado = peticion(data, 'modificarUbicaciongeo');
        if (resultado) {
            window.location.href = "/ubicacionesgeo/mostrar";
        } else {
            alert('ERROR AL MODIFICAR UNA UBICACION')
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para eliminar una ubicacion">
    $('#cEliminarUbicaciongeo').on("click", function ()
    {
        var id = $('#idE').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarUbicaciongeo');
        if (resultado) {
            window.location.href = "/ubicacionesgeo/mostrar";
        } else {
            alert('ERROR AL ELIMINAR UNA UBICACION');
        }

    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Function peticion de datos">
    function peticion(data, funcion)
    {
        var respuesta;
        $.ajax({
            type: "POST",
            url: '/ubicacionesgeo/' + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});

