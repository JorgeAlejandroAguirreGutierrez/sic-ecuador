/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    //<editor-fold defaultstate="collapsed" desc="Evento para obtener una retencion para modificacion">
    $('[name=modificarRetencion]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerRetencion');
        if (resultado != null) {
            $('#idM').val(resultado.id);
            $('#codigoM').val(resultado.codigo);
            $('#descripcionM').val(resultado.descripcion);
            $('#porcentajeM').val(resultado.porcentaje);
        } else {
            alert('ERROR AL OBTENER LA RETENCION');
        }

    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para obtener una retencion para eliminacion">
    $('[name=eliminarRetencion]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerRetencion');
        if (resultado != null) {
            $('#idE').val(resultado.id);
            $('#codigoE').val(resultado.codigo);
            $('#descripcionE').val(resultado.descripcion);
            $('#porcentajeE').val(resultado.porcentaje);
        } else {
            alert('ERROR AL OBTENER LA RETENCION');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para modificar una retencion">
    $('#cModificarRetencion').on("click", function ()
    {
        var id = $('#idM').val();
        var codigo = $('#codigoM').val().toUpperCase();
        var descripcion = $('#descripcionM').val().toUpperCase();
        var porcentaje = $('#porcentajeM').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id, codigo: codigo, descripcion: descripcion, porcentaje: porcentaje};
        var resultado = peticion(data, 'modificarRetencion');
        if (resultado) {
            window.location.href = "/retenciones/mostrar";
        } else {
            alert('ERROR AL MODIFICAR RETENCION');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para eliminar una retencion">
    $('#cEliminarRetencion').on("click", function ()
    {
        var id = $('#idE').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarRetencion');
        if (resultado) {
            window.location.href = "/retenciones/mostrar";
        } else {
            alert(resultado);
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Function peticion de datos">
    function peticion(data, funcion)
    {
        var respuesta;
        $.ajax({
            type: "POST",
            url: '/retenciones/' + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});

