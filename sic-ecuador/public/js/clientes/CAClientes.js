/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * CELULAR: DEBE SER IGUAL A  10 numeros. UNO O MAS --->LISTO
 * TELEFONO: MINIMO 6 DIGITOS MAXIMO 9. UNO O MAS ----> LISTO
 * CORREO: VERIFICAR QUE TENGA EL ARROBA, UNO O MAS ----->LISTO
 * ACTIVO: POR DEFECTO EN ACTIVO ---> LISTO
 * CLIENTE PREPAGO: POR DEFECTO NO ACTIVO ---->LISTO
 * CUPO DE VENTA POR DEFECTO DEBE ESTAR EN 0 ---->LISTO
 * ESTADO CIVIL  EN ABREVIATURA POR DEFECTO SOLTERO ----->LISTO
 * SEXO: EN ABREVIATURA POR DEFECTO MASCULINO  --->LISTO
 * ORIGEN DE INGRESOS EN ABREVIATURA POR DEFECTO INDEPENDIENTE --->LISTO
 * TIPO DE CLIENTE PONER DE ACUERDO AL TIPO DE CONTRIBUYENTE---> LISTO
 * 
 * 
 * PERSONA NATURAL - NO OBLIGADO A CONTABILIDAD -> EN LISTA DE PRIMERO --->LISTO
 * PERSONA JURIDICA -> PUBLICA ES 6 ---->LISTO
 * PERSONA JURIDICA -> PRIVADA ES 9 .--->LISTO
 * 
 * DATOS DE CONTACTO NO SON OBLIGATORIOS
 * SI NO EXISTE CORREO, ALMACENAR CORREO DEFAULT DE LA EMPRESA
 * 
 * 
 * -- PARA LAS PLACAS DE TRES DIGITOS AGREGAR UN 0 A LA IZQUIERDA
 * --- PARA CONSUMIDOR FINAL BLOQUEAR CONTRIBUYENTE ESPECIAL
 * -- PARA PASAPORTE MINIMO 3 Y MAXIMO 13
 * -- ESCOGER LETRA E PARA PASAPORTE Y ESCRIBIR LA IDENTIFICACION
 * -- PONER ASTERISCO A CAMPOS OBLIGATORIOS Y A LOS ERROR SOMBREAR CON ROJO
 */


$(function ()
{
    var ubicacion = null;
    var tiposContribuyentes = null;
    var cantidad_telefonos = 1;
    var cantidad_celulares = 1;
    var cantidad_correos = 1;
    var ventana = null;

    var actualizar_ubicaciongeo = false;
    var actualizar_grupo_contable = false;
    var actualizar_tipo_contribuyente = false;
    var actualizar_forma_pago = false;
    var actualizar_categoria_cliente = false;
    var actualizar_retencion_iva = false;
    var actualizar_retencion_ir = false;
    var actualizar_estado_civil = false;
    var actualizar_sexo = false;
    var actualizar_origen_ingreso = false;
    var actualizar_plazo_credito = false;

    $(document).ready(function ()
    {
        $('[data-toggle="tooltip"]').tooltip();
        //obtenerCodigoCliente();
        obtenerProvincias();
        obtenerGruposClientes();
        obtenerFormasPagos();
        obtenerPlazosCreditos();
        obtenerCategoriasClientes();
        obtenerRetencionesIVAsBS();
        obtenerRetencionesIRsBS();
        obtenerEstadosCiviles();
        obtenerSexo();
        obtenerOrigenesIngresos();
        cambioIdentificacion();
        obtenerActualizacionCliente();
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite a traves de la tecla F1 ir a otros submodulos">
    shortcut.add("F1", function () {
        if (ventana.includes('provincia')) {
            actualizar_ubicaciongeo = true;
            window.open('/ubicacionesgeo/c', '', 'width=900px, height=600px toolbar=yes');
        } else if (ventana.includes('canton')) {
            actualizar_ubicaciongeo = true;
            window.open('/ubicacionesgeo/c', '', 'width=900px, height=600px toolbar=yes');
        } else if (ventana.includes('parroquia')) {
            actualizar_ubicaciongeo = true;
            window.open('/ubicacionesgeo/c', '', 'width=900px, height=600px toolbar=yes');
        } else if (ventana.includes('grupo_contable')) {
            actualizar_grupo_contable = true;
            window.open('/gruposcontables/c', '', 'width=900px, height=600px toolbar=yes');
        } else if (ventana.includes('tipo_contribuyente')) {
            actualizar_tipo_contribuyente = true;
            window.open('/tiposcontribuyentes/c', '', 'width=900px, height=600px toolbar=yes');
        } else if (ventana.includes('forma_pago')) {
            actualizar_forma_pago = true;
            window.open('/datosadicionales/c', '', 'width=900px, height=600px toolbar=yes');
        } else if (ventana.includes('plazo_credito')) {
            actualizar_plazo_credito = true;
            window.open('/plazoscreditos/c', '', 'width=900px, height=600px toolbar=yes');
        } else if (ventana.includes('categoria_cliente')) {
            actualizar_categoria_cliente = true;
            window.open('/datosadicionales/c', '', 'width=900px, height=600px toolbar=yes');
        } else if (ventana.includes('retencion_iva')) {
            actualizar_retencion_iva = true;
            window.open('/retenciones/c', '', 'width=900px, height=600px toolbar=yes');
        } else if (ventana.includes('retencion_ir')) {
            actualizar_retencion_ir = true;
            window.open('/retenciones/c', '', 'width=900px, height=600px toolbar=yes');
        } else if (ventana.includes('sexo')) {
            actualizar_sexo = true;
            window.open('/datosadicionales/c', '', 'width=900px, height=600px toolbar=yes');
        } else if (ventana.includes('estado_civil')) {
            actualizar_estado_civil = true;
            window.open('/datosadicionales/c', '', 'width=900px, height=600px toolbar=yes');
        } else if (ventana.includes('origen_ingreso')) {
            actualizar_origen_ingreso = true;
            window.open('/datosadicionales/c', '', 'width=900px, height=600px toolbar=yes');
        }
    });
    // </editor-fold>

    $("input,select").focus(function (event) {
        ventana = event.target.id;
        if (actualizar_ubicaciongeo) {
            obtenerProvincias();
            actualizar_ubicaciongeo = false;
        }
        if (actualizar_grupo_contable) {
            obtenerGruposClientes();
            actualizar_grupo_contable = false;
        }
        if (actualizar_forma_pago) {
            obtenerFormasPagos();
            actualizar_forma_pago = false;
        }
        if (actualizar_plazo_credito) {
            obtenerPlazosCreditos();
            actualizar_plazo_credito = false;
        }
        if (actualizar_plazo_credito) {
            obtenerPlazosCreditos();
            actualizar_plazo_credito = false;
        }
        if (actualizar_categoria_cliente) {
            obtenerCategoriasClientes();
            actualizar_categoria_cliente = false;
        }
        if (actualizar_retencion_iva) {
            obtenerRetencionesIVAsBS();
            actualizar_retencion_iva = false;
        }
        if (actualizar_retencion_ir) {
            obtenerRetencionesIRsBS();
            actualizar_retencion_ir = false;
        }
        if (actualizar_sexo) {
            obtenerSexo();
            actualizar_sexo = false;
        }
        if (actualizar_estado_civil) {
            obtenerEstadoCivil();
            actualizar_estado_civil = false;
        }
        if (actualizar_origen_ingreso) {
            obtenerOrigenesIngresos();
            actualizar_origen_ingreso = false;
        }
    });

    $('#provincia').autocomplete({
        source: [],
        select: function (event, ui) {
            var provincia = ui.item.val;
            cambioProvincia(provincia);
        },
        change: function (event, ui) {
            $(this).val((ui.item ? ui.item.label : ""));
        },
        search: function (event, ui) {
            if (actualizar_ubicaciongeo) {
                obtenerProvincias();
                actualizar_ubicaciongeo = false;
            }
            ventana = event.target.id;
        }
    });
    $('#canton').autocomplete({
        source: [],
        select: function (event, ui) {
            var canton = ui.item.val;
            cambioCanton(canton);
        },
        change: function (event, ui) {
            $(this).val((ui.item ? ui.item.label : ""));
        },
        search: function (event, ui) {
            if (actualizar_ubicaciongeo) {
                obtenerProvincias();
                actualizar_ubicaciongeo = false;
            }
            ventana = event.target.id;
        }
    });

    $('#parroquia').autocomplete({
        source: [],
        select: function (event, ui) {
            ubicacion = ui.item.val;
        },
        change: function (event, ui) {
            $(this).val((ui.item ? ui.item.label : ""));
            ubicacion = (ui.item ? ui.item.val : null);
        },
        search: function (event, ui) {
            if (actualizar_ubicaciongeo) {
                obtenerProvincias();
                actualizar_ubicaciongeo = false;
            }
            ventana = event.target.id;
        }
    });

    function obtenerActualizacionCliente() {
        var cliente_id = $('#cliente_id').val();
        if (cliente_id) {
            var temp_ubicaciongeo_id = $('#temp_ubicaciongeo_id').val();
            var temp_forma_pago_id = $('#temp_forma_pago_id').val();
            var temp_categoria_cliente_id = $('#temp_categoria_cliente_id').val();
            var temp_grupo_contable_id = $('#temp_grupo_contable_id').val();
            var temp_plazo_credito_id = $('#temp_plazo_credito_id').val();
            var temp_retencion_iva_b_id = $('#temp_retencion_iva_b_id').val();
            var temp_retencion_ir_b_id = $('#temp_retencion_ir_b_id').val();
            var temp_retencion_iva_s_id = $('#temp_retencion_iva_s_id').val();
            var temp_retencion_ir_s_id = $('#temp_retencion_ir_s_id').val();
            var temp_estado_civil_id = $('#temp_estado_civil_id').val();
            var temp_sexo_id = $('#temp_sexo_id').val();
            var temp_origen_ingreso_id = $('#temp_origen_ingreso_id').val();
            ubicacion = temp_ubicaciongeo_id;
            $('#forma_pago_id').val(temp_forma_pago_id);
            $('#categoria_cliente_id').val(temp_categoria_cliente_id);
            $('#grupo_contable_id').val(temp_grupo_contable_id);
            $('#plazo_credito_id').val(temp_plazo_credito_id);
            $('#retencion_iva_b_id').val(temp_retencion_iva_b_id);
            $('#retencion_ir_b_id').val(temp_retencion_ir_b_id);
            $('#retencion_iva_s_id').val(temp_retencion_iva_s_id);
            $('#retencion_ir_s_id').val(temp_retencion_ir_s_id);
            $('#estado_civil_id').val(temp_estado_civil_id);
            $('#sexo_id').val(temp_sexo_id);
            $('#origen_ingreso_id').val(temp_origen_ingreso_id);

        }
    }

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener los grupos de clientes">
    function obtenerGruposClientes()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerGruposClientes', '/gruposcontables/');
        if (resultado != null) {
            var grupo_contable_id = $('#grupo_contable_id');
            for (var i = 0; i < resultado.length; i++)
            {
                var texto = resultado[i].denominacion;
                var opcion = $('<option>', {text: texto, value: resultado[i].id});
                grupo_contable_id.append(opcion);
            }
        } else {
            alert('ERROR AL OBTENER LOS GRUPOS DE CLIENTES');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener las ubicaciones">
    function obtenerProvincias()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerProvincias', '/ubicacionesgeo/');
        if (resultado != null) {
            var provincias = [];
            for (var i = 0; i < resultado.length; i++) {
                var opcion = {val: resultado[i].provincia, label: resultado[i].provincia};
                provincias.push(opcion);
            }
            $('#provincia').autocomplete('option', 'source', provincias);
        } else {
            alert('ERROR AL OBTENER LAS UBICACIONES');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite apartir de la provincia obtener los cantones">
    function cambioProvincia(provincia) {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), provincia: provincia};
        var resultado = peticion(data, 'obtenerCantones', '/ubicacionesgeo/');
        if (resultado != null) {
            var cantones = [];
            for (var i = 0; i < resultado.length; i++) {
                var opcion = {val: resultado[i].canton, label: resultado[i].canton};
                cantones.push(opcion);
            }

            $('#canton').autocomplete('option', 'source', cantones);
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento que permite apartir de un canton obtener las parroquias">
    function cambioCanton(canton) {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), canton: canton};
        var resultado = peticion(data, 'obtenerParroquias', '/ubicacionesgeo/');
        if (resultado != null) {
            var parroquias = [];
            for (var i = 0; i < resultado.length; i++) {
                var opcion = {val: resultado[i].id, label: resultado[i].parroquia};
                parroquias.push(opcion);
            }
            $('#parroquia').autocomplete('option', 'source', parroquias);
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener los tipos de contribuyente PP">
    function obtenerTiposContribuyentes_PP(tipo, ruc)
    {
        var pp = ruc[2];
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), tipo: tipo};
        var resultado = peticion(data, 'obtenerTiposContribuyentes', '/tiposcontribuyentes/');
        if (resultado != null) {
            tiposContribuyentes = resultado;
            $('#c_tipo_persona').empty();
            $('#c_tipo_persona').append($('<input>', {type: 'text', id: 'tipo_persona', name: 'tipo_persona', class: 'form-control', disabled: ''}));
            $('#tipo_persona').val(resultado[0].tipo);
            $('#tipo_cliente').val(resultado[0].tipo); //DATOS DINARDAP
            var tipo_contribuyente_id = $('#tipo_contribuyente_id');
            tipo_contribuyente_id.empty();
            if (pp === '6')
            {
                var opcion = null;
                for (var i = 0; i < resultado.length; i++)
                {
                    if (resultado[i].subtipo === 'PUBLICA')
                    {
                        opcion = resultado[i];
                    }
                }
                tipo_contribuyente_id.append($('<option>', {value: opcion.id, text: opcion.subtipo}));
                tipo_contribuyente_id.prop('disabled', true);
            } else
            {
                var opcion = null;
                for (var i = 0; i < resultado.length; i++)
                {
                    if (resultado[i].subtipo === 'PRIVADA')
                    {
                        opcion = resultado[i].id;
                    }
                }
                tipo_contribuyente_id.append($('<option>', {value: opcion.id, text: opcion.subtipo}));
                tipo_contribuyente_id.prop('disabled', true);
            }
            cambiarContribuyenteEspecial();
        } else {
            alert('ERROR AL OBTENER LOS TIPOS DE CONTRIBUYENTE PP');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener los tipos de contribuyentes C_R">
    function obtenerTiposContribuyentes_C_R(tipo)
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), tipo: tipo};
        var resultado = peticion(data, 'obtenerTiposContribuyentes', '/tiposcontribuyentes/');
        if (resultado != null) {
            tiposContribuyentes = resultado;
            $('#c_tipo_persona').empty();
            $('#c_tipo_persona').append($('<input>', {type: 'text', id: 'tipo_persona', name: 'tipo_persona', class: 'form-control', disabled: ''}));
            $('#tipo_persona').val(resultado[0].tipo);
            $('#tipo_cliente').val(resultado[0].tipo); //DATOS DINARDAP
            var tipo_contribuyente_id = $('#tipo_contribuyente_id');
            tipo_contribuyente_id.empty();
            for (var i = 0; i < resultado.length; i++)
            {
                var texto = resultado[i].subtipo;
                var opcion = $('<option>', {text: texto, value: resultado[i].id});
                tipo_contribuyente_id.append(opcion);
            }
            tipo_contribuyente_id.prop("disabled", false);
            cambiarContribuyenteEspecial();
        } else {
            alert('ERROR AL OBTENER LOS TIPOS DE CONTRIBUYENTE C_R');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener los tipos de contribuyente CF_PL_E">
    function obtenerTiposContribuyentes_CF_PL_E(tipo)
    {
        $('#tipo_identificacion').val(tipo);
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), tipo: tipo};
        var resultado = peticion(data, 'obtenerTiposContribuyentes', '/tiposcontribuyentes/');
        if (resultado != null) {
            tiposContribuyentes = resultado;
            var contribuyente = null;
            for (var i = 0; i < resultado.length; i++)
            {
                if (resultado[i].tipo === 'PERSONA NATURAL' && resultado[i].subtipo === 'NO OBLIGADO A CONTABILIDAD')
                {
                    contribuyente = resultado[i];
                }
            }
            $('#tipo_persona').val(contribuyente.tipo);
            $('#tipo_cliente').val(contribuyente.tipo);
            var tipo_contribuyente_id = $('#tipo_contribuyente_id');
            tipo_contribuyente_id.empty();
            var opcion = $('<option>', {text: contribuyente.subtipo, value: contribuyente.id});
            tipo_contribuyente_id.append(opcion);
            tipo_contribuyente_id.prop("disabled", true);
        } else {
            alert('ERROR AL OBTENER LOS TIPOS DE CONTRIBUYENTES CF_PL_E');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener los plazos de creditos">
    function obtenerPlazosCreditos()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerPlazosCreditos', '/plazoscreditos/');
        if (resultado != null) {
            var plazo_credito_id = $('#plazo_credito_id');
            for (var i = 0; i < resultado.length; i++)
            {
                var texto = resultado[i].plazo;
                var opcion = $('<option>', {text: texto, value: resultado[i].id});
                plazo_credito_id.append(opcion);
            }
        } else {
            alert('ERROR AL OBTENER LOS PLAZOS DE CREDITOS');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener las formas de pago">
    function obtenerFormasPagos()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), tipo: 'FORMA PAGO'};
        var resultado = peticion(data, 'obtenerDatosAdicionales', '/datosadicionales/');
        if (resultado != null) {
            var forma_pago_id = $('#forma_pago_id');
            for (var i = 0; i < resultado.length; i++)
            {
                var texto = resultado[i].nombre;
                var opcion = $('<option>', {text: texto, value: resultado[i].id});
                forma_pago_id.append(opcion);
            }
        } else {
            alert('ERROR AL OBTENER LAS FORMAS DE PAGO');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener las categorias para el cliente">
    function obtenerCategoriasClientes()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), tipo: 'CATEGORIA CLIENTE'};
        var resultado = peticion(data, 'obtenerDatosAdicionales', '/datosadicionales/');
        if (resultado != null) {
            var categoria_cliente_id = $('#categoria_cliente_id');
            for (var i = 0; i < resultado.length; i++)
            {
                var texto = resultado[i].nombre;
                var opcion = $('<option>', {text: texto, value: resultado[i].id});
                categoria_cliente_id.append(opcion);
            }
        } else {
            alert('ERROR AL OBTENER LAS CATEGORIAS DE LOS CLIENTES');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener la retencion IVA de bienes y servicios">
    //Obtener IVAs para bienes y servicios
    function obtenerRetencionesIVAsBS()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerIVAs', '/retenciones/');
        if (resultado != null) {
            var retencion_iva_b_id = $('#retencion_iva_b_id');
            var retencion_iva_s_id = $('#retencion_iva_s_id');
            for (var i = 0; i < resultado.length; i++)
            {
                var opcion = $('<option>', {text: resultado[i].descripcion, value: resultado[i].id});
                var opcion2 = $('<option>', {text: resultado[i].descripcion, value: resultado[i].id});
                retencion_iva_b_id.append(opcion);
                retencion_iva_s_id.append(opcion2);
            }
        } else {
            alert('ERROR AL OBTENER LOS IVAs');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener las retenciones IR en bienes y servicios">
    //Obtener IRs para bienes y servicios
    function obtenerRetencionesIRsBS()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerIRs', '/retenciones/');
        if (resultado != null) {
            var retencion_ir_b_id = $('#retencion_ir_b_id');
            var retencion_ir_s_id = $('#retencion_ir_s_id');
            for (var i = 0; i < resultado.length; i++)
            {
                var opcion = $('<option>', {text: resultado[i].descripcion, value: resultado[i].id});
                var opcion2 = $('<option>', {text: resultado[i].descripcion, value: resultado[i].id});
                opcion.data("atributos", {codigo: resultado[i].codigo, porcentaje: resultado[i].porcentaje});
                opcion2.data("atributos", {codigo: resultado[i].codigo, porcentaje: resultado[i].porcentaje});
                retencion_ir_b_id.append(opcion);
                retencion_ir_s_id.append(opcion2);
            }
        } else {
            alert('ERROR AL OBTENER LAS RETENCIONES IRs');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener los estados civiles para el cliente">
    function obtenerEstadosCiviles()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), tipo: 'ESTADO CIVIL'};
        var resultado = peticion(data, 'obtenerDatosAdicionales', '/datosadicionales/');
        if (resultado != null) {
            var estado_civil_id = $('#estado_civil_id');
            for (var i = 0; i < resultado.length; i++)
            {
                var texto = resultado[i].abreviatura;
                var opcion = $('<option>', {text: texto, value: resultado[i].id});
                estado_civil_id.append(opcion);
            }
        } else {
            alert('ERROR AL OBTENER LOS ESTADOS CIVILES');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener el sexo para el cliente">
    function obtenerSexo()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), tipo: 'SEXO'};
        var resultado = peticion(data, 'obtenerDatosAdicionales', '/datosadicionales/');
        if (resultado != null) {
            var sexo_id = $('#sexo_id');
            for (var i = 0; i < resultado.length; i++) {
                var texto = resultado[i].abreviatura;
                var opcion = $('<option>', {text: texto, value: resultado[i].id});
                sexo_id.append(opcion);
            }
        } else {
            alert('ERROR AL OBTENER EL SEXO')
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener los origenes de ingresos">
    function obtenerOrigenesIngresos()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), tipo: 'ORIGEN INGRESO'};
        var resultado = peticion(data, 'obtenerDatosAdicionales', '/datosadicionales/');
        if (resultado != null) {
            var origen_ingreso_id = $('#origen_ingreso_id');
            for (var i = 0; i < resultado.length; i++) {
                var texto = resultado[i].abreviatura;
                var opcion = $('<option>', {text: texto, value: resultado[i].id});
                origen_ingreso_id.append(opcion);
            }
        } else {
            alert('ERROR AL OBTENER LOS ORIGENES DE INGRESOS');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Eventos que permite ingresar mas telefonos, celulares y verificacion de los mismos">
    $("#masTelefono").on("click", function ()
    {
        cantidad_telefonos++;
        var input = $('<input>', {type: 'tel', id: 'telefono' + cantidad_telefonos, name: 'telefono', class: 'form-control'});
        var td = $('#telefonos');
        td.append(input);
    });

    $("#masCelular").on("click", function ()
    {
        cantidad_celulares++;
        var input = $('<input>', {type: 'tel', id: 'celular' + cantidad_celulares, name: 'celular', class: 'form-control'});
        var td = $('#celulares');
        td.append(input);
    });

    $("#masCorreo").on("click", function ()
    {
        cantidad_correos++;
        var input = $('<input>', {type: 'email', id: 'correo' + cantidad_correos, name: 'correo', class: 'form-control'});
        var td = $('#correos');
        td.append(input);
    });

    $('#telefonos').on("change", '[name=telefono]', function ()
    {
        if (!($(this).val().length >= 6 && $(this).val().length <= 9))
        {
            alert("Error en el telefono");
        }
    });

    $('#celulares').on("change", '[name=celular]', function ()
    {
        if (!($(this).val().length == 10))
        {
            alert("Error en el celular");
        }
    });

    $('#correos').on("change", '[name=correo]', function ()
    {
        if (!($(this).val().indexOf('@') > 0))
        {
            alert("Error en el correo");
        }
    });

    $("#celular").on("change", function ()
    {
        var celular = $('#celular').val();
        if (celular.length !== 10)
        {
            alert("ERROR");
        }
    });
    // </editor-fold>

    $("#cliente_prepago").on("change", function () {
        var seleccionado = $('#cliente_prepago').prop('checked');
        if (seleccionado) {
            $('#valor_prepago').attr('disabled', false);
        } else {
            $('#valor_prepago').attr('disabled', true);
        }
    });

    //<editor-fold defaultstate="collapsed" desc="Evento que permite crear un cliente">
    $("#guardarCliente").on("click", function ()
    {
        var id = $('#cliente_id').val();
        var codigo = $('#codigo').val();
        var identificacion = $('#identificacion').val();
        var razon_social = $('#razon_social').val().toUpperCase();
        var grupo_contable_id = $('select[name=grupo_contable_id]').val();
        var direccion = $('#direccion').val().toUpperCase();
        var ubicaciongeo_id = ubicacion;
        var latitudgeo = $('#latitudgeo').val();
        var longitudgeo = $('#longitudgeo').val();
        var tipo_contribuyente_id = $('select[name=tipo_contribuyente_id]').val();
        var contribuyente_especial = $('#contribuyente_especial').prop('checked');
        if (contribuyente_especial) {
            contribuyente_especial = 1;
        } else {
            contribuyente_especial = 0;
        }
        var telefonos = [];
        var celulares = [];
        var correos = [];
        for (var i = 0; i < cantidad_telefonos; i++) {
            var telefono = $('#telefono' + i).val();
            telefonos.push(telefono);
        }
        for (var i = 0; i < cantidad_celulares; i++) {
            var celular = $('#celular' + i).val();
            celulares.push(celular);
        }

        for (var i = 0; i < cantidad_correos; i++) {
            var correo = $('#correo' + i).val().toUpperCase();
            correos.push(correo);
        }

        var forma_pago_id = $('select[name=forma_pago_id]').val();
        var estado = $('#estado').prop('checked');
        if (estado) {
            estado = 1;
        } else {
            estado = 0;
        }

        var cliente_prepago = $('#cliente_prepago').prop('checked');
        var valor_prepago = null;
        if (cliente_prepago) {
            cliente_prepago = 1;
            valor_prepago = $('#valor_prepago').val();
        } else {
            cliente_prepago = 0;
        }

        var cliente_pospago = $('#cliente_pospago').prop('checked');
        var valor_pospago = null;
        if (cliente_pospago) {
            cliente_pospago = 1;
            valor_pospago = $('#valor_pospago').val();
        } else {
            cliente_pospago = 0;
        }

        var auxiliar = 0;
        var plazo_credito_id = $('select[name=plazo_credito_id]').val();
        var categoria_cliente_id = $('select[name=categoria_cliente_id]').val();
        var retencion_iva_b_id = $('select[name=retencion_iva_b_id]').val();
        var retencion_ir_b_id = $('select[name=retencion_ir_b_id]').val();
        var retencion_iva_s_id = $('select[name=retencion_iva_s_id]').val();
        var retencion_ir_s_id = $('select[name=retencion_ir_s_id]').val();
        var estado_civil_id = $('select[name=estado_civil_id]').val();
        var sexo_id = $('select[name=sexo_id]').val();
        var origen_ingreso_id = $('select[name=origen_ingreso_id]').val();

        var eliminado = 0;

        var operacion = $('#operacion').val();

        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id, codigo: codigo,
            identificacion: identificacion, razon_social: razon_social, direccion: direccion, latitudgeo: latitudgeo, longitudgeo: longitudgeo,
            contribuyente_especial: contribuyente_especial, telefonos: telefonos, celulares: celulares, correos: correos,
            estado: estado, cliente_prepago: cliente_prepago, valor_prepago: valor_prepago, cliente_pospago: cliente_pospago, valor_pospago: valor_pospago, auxiliar: auxiliar, eliminado: eliminado,
            grupo_contable_id: grupo_contable_id, tipo_contribuyente_id: tipo_contribuyente_id, ubicaciongeo_id: ubicaciongeo_id,
            forma_pago_id: forma_pago_id, plazo_credito_id: plazo_credito_id, categoria_cliente_id: categoria_cliente_id, retencion_iva_b_id: retencion_iva_b_id,
            retencion_iva_s_id: retencion_iva_s_id, retencion_ir_b_id: retencion_ir_b_id, retencion_ir_s_id: retencion_ir_s_id,
            estado_civil_id: estado_civil_id, sexo_id: sexo_id, origen_ingreso_id: origen_ingreso_id};
        if (operacion === 'crear') {
            var resultado = peticion(data, 'crearCliente');
            if (resultado) {
                $('#mensaje').addClass('alert-success');
                $('#mensaje').html('Se ha creado el cliente correctamente');
                $('#respuesta').modal();
                $('#formulario').reset();
            } else {
                $('#mensaje').addClass('alert-danger');
                $('#mensaje').html('Error al crear el cliente');
                $('#respuesta').modal();
            }
        } else {
            var resultado = peticion(data, 'actualizar');
            if (resultado) {
                $('#mensaje').addClass('alert-success');
                $('#mensaje').html('El cliente se ha actualizado correctamente');
                $('#respuesta').modal();
            } else {
                $('#mensaje').addClass('alert-danger');
                $('#mensaje').html('Error al actualizar el cliente');
                $('#respuesta').modal();
            }
        }
    });
    // </editor-fold>

    $("#tipo_contribuyente_id").on("change", function ()
    {
        cambiarContribuyenteEspecial();
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite cambiar a contribuyente especial">
    function cambiarContribuyenteEspecial()
    {
        var subtipo_id = parseInt($('select[name=tipo_contribuyente_id]').val());
        for (var i = 0; i < tiposContribuyentes.length; i++)
        {
            if (subtipo_id === tiposContribuyentes[i].id && tiposContribuyentes[i].especial === 1)
            {
                $('#especial').prop("disabled", false);
                $('#especial').prop('checked', false);
            } else if (subtipo_id === tiposContribuyentes[i].id && tiposContribuyentes[i].especial === 0)
            {
                $('#especial').prop("disabled", true);
                $('#especial').prop('checked', false);
            }
        }
    }
    // </editor-fold>

    function cambioIdentificacion() {
        var identificacion = $("#identificacion").val();
        if (identificacion) {
            var tipo = null;
            if (identificacion.length == 10) {
                tipo = "C";
                var bandera = verificarCedula(identificacion);
                if (bandera)
                {
                    $('#tipo_identificacion').val(tipo);
                    obtenerTiposContribuyentes_C_R(tipo);
                } else
                {
                    alert("Error en cedula");
                }
            } else if (identificacion === "9999999999999") {
                tipo = "CF";
                obtenerTiposContribuyentes_CF_PL_E(tipo);
            } else if (identificacion.length == 13 && parseInt(identificacion.substr(2, 1)) == 6) {
                tipo = "R";
                var bandera = verificarSociedadesPublicas(identificacion);
                if (bandera)
                {
                    $('#tipo_identificacion').val(tipo);
                    obtenerTiposContribuyentes_PP(tipo, identificacion);
                } else
                {
                    alert("Error en el RUC");
                }
            } else if (identificacion.length == 13 && parseInt(identificacion.substr(2, 1)) == 9) {
                tipo = "R";
                var bandera = verificarSociedadesPrivadas(identificacion);
                if (bandera)
                {
                    $('#tipo_identificacion').val(tipo);
                    obtenerTiposContribuyentes_PP(tipo, identificacion);
                } else
                {
                    alert("Error en el RUC");
                }
            } else if (identificacion.length == 13)
            {
                tipo = "C";
                var tipo2 = "R";
                var bandera = verificarPersonaNatural(identificacion);
                if (bandera)
                {
                    $('#tipo_identificacion').val(tipo2);
                    obtenerTiposContribuyentes_C_R(tipo);
                } else
                {
                    alert("Error en el RUC");
                }
            } else if (identificacion.length <= 8)
            {
                tipo = "PL";
                var bandera = verificarPlaca(identificacion);
                if (bandera)
                {
                    obtenerTiposContribuyentes_CF_PL_E(tipo);
                } else
                {
                    alert("Error en placa");
                }

            } else
            {
                tipo = "E";
                obtenerTiposContribuyentes_CF_PL_E(tipo);
            }
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Evento al cambiar el numero de identificacion para verificacion">
    $("#identificacion").on("change", function ()
    {
        cambioIdentificacion();
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para verificar la identificacion (cedula)">
    function verificarCedula(cedula)
    {
        var numv = 10;
        var div = 11;
        var coeficientes;
        if (parseInt(cedula[2]) < 6)
        {
            coeficientes = [2, 1, 2, 1, 2, 1, 2, 1, 2];
            div = 10;
        } else
        {
            if (parseInt(cedula[2]) === 6)
            {
                coeficientes = [3, 2, 7, 6, 5, 4, 3, 2];
                numv = 9;
            } else
            {
                coeficientes = [4, 3, 2, 7, 6, 5, 4, 3, 2];
            }
        }
        var total = 0;
        var provincias = 24;
        var calculo = 0;
        if ((parseInt(cedula[2]) <= 6 || parseInt(cedula[2]) === 9) && (parseInt(cedula[0] + cedula[1]) <= provincias))
        {
            for (var i = 0; i < numv - 1; i++)
            {
                calculo = parseInt(cedula[i]) * coeficientes[i];
                if (div === 10)
                {
                    total += calculo > 9 ? calculo - 9 : calculo;
                } else
                {
                    total += calculo;
                }
            }
            return (div - (total % div)) >= 10 ? 0 === parseInt(cedula[numv - 1]) : (div - (total % div)) === parseInt(cedula[numv - 1]);
        } else
        {
            return false;
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para verificar la identificacion sociedades publicas">
    function verificarSociedadesPublicas(ruc)
    {
        var modulo = 11;
        var total = 0;
        var coeficientes = [3, 2, 7, 6, 5, 4, 3, 2];
        var numeroProvincia = ruc[0] + ruc[1];
        var establecimiento = ruc.substr(9, 4);
        if (parseInt(numeroProvincia) >= 1 && parseInt(numeroProvincia) <= 24 && establecimiento === '0001')
        {
            var digitoVerificador = parseInt(ruc[8]);
            for (var i = 0; i < coeficientes.length; i++)
            {
                total = total + (coeficientes[i] * parseInt(ruc[i]));
            }
            var digitoVerificadorObtenido = modulo - (total % modulo);
            return digitoVerificadorObtenido == digitoVerificador;
        }
        false;
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para verificar la identificacion de persona natural">
    function verificarPersonaNatural(ruc)
    {
        var provincia = parseInt(ruc[0] + ruc[1]);
        var personaNatural = parseInt(ruc[2]);
        if (provincia >= 1 && provincia <= 24 && personaNatural >= 0 && personaNatural < 6)
        {
            return ruc.substr(10, 3) === '001' && verificarCedula(ruc.substr(0, 10));
        }
        return false;
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para verificar las sociedades privadas">
    function verificarSociedadesPrivadas(ruc)
    {
        var modulo = 11;
        var total = 0;
        var coeficientes = [4, 3, 2, 7, 6, 5, 4, 3, 2];
        var numeroProvincia = ruc[0] + ruc[1];
        var establecimiento = ruc.substr(10, 3);
        if (parseInt(numeroProvincia) >= 1 && parseInt(numeroProvincia) <= 24 && establecimiento === '001')
        {
            var digitoVerificador = parseInt(ruc[9]);
            for (var i = 0; i < coeficientes.length; i++)
            {
                total = total + (coeficientes[i] * parseInt(ruc[i]));
            }
            var digitoVerificadorObtenido = (total % modulo) === 0 ? 0 : modulo - (total % modulo);

            return digitoVerificadorObtenido === digitoVerificador;
        }
        return false;
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para verificar la placa de un vehiculo">
    function verificarPlaca(placa)
    {
        var letrasPrimeras = ['A', 'B', 'U', 'C', 'H', 'X', 'O', 'E', 'W', 'G', 'I', 'L', 'R', 'M', 'V', 'N', 'Q', 'S', 'P', 'Y', 'J', 'K', 'T', 'Z'];
        var matriculasEspeciales = ['CC', 'CD', 'OI', 'AT', 'IT'];
        if (placa.length == 7)
        {
            var dosPrimerasLetas = placa[0] + placa[1];
            var bandera = matriculasEspeciales.indexOf(dosPrimerasLetas);
            if (bandera >= 0)
            {
                return true;
            } else
            {
                return false;
            }

        } else
        {
            var bandera = letrasPrimeras.indexOf(placa[0]);
            if (bandera >= 0)
            {
                return true;
            } else
            {
                return false;
            }
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener el codigo del cliente">
    function obtenerCodigoCliente()
    {
        var codigo = $('#codigo').val();
        if (!codigo) {
            var data = {'_token': $('meta[name=csrf-token]').attr('content')};
            var resultado = peticion(data, 'obtenerCodigoCliente');
            if (resultado != null) {
                $('#codigo').val(resultado);
            } else {
                alert('ERROR AL OBTENER EL CODIGO PARA EL CLIENTE');
            }
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/clientes/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});

