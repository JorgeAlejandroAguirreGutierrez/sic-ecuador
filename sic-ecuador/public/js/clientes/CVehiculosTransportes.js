/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function ()
{
    $(document).ready(function ()
    {
        obtenerCodigoVehiculoTransporte();
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener el codigo de un vehiculo de transporte">
    function obtenerCodigoVehiculoTransporte()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCodigoVehiculoTransporte');
        if (resultado != null) {
            $('#codigo').val(resultado);
        } else {
            alert('ERROR AL OBTENER EL CODIGO DEL VEHICULO DE TRANSPORTE');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para crear un vehiculo de transporte">
    $("#crearVehiculoTransporte").on("click", function () {
        var codigo = $('#codigo').val().toUpperCase();
        var modelo = $('#modelo').val();
        var placa = $('#placa').val().toUpperCase();
        var marca = $('#marca').val().toUpperCase();
        var cilindraje = $('#cilindraje').val();
        var clase = $('#clase').val().toUpperCase();
        var color = $('#color').val().toUpperCase();
        var fabricacion = $('#fabricacion').val();
        var activo = $('#activo').prop('checked');
        if (activo) {
            activo = 1;
        } else {
            activo = 0;
        }
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, modelo: modelo, placa: placa,
            marca: marca, cilindraje: cilindraje, clase: clase, color: color, fabricacion: fabricacion, activo: activo};
        var resultado = peticion(data, 'crearVehiculoTransporte');
        if (resultado) {
            $('#mensaje').addClass('alert-success');
            $('#mensaje').html('Se ha creado un vehiculo de transporte correctamente');
            $('#respuesta').modal();
            limpiarFormulario('formulario');
        } else {
            $('#mensaje').addClass('alert-danger');
            $('#mensaje').html('Error al crear un vehiculo de transporte');
            $('#respuesta').modal();
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion)
    {
        var respuesta;
        $.ajax({
            type: "POST",
            url: '/vehiculostransportes/' + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});