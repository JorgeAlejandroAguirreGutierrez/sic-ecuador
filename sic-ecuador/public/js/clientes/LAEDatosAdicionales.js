/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    //<editor-fold defaultstate="collapsed" desc="Evento para obtener el dato adicional para modificacion">
    $('[name=modificarDatoAdicional]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerDatoAdicional');
        if (resultado != null) {
            $('#idM').val(resultado.id);
            $('#codigoM').val(resultado.codigo);
            $('#tipoM').val(resultado.tipo);
            $('#nombreM').val(resultado.nombre);
            $('#abreviaturaM').val(resultado.abreviatura);
        } else {
            alert('ERROR AL OBTENER EL DATO ADICIONAL');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para obtener el dato adicional para eliminacion">
    $('[name=eliminarDatoAdicional]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerDatoAdicional');
        if (resultado) {
            $('#idE').val(resultado.id);
            $('#codigoE').val(resultado.codigo);
            $('#tipoE').val(resultado.tipo);
            $('#nombreE').val(resultado.nombre);
            $('#abreviaturaE').val(resultado.abreviatura);
        } else {
            alert('ERROR AL OBTENER EL DATO ADICIONAL');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para modificacion de dato adicional">
    $('#cModificarDatoAdicional').on("click", function ()
    {
        var id = $('#idM').val();
        var codigo = $('#codigoM').val().toUpperCase();
        var tipo = $('#tipoM').val().toUpperCase();
        var nombre = $('#nombreM').val().toUpperCase();
        var abreviatura = $('#abreviaturaM').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id, codigo: codigo, tipo: tipo, nombre: nombre, abreviatura: abreviatura};
        var resultado = peticion(data, 'modificarDatoAdicional');
        if (resultado) {
            window.location.href = "/datosadicionales/mostrar";
        } else {
            alert('ERROR AL MODIFICAR EL DATO ADICIONAL');
        }
    });
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Evento para eliminacion de dato adicional">
    $('#cEliminarDatoAdicional').on("click", function ()
    {
        var id = $('#idE').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarDatoAdicional');
        if (resultado) {
            window.location.href = "/datosadicionales/mostrar";
        } else {
            alert('ERROR AL ELIMINAR EL DATO ADICIONAL');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion)
    {
        var respuesta;
        $.ajax({
            type: "POST",
            url: '/datosadicionales/' + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});

