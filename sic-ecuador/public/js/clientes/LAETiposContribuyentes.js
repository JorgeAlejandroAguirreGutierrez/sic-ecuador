/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    //<editor-fold defaultstate="collapsed" desc="Evento para obtener el tipo de contribuyente para modificacion o eliminacion ">
    $('[name=modificarTipoContribuyente]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerTipoContribuyente');
        if (resultado != null) {
            $('#idM').val(resultado.id);
            $('#codigoM').val(resultado.codigo);
            $('#tipoM').val(resultado.tipo);
            $('#subtipoM').val(resultado.subtipo);
            $('#especialM').prop('checked', resultado.especial);
        } else {
            alert('ERROR AL OBTENER EL TIPO DE CONTRIBUYENTE');
        }

    });

    
    $('[name=eliminarTipoContribuyente]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerTipoContribuyente');
        if (resultado != null) {
            $('#idE').val(resultado.id);
            $('#codigoE').val(resultado.codigo);
            $('#tipoE').val(resultado.tipo);
            $('#subtipoE').val(resultado.subtipo);
            $('#especialE').prop('checked', resultado.especial);
        } else {
            alert('ERRRO AL OBTENER EL TIPO DE CONTRIBUYENTE');
        }
    });
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Evento para modificar el tipo de contribuyente">
    $('#cModificarTipoContribuyente').on("click", function ()
    {
        var id = $('#idM').val();
        var codigo = $('#codigoM').val().toUpperCase();
        var tipo = $('#tipoM').val().toUpperCase();
        var subtipo = $('#subtipoM').val().toUpperCase();
        var especial = $('#especial').prop('checked');
        if (especial) {
            especial = 1;
        } else {
            especial = 0;
        }
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id, codigo: codigo, tipo: tipo, subtipo: subtipo, especial: especial};
        var resultado = peticion(data, 'modificarTipoContribuyente');
        if (resultado) {
            window.location.href = "/tiposcontribuyentes/mostrar";
        } else {
            alert(resultado);
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para eliminar el tipo de contribuyente">
    $('#cEliminarTipoContribuyente').on("click", function ()
    {
        var id = $('#idE').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarTipoContribuyente');
        if (resultado) {
            window.location.href = "/tiposcontribuyentes/mostrar";
        } else {
            alert('ERROR AL ELIMINAR EL TIPO DE CONTRIBUYENTE');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Function peticion de datos">
    function peticion(data, funcion)
    {
        var respuesta;
        $.ajax({
            type: "POST",
            url: '/tiposcontribuyentes/' + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});

