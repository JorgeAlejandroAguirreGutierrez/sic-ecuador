/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function ()
{

    $(document).ready(function ()
    {
        obtenerCodigoUbicaciongeo();
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener un codigo de ubicacion">
    function obtenerCodigoUbicaciongeo()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCodigoUbicaciongeo');
        if (resultado != null) {
            $('#codigo').val(resultado);
        } else {
            alert('ERROR AL OBTENER EL CODIGO PARA LA UBICACION');
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para crear una ubicacion">
    $("#crearUbicaciongeo").on("click", function () {
        var codigo = $('#codigo').val().toUpperCase();
        var codigo_norma = $('#codigo_norma').val();
        var provincia = $('#provincia').val().toUpperCase();
        var canton = $('#canton').val().toUpperCase();
        var parroquia = $('#parroquia').val().toUpperCase();
        if (provincia === '' || canton === '' || parroquia === '') {
            $('#provincia').addClass('is-invalid');
            $('#canton').addClass('is-invalid');
            $('#parroquia').addClass('is-invalid');
            $('[name=invalido]').text('Error en el campo');
        }
        if (provincia !== '' || canton !== '' || parroquia !== '') {
            var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, codigo_norma: codigo_norma, provincia: provincia, canton: canton, parroquia: parroquia};
            var resultado = peticion(data, 'crearUbicaciongeo');
            if (resultado) {
                $('#mensaje').addClass('alert-success');
                $('#mensaje').html('Se ha creado una ubicaciongeo correctamente');
                $('#respuesta').modal();
                limpiarFormulario('formulario');
            } else {
                $('#mensaje').addClass('alert-danger');
                $('#mensaje').html('Error al crear una ubicaciongeo');
                $('#respuesta').modal();
            }
        }
    });
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion)
    {
        var respuesta;
        $.ajax({
            type: "POST",
            url: '/ubicacionesgeo/' + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});

