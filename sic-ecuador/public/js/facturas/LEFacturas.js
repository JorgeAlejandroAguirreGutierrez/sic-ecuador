/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    //<editor-fold defaultstate="collapsed" desc="Evento para redireccionar a modificar factura">
    $('[name=modificarFactura]').on("click", function ()
    {
        var id = $(this).attr('id');
        window.location.href = "/facturas/modificar?id=" + id;
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para obtener los datos para eliminar factura">
    $('[name=eliminarFactura]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerCabeceraFactura');
        if (resultado != null) {
            $('#codigoE').val(resultado.codigo);
            $('#codigo_internoE').val(resultado.codigo_interno);
            $('#totalE').val(resultado.total);
            $('#punto_ventaE').val(resultado.punto_venta.codigo);
            if (resultado.descarga_inventario == 1) {
                $('#descarga_inventarioE').val('SI');
            } else {
                $('#descarga_inventarioE').val('NO');
            }
            $('#vendedorE').val(resultado.vendedor.nombre);
            $('#cajeroE').val(resultado.cajero.nombre);
        } else {
            alert('ERROR AL OBTENER LA CABECERA DE LA FACTURA');
        }
    });
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Evento para eliminar la factura">
    $('#cEliminarFactura').on("click", function ()
    {
        var id = $('#idE').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarCabeceraFactura');
        if (resultado) {
            window.location.href = "/facturas/mostrar";
        } else {
            alert('ERROR AL ELIMINAR LA FACTURA');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para redireccionar a cobros de la factura">
    $('[name=cobroFactura]').on("click", function ()
    {
        var id = $(this).attr('id');
        window.location.href = '/recaudacionesefectivo/crear?id=' + id;
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/facturas/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});