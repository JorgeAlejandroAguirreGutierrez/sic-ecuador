/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 *  NO TIENE RUC LOS AUXILIARES PORQUE ES EL PRINCIPAL
 *  TRAER LA FECHA DE FACTURACION PERO CON POSIBILIDAD DE MODIFICARLA
 *  FECHA DE FACTURA DIA/MES/AÑO.
 *  MODIFICAR DATOS DEL CLIENTE DESDE LA PANTALLA DE FACTURAS
 *  EVENTO FOCUS PARA DETECTAR CAMBIOS EN PANTALLA
 *  ABRIR SUBVENTANA DE CREACION DE CLIENTE DENTRO DE LA PANTALLA DE FACTURACION USANDO TECLA FUNCION F5 TANTO EN NOMBRE DEL CLIENTE COMO EN RUC
 *  ABRIR SUBVENTANA DE MODIFICACION DE UN CLIENTE DENTRO DE LA PANTALLA DE FACTURACION USANDO TECLA FUNCION F5 TANTO EN NOMBRE DEL CLIENTE COMO EN RUC
 *  ABRIR SUBVENTANA DE CREACION DE AUXILIARES DENTRO DE LA PANTALLA DE FACTURACION USANDO TECLA FUNCION F5 TANTO EN NOMBRE DEL CLIENTE COMO EN RUC
 *  ABRIR SUBVENTANA DE MODIFICACION DE UN AUXILIAR DENTRO DE LA PANTALLA DE FACTURACION USANDO TECLA FUNCION F5 TANTO EN NOMBRE DEL CLIENTE COMO EN RUC
 *  CODIGO INTERNO Y SECUENCIA DE FACTURA GENERAR CUANDO SE GUARDE LA FACTURA 
 *  TELEFONO, CELULAR Y CORREO SEPARADOS POR GUIONES
 *  
 *  USAR AUX. DISMINUIR TAMAÑO Y AGREGARLE TAMAÑO A DIRECCION AUXILIAR
 *  
 *  
 *  -------------------------------------------------------
 *  08/06
 *  POR DEFECTO CANTIDAD EN 1 -->LISTO
 *  SERVICIOS EN CONSIGNACION PONER UN GUION
 *  SERVICIOS EN MEDIDA POR DEFECTO "UNIDAD"
 *  SERVICIOS EN DESCARGA INVENTARIO UN GUION
 *  
 *  EN PROCESO DE FACTURACION CON 4 DECIMALES
 *  EN TOTALES Y RESUMEN DE IMPUESTOS CON DOS DECIMALES
 *  
 *  ULTIMO COSTO: COSTO EN COMPRAS
 *  COSTO PROMEDIO: COSTO EN KARDEX
 *  TOTAL UNIDADES FACTURA= SUMA DE CANTIDADES
 *  
 *  --------------
 *  
 *  12/06 
 *  PARA SERVICIOS ABRIR VENTANA CON EVENTO DE TECLA Y QUE PUEDA SELECCIONAR LOS SERVICIOS
 *  Disminuir espacio manualmente descarga inventario, cantidad
 *  Disminuir espacio entre filas
 *  
 */
$(function ()
{
    var lineas_factura = 1;
    var linea_factura = null;

    var cliente = null;
    var auxiliar = null;
    var transportista = null;
    var vehiculo_transporte = null;
    var vendedor = null;
    var cajero = null;

    $(document).ready(function ()
    {
        limpiarFormulario();
        obtenerEstablecimiento();
        obtenerPuntoVenta();
        obtenerFecha();
        obtenerSucursal();
        obtenerClientes();
        obtenerEstablecimientosGR();
        obtenerBodegas();
        obtenerProductosBodega(0);
        obtenerTransportistas();
        obtenerVehiculosTransportes();
        obtenerVendedores();
        obtenerCajeros();

    });

    $('#razon_social_clienteB').autocomplete({
        source: [],
        select: function (event, ui) {
            var cliente = ui.item.val;
            obtenerCliente(cliente);
        },
        change: function (event, ui) {
            cliente = (ui.item ? ui.item.val : null);
        }
    });
    $('#identificacion_clienteB').autocomplete({
        source: [],
        select: function (event, ui) {
            var cliente = ui.item.value;
            obtenerCliente(cliente);
        },
        change: function (event, ui) {
            cliente = (ui.item ? ui.item.label : null)
        }
    });

    $('#razon_social_auxiliar_clienteB').autocomplete({
        source: [],
        select: function (event, ui) {
            var auxiliar_cliente = ui.item.value;
            completarAuxiliar(auxiliar_cliente);
        },
        change: function (event, ui) {
            $(this).val((ui.item ? ui.item.label : ""));
            auxiliar = (ui.item ? ui.item.label : null);
        }
    });

    $('#transportistaB').autocomplete({
        source: [],
        select: function (event, ui) {
            transportista = ui.item.val;
        },
        change: function (event, ui) {
            $(this).val((ui.item ? ui.item.label : ""));
            transportista = (ui.item ? ui.item.label : null);
        }
    });

    $('#vehiculo_transporteB').autocomplete({
        source: [],
        select: function (event, ui) {
            vehiculo_transporte = ui.item.val;
        },
        change: function (event, ui) {
            $(this).val((ui.item ? ui.item.label : ""));
            transportista = (ui.item ? ui.item.label : null);
        }
    });

    $('#vendedorB').autocomplete({
        source: [],
        select: function (event, ui) {
            vendedor = ui.item.val;
        },
        change: function (event, ui) {
            $(this).val((ui.item ? ui.item.label : ""));
            vendedor = (ui.item ? ui.item.label : null);
        }
    });
    ;

    $('#cajeroB').autocomplete({
        source: [],
        select: function (event, ui) {
            cajero = ui.item.val;
        },
        change: function (event, ui) {
            $(this).val((ui.item ? ui.item.label : ""));
            cajero = (ui.item ? ui.item.label : null);
        }
    });

    $('#nombre_sB').autocomplete({
        source: [],
        select: function (event, ui) {
            $('#idS').val(ui.item.value);
        },
        change: function (event, ui) {
            $(this).val((ui.item ? ui.item.label : ""));
        }
    });

    $('#nombre_pB0').autocomplete({
        source: [],
        select: function (event, ui) {
            var linea=event.target.id;
            linea=linea.charAt(linea.length - 1);
            var id=ui.item.val;
            $('#id_ps' + linea).val(id);
            cambioLineaFactura(linea, id);
        },
        change: function (event, ui) {
            $(this).val((ui.item ? ui.item.label : ""));
        }
    });

    var eventoLineaFactura = {
        source: [],
        select: function (event, ui) {
            var linea=event.target.id;
            linea=linea.charAt(linea.length - 1);
            var id=ui.item.val;
            $('#id_ps'+linea).val(id);
            cambioLineaFactura(linea, id);
        },
        change: function (event, ui) {
            $(this).val((ui.item ? ui.item.label : ""));
        }
    }

    shortcut.add("F1", function () { //para creacion de un cliente

    });
    shortcut.add("F2", function () { //para modificacion de un cliente 

    });
    shortcut.add("F4", function () { //para seleccionar un servicio 
        obtenerServicios();
        $('#modalObtenerServicios').modal('toggle');
        $('#modalObtenerServicios').modal('show');
    });

    //<editor-fold defaultstate="collapsed" desc="Resumen Consignacion, IVA, Tipo de bien">
    shortcut.add("F8", function () {
        var id = $('#id_ps' + linea_factura).val();
        var codigo = $('#codigo_ps' + linea_factura).val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id, codigo: codigo};
        var resultado = peticion(data, 'obtenerPS', '/utilidades/');
        $('#nombre_ps').val(resultado.nombre);
        if (resultado.consignacion == null) {
            $('#consignacion_ps').val('-');
        } else {
            $('#consignacion_ps').val('SI');
        }
        $('#tipo_ps').val(resultado.tipo.abreviatura);
        $('#impuesto_ps').val(resultado.impuesto.codigo_norma);
        $('#modalObtenerResumenProductosServicios').modal('toggle');
        $('#modalObtenerResumenProductosServicios').modal('show');
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Limpiar Formulario">
    function limpiarFormulario()
    {
        $("#formulario")[0].reset();
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Nuevo item de factura">
    $("#nuevoItemFactura").on("click", function ()
    {
        var div = $('#items_factura');
        var div_row = $('<div>', {class: 'form-row'});
        var grupo1 = $('<div>', {class: 'form-group col-1'});
        var grupo2 = $('<div>', {class: 'form-group col-4'});
        var grupo3 = $('<div>', {class: 'form-group col-5'});
        var grupo3_row = $('<div>', {class: 'form-row'});
        var grupo4 = $('<div>', {class: 'form-group col-2'});
        var input0 = $('<input>', {type: 'hidden', name: 'id_ps', id: 'id_ps' + lineas_factura, value: '0'});
        var input1 = $('<input>', {type: 'text', class: 'form-control form-control-sm', name: 'codigo_ps', id: 'codigo_ps' + lineas_factura, disabled: 'true'});
        var input2 = $('<input>', {class: 'form-control form-control-sm', name: 'nombre_pB', id: 'nombre_pB' + lineas_factura});
        input2.autocomplete(eventoLineaFactura);
        var input4 = $('<input>', {type: 'text', class: 'form-control form-control-sm col-2', name: 'medida_ps', id: 'medida_ps' + lineas_factura, disabled: 'true'});
        var input5 = $('<select>', {class: 'form-control form-control-sm col-2', name: 'descarga_inventario_ps', id: 'descarga_inventario_ps' + lineas_factura});
        var option1 = $('<option>', {value: '0', text: 'No'});
        var option2 = $('<option>', {value: '1', text: 'Si'});
        input5.append(option1);
        input5.append(option2);
        var input6 = $('<input>', {type: 'number', class: 'form-control form-control-sm col-2', name: 'cantidad_ps', id: 'cantidad_ps' + lineas_factura, value: '1'});
        var input7 = $('<input>', {type: 'text', class: 'form-control form-control-sm col-2', name: 'precio_ps', id: 'precio_ps' + lineas_factura, disabled: 'true'});
        var input9 = $('<input>', {type: 'text', class: 'form-control form-control-sm col-1', name: 'descuento_ps', id: 'descuento_ps' + lineas_factura, value: '0'});
        var input10 = $('<input>', {type: 'text', class: 'form-control form-control-sm col-1', name: 'porcentaje_descuento_ps', id: 'porcentaje_descuento_ps' + lineas_factura, value: '0'});
        var input11 = $('<input>', {type: 'text', class: 'form-control form-control-sm col-2', name: 'total_neto_ps', id: 'total_neto_ps' + lineas_factura, value: '0'});
        var input12 = $('<input>', {type: 'text', class: 'form-control form-control-sm', name: 'total_ps', id: 'total_ps' + lineas_factura, disabled: 'true'});
        grupo1.append(input0);
        grupo1.append(input1);
        grupo2.append(input2);
        grupo3_row.append(input4);
        grupo3_row.append(input5);
        grupo3_row.append(input6);
        grupo3_row.append(input7);
        grupo3_row.append(input9);
        grupo3_row.append(input10);
        grupo3_row.append(input11);
        grupo3.append(grupo3_row);
        grupo4.append(input12);
        div_row.append(grupo1);
        div_row.append(grupo2);
        div_row.append(grupo3);
        div_row.append(grupo4);
        div.append(div_row);
        obtenerProductosBodega(lineas_factura);
        lineas_factura++;
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Crear factura">
    $("#guardarFactura").on("click", function ()
    {
        var codigo = null;
        var codigo_interno = null;
        var auxiliar_cliente = $('select[name=auxiliar_cliente]').val();
        var descarga_inventario = $('select[name=descarga_inventario]').val();
        var guia_remision = $('select[name=guia_remision]').val();
        var guia_remision_id = null;
        var factura_anulada = 0;
        var subtotal = $('#subtotal').val();
        var importe_iva = $('#importe_iva').val();
        var total = $('#total').val();
        var punto_venta_id = $('select[name=punto_venta_id]').val();
        var punto_venta_gr_id = null;
        if (guia_remision == 1) {
            punto_venta_gr_id = $('select[name=punto_venta_gr_id]').val();
            guia_remision_id = $('#guia_remision_id').val();
        }
        var cliente_id = cliente;
        var auxiliar_id = null;
        if (auxiliar_cliente == 1) {
            auxiliar_id = auxiliar;
        }
        var bodega_id = $('select[name=bodega_id]').val();
        var transportista_id = transportista;
        var vehiculo_transporte_id = vehiculo_transporte;
        var vendedor_id = vendedor;
        var cajero_id = cajero;
        var cabecera_factura_id = null;

        var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, codigo_interno: codigo_interno,
            auxiliar: auxiliar, auxiliar_id: auxiliar_id, descarga_inventario: descarga_inventario, guia_remision: guia_remision,
            guia_remision_id: guia_remision_id, factura_anulada: factura_anulada, subtotal: subtotal, importe_iva: importe_iva, total: total,
            punto_venta_id: punto_venta_id, punto_venta_gr_id: punto_venta_gr_id, cliente_id: cliente_id, bodega_id: bodega_id,
            transportista_id: transportista_id, vehiculo_transporte_id: vehiculo_transporte_id,
            vendedor_id: vendedor_id, cajero_id: cajero_id};
        var resultado = peticion(data, 'crearCabeceraFactura');
        var bandera = true;
        if (resultado != null) {
            cabecera_factura_id = resultado.id;
            $('#id').val(resultado.id);
            $('#codigo').val(resultado.codigo);
            $('#codigo_interno').val(resultado.codigo_interno);
        } else {
            bandera = false;
            alert('ERROR AL CREAR LA CABECERA DE LA FACTURA');
        }
        if (bandera) {
            bandera = true;
            for (var i = 0; i < lineas_factura; i++) {
                var linea = i;
                var descarga_inventario = $('#descarga_inventario_ps' + i).val();
                var cantidad = $('#cantidad_ps' + i).val();
                var descuento = $('#descuento_ps' + i).val();
                var codigo = $('#codigo_ps' + i).val();
                var producto_id = $('#id_ps' + i).val();
                var servicio_id = $('#id_ps' + i).val();
                var total = $('#total_ps' + i).val();

                var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo};
                var resultado = peticion(data, 'esProducto', '/productos/');
                if (resultado) {
                    servicio_id = null;
                } else {
                    producto_id = null;
                }
                var data = {'_token': $('meta[name=csrf-token]').attr('content'), linea: linea, descarga_inventario: descarga_inventario,
                    cantidad: cantidad, descuento: descuento, total: total, producto_id: producto_id, servicio_id: servicio_id,
                    cabecera_factura_id: cabecera_factura_id};
                var resultado = peticion(data, 'crearLineaFactura');
                if (resultado) {
                    $('#cobroFactura').attr('disabled', false);
                } else {
                    bandera = false;
                    alert("ERROR AL CREAR LA LINEA DE FACTURA");
                }
            }
        }
        if (bandera) {
            alert('CREADA LA FACTURA CORRECTAMENTE');
        } else {
            alert('ERROR AL CREAR LA FACTURA');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Obtener Establecimientos">
    function obtenerEstablecimiento()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerEstablecimientoUsuario', '/usuarios/');
        if (resultado != null) {
            $('#establecimiento_id').val(resultado.codigo);
        } else {
            alert('ERROR AL OBTENER EL ESTABLECIMIENTO');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Obtener puntos de ventas de un establecimiento">
    function obtenerPuntoVenta()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerPuntoVentaUsuario', '/usuarios/');
        if (resultado != null) {
            $('#punto_venta_id').val(resultado.codigo);
        } else {
            alert('ERROR AL OBTENER EL ESTABLECIMIENTO');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Obtener clientes">
    function obtenerClientes()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerClientes', '/clientes/');
        if (resultado != null) {
            var razones_sociales_clientes = [];
            var identificaciones_clientes = [];
            for (var i = 0; i < resultado.length; i++) {
                var opcion1 = {val: resultado[i].id, label: resultado[i].razon_social};
                razones_sociales_clientes.push(opcion1);
                var opcion2 = {val: resultado[i].id, label: resultado[i].identificacion};
                identificaciones_clientes.push(opcion2);
            }
            $('#razon_social_clienteB').autocomplete('option', 'source', razones_sociales_clientes);
            $('#identificacion_clienteB').autocomplete('option', 'source', identificaciones_clientes);
        } else {
            alert('ERROR AL OBTENER LOS CLIENTES');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Obtener cliente (id)">
    function obtenerCliente(id)
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerCliente', '/clientes/');
        if (resultado != null) {
            $('#codigo_cliente').val(resultado.codigo);
            $('#razon_social_clienteB').val(resultado.razon_social);
            $('#identificacion_clienteB').val(resultado.identificacion);
            $('#direccion_cliente').val(resultado.direccion);
            $('#telefono_cliente').val(resultado.telefono);
            $('#celular_cliente').val(resultado.celular);
            $('#correo_cliente').val(resultado.correo);
            if (resultado.prepago == 1) {
                $('#prepago_cliente').val('Si');
                $('#valor_prepago_cliente').val('');
            } else {
                $('#prepago_cliente').val('No');
                $('#valor_prepago_cliente').val(resultado.cupo_venta);
            }
        } else {
            alert('ERROR AL OBTENER EL CLIENTE');
        }
        $('#razon_social_auxiliar_clienteB').empty();
    }
    // </editor-fold>

    $('#auxiliar_cliente').on('change', function ()
    {
        var auxiliar_cliente = $('select[name=auxiliar_cliente]').val();
        if (auxiliar_cliente == 1) {
            gestionarAuxiliar();
        } else {
            gestionarAuxiliarNo();
        }
    });

    //<editor-fold defaultstate="collapsed" desc="Gestionar Auxiliar">
    function gestionarAuxiliar()
    {
        $('#razon_social_auxiliar_clienteB').prop('disabled', false);
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), cliente_id: cliente};
        var resultado = peticion(data, 'obtenerAuxiliaresCliente', '/auxiliares/');
        for (var i = 0; i < resultado.length; i++) {
            var opcion = $('<option>', {value: resultado[i].id, text: resultado[i].razon_social})
            $('#razon_social_auxiliar_clienteB').append(opcion);
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Completar Auxiliar (auxiliar_cliente_id)">
    function completarAuxiliar(auxiliar_cliente_id)
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: auxiliar_cliente_id};
        var resultado = peticion(data, 'obtenerAuxiliar', '/auxiliares/');
        if (resultado != null) {
            $('#identificacion_auxiliar_cliente').val($('#identificacion_clienteB').val());
            $('#direccion_auxiliar_cliente').val(resultado.direccion);
            $('#telefono_cliente').val(resultado.telefono);
            $('#celular_cliente').val(resultado.celular);
            $('#correo_cliente').val(resultado.correo);
        } else {
            alert('ERROR AL OBTENER EL AUXILIAR');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Gestionar Auxiliar No">
    function gestionarAuxiliarNo()
    {
        $('#razon_social_auxiliar_clienteB').val('');
        $('#razon_social_auxiliar_clienteB').empty();
        $('#razon_social_auxiliar_clienteB').prop('disabled', true);
        $('#identificacion_auxiliar_cliente').val('');
        $('#identificacion_auxiliar_cliente').prop('disabled', true);
        $('#direccion_auxiliar_cliente').val('');

        var cliente = $('#identificacion_clienteB').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: cliente};
        var resultado = peticion(data, 'obtenerCliente', '/clientes/');
        $('#telefono_cliente').val(resultado.telefono);
        $('#celular_cliente').val(resultado.celular);
        $('#correo_cliente').val(resultado.correo);
    }
    // </editor-fold>

    /**
     * Ontiene la fecha de factura (FUNCIONANDO 1.0)
     * @returns {undefined}
     */
    //<editor-fold defaultstate="collapsed" desc="Obtener fecha">
    function obtenerFecha()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerFecha');
        if (resultado != null) {
            $('#fecha').val(resultado);
        } else {
            alert('ERROR AL OBTENER LA FECHA');
        }
    }
    // </editor-fold>

    /**
     * Obtiene las sucursales para insertar en factura (FUNCIONANDO 1.0)
     * @returns {undefined}
     */
    //<editor-fold defaultstate="collapsed" desc="Obtener sucursal">
    function obtenerSucursal()
    {
        var establecimiento_id = $('select[name=establecimiento_id]').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: establecimiento_id};
        var resultado = peticion(data, 'obtenerSucursal');
        if (resultado != null) {
            $('#codigo_sucursal').val(resultado);
        } else {
            alert('ERROR AL OBTENER LA SUCURSAL');
        }
    }
    // </editor-fold>

    /**
     * Obtiene las bodegas para insertar en la factura (FUNCIONANDO 1.0)
     * @returns {undefined}
     */
    //<editor-fold defaultstate="collapsed" desc="Obtener bodegas">
    function obtenerBodegas()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerBodegasUsuario', '/bodegas/');
        if (resultado != null) {
            $('#bodega_id').empty();
            for (var i = 0; i < resultado.length; i++) {
                var option = $('<option>', {value: resultado[i].id, text: resultado[i].codigo});
                $('#bodega_id').append(option);
            }
            
        } else {
            alert('ERROR AL OBTENER LAS BODEGAS');
        }
    }
    // </editor-fold>

    $('#bodega_id').on('change', function () {
        var bodega_id = $('select[name=bodega_id]').val();
        obtenerProductosBodega(bodega_id);

    });

    $('#cliente_prepago').on('change', function () {
        var establecimiento_id = $('select[name=establecimiento_id]').val();
        obtenerPuntosVentasEstablecimiento(establecimiento_id);
    });

    /**
     * Obtiene los estasblecimientos para guia de remision (FUNCIONANDO 1.0)
     * @returns {undefined}
     */
    //<editor-fold defaultstate="collapsed" desc="Obtener establecimientos de guia de remision">
    function obtenerEstablecimientosGR()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerEstablecimientos', '/establecimientos/');
        $('#establecimiento_gr_id').empty();
        for (var i = 0; i < resultado.length; i++)
        {
            var option = $('<option>', {value: resultado[i].id, text: resultado[i].codigo});
            $('#establecimiento_gr_id').append(option);
        }
        var establecimiento_id = $('select[name=establecimiento_gr_id]').val();
        obtenerPuntosVentasEstablecimientoGR(establecimiento_id);
    }
    // </editor-fold>

    /**
     * Obtiene los puntos de ventas de un establecimiento (FUNCIONANDO 1.0)
     * @param {integer} establecimiento_id
     * @returns {undefined}
     */
    //<editor-fold defaultstate="collapsed" desc="Obtener puntos de ventas de un establecimiento para guia de remision">
    function obtenerPuntosVentasEstablecimientoGR(establecimiento_id)
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), establecimiento_id: establecimiento_id};
        var resultado = peticion(data, 'obtenerPuntosVentasEstablecimiento', '/puntosventas/');
        if (resultado != null) {
            $('#punto_venta_gr_id').empty();
            for (var i = 0; i < resultado.length; i++)
            {
                var option = $('<option>', {value: resultado[i].id, text: resultado[i].codigo});
                $('#punto_venta_gr_id').append(option);
            }
        } else {
            alert('ERROR AL OBTENER LOS PUNTOS DE VENTA POR ESTABLECIMIENTO');
        }
    }
    // </editor-fold>

    $('#guia_remision').on('change', function () {
        var guia_remision = $('select[name=guia_remision]').val();
        if (guia_remision == 1)
        {
            $('#transportistaB').prop('disabled', false);
            $('#vehiculo_transporteB').prop('disabled', false);
            $('#establecimiento_gr_id').prop('disabled', false);
            $('#punto_venta_gr_id').prop('disabled', false);
            obtenerEstablecimientosGR();
        } else
        {
            $('#establecimiento_gr_id').empty();
            $('#establecimiento_gr_id').prop('disabled', true);
            $('#punto_venta_gr_id').empty();
            $('#punto_venta_gr_id').prop('disabled', true);
            $('#secuencia_guia_remision').val('');
            $('#transportistaB').prop('disabled', true);
            $('#transportistaB').val('');
            transportista = null;
            $('#vehiculo_transporteB').prop('disabled', true);
            $('#vehiculo_transporteB').val('');
            vehiculo_transporte = null;

        }
    });

    $('#establecimiento_gr_id').on('change', function () {
        var establecimiento_id = $('select[name=establecimiento_gr_id]').val();
        obtenerPuntosVentasEstablecimientoGR(establecimiento_id);

    });

    /**
     * Obtiene los transportistas para guias de remision (FUNCIONANDO 1.0)
     * @returns {undefined}
     */
    //<editor-fold defaultstate="collapsed" desc="Obtener transportistas">
    function obtenerTransportistas()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerTransportistas', '/transportistas/');
        if (resultado != null) {
            var transportistas = [];
            for (var i = 0; i < resultado.length; i++) {
                var opcion = {val: resultado[i].id, label: resultado[i].nombre};
                transportistas.push(opcion);
            }
            $('#transportistaB').autocomplete('option', 'source', transportistas);
        } else {
            alert('ERROR AL OBTENER LOS TRANSPORTISTAS');
        }
    }
    // </editor-fold>

    /**
     * Obtiene los vehiculos de transporte para guia de remision (FUNCIONANDO 1.0)
     * @returns {undefined}
     */
    //<editor-fold defaultstate="collapsed" desc="Obtener vehiculos de transporte">
    function obtenerVehiculosTransportes()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerVehiculosTransportes', '/vehiculostransportes/');
        if (resultado != null) {
            var vehiculos_transportes = [];
            for (var i = 0; i < resultado.length; i++) {
                var opcion = {val: resultado[i].id, label: resultado[i].modelo + ' ' + resultado[i].placa + ' ' + resultado[i].marca + ' ' + resultado[i].cilindraje + ' ' + resultado[i].clase + ' ' + resultado[i].color + ' ' + resultado[i].fabricacion};
                vehiculos_transportes.push(opcion);
            }
            $('#vehiculo_transporteB').autocomplete('option', 'source', vehiculos_transportes);
        } else {
            alert('ERROR AL OBTENER VEHICULOS DE TRANSPORTES');
        }
    }
    // </editor-fold>

    /**
     * Obtiene los vendedores para insertar en la factura (FUNCIONANDO 1.0)
     * @returns {undefined}
     */
    //<editor-fold defaultstate="collapsed" desc="Obtener vendedores">
    function obtenerVendedores()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerVendedores', '/usuarios/');
        if (resultado != null) {
            var vendedores = [];
            for (var i = 0; i < resultado.length; i++) {
                var opcion = {val: resultado[i].id, label: resultado[i].nombre};
                vendedores.push(opcion);
            }
            $('#vendedorB').autocomplete('option', 'source', vendedores);
        } else {
            alert('ERROR AL OBTENER LOS VENDEDORES');
        }
    }
    // </editor-fold>

    /**
     * Obtiene los cajeros para insertar en factura (FUNCIONANDO 1.0)
     * @returns {undefined}
     */
    //<editor-fold defaultstate="collapsed" desc="Obtener cajeros">
    function obtenerCajeros()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCajeros', '/usuarios/');
        if (resultado != null) {
            var cajeros = [];
            for (var i = 0; i < resultado.length; i++) {
                var opcion = {val: resultado[i].id, label: resultado[i].nombre};
                cajeros.push(opcion);
            }
            $('#cajeroB').autocomplete('option', 'source', cajeros);
        } else {
            alert('ERROR AL OBTENER LOS CAJEROS');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Obtener Productos de una bodega">
    function obtenerProductosBodega(linea_factura)
    {
        var bodega_id = $('select[name=bodega_id]').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), bodega_id};
        var resultado = peticion(data, 'obtenerProductosBodega', '/productos/');
        if (resultado != null) {
            var productos = [];
            for (var i = 0; i < resultado.length; i++) {
                var opcion = {val: resultado[i].id, label: resultado[i].nombre, tipo: 'PRODUCTO'};
                productos.push(opcion);
            }
            $('#nombre_pB' + linea_factura).autocomplete('option', 'source', productos);
        } else {
            alert('ERROR AL OBTENER LOS PRODUCTOS POR BODEGA');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Obtener servicios">
    function obtenerServicios()
    {
        $('#nombre_sB').empty();
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerServicios', '/servicios/');
        if (resultado != null) {
            for (var i = 0; i < resultado.length; i++) {
                var opcion = $('<option>', {value: resultado[i].id, text: resultado[i].nombre});
                $('#nombre_sB').append(opcion);
            }
        } else {
            alert('ERROR AL OBTENER LOS SERVICIOS');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento change en nombre_pB">
    function cambioLineaFactura(linea, id) {
        console.log(linea);
        console.log(id);
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerProducto', '/productos/');
        if (resultado != null) {
            $('#id_ps' + linea).val(resultado.id);
            $('#codigo_ps' + linea).val(resultado.codigo);
            $('#medida_ps' + linea).val(resultado.medida.abreviatura);
            $('#precio_ps' + linea).val(resultado.precio);
            cambiarTotalPS(linea);
            cambiarTotalNetoPS(linea);
            cambiarTotalUnidades();
            existenciasCostoPromedioUltimo(linea);
            resumen();
        } else {
            alert('ERROR AL OBTENER EL PRODUCTO');
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento seleccionar servicio">
    $('#cSeleccionServicio').on("click", function () {
        var id = $('#idS').val();
        var item = linea_factura;
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerServicio', '/servicios/');
        if (resultado != null) {
            $('#id_ps' + item).val(resultado.id);
            $('#codigo_ps' + item).val(resultado.codigo);
            $('#nombre_pB' + item).val(resultado.nombre);
            $('#medida_ps' + item).val('U');
            var descarga_inventario = $('#descarga_inventario_ps' + item);
            descarga_inventario.replaceWith($('<input>', {type: 'text', class: 'form-control form-control-sm col-2', name: 'descarga_inventario_ps', id: 'descarga_inventario_ps' + item, value: '-', disabled: 'true'}));
            var precio = $('#precio_ps' + item);
            var input = $('<select>', {class: 'form-control form-control-sm col-2', name: 'precio_ps', id: 'precio_ps' + item});
            var option1 = $('<option>', {value: parseFloat(resultado.precio1.toFixed(4)), text: parseFloat(resultado.precio1.toFixed(4))});
            var option2 = $('<option>', {value: parseFloat(resultado.precio2.toFixed(4)), text: parseFloat(resultado.precio2.toFixed(4))});
            var option3 = $('<option>', {value: parseFloat(resultado.precio3.toFixed(4)), text: parseFloat(resultado.precio3.toFixed(4))});
            var option4 = $('<option>', {value: parseFloat(resultado.precio4.toFixed(4)), text: parseFloat(resultado.precio4.toFixed(4))});
            input.append(option1);
            input.append(option2);
            input.append(option3);
            input.append(option4);
            precio.replaceWith(input);
            cambiarTotalPS(item);
            cambiarTotalUnidades();
            existenciasCostoPromedioUltimo(item);
            resumen();
            $('#modalObtenerServicios').modal('hide');
        } else {
            alert('ERROR AL OBTENER EL SERVICIO');
        }
    });
    // </editor-fold>


    $('#items_factura').on('change', '[name=cantidad_ps]', function () {
        var id = $(this).attr('id');
        var item = id.charAt(id.length - 1);
        cambiarTotalNetoPS(item);
        cambiarTotalPS(item);
        cambiarTotalUnidades();
        resumen();

    });

    $('#items_factura').on('change', '[name=descuento_ps]', function () {
        var id = $(this).attr('id');
        var item = id.charAt(id.length - 1);
        $('#total_descuento').val(0);
        $('#total_porcentaje_descuento').val(0);
        cambiarPorcentajeDescuentoPS(item);
        cambiarTotalNetoPS(item);
        cambiarTotalPS(item);
        resumen();

    });

    $('#items_factura').on('change', '[name=porcentaje_descuento_ps]', function () {
        var id = $(this).attr('id');
        var item = id.charAt(id.length - 1);
        $('#total_descuento').val(0);
        $('#total_porcentaje_descuento').val(0);
        cambiarDescuentoPS(item);
        cambiarTotalNetoPS(item);
        cambiarTotalPS(item);
        resumen();

    });

    //<editor-fold defaultstate="collapsed" desc="Cambiar total neto por linea">
    function cambiarTotalNetoPS(item)
    {
        var cantidad = parseFloat($('#cantidad_ps' + item).val());
        var precio_ps = parseFloat($('#precio_ps' + item).val());
        var descuento_ps = parseFloat($('#descuento_ps' + item).val());
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), precio_ps: precio_ps, descuento_ps: descuento_ps, cantidad: cantidad};
        var resultado = peticion(data, 'cambiarTotalNetoPS');
        if (resultado != null) {
            $('#total_neto_ps' + item).val(resultado);
        } else {
            alert('ERROR AL OBTENER EL TOTAL NETO');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Cambiar total para cada item (item)">
    function cambiarTotalPS(item)
    {
        var cantidad = $('#cantidad_ps' + item).val();
        var precio_ps = $('#precio_ps' + item).val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), precio_ps: precio_ps, cantidad: cantidad};
        var resultado = peticion(data, 'cambiarTotalPS');
        if (resultado != null) {
            $('#total_ps' + item).val(resultado);
        } else {
            alert('ERROR AL OBTENER EL TOTAL');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Cambiar porcentaje de descuento PS (item)">
    function cambiarPorcentajeDescuentoPS(item)
    {
        var precio_ps = $('#precio_ps' + item).val();
        var cantidad = $('#cantidad_ps' + item).val();
        var descuento_ps = $('#descuento_ps' + item).val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), precio_ps: precio_ps, descuento_ps: descuento_ps, cantidad: cantidad};
        var resultado = peticion(data, 'cambiarPorcentajeDescuentoPS');
        if (resultado != null) {
            $('#porcentaje_descuento_ps' + item).val(resultado);
        } else {
            alert('ERROR AL OBTENER EL PORCENTAJE DE DESCUENTO');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Cambiar descuento PS (item)">
    function cambiarDescuentoPS(item)
    {
        var precio_ps = $('#precio_ps' + item).val();
        var cantidad = $('#cantidad_ps' + item).val();
        var porcentaje_descuento_ps = $('#porcentaje_descuento_ps' + item).val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), precio_ps: precio_ps, porcentaje_descuento_ps: porcentaje_descuento_ps, cantidad: cantidad};
        var resultado = peticion(data, 'cambiarDescuentoPS');
        if (resultado != null) {
            $('#descuento_ps' + item).val(resultado);
        } else {
            alert('ERROR AL OBTENER EL PORCENTAJE DE DESCUENTO');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Cambiar total descuento PS (item)">
    function cambiarTotalDescuentoPS(item) {
        var total_descuento = parseFloat($('#total_descuento').val());
        var total_cantidad = parseInt($('#total_unidades').val());
        var cantidad = parseInt($('#cantidad_ps' + item).val());
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), total_descuento: total_descuento, total_cantidad: total_cantidad, cantidad: cantidad};
        var resultado = peticion(data, 'cambiarTotalDescuentoPS');
        if (resultado != null) {
            $('#descuento_ps' + item).val(resultado);
        } else {
            alert('ERROR AL OBTENER EL TOTAL DEL DESCUENTO');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="resumen">
    function resumen()
    {
        var total_ps = 0;
        var subtotal = 0;
        var descuento = 0;
        var base12 = 0;
        var base0 = 0;
        var importe_iva = 0;
        var total = 0;

        for (var i = 0; i < lineas_factura; i++) {
            var id = $('#id_ps' + i).val();
            var codigo = $('#codigo_ps' + i).val();
            total_ps = parseFloat($('#total_ps' + i).val());
            subtotal = total_ps + subtotal;
            var descuento_ps = parseFloat($('#descuento_ps' + i).val())
            descuento = descuento_ps + descuento;
            var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id, codigo: codigo};
            var resultado = peticion(data, 'obtenerPS', '/utilidades/');
            if (resultado != null) {
                if (resultado.impuesto.porcentaje == 12) {
                    base12 = total_ps - descuento_ps + base12;
                } else if (resultado.impuesto.porcentaje == 0) {
                    base0 = total_ps - descuento_ps + base0;
                }
            } else {
                alert('ERROR AL OBTENER SI ES PRODUCTO O SERVICIO');
            }
        }
        importe_iva = base12 * 12 / 100;
        total = importe_iva + base0 + base12;
        subtotal = parseFloat(subtotal.toFixed(2));
        descuento = parseFloat(descuento.toFixed(2));
        base0 = parseFloat(base0.toFixed(2));
        base12 = parseFloat(base12.toFixed(2));
        importe_iva = parseFloat(importe_iva.toFixed(2));
        total = parseFloat(total.toFixed(2));

        $('#subtotal').val(subtotal);
        $('#descuento').val(descuento);
        $('#base0').val(base0);
        $('#base12').val(base12);
        $('#importe_iva').val(importe_iva);
        $('#total').val(total);
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Cambiar total de unidades">
    function cambiarTotalUnidades()
    {
        var total_unidades = 0;
        for (var i = 0; i < lineas_factura; i++) {
            total_unidades = parseInt($('#cantidad_ps' + [i]).val()) + total_unidades;
        }
        $('#total_unidades').val(total_unidades);
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Extencias de producto, costo promedio y ultimo costo">
    function existenciasCostoPromedioUltimo(item)
    {
        var id = $('#id_ps' + item).val();
        var codigo = $('#codigo_ps' + item).val();
        if (id !== '0') {
            var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id, codigo: codigo};
            var resultado = peticion(data, 'obtenerPS', '/utilidades/');
            if (resultado != null) {
                if (resultado.costo_kardex != null) {
                    $('#costo_promedio').val(resultado.costo_kardex);
                } else {
                    $('#costo_promedio').val('-');
                }
                if (resultado.costo_compra != null) {
                    $('#ultimo_costo').val(resultado.costo_compra);
                } else {
                    $('#ultimo_costo').val('-');
                }
                if (resultado.cantidad != null) {
                    $('#existencias_producto').val(resultado.cantidad);
                } else {
                    $('#existencias_producto').val('-');
                }
            } else {
                alert('ERROR AL OBTENER SI ES PRODUCTO O SERVICIO');
            }
        }
    }
    // </editor-fold>

//    $('#items_factura').on('focus', '[name=nombre_pB]', function () {
//        var id = $(this).attr('id');
//        var item = id.charAt(id.length - 1);
//        linea_factura = item;
//        existenciasCostoPromedioUltimo(item);
//    });

    $('#total_descuento').on('change', function () {
        for (var i = 0; i < lineas_factura; i++) {
            cambiarTotalDescuentoPS(i);
            cambiarPorcentajeDescuentoPS(i);
            cambiarTotalNetoPS(i);
            cambiarTotalPS(i);
        }
        $('#total_porcentaje_descuento').val(0);
        resumen();
    });

    $('#total_porcentaje_descuento').on('change', function () {
        var total_porcentaje_descuento = parseFloat($('#total_porcentaje_descuento').val());
        for (var i = 0; i < lineas_factura; i++)
        {
            $('#porcentaje_descuento_ps' + i).val(total_porcentaje_descuento);
            cambiarDescuentoPS(i);
            cambiarTotalPS(i);
        }
        $('#total_descuento').val(0);
        resumen();
    });


    //<editor-fold defaultstate="collapsed" desc="Resumen de impuestos">
    function resumenImpuestos()
    {
        var bienes_gravados_0 = 0;
        var bienes_gravados_12 = 0;
        var servicios_gravados_0 = 0;
        var servicios_gravados_12 = 0;
        var activos_fijos_gravados_0 = 0;
        var activos_fijos_gravados_12 = 0;
        var iva_bienes = 0;
        var iva_servicios = 0;
        var iva_activos_fijos = 0;
        var rete_iva = 0;
        var rete_fuente_ir = 0;
        for (var i = 0; i < lineas_factura; i++) {
            var id = $('#id_ps' + i).val();
            var codigo = $('#codigo_ps' + i).val();
            var cantidad = parseInt($('#cantidad_ps' + i).val());
            var precio = parseFloat($('#precio_ps' + i).val());
            var precio_cantidad = precio * cantidad;
            var descuento = parseFloat($('#descuento_ps' + i).val());
            var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo};
            var resultado = peticion(data, 'esProducto', '/productos/');
            if (resultado) {
                data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
                var resultado = peticion(data, 'obtenerProducto', '/productos/');
                if (resultado != null) {
                    if (resultado.impuesto.porcentaje == 0)
                        bienes_gravados_0 = precio_cantidad - descuento + bienes_gravados_0;
                    else if (resultado.impuesto.porcentaje == 12)
                        bienes_gravados_12 = precio_cantidad - descuento + bienes_gravados_12;
                    iva_bienes = (precio_cantidad - descuento) * resultado.impuesto.porcentaje / 100 + iva_bienes;
                } else {
                    alert('ERROR AL OBTENER EL PRODUCTO');
                }
            } else {
                data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
                var resultado = peticion(data, 'esServicio', '/servicios/');
                if (resultado) {
                    data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
                    var resultado = peticion(data, 'obtenerServicio', '/productos/');
                    if (resultado != null) {
                        if (resultado.impuesto.porcentaje == 0)
                            servicios_gravados_0 = precio_cantidad - descuento + servicios_gravados_0;
                        else if (resultado.impuesto.porcentaje == 12)
                            servicios_gravados_12 = precio_cantidad - descuento + servicios_gravados_12;
                        iva_servicios = (precio_cantidad - descuento) * resultado.impuesto.porcentaje / 100 + iva_servicios;
                    } else {
                        alert('ERROR AL OBTENER EL SERVICIO');
                    }
                } else {
                    data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
                    resultado = peticion(data, 'obtenerServicio', '/productos/');
                    if (resultado != null) {
                        if (resultado.impuesto.porcentaje == 0)
                            activos_fijos_gravados_0 = precio_cantidad - descuento + activos_fijos_gravados_0;
                        else if (resultado.impuesto.porcentaje == 12)
                            activos_fijos_gravados_12 = precio_cantidad - descuento + activos_fijos_gravados_12;
                        iva_activos_fijos = (precio_cantidad - descuento) * resultado.impuesto.porcentaje / 100 + iva_activos_fijos;
                    } else {
                        alert('ERROR AL OBTENER EL SERVICIO');
                    }
                }
            }
        }
        $('#bienes_gravados_0').val(parseFloat(bienes_gravados_0.toFixed(2)));
        $('#bienes_gravados_12').val(parseFloat(bienes_gravados_12.toFixed(2)));
        $('#servicios_gravados_0').val(parseFloat(servicios_gravados_0.toFixed(2)));
        $('#servicios_gravados_12').val(parseFloat(servicios_gravados_12.toFixed(2)));
        $('#activos_fijos_gravados_0').val(parseFloat(activos_fijos_gravados_0.toFixed(2)));
        $('#activos_fijos_gravados_12').val(parseFloat(activos_fijos_gravados_12.toFixed(2)));
        $('#iva_bienes').val(parseFloat(iva_bienes.toFixed(2)));
        $('#iva_servicios').val(parseFloat(iva_servicios.toFixed(2)));
        $('#iva_activos_fijos').val(parseFloat(iva_activos_fijos.toFixed(2)));
        $('#rete_iva').val(rete_iva);
        $('#rete_fuente_ir').val(rete_fuente_ir);
    }
    // </editor-fold>

    $("#resumenImpuestos").on("click", function () {
        resumenImpuestos();
    });

    $('#cobroFactura').on("click", function () {
        var cabecera_factura_id = $('#id').val();
        window.location.href = "/recaudacionesefectivo/crear?id=" + cabecera_factura_id;
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/facturas/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});

