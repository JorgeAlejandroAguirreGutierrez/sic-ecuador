/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    $(document).ready(function ()
    {
        obtenerCodigoUsuario();
        obtenerTipos();
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener el codigo de un usuario">
    function obtenerCodigoUsuario()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCodigoUsuario');
        if (resultado != null) {
            $('#codigo').val(resultado);
        } else {
            alert('ERROR AL OBTENER EL CODIGO DE USUARIO');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento que permite crear un usuario">
    $('#crearUsuario').on("click", function ()
    {
        var codigo = $('#codigo').val();
        var nombre = $('#nombre').val();
        var correo = $('#correo').val();
        var contrasena = $('#contrasena').val();
        var identificacion = $('#identificacion').val();
        var tipo_id = $('select[name=tipo_id]').val();
        var activo = $('#activo').prop('checked');
        if (activo) {
            activo = 1;
        } else {
            activo = 0;
        }
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, nombre: nombre, correo: correo, contrasena: contrasena,
            identificacion: identificacion, tipo_id: tipo_id, activo:activo};
        var resultado = peticion(data, 'crear');
        if (resultado) {
            window.location.href = '/usuarios/mostrar';
        } else {
            alert("ERROR AL CREAR UN USUARIO");
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener los tipos de usuarios">
    function obtenerTipos()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), tipo: 'TIPOS USUARIOS'};
        var resultado = peticion(data, 'obtenerDatosAdicionales', '/datosadicionales/');
        if (resultado != null) {
            if (resultado.length > 0) {
                for (var i = 0; i < resultado.length; i++) {
                    var option = $('<option>', {value: resultado[i].id, text: resultado[i].nombre});
                    $('#tipo_id').append(option);
                }
            }
        } else {
            alert('ERROR AL OBTENER LOS TIPOS');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/usuarios/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});

