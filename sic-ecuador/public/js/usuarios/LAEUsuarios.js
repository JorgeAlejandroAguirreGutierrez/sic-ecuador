/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    //<editor-fold defaultstate="collapsed" desc="Evento que permite obtener los datos para eliminar un usuario">
    $('[name=eliminarUsuario]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerUsuario');
        if (resultado != null) {
            $('#idE').val(resultado.id);
            $('#codigoE').val(resultado.codigo);
            $('#nombreE').val(resultado.nombre);
            $('#identificacionE').val(resultado.identificacion);
        } else {
            alert('ERROR AL OBTENER EL USUARIO');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento que permite eliminar un usuario">
    $('#cEliminarUsuario').on("click", function ()
    {
        var id = $('#idE').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarUsuario');
        if (resultado) {
            window.location.href = '/usuarios/mostrar';
        } else {
            alert("ERROR");
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/usuarios/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});
