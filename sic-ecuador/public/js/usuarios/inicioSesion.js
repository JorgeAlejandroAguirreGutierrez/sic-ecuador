/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{

    $(document).ready(function ()
    {
        obtenerEmpresa();
//        verificarIniciarSesion();
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite verificar un inicio de sesion">
    function verificarIniciarSesion()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'verificarIniciarSesion');
        if (resultado) {
            window.location.href = '/facturas/mostrar';
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Evento para iniciar sesion">
    $('#iniciarSesion').on("click", function ()
    {
        var identificacion = $('#identificacion').val();
        var contrasena = $('#contrasena').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), identificacion: identificacion, contrasena: contrasena};
        var resultado = peticion(data, 'iniciarSesion');
        if (resultado != null) {
            window.location.href = '/' + resultado;
        } else {
            alert("ERROR AL INICIAR SESION");
        }
    });

    //<editor-fold defaultstate="collapsed" desc="Evento que permite activar una sesion">
    $('#activarSesion').on("click", function ()
    {
        var identificacion = $('#identificacion_AS').val();
        var contrasena = $('#contrasena_AS').val();
        var repetir_contrasena = $('#repetir_contrasena_AS').val();
        var correo = $('#correo_AS').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), identificacion: identificacion, contrasena: contrasena, repetir_contrasena: repetir_contrasena, correo: correo};
        var resultado = peticion(data, 'activarSesion');
        if (resultado != null) {
            alert("Se ha activado exitosamente");
            window.location.href = '/';
        } else {
            alert("ERROR AL ACTIVAR EL USUARIO");
        }
    });
    
    function obtenerEmpresa() {
        var id=1;
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id:id};
        var peticion = peticion(data, 'obtenerEmpresa', '/empresa/');
        if (peticion.respuesta) {
            alert(peticion.mensaje);
        } else {
            alert(peticion.mensaje);
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/usuarios/')
    {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function (){
                respuesta = null;
            }
        });
        return respuesta;
    }
});

