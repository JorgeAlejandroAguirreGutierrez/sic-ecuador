/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    var tabs = $("#tabs").tabs();
    var plantilla = "<li><a href='#{href}' id='#{id}'>#{label}</a> <span class='ui-icon ui-icon-close' role='presentation'>Remove Tab</span></li>";
    var conteo = 0;

    var tabs = $("#tabs").tabs();
    tabs.find(".ui-tabs-nav").sortable({
        axis: "x",
        stop: function () {
            tabs.tabs("refresh");
        }
    });

    // Close icon: removing the tab on click
    tabs.on("click", "span.ui-icon-close", function () {
        var panelId = $(this).closest("li").remove().attr("aria-controls");
        $("#" + panelId).remove();
        tabs.tabs("refresh");
    });

    // Actual addTab function: adds new tab using the input from the form above
    function agregarVentana(titulo, contenido) {
        var id = titulo + "-" + conteo;
        var li = $(plantilla.replace(/#\{href\}/g, "#" + id).replace(/#\{label\}/g, titulo).replace(/#\{id\}/g, contenido));
        var hidden = $('<input>', {type: 'hidden', id: titulo, val: id});
        li.append(hidden);
        tabs.find(".ui-tabs-nav").append(li);
        var div = $('<div>', {id: id, class: 'embed-responsive embed-responsive-16by9'});
        var iframe = $('<iframe>', {src: contenido, class: 'embed-responsive-item'});
        div.append(iframe);
        tabs.append(div);
        tabs.tabs("refresh");
        conteo++;
    }
    
    $('#cerrarSesion').on("click", function () {
        cerrarSesion();
    });
    
    function cerrarSesion() {
        var data={};
        var solicitud = peticion(data, 'cerrarSesion', '/usuarios/');
        var respuesta=solicitud.respuesta;
        if (respuesta) {
            window.location.href = respuesta.resultado;
        }
    }

    $("#tabs").tabs().on("dblclick", '[role="tab"]', function(event) {
        var contenido=event.target.id;
        window.open(contenido,'','width=900px, height=600px toolbar=yes');
    });

    $('#clientes').on("click", function () {
        agregarVentana('clientes', '/clientes/c');
    });

    $('#facturas').on("click", function () {
        agregarVentana('facturas', '/facturas/c');
    });

    $('#egresosinventarios').on("click", function () {
        agregarVentana('egresosinventarios', '/egresosinventarios/c');
    });

    $('#gruposcontables').on("click", function () {
        agregarVentana('gruposcontables', '/gruposcontables/c');
    });

    $('#plazoscreditos').on("click", function () {
        agregarVentana('plazoscreditos', '/plazoscreditos/c');
    });

    $('#tiposcontribuyentes').on("click", function () {
        agregarVentana('tiposcontribuyentes', '/tiposcontribuyentes/c');
    });
    $('#retenciones').on("click", function () {
        agregarVentana('retenciones', '/retenciones/c');
    });
    $('#impuestos').on("click", function () {
        agregarVentana('impuestos', '/impuestos/c');
    });
    $('#servicios').on("click", function () {
        agregarVentana('servicios', '/servicios/c');
    });
    $('#transportistas').on("click", function () {
        agregarVentana('transportistas', '/transportistas/c');
    });
    $('#vehiculostransportes').on("click", function () {
        agregarVentana('vehiculostransportes', '/vehiculostransportes/c');
    });

    $('#ubicacionesgeo').on("click", function () {
        agregarVentana('ubicacionesgeo', '/ubicacionesgeo/c');
    });
    $('#datosadicionales').on("click", function () {
        agregarVentana('datosadicionales', '/datosadicionales/c');
    });
    $('#empresas').on("click", function () {
        agregarVentana('empresas', '/empresas/c');
    });
    $('#establecimientos').on("click", function () {
        agregarVentana('establecimientos', '/establecimientos/c');
    });
    $('#puntosventas').on("click", function () {
        agregarVentana('puntosventas', '/puntosventas/c');
    });

    $('#usuarios').on("click", function () {
        agregarVentana('usuarios', '/usuarios/c');
    });

    $('#bodegas').on("click", function () {
        agregarVentana('bodegas', '/bodegas/c');
    });
    $('#productos').on("click", function () {
        agregarVentana('productos', '/productos/c');
    });
    
    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/usuarios/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data;
                respuesta = resultado;
            },
            error: function ()
            {
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>

});

