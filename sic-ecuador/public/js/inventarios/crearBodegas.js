/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    $(document).ready(function ()
    {
        obtenerCodigoBodega();
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener el codigo de bodega">
    function obtenerCodigoBodega()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCodigoBodega');
        if (resultado != null) {
            $('#codigo').val(resultado);
        } else {
            alert('ERROR AL OBTENER EL CODIGO DE LA BODEGA');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para crear una bodega">
    $('#crearBodega').on("click", function ()
    {
        var codigo = $('#codigo').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo};
        var resultado = peticion(data, 'crear');
        if (resultado) {
            window.location.href = '/bodegas/mostrar';
        } else {
            alert("ERROR AL CREAR LA BODEGA");
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion) {
        var respuesta;
        $.ajax({
            type: "POST",
            url: '/bodegas/' + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});

