/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    $(document).ready(function ()
    {
        obtenerCodigoProducto();
        obtenerMedidas();
        obtenerImpuestos();
        obtenerTipos();
        obtenerBodegas();
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener un codigo para el producto">
    function obtenerCodigoProducto()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCodigoProducto');
        if (resultado != null) {
            $('#codigo').val(resultado);
        } else {
            alert('ERROR AL OBTENER EL CODIGO DEL PRODUCTO');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para crear un producto">
    $('#crearProducto').on("click", function ()
    {
        var codigo = $('#codigo').val();
        var nombre = $('#nombre').val();
        var cantidad = $('#cantidad').val();
        var precio = $('#precio').val();
        var consignacion = $('select[name=consignacion]').val();
        var costo_compra = $('#costo_compra').val();
        var costo_kardex = $('#costo_kardex').val();
        var medida_id = $('select[name=medida_id]').val();
        var impuesto_id = $('select[name=impuesto_id]').val();
        var tipo_id = $('select[name=tipo_id]').val();
        var bodega_id = $('select[name=bodega_id]').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, nombre: nombre, cantidad: cantidad, precio: precio, consignacion: consignacion, 
            costo_compra: costo_compra, costo_kardex: costo_kardex, medida_id: medida_id, impuesto_id: impuesto_id, tipo_id: tipo_id, bodega_id: bodega_id};
        var resultado = peticion(data, 'crear');
        if (resultado) {
            window.location.href = '/productos/mostrar';
        } else {
            alert("ERROR AL CREAR EL PRODUCTO");
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener las medidas para los productos">
    function obtenerMedidas()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), tipo: 'TIPOS MEDIDAS'};
        var resultado = peticion(data, 'obtenerDatosAdicionales', '/datosadicionales/');
        if (resultado != null) {
            if (resultado.length > 0) {
                for (var i = 0; i < resultado.length; i++) {
                    var option = $('<option>', {value: resultado[i].id, text: resultado[i].nombre});
                    $('#medida_id').append(option);
                }
            }
        } else {
            alert('ERROR AL OBTENER LAS MEDIDAS');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener los impuestos IVAs">
    function obtenerImpuestos()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerImpuestos', '/impuestos/');
        if (resultado != null) {
            if (resultado.length > 0) {
                for (var i = 0; i < resultado.length; i++) {
                    var option = $('<option>', {value: resultado[i].id, text: resultado[i].codigo_norma});
                    $('#impuesto_id').append(option);
                }
            }
        } else {
            alert('ERROR AL OBTENER LAS RETENCIONES IVA');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener los tipos de producto">
    function obtenerTipos()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), tipo: 'TIPOS PS'};
        var resultado = peticion(data, 'obtenerDatosAdicionales', '/datosadicionales/');
        if (resultado != null)
            if (resultado.length > 0)
            {
                for (var i = 0; i < resultado.length; i++)
                {
                    var option = $('<option>', {value: resultado[i].id, text: resultado[i].nombre});
                    $('#tipo_id').append(option);
                }
            }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener las bodegas">
    function obtenerBodegas()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerBodegas', '/bodegas/');
        if (resultado != null) {
            if (resultado.length > 0) {
                for (var i = 0; i < resultado.length; i++) {
                    var option = $('<option>', {value: resultado[i].id, text: resultado[i].codigo});
                    $('#bodega_id').append(option);
                }
            }
        } else {
            alert('ERROR AL OBTENER LAS BODEGAS');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/productos/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});