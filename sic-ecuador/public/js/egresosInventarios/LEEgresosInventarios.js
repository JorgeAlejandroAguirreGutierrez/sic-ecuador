/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    //<editor-fold defaultstate="collapsed" desc="Evento para redireccionar a modificar factura">
    $('[name=modificarEgresoInventario]').on("click", function ()
    {
        var id = $(this).attr('id');
        window.location.href = "/egresosinventarios/modificar?id=" + id;
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para obtener los datos para eliminar factura">
    $('[name=eliminarEgresoInventario]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerCabeceraEgresoInventario');
        if (resultado != null) {
            $('#codigo_E').val(resultado.codigo);
            $('#codigo_interno_E').val(resultado.codigo_interno);
            $('#total_E').val(resultado.total);
            $('#punto_venta_E').val(resultado.punto_venta.codigo);
            $('#vendedor_E').val(resultado.vendedor.nombre);
            $('#cajero_E').val(resultado.cajero.nombre);
        } else {
            alert('ERROR AL OBTENER LA CABECERA DEL EGRESO DE INVENTARIO');
        }
    });
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Evento para eliminar la factura">
    $('#cEliminarEgresoInventario').on("click", function ()
    {
        var id = $('#id_E').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarCabeceraEgresoInventario');
        if (resultado) {
            window.location.href = "/egresosinventarios/mostrar";
        } else {
            alert('ERROR AL ELIMINAR EL EGRESO DE INVENTARIO');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/egresosinventarios/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});