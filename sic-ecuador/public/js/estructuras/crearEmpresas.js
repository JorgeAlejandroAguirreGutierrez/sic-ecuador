/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    //<editor-fold defaultstate="collapsed" desc="Evento para crear empresa">
    $('#crearEmpresa').on("click", function ()
    {
        var codigo = $('#codigo').val();
        var razon_social = $('#razon_social').val().toUpperCase();
        var identificacion = $('#identificacion').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, razon_social: razon_social, identificacion: identificacion};
        var resultado = peticion(data, 'crear');
        if (resultado) {
            window.location.href = '/empresas/mostrar';
        } else {
            alert("No Registrado correctamente");
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/empresas/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});
