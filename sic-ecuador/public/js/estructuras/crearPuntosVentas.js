/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{

    $(document).ready(function ()
    {
        obtenerEmpresas();
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite crear un punto de venta">
    $('#crearPuntoVenta').on("click", function ()
    {
        var codigo = $('#codigo').val();
        var establecimiento_id = $('select[name=establecimiento_id]').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, establecimiento_id: establecimiento_id};
        var resultado = peticion(data, 'crear');
        if (resultado) {
            window.location.href = '/puntosventas/mostrar';
        } else {
            alert("No Registrado correctamente");
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion uqe permite obtener las empresas">
    function obtenerEmpresas()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerEmpresas', '/empresas/');
        if (resultado.length > 0) {
            for (var i = 0; i < resultado.length; i++) {
                var option = $('<option>', {value: resultado[i].id, text: resultado[i].razon_social});
                $('#empresa_id').append(option);
            }
            obtenerEstablecimientosEmpresa(resultado[0].id);
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener los establecimientos por una empresa">
    function obtenerEstablecimientosEmpresa(empresa_id)
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), empresa_id: empresa_id};
        var resultado = peticion(data, 'obtenerEstablecimientosEmpresa', '/establecimientos/');
        if (resultado != null) {
            $('#establecimiento_id').empty();
            for (var i = 0; i < resultado.length; i++)
            {
                var option = $('<option>', {value: resultado[i].id, text: resultado[i].direccion});
                $('#establecimiento_id').append(option);
            }
        } else {
            alert('ERROR AL OBTENER LOS ESTABLECIMIENTOS POR UNA EMPRESA');
        }
    }
    // </editor-fold>

    $('#empresa_id').on('change', function () {
        var empresa_id = $('select[name=empresa_id]').val();
        obtenerEstablecimientosEmpresa(empresa_id);
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/puntosventas/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>

});