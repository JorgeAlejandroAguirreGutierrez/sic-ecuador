/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    //<editor-fold defaultstate="collapsed" desc="Evento que permite obtener los datos para eliminar una empresa">
    $('[name=eliminarEmpresa]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerEmpresa');
        if (resultado != null) {
            $('#idE').val(resultado.id);
            $('#codigoE').val(resultado.codigo);
            $('#razon_socialE').val(resultado.razon_social);
            $('#identificacionE').val(resultado.identificacion);
        } else {
            alert('ERROR AL OBTENER LOS DATOS PARA ELIMINAR UNA EMPRESA');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento que permite eliminar una empresa">
    $('#cEliminarEmpresa').on("click", function ()
    {
        var id = $('#idE').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarEmpresa');
        if (resultado) {
            window.location.href = '/empresas/mostrar';
        } else {
            alert("ERROR AL ELIMINAR LA EMPRESA");
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/empresas/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});
