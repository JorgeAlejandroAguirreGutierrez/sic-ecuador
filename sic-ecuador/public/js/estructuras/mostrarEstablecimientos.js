/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    //<editor-fold defaultstate="collapsed" desc="Evento para obtener datos para eliminar un establecimiento">
    $('[name=eliminarEstablecimiento]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerEstablecimiento');
        if (resultado != null) {
            $('#idE').val(resultado.id);
            $('#codigoE').val(resultado.codigo);
            $('#direccionE').val(resultado.direccion);
            $('#provinciaE').val(resultado.ubicaciongeo.provincia);
            $('#cantonE').val(resultado.ubicaciongeo.canton);
            $('#parroquiaE').val(resultado.ubicaciongeo.parroquia);
            $('#empresaE').val(resultado.empresa.razon_social);
        } else {
            alert('ERROR AL OBTENER EL ESTABLECIMIENTO');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para eliminar un establecimiento">
    $('#cEliminarEstablecimiento').on("click", function ()
    {
        var id = $('#idE').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarEstablecimiento');
        if (resultado) {
            window.location.href = '/establecimientos/mostrar';
        } else {
            alert("ERROR");
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/establecimientos/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});