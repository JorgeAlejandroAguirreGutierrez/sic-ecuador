/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    $(document).ready(function ()
    {
        obtenerUbicaciones();
        obtenerEmpresas();
    });

    var ubicaciones = null;
    var provincia_seleccionada = null;
    var canton_seleccionada = null;
    var parroquia_seleccionada = null;

    var cantones_anteriores = null;
    var parroquias_anteriores = null;

    //<editor-fold defaultstate="collapsed" desc="Evento que permite crear el establecimiento">
    $('#crearEstablecimiento').on("click", function ()
    {
        var codigo = $('#codigo').val();
        var direccion = $('#direccion').val().toUpperCase();
        var ubicaciongeo_id = obtenerUbicaciongeo_id(provincia_seleccionada, canton_seleccionada, parroquia_seleccionada);
        var empresa_id = $('select[name=empresa_id]').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, direccion: direccion, ubicaciongeo_id: ubicaciongeo_id, empresa_id: empresa_id};
        var resultado = peticion(data, 'crear');
        if (resultado) {
            window.location.href = '/establecimientos/crear';
        } else {
            alert("No Registrado correctamente");
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener las ubicaciones">
    function obtenerUbicaciones()
    {
        $('#provincia').editableSelect();
        $('#canton').editableSelect();
        $('#parroquia').editableSelect();
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerUbicaciones', '/ubicacionesgeo/');
        if (resultado != null) {
            ubicaciones = resultado;
            var flags = [], provincias = [], l = ubicaciones.length, i;
            for (i = 0; i < l; i++) {
                if (flags[ubicaciones[i].provincia])
                    continue;
                flags[ubicaciones[i].provincia] = true;
                provincias.push(ubicaciones[i].provincia);
            }
            for (var i = 0; i < provincias.length; i++) {
                $('#provincia').editableSelect('add', function () {
                    $(this).val(i);
                    $(this).text(provincias[i]);
                });
            }
        } else {
            alert('ERROR AL OBTENER LAS UBICACIONES');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento que permite apartir del provincia obtener los cantones">
    $('#provincia').editableSelect().on('select.editable-select', function (e, li) {
        var provincia = li.text();
        provincia_seleccionada = provincia;
        var cantones = [];
        for (var i = 0; i < ubicaciones.length; i++) {
            if (ubicaciones[i].provincia === provincia) {
                cantones.push(ubicaciones[i].canton);
            }
        }

        var flags = [], cantonesD = [], l = cantones.length;
        for (var i = 0; i < l; i++) {
            if (flags[cantones[i]])
                continue;
            flags[cantones[i]] = true;
            cantonesD.push(cantones[i]);

        }
        for (var i = 0; i < cantones_anteriores; i++)
        {
            $('#canton').editableSelect('remove', i);
        }
        for (var i = 0; i < cantonesD.length; i++) {
            $('#canton').editableSelect('add', function ()
            {
                $(this).val(i);
                $(this).text(cantonesD[i]);
            });
        }
        cantones_anteriores = cantonesD.length;
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento que permite apartir del canton obtener las parroquias">
    $("#canton").editableSelect().on('select.editable-select', function (e, li) {
        var provincia = provincia_seleccionada;
        var canton = li.text();
        canton_seleccionada = canton;
        var parroquias = [];
        for (var i = 0; i < ubicaciones.length; i++) {
            if (ubicaciones[i].provincia === provincia && ubicaciones[i].canton === canton) {
                parroquias.push(ubicaciones[i].parroquia);
            }
        }
        var flags = [], parroquiasD = [], l = parroquias.length;
        for (var i = 0; i < l; i++) {
            if (flags[parroquias[i]])
                continue;
            flags[parroquias[i]] = true;
            parroquiasD.push(parroquias[i]);

        }
        for (var i = 0; i < parroquias_anteriores; i++)
        {
            $('#parroquia').editableSelect('remove', i);
        }
        for (var i = 0; i < parroquiasD.length; i++) {
            $('#parroquia').editableSelect('add', function () {
                $(this).val(i);
                $(this).text(parroquiasD[i]);
            });
        }
        parroquias_anteriores = parroquiasD.length;
    });
    // </editor-fold>

    $('#parroquia').editableSelect().on('select.editable-select', function (e, li) {
        var parroquia = li.text();
        parroquia_seleccionada = parroquia;
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener el id de las ubicaciones">
    function obtenerUbicaciongeo_id(provincia, canton, parroquia)
    {
        for (var i = 0; i < ubicaciones.length; i++)
        {
            if (ubicaciones[i].provincia === provincia && ubicaciones[i].canton === canton && ubicaciones[i].parroquia === parroquia)
            {
                return ubicaciones[i].id;
            }
        }
        return -1;
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite obtener las empresas">
    function obtenerEmpresas()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerEmpresas', '/empresas/');
        if (resultado != null) {
            for (var i = 0; i < resultado.length; i++) {
                var option = $('<option>', {value: resultado[i].id, text: resultado[i].razon_social});
                $('#empresa_id').append(option);
            }
        } else {
            alert('ERROR AL OBTENER LAS EMPRESAS');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/establecimientos/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});