/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function ()
{
    //<editor-fold defaultstate="collapsed" desc="Evento que permite crear una configuracion">
    $("#crearConfiguracion").on("click", function ()
    {
        var tabla = $('#tabla').val();
        var tipo = $('#tipo').val().toUpperCase();
        var codigo = $('#codigo').val().toUpperCase();
        if (tabla === '') {
            $('#tabla').addClass('is-invalid');
            $('#invalido_tabla').text('Error en el campo');
        }
        if (tipo === '') {
            $('#tipo').addClass('is-invalid');
            $('#invalido_tipo').text('Error en el campo');
        }
        if (codigo === '') {
            $('#codigo').addClass('is-invalid');
            $('#invalido_codigo').text('Error en el campo');
        }
        if (tabla !== '' && tipo !== '' && codigo !== '') {
            var data = {'_token': $('meta[name=csrf-token]').attr('content'), tabla: tabla, tipo: tipo,
                codigo: codigo};
            var resultado = peticion(data, 'crear');
            alert(resultado);
            if (resultado) {
                window.location.href = "/configuraciones/crear";
            } else {
                alert('ERROR AL CRER UNA CONFIGURACION');
            }
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/configuraciones/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});