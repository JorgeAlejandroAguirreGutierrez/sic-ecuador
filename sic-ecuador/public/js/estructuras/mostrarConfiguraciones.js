/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function ()
{
    //<editor-fold defaultstate="collapsed" desc="Evento que permite obtener los datos para modificar una configuracion">
    $('[name=modificarConfiguracion]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerConfiguracion');
        if (resultado != null) {
            $('#idM').val(resultado.id);
            $('#tablaM').val(resultado.tabla);
            $('#tipoM').val(resultado.tipo);
            $('#codigoM').val(resultado.codigo);
        } else {
            alert('ERROR AL OBTENER LOS DATOS PARA MODIFICAR CONFIGURACION');
        }
    });

    //<editor-fold defaultstate="collapsed" desc="Evento que permite obtener los datos para eliminar una configuracion">
    $('[name=eliminarConfiguracion]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerConfiguracion');
        if (resultado != null) {
            $('#idE').val(resultado.id);
            $('#tablaE').val(resultado.tabla);
            $('#tipoE').val(resultado.tipo);
            $('#codigoE').val(resultado.codigo);
        } else {
            alert('ERROR AL OBTENER LOS DATOS PARA ELIMINAR LA CONFIGURACION');
        }
    });

    //<editor-fold defaultstate="collapsed" desc="Evento que permite modificar una configuracion">
    $('#cModificarConfiguracion').on("click", function ()
    {
        var id = $('#idM').val();
        var tabla = $('#tablaM').val();
        var tipo = $('#tipoM').val().toUpperCase();
        var codigo = $('#codigoM').val().toUpperCase();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id, tabla: tabla, tipo: tipo, codigo: codigo};
        var resultado = jQuery.parseJSON(data, 'modificarConfiguracion');
        if (resultado) {
            window.location.href = "/configuraciones/mostrar";
        } else {
            alert('ERROR AL MODIFICAR UNA CONFIGURACION');
        }
    });

    //<editor-fold defaultstate="collapsed" desc="Evento que permite eliminar una configuracion">
    $('#cEliminarConfiguracion').on("click", function ()
    {
        var id = $('#idE').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = jQuery.parseJSON(data, 'eliminarConfiguracion');
        if (resultado) {
            window.location.href = "/configuraciones/mostrar";
        } else {
            alert('ERROR AL ELIMINAR LA CONFIGURACION');
        }
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/configuraciones/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});

