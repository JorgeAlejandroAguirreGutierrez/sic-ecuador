/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//QUITAR SALDO Y ABONO->LISTO
//TRABAJAR INTERNAMENTE CON LA TABLA DE AMORTIZACION A LA DEUDA
//EN CONCEPTO AGREGAR: CUOTA SECUENCIA/TOTAL SECUENCIA DE CUOTAS ->LISTO

//AGREGAR TOTALES A LA TABLA DE AMORTIZACION -> LISTO
//AGREGAR CAMPO TOTAL A LA TABLA DE CUADRO DE PAGOS->LISTO

//FORMATO DE 0 CON DECIMALES= 0.00
//Castigar la ultima cuota saldo final a dividendo y pago a capital

$(function ()
{
    $(document).ready(function ()
    {
        obtenerCodigoCredito();
        obtenerFormasTiempo();
        cambiarFormaTiempo();
        $('#crearCredito').prop('disabled', true);
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener el codigo de credito">
    function obtenerCodigoCredito() {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCodigoCredito');
        if (resultado != null) {
            $('#codigo').val(resultado);
        } else {
            alert('ERRIR AL OBTENER EL CODIGO DE CREDITO');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener las formas de tiempo">
    function obtenerFormasTiempo() {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), tipo: 'FORMA TIEMPO'};
        var resultado = peticion(data, 'obtenerDatosAdicionales', '/datosadicionales/');
        if (resultado != null) {
            var forma_tiempo_id = $('#forma_tiempo_id');
            for (var i = 0; i < resultado.length; i++) {
                var opcion = $('<option>', {value: resultado[i].id, text: resultado[i].nombre});
                forma_tiempo_id.append(opcion);
            }
        } else {
            alert('ERRIR AL OBTENER EL CODIGO DE CREDITO');
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Funcion para crear un credito">
    function crearCredito()
    {
        var codigo = $('#codigo').val();
        var saldo = $('#saldo').val();
        var cuotas = $('#cuotas').val();
        var forma_tiempo_id = $('#forma_tiempo_id').val();
        var porcentaje_interes = $('#porcentaje_interes').val();
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, saldo: saldo, cuotas: cuotas, porcentaje_interes: porcentaje_interes, forma_tiempo_id: forma_tiempo_id,cabecera_factura_id: cabecera_factura_id};
        var resultado = peticion(data, 'crearCredito');
        if (resultado != null) {
            $('#credito_id').val(resultado.id);
            var bandera = crearItemsCredito();
            if (bandera) {
                var credito_id = $('#credito_id').val();
                data = {'_token': $('meta[name=csrf-token]').attr('content'), credito_id: credito_id};
                bandera = peticion(data, 'actualizarValorCredito');
                if (bandera) {
                    window.location.href = '/recaudacionescreditos/crear?id=' + cabecera_factura_id;
                } else {
                    alert('ERROR AL ACTUALIZAR EL VALOR DEL CREDITO');
                }
            } else {
                alert('ERROR AL CREAR LOS ITEMS DEL CREDITO');
            }
        } else {
            alert('ERROR AL CREAR EL CREDITO');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para crear un item de credito">
    function crearItemsCredito()
    {
        var credito_id = $('#credito_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'),credito_id: credito_id};
        var resultado = peticion(data, 'crearItemsCredito');
        return resultado;
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para generar credito">
    function generarCredito()
    {
        var saldo = $('#saldo').val();
        var cuotas = $('#cuotas').val();
        var tiempo = $('#tiempo').val();
        var forma_tiempo_id = $('#forma_tiempo_id').val();
        var porcentaje_interes = $('#porcentaje_interes').val();
        var cabecera_factura_id = $('#cabecera_factura_id').val();

        var data = {'_token': $('meta[name=csrf-token]').attr('content'), saldo: saldo, cuotas: cuotas, tiempo:tiempo, forma_tiempo_id: forma_tiempo_id,
            porcentaje_interes: porcentaje_interes, cabecera_factura_id: cabecera_factura_id};
        var resultado = peticion(data, 'generarCredito');
        if (resultado != null) {
            var tabla = $('#tabla_generar_credito');
            tabla.empty();
            var itemsCredito = resultado.itemsCredito;
            for (var i = 0; i < itemsCredito.length; i++) {
                var tr = $('<tr>');
                var td1 = $('<td>', {text: itemsCredito[i].secuencia});
                var codigo_interno_factura = $('#codigo_interno_factura').val();
                var td2 = $('<td>', {text: codigo_interno_factura});
                var fecha_factura = $('#fecha_factura').val();
                var td3 = $('<td>', {text: fecha_factura});
                var td4 = $('<td>', {text: itemsCredito[i].vencimiento});
                var td5 = $('<td>', {text: itemsCredito[i].concepto});
                var td6 = $('<td>', {text: itemsCredito[i].valor});
                tr.append(td1);
                tr.append(td2);
                tr.append(td3);
                tr.append(td4);
                tr.append(td5);
                tr.append(td6);
                tabla.append(tr);

            }
            var tr = $('<tr>');
            var td1 = $('<td>', {text: 'TOTAL'});
            var td2 = $('<td>', {text: ''});
            var td3 = $('<td>', {text: ''});
            var td4 = $('<td>', {text: ''});
            var td5 = $('<td>', {text: ''});
            var td6 = $('<td>', {text: resultado.total_valor});
            tr.append(td1);
            tr.append(td2);
            tr.append(td3);
            tr.append(td4);
            tr.append(td5);
            tr.append(td6);
            tabla.append(tr);
            $('#crearCredito').prop('disabled', false);
        } else {
            alert('ERROR AL GENERAR CREDITO');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para generar amortizacion">
    function generarAmortizacionCredito()
    {
        var saldo = $('#saldo').val();
        var cuotas = $('#cuotas').val();
        var tiempo = $('#tiempo').val();
        var periodos_anio = $('#periodos_anio').val();
        var porcentaje_interes = $('#porcentaje_interes').val();
        var forma_tiempo_id = $('#forma_tiempo_id').val();
        var cabecera_factura_id = $('#cabecera_factura_id').val();

        var data = {'_token': $('meta[name=csrf-token]').attr('content'), saldo: saldo, cuotas: cuotas, tiempo:tiempo, periodos_anio: periodos_anio,
            porcentaje_interes: porcentaje_interes, forma_tiempo_id:forma_tiempo_id, cabecera_factura_id: cabecera_factura_id};
        var resultado = peticion(data, 'generarAmortizacionCredito');
        var tabla = $('#tabla_generar_amortizacion');
        tabla.empty();
        if (resultado != null) {
            var itemsCredito = resultado.itemsCredito;
            $('#pago_A').val(itemsCredito[0].valor);
            for (var i = 0; i < itemsCredito.length; i++) {
                var tr = $('<tr>');
                var td1 = $('<td>', {text: itemsCredito[i].secuencia});
                var td2 = $('<td>', {text: itemsCredito[i].saldo_inicial});
                var td3 = $('<td>', {text: itemsCredito[i].valor});
                var td4 = $('<td>', {text: itemsCredito[i].pago_capital});
                var td5 = $('<td>', {text: itemsCredito[i].pago_interes});
                var td6 = $('<td>', {text: itemsCredito[i].saldo_final});
                tr.append(td1);
                tr.append(td2);
                tr.append(td3);
                tr.append(td4);
                tr.append(td5);
                tr.append(td6);
                tabla.append(tr);
            }
            tr = $('<tr>');
            td1 = $('<td>', {text: 'TOTAL:'});
            td2 = $('<td>', {text: ''});
            td3 = $('<td>', {text: resultado.total_valor});
            td4 = $('<td>', {text: resultado.total_pago_capital});
            td5 = $('<td>', {text: resultado.total_pago_interes});
            td6 = $('<td>', {text: ''});
            tr.append(td1);
            tr.append(td2);
            tr.append(td3);
            tr.append(td4);
            tr.append(td5);
            tr.append(td6);
            tabla.append(tr);
            $('#crearCredito').prop('disabled', false);
        } else {
            alert('ERROR AL GENERAR CREDITO');
        }
    }
    // </editor-fold>
    
    function cambiarFormaTiempo() {
        var forma_tiempo_id = $('#forma_tiempo_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), forma_tiempo_id: forma_tiempo_id};
        var tiempo = peticion(data, 'obtenerTiempo');
        if (tiempo != null) {
            if (tiempo == 0) {
                $('#tiempo').val(tiempo);
                $('#tiempo').attr('disabled', false);
            } else {
                $('#tiempo').val(tiempo);
                $('#tiempo').attr('disabled', true);
            }
        } else {
            alert('ERROR AL CAMBIAR EL TIPO DE CREDITO');
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Evento para obtener un credito para eliminarlo">
    function eliminarCredito() {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerCredito');
        if (resultado != null) {
            $('#id_E').val(resultado.id);
            $('#codigo_E').val(resultado.codigo);
            $('#cuotas_E').val(resultado.cuotas);
            $('#porcentaje_interes_E').val(resultado.porcentaje_interes);
            $('#saldo_E').val(resultado.saldo);
            $('#valor_E').val(resultado.valor);
        } else {
            alert('ERROR AL OBTENER EL IMPUESTO');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para eliminar un credito">
    function cEliminarCredito()
    {
        var id = $('#id_E').val();
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarCredito');
        if (resultado) {
            window.location.href = "/recaudacionescreditos/crear?id=" + cabecera_factura_id;
        } else {
            alert(resultado);
        }
    }
    // </editor-fold>

    $("#generarCredito").on("click", function ()
    {
        generarCredito();
    });
    $("#generarAmortizacionCredito").on("click", function ()
    {
        generarAmortizacionCredito();
    });
    $("#crearCredito").on("click", function ()
    {
        crearCredito();
    });

    $('[name=eliminarCredito]').on("click", function ()
    {
        eliminarCredito();
    });

    $("#cEliminarCredito").on("click", function ()
    {
        cEliminarCredito();
    });

    $('#saldo').on('change', function () {
        $('#crearCredito').prop('disabled', true);
    });
    $('#cuotas').on('change', function () {
        $('#crearCredito').prop('disabled', true);
    });
    $('#periodos_anio').on('change', function () {
        $('#crearCredito').prop('disabled', true);
    });
    $('#porcentaje_interes').on('change', function () {
        $('#crearCredito').prop('disabled', true);
    });

    $('#forma_tiempo_id').on("change", function () {
        cambiarFormaTiempo();
    });
    

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/recaudacionescreditos/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }

        });
        return respuesta;
    }
    // </editor-fold>


});

