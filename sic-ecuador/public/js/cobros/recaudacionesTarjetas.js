/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * - Forma de cobro representarlo con texto largo->LISTO
 * - en comentario para tarjetas credito y tarjetas debito: factura cliente b/s , tarjeta / ifis / operador / boucher / lote / fecha ->LISTO
 * - Boton 'Guardar' para tarjetas de credito y debito ->LISTO
 * - Añadir operador a la tabla de registros ->LISTO
 * - boton de editar para registros de cobros tarjeta credito y tarjeta debito->LISTO
 * - Modificar comentario productos por bienes.
 * - Numero Boucher, numero de lote, numero de cheque son numeros, no permitir alfabeto -> PENDIENTE
 * - Ampliar tabla de registro de recaudaciones.
 * 
 * - Permitir subir la imagen de un cheque y del recibo de transaccion por tarjeta de credito o debito a traves de un dispositivo movil ->PENDIENTE TRABAJARLO CON VERONICA
 * - 
 * 
 */
$(function ()
{
    var tarjeta_id_tarjeta_credito = null;
    var tarjeta_id_tarjeta_debito = null;
    var ifi_id_tarjeta_credito = null;
    var ifi_id_tarjeta_debito = null;
    var operador_id_tarjeta_credito = null;
    var operador_id_tarjeta_debito = null;
    var forma_cobro_id_tarjeta_credito = null;
    var tarjeta_tarjeta_credito = null;
    var tarjeta_tarjeta_debito = null;
    var ifi_tarjeta_credito = null;
    var ifi_tarjeta_credito = null;
    var ifi_tarjeta_debito = null;
    var operador_tarjeta_credito = null;
    var operador_tarjeta_debito = null;
    var forma_cobro_tarjeta_credito = null;

    var tarjetas = 0;
    var ifis = 0;
    var operadores = 0;
    var formas_cobros = 0;

    //<editor-fold defaultstate="collapsed" desc="Eventos al cargar la pantalla">
    //Obtener establecimiento, obtener puntos de venta por establecimiento, obtener bancos, obtener tipos de cheque
    //obtener tipo deposito o transferencia, obtener codigo de cheque, obtener codigo de efectivo y obtener codigo de deposito o transferencia
    $(document).ready(function ()
    {
        obtenerTarjetas();
        obtenerIfis();
        obtenerOperadores();
        obtenerFormasCobrosTarjetaCredito();

        ObtenerTipoLineas();

        obtenerCodigoTarjetaCredito();
        obtenerCodigoTarjetaDebito();

        obtenerFechaTarjetaCredito();
        obtenerFechaTarjetaDebito();

        actualizarComentarioTarjetaCredito();
        actualizarComentarioTarjetaDebito();

        $("#nav-tarjeta_credito-tab").addClass('active');
        $("#nav-tarjeta_debito-tab").removeClass('active');

        $("#nav-tarjeta_credito").addClass('active');
        $("#nav-tarjeta_credito").addClass('show');
        $("#nav-tarjeta_debito").removeClass('active');
        $("#nav-tarjeta_debito").removeClass('show');
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Eventos nav-tab y eventos crear cheque, efectivo, deposito, transferencia">
    $("#nav-tarjeta_credito-tab").on("click", function ()
    {
        $("#nav-tarjeta_credito-tab").addClass('active');
        $("#nav-tarjeta_debito-tab").removeClass('active');

        $("#nav-tarjeta_credito").addClass('active');
        $("#nav-tarjeta_credito").addClass('show');
        $("#nav-tarjeta_debito").removeClass('active');
        $("#nav-tarjeta_debito").removeClass('show');

    });
    $("#nav-tarjeta_debito-tab").on("click", function ()
    {
        $("#nav-tarjeta_credito-tab").removeClass('active');
        $("#nav-tarjeta_debito-tab").addClass('active');

        $("#nav-tarjeta_credito").removeClass('active');
        $("#nav-tarjeta_credito").removeClass('show');
        $("#nav-tarjeta_debito").addClass('active');
        $("#nav-tarjeta_debito").addClass('show');
    });

    $('#crearRegistroTarjetaCredito').on("click", function ()
    {
        crearRegistroTarjetaCredito();
    });

    $('#crearRegistroTarjetaDebito').on("click", function ()
    {
        crearRegistroTarjetaDebito();
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion crear registro de tarjeta credito">
    function crearRegistroTarjetaCredito()
    {
        var codigo = $('#codigo_tarjeta_credito').val();
        var boucher = $('#boucher_tarjeta_credito').val();
        var lote = $('#lote_tarjeta_credito').val();
        var fecha = $('#fecha_tarjeta_credito').val();
        var comentario = $('#comentario_tarjeta_credito').val();
        var valor = $('#valor_tarjeta_credito').val();
        var tarjeta_id = tarjeta_id_tarjeta_credito;
        var ifi_id = ifi_id_tarjeta_credito;
        var operador_id = operador_id_tarjeta_credito;
        var forma_cobro_id = forma_cobro_id_tarjeta_credito;
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, boucher: boucher, lote: lote, fecha: fecha, forma_cobro_id: forma_cobro_id, comentario: comentario, valor: valor, tarjeta_id: tarjeta_id, ifi_id: ifi_id, operador_id: operador_id, cabecera_factura_id: cabecera_factura_id};
        var resultado = peticion(data, 'crearTarjetaCredito');
        if (resultado) {
            window.location.href = '/recaudacionestarjetas/crear?id=' + cabecera_factura_id;
        } else {
            alert("No Registrado correctamente");
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion crear registro de tarjeta debito">
    function crearRegistroTarjetaDebito()
    {
        var codigo = $('#codigo_tarjeta_debito').val();
        var boucher = $('#boucher_tarjeta_debito').val();
        var lote = $('#lote_tarjeta_debito').val();
        var fecha = $('#fecha_tarjeta_debito').val();
        var comentario = $('#comentario_tarjeta_debito').val();
        var valor = $('#valor_tarjeta_debito').val();
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, boucher: boucher, lote: lote, fecha: fecha, comentario: comentario, valor: valor, tarjeta_id: tarjeta_id_tarjeta_debito, ifi_id: ifi_id_tarjeta_debito, operador_id: operador_id_tarjeta_debito, cabecera_factura_id: cabecera_factura_id};
        var resultado = peticion(data, 'crearTarjetaDebito');
        if (resultado) {
            window.location.href = '/recaudacionestarjetas/crear?id=' + cabecera_factura_id;
        } else {
            alert("ERROR AL CREAR LA TARJETA DEBITO");
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener las tarjetas">
    function obtenerTarjetas(parametro = '')
    {
        for (var i = 0; i < tarjetas; i++)
            $('#tarjeta_id_tarjeta_credito' + parametro).editableSelect('remove', 0);
        for (var i = 0; i < tarjetas; i++)
            $('#tarjeta_id_tarjeta_debito' + parametro).editableSelect('remove', 0);
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), tipo: 'TARJETA'};
        var resultado = peticion(data, 'obtenerDatosAdicionales', '/datosadicionales/');
        tarjetas = resultado.length;
        for (var i = 0; i < resultado.length; i++)
        {
            $('#tarjeta_id_tarjeta_credito' + parametro).editableSelect('add', function () {
                $(this).val(resultado[i].id);
                $(this).text(resultado[i].nombre);
            });
            $('#tarjeta_id_tarjeta_debito' + parametro).editableSelect('add', function () {
                $(this).val(resultado[i].id);
                $(this).text(resultado[i].nombre);
            });
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener los ifis">
    function obtenerIfis(parametro = '')
    {
        for (var i = 0; i < ifis; i++)
            $('#ifi_id_tarjeta_credito' + parametro).editableSelect('remove', 0);
        for (var i = 0; i < ifis; i++)
            $('#ifi_id_tarjeta_debito' + parametro).editableSelect('remove', 0);
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), tipo: 'BANCO'};
        var resultado = peticion(data, 'obtenerDatosAdicionales', '/datosadicionales/');
        ifis = resultado.length;
        for (var i = 0; i < resultado.length; i++) {
            $('#ifi_id_tarjeta_credito' + parametro).editableSelect('add', function () {
                $(this).val(resultado[i].id);
                $(this).text(resultado[i].nombre);
            });
            $('#ifi_id_tarjeta_debito' + parametro).editableSelect('add', function () {
                $(this).val(resultado[i].id);
                $(this).text(resultado[i].nombre);
            });
    }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener los operadores">
    function obtenerOperadores(parametro = '')
    {
        for (var i = 0; i < operadores; i++)
            $('#operador_id_tarjeta_credito' + parametro).editableSelect('remove', 0);
        for (var i = 0; i < operadores; i++)
            $('#operador_id_tarjeta_debito' + parametro).editableSelect('remove', 0);
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), tipo: 'OPERADOR'};
        var resultado = peticion(data, 'obtenerDatosAdicionales', '/datosadicionales/');
        operadores = resultado.length;
        for (var i = 0; i < resultado.length; i++)
        {
            $('#operador_id_tarjeta_credito' + parametro).editableSelect('add', function () {
                $(this).val(resultado[i].id);
                $(this).text(resultado[i].nombre);
            });
            $('#operador_id_tarjeta_debito' + parametro).editableSelect('add', function () {
                $(this).val(resultado[i].id);
                $(this).text(resultado[i].nombre);
            });
    }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion obtener la forma de cobro para tarjetas credito">
    function obtenerFormasCobrosTarjetaCredito(parametro = '')
    {
        for (var i = 0; i < formas_cobros; i++)
            $('#forma_cobro_id_tarjeta_credito' + parametro).editableSelect('remove', 0);
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), tipo: 'FORMA COBRO'};
        var resultado = peticion(data, 'obtenerDatosAdicionales', '/datosadicionales/');
        formas_cobros = resultado.length;
        for (var i = 0; i < resultado.length; i++) {
            $('#forma_cobro_id_tarjeta_credito' + parametro).editableSelect('add', function () {
                $(this).val(resultado[i].id);
                $(this).text(resultado[i].nombre);
            });
    }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion obtener la fecha de cobro para tarjeta credito">
    function obtenerFechaTarjetaCredito()
    {
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), cabecera_factura_id: cabecera_factura_id};
        var resultado = peticion(data, 'obtenerFecha');
        if (resultado != null) {
            $('#fecha_tarjeta_credito').val(resultado);
        } else {
            alert('ERROR AL OBTENER LA FECHA PARA LA TARJETA DE CREDITO');
        }

    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion obtener la fecha de cobro para tarjeta debito">
    function obtenerFechaTarjetaDebito()
    {
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), cabecera_factura_id: cabecera_factura_id};
        var resultado = peticion(data, 'obtenerFecha');
        if (resultado != null) {
            $('#fecha_tarjeta_debito').val(resultado);
        } else {
            alert('ERROR AL OBTENER LA FECHA PARA LA TARJETA DEBITO');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener el codigo para registro de tarjeta de credito">
    function obtenerCodigoTarjetaCredito()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCodigoTarjetaCredito');
        if (resultado != null) {
            $('#codigo_tarjeta_credito').val(resultado);
        } else {
            alert('ERROR AL OBTENER EL CODIGO DE LA TARJETA DE CREDITO');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener el codigo para registro de tarjeta de credito">
    function obtenerCodigoTarjetaDebito()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCodigoTarjetaDebito');
        if (resultado != null) {
            $('#codigo_tarjeta_debito').val(resultado);
        } else {
            alert('ERROR AL OBTENER EL CODIGO PARA LA TARJETA DEBITO');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener los tipos de liena de la factura">
    function ObtenerTipoLineas()
    {
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), cabecera_factura_id: cabecera_factura_id};
        var resultado = peticion(data, 'obtenerTiposLineas');
        if (resultado != null) {
            $('#productos').val(resultado.productos);
            $('#servicios').val(resultado.servicios);
        } else {
            alert('ERROR AL OBTENER LOS TIPOS DE LAS LINEAS DE LA FACTURA');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento select editable para tarjetas, bancos, operadores">
    $('#tarjeta_id_tarjeta_credito').editableSelect().on('select.editable-select', function (e, li) {
        tarjeta_id_tarjeta_credito = li.val();
        tarjeta_tarjeta_credito = li.text();
        actualizarComentarioTarjetaCredito();
    });

    $('#tarjeta_id_tarjeta_debito').editableSelect().on('select.editable-select', function (e, li) {
        tarjeta_id_tarjeta_debito = li.val();
        tarjeta_tarjeta_debito = li.text();
        actualizarComentarioTarjetaDebito();

    });

    $('#ifi_id_tarjeta_credito').editableSelect().on('select.editable-select', function (e, li) {
        ifi_id_tarjeta_credito = li.val();
        ifi_tarjeta_credito = li.text();
        actualizarComentarioTarjetaCredito();
    });

    $('#ifi_id_tarjeta_debito').editableSelect().on('select.editable-select', function (e, li) {
        ifi_id_tarjeta_debito = li.val();
        ifi_tarjeta_debito = li.text();
        actualizarComentarioTarjetaDebito();
    });

    $('#operador_id_tarjeta_credito').editableSelect().on('select.editable-select', function (e, li) {
        operador_id_tarjeta_credito = li.val();
        operador_tarjeta_credito = li.text();
        actualizarComentarioTarjetaCredito();
    });

    $('#operador_id_tarjeta_debito').editableSelect().on('select.editable-select', function (e, li) {
        operador_id_tarjeta_debito = li.val();
        operador_tarjeta_debito = li.text();
        actualizarComentarioTarjetaDebito();
    });

    $('#forma_cobro_id_tarjeta_credito').editableSelect().on('select.editable-select', function (e, li) {
        forma_cobro_id_tarjeta_credito = li.val();
        forma_cobro_tarjeta_credito = li.text();
        actualizarComentarioTarjetaCredito();
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento select editable para tarjetas, bancos, operadores y forma de cobro en operacion de modificacion">
    $('#tarjeta_id_tarjeta_credito_M').editableSelect().on('select.editable-select', function (e, li) {
        tarjeta_id_tarjeta_credito = li.val();
        tarjeta_tarjeta_credito = li.text();
    });
    $('#tarjeta_id_tarjeta_debito_M').editableSelect().on('select.editable-select', function (e, li) {
        tarjeta_id_tarjeta_debito = li.val();
        tarjeta_tarjeta_debito = li.text();
    });

    $('#ifi_id_tarjeta_credito_M').editableSelect().on('select.editable-select', function (e, li) {
        ifi_id_tarjeta_credito = li.val();
        ifi_tarjeta_credito = li.text();
    });

    $('#ifi_id_tarjeta_debito_M').editableSelect().on('select.editable-select', function (e, li) {
        ifi_id_tarjeta_debito = li.val();
        ifi_tarjeta_debito = li.text();
    });

    $('#operador_id_tarjeta_credito_M').editableSelect().on('select.editable-select', function (e, li) {
        operador_id_tarjeta_credito = li.val();
        operador_tarjeta_credito = li.text();
    });

    $('#operador_id_tarjeta_debito_M').editableSelect().on('select.editable-select', function (e, li) {
        operador_id_tarjeta_debito = li.val();
        operador_tarjeta_debito = li.text();
    });

    $('#forma_cobro_id_tarjeta_credito_M').editableSelect().on('select.editable-select', function (e, li) {
        forma_cobro_id_tarjeta_credito = li.val();
        forma_cobro_tarjeta_credito = li.text();
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento numero de soporte para recaudo para comentario">
    $('#boucher_tarjeta_credito').on('change', function () {
        actualizarComentarioTarjetaCredito();
    });

    $('#lote_tarjeta_credito').on('change', function () {
        actualizarComentarioTarjetaCredito();
    });

    $('#boucher_tarjeta_debito').on('change', function () {
        actualizarComentarioTarjetaDebito();
    });

    $('#lote_tarjeta_debito').on('change', function () {
        actualizarComentarioTarjetaDebito();
    });

    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para actualizar comentario">
    function actualizarComentarioTarjetaCredito()
    {
        var codigo_interno_factura = $('#codigo_interno_factura').val();
        var cliente = $('#cliente').val();
        var productos = $('#productos').val();
        var servicios = $('#servicios').val();
        var fecha = $('#fecha_tarjeta_credito').val();
        var boucher = $('#boucher_tarjeta_credito').val();
        var lote = $('#lote_tarjeta_credito').val();
        $('#comentario_tarjeta_credito').val(codigo_interno_factura + ' ' + cliente + ' ' + productos + ' ' + servicios + ', ' + tarjeta_tarjeta_credito + ' / ' + ifi_tarjeta_credito + ' / ' + operador_tarjeta_credito + ' / ' + boucher + ' / ' + lote + ' / ' + fecha);
    }
    function actualizarComentarioTarjetaDebito()
    {
        var codigo_interno_factura = $('#codigo_interno_factura').val();
        var cliente = $('#cliente').val();
        var productos = $('#productos').val();
        var servicios = $('#servicios').val();
        var fecha = $('#fecha_tarjeta_debito').val();
        var boucher = $('#boucher_tarjeta_debito').val();
        var lote = $('#lote_tarjeta_debito').val();
        $('#comentario_tarjeta_debito').val(codigo_interno_factura + ' ' + cliente + ' ' + productos + ' ' + servicios + ', ' + tarjeta_tarjeta_debito + ' / ' + ifi_tarjeta_debito + ' / ' + operador_tarjeta_debito + ' / ' + boucher + ' / ' + lote + ' / ' + fecha);
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para modificar una tarjeta de credito">
    $('[name=modificarTarjetaCredito]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerTarjetaCredito');
        if (resultado != null) {
            $('#id_tarjeta_credito_M').val(resultado.id);
            $('#codigo_tarjeta_credito_M').val(resultado.codigo);
            $('#boucher_tarjeta_credito_M').val(resultado.boucher);
            $('#lote_tarjeta_credito_M').val(resultado.lote);
            $('#fecha_tarjeta_credito_M').val(resultado.fecha);
            $('#comentario_tarjeta_credito_M').val(resultado.comentario);

            $('#valor_tarjeta_credito_M').val(resultado.valor);
            obtenerTarjetas('_M');
            $('#tarjeta_id_tarjeta_credito_M').val(resultado.tarjeta.nombre);
            obtenerIfis('_M');
            $('#ifi_id_tarjeta_credito_M').val(resultado.ifi.nombre);
            obtenerOperadores('_M');
            $('#operador_id_tarjeta_credito_M').val(resultado.operador.nombre);
            obtenerFormasCobrosTarjetaCredito('_M');
            $('#forma_cobro_id_tarjeta_credito_M').val(resultado.forma_cobro.nombre);
        } else {
            alert('ERROR AL OBTENER LA TARJEA DE CREDITO');
        }
    });

    $('#cModificarTarjetaCredito').on("click", function ()
    {
        var id = $('#id_tarjeta_credito_M').val();
        var boucher = $('#boucher_tarjeta_credito_M').val();
        var lote = $('#lote_tarjeta_credito_M').val();
        var fecha = $('#fecha_tarjeta_credito_M').val();
        var comentario = $('#comentario_tarjeta_credito_M').val();
        var valor = $('#valor_tarjeta_credito_M').val();
        var tarjeta_id = tarjeta_id_tarjeta_credito;
        var ifi_id = ifi_id_tarjeta_credito;
        var operador_id = operador_id_tarjeta_credito;
        var forma_cobro_id = forma_cobro_id_tarjeta_credito;
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id, boucher: boucher, lote: lote, fecha: fecha, comentario: comentario, valor: valor, tarjeta_id: tarjeta_id, ifi_id: ifi_id, operador_id: operador_id, forma_cobro_id: forma_cobro_id};
        var resultado = peticion(data, 'modificarTarjetaCredito');
        if (resultado) {
            window.location.href = '/recaudacionestarjetas/crear?id=' + cabecera_factura_id;
        } else {
            alert("ERROR AL MODIFICAR LA TARJETA CREDITO");
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para modificar una tarjeta debito">
    $('[name=modificarTarjetaDebito]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerTarjetaDebito');
        if (resultado != null) {
            $('#id_tarjeta_debito_M').val(resultado.id);
            $('#codigo_tarjeta_debito_M').val(resultado.codigo);
            $('#boucher_tarjeta_debito_M').val(resultado.boucher);
            $('#lote_tarjeta_debito_M').val(resultado.lote);
            $('#fecha_tarjeta_debito_M').val(resultado.fecha);
            $('#comentario_tarjeta_debito_M').val(resultado.comentario);
            $('#valor_tarjeta_debito_M').val(resultado.valor);
            obtenerTarjetas('_M');
            $('#tarjeta_id_tarjeta_debito_M').val(resultado.tarjeta.nombre);
            obtenerIfis('_M');
            $('#ifi_id_tarjeta_debito_M').val(resultado.ifi.nombre);
            obtenerOperadores('_M');
            $('#operador_id_tarjeta_debito_M').val(resultado.operador.nombre);
        } else {
            alert('ERROR AL OBTENER LA TARJETA DEBITO');
        }
    });

    $('#cModificarTarjetaDebito').on("click", function ()
    {
        var id = $('#id_tarjeta_debito_M').val();
        var codigo = $('#codigo_tarjeta_debito_M').val();
        var boucher = $('#boucher_tarjeta_debito_M').val();
        var lote = $('#lote_tarjeta_debito_M').val();
        var fecha = $('#fecha_tarjeta_debito_M').val();
        var comentario = $('#comentario_tarjeta_debito_M').val();
        var valor = $('#valor_tarjeta_debito_M').val();
        var tarjeta_id = tarjeta_id_tarjeta_debito;
        var ifi_id = ifi_id_tarjeta_debito;
        var operador_id = operador_id_tarjeta_debito;
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id, codigo: codigo, boucher: boucher, lote: lote, fecha: fecha, comentario: comentario, valor: valor, tarjeta_id: tarjeta_id, ifi_id: ifi_id, operador_id: operador_id, cabecera_factura_id: cabecera_factura_id};
        var resultado = peticion(data, 'modificarTarjetaDebito');
        if (resultado) {
            window.location.href = '/recaudacionestarjetas/crear?id=' + cabecera_factura_id;
        } else {
            alert("ERROR AL MODIFICAR LA TARJETA DEBITO");
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Eventos para eliminar una tarjeta de credito Y debito">
    $('[name=eliminarTarjetaCredito]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerTarjetaCredito');
        if (resultado != null) {
            $('#id_tarjeta_credito_E').val(resultado.id);
            $('#codigo_tarjeta_credito_E').val(resultado.codigo);
            $('#boucher_tarjeta_credito_E').val(resultado.boucher);
            $('#lote_tarjeta_credito_E').val(resultado.lote);
            $('#comentario_tarjeta_credito_E').val(resultado.comentario);
            $('#tarjeta_id_tarjeta_credito_E').val(resultado.tarjeta.nombre);
            $('#operador_id_tarjeta_credito_E').val(resultado.operador.nombre);
            $('#ifi_id_tarjeta_credito_E').val(resultado.ifi.nombre);
            $('#fecha_tarjeta_credito_E').val(resultado.fecha);
            $('#forma_cobro_id_tarjeta_credito_E').val(resultado.forma_cobro.nombre);
            $('#valor_tarjeta_credito_E').val(resultado.valor);
        } else {
            alert('ERROR AL OBTENER LA TARJETA DE CREDITO');
        }

    });

    $('[name=eliminarTarjetaDebito]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerTarjetaDebito');
        if (resultado != null) {
            $('#id_tarjeta_debito_E').val(resultado.id);
            $('#codigo_tarjeta_debito_E').val(resultado.codigo);
            $('#boucher_tarjeta_debito_E').val(resultado.boucher);
            $('#lote_tarjeta_debito_E').val(resultado.lote);
            $('#comentario_tarjeta_debito_E').val(resultado.comentario);
            $('#tarjeta_id_tarjeta_debito_E').val(resultado.tarjeta.nombre);
            $('#fecha_tarjeta_debito_E').val(resultado.fecha);
            $('#ifi_id_tarjeta_debito_E').val(resultado.ifi.nombre);
            $('#valor_tarjeta_debito_E').val(resultado.valor);
        } else {
            alert('ERROR AL OBTENER TARJETA DEBITO');
        }
    });

    $('#cEliminarTarjetaCredito').on("click", function ()
    {
        var id = $('#id_tarjeta_credito_E').val();
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarTarjetaCredito');
        if (resultado) {
            window.location.href = "/recaudacionestarjetas/crear?id=" + cabecera_factura_id;
        } else {
            alert('ERROR AL ELIMINAR UNA TARJETA DE CREDITO');
        }
    });

    $('#cEliminarTarjetaDebito').on("click", function ()
    {
        var id = $('#id_tarjeta_debito_E').val();
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarTarjetaDebito');
        if (resultado) {
            window.location.href = "/recaudacionestarjetas/crear?id=" + cabecera_factura_id;
        } else {
            alert('ERROR AL ELIMINAR UNA TARJETA DEBITO');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/recaudacionestarjetas/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>
});

