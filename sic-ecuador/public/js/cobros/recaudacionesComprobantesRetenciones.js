/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 * bloquear la fecha de registro pero debe contener la fecha de la factura ->LISTO
 * Secuencia de comprobante de retencion trece ceros ->LISTO
 * Año-> con la fecha de sistema ->LISTO
 * Serie->establecimiento + "-"+punto de venta ->LISTO
 * Factura: Es la secuencia ->LISTO
 * dentro del grupo IR->Corresponde como base imponible al subtotal de la factura->LISTO
 * Dentro del grupo de IVA->Corresponde como base imponible al valor de iva de la factura->LISTO
 * Quitar visualizacion de devolucion de retencion y afecta contabilidad->LISTO
 * Base imponible debe ser modificable-> LISTO
 * Valor tambien es editable -> LISTO
 * La suma de las codigos de retencion ir debe ser igual a la base imponible de la factura -> LISTO
 * la suma de los codigos de retencion iva deberia ser igual al valor del iva de la factura ->LISTO
 * si numero de autorizacion esta en blanco no debe almacenar el comprobante de retencion
 * 
 * RELLENAR CON TRES CEROS PARA ESTABLECIMIENTO Y PUNTO DE VENTA
 * 
 * TRABAJAR CON DOS DECIMALES.
 * 
 * - fecha de emision es igual fecha de registro al cargar el comprobante de retencion
 * - HOMOLOGACION DE LOS CODIGOS DE RETENCION
 * - DESENCRIPTACION CDATA
 */
$(function ()
{
    var items_comprobante_retencion = 1;

    $(document).ready(function ()
    {
        obtenerCodigoComprobanteRetencion();
        cargarItemComprobanteRetencion(0);
    });

    shortcut.add("F1", function () { //para creacion de un item de comprobante de retencion
        nuevoItemComprobanteRetencion();
        cargarItemComprobanteRetencion(items_comprobante_retencion - 1);
    });

    //<editor-fold defaultstate="collapsed" desc="Crear un nuevo item de retencion">
    function nuevoItemComprobanteRetencion()
    {
        var item = items_comprobante_retencion;
        var div = $('#items_comprobante_retencion');
        var div_row = $('<div>', {class: 'form-row form-group'});
        var input1 = $('<input>', {type: 'text', class: 'form-control form-control-sm col-1', name: 'ano_retencion', id: 'ano_retencion_' + item, disabled: true});
        var input2 = $('<input>', {type: 'text', class: 'form-control form-control-sm col-1', name: 'serie_establecimiento_retencion', id: 'serie_establecimiento_retencion_' + item, disabled: true});
        var input3 = $('<input>', {type: 'text', class: 'form-control form-control-sm col-1', name: 'serie_punto_venta_retencion', id: 'serie_punto_venta_retencion_' + item, disabled: true});
        var input4 = $('<input>', {type: 'text', class: 'form-control form-control-sm col-2', name: 'cabecera_factura_retencion', id: 'cabecera_factura_retencion_' + item, disabled: true});
        var input5 = $('<select>', {class: 'form-control form-control-sm col-2', name: 'retencion_id', id: 'retencion_id_' + item, });
        var input6 = $('<input>', {type: 'text', class: 'form-control form-control-sm col-2', name: 'nombre_retencion', id: 'nombre_retencion_' + item, disabled: true});
        var input7 = $('<input>', {type: 'text', class: 'form-control form-control-sm col-1', name: 'base_imponible_retencion', id: 'base_imponible_retencion_' + item});
        var input8 = $('<input>', {type: 'text', class: 'form-control form-control-sm col-1', name: 'porcentaje_retencion', id: 'porcentaje_retencion_' + item, disabled: true});
        var input9 = $('<input>', {type: 'text', class: 'form-control form-control-sm col-1', name: 'valor_retencion', id: 'valor_retencion_' + item});
        div_row.append(input1);
        div_row.append(input2);
        div_row.append(input3);
        div_row.append(input4);
        div_row.append(input5);
        div_row.append(input6);
        div_row.append(input7);
        div_row.append(input8);
        div_row.append(input9);
        div.append(div_row);
        items_comprobante_retencion++;
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener el codigo del comprobante de retencion">
    function obtenerCodigoComprobanteRetencion()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCodigoComprobanteRetencion');
        if (resultado != null) {
            $('#codigo').val(resultado);
        } else {
            alert('ERRIR AL OBTENER EL CODIGO DE COMPROBANTE DE RETENCION');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para crear un comprobante de retencion">
    function crearComprobanteRetencion()
    {
        var codigo = $('#codigo').val();
        var comprobante_retencion_establecimiento = $('#comprobante_retencion_establecimiento').val();
        var comprobante_retencion_punto_venta = $('#comprobante_retencion_punto_venta').val();
        var comprobante_retencion_secuencia = $('#comprobante_retencion_secuencia').val();
        var comprobante_retencion = comprobante_retencion_establecimiento + "-" + comprobante_retencion_punto_venta + "-" + comprobante_retencion_secuencia;
        var fecha_registro = $('#fecha_registro').val();
        var fecha_emision = $('#fecha_emision').val();
        var devolucion = null;
        var afecta_contabilidad = null;
        var autorizacion = $('#autorizacion').val();
        var tipo_id = $('select[name=tipo_id]').val();
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, comprobante_retencion: comprobante_retencion, fecha_registro: fecha_registro, fecha_emision: fecha_emision,
            devolucion: devolucion, afecta_contabilidad: afecta_contabilidad, autorizacion: autorizacion, tipo_id: tipo_id, cabecera_factura_id: cabecera_factura_id};
        var resultado = peticion(data, 'crearComprobanteRetencion');
        if (resultado != null) {
            var bandera = true;
            $('#comprobante_retencion_id').val(resultado.id);
            for (var i = 0; i < items_comprobante_retencion; i++) {
                var banderaItem = crearItemComprobanteRetencion(i);
                if (!banderaItem) {
                    bandera = false;
                }
            }
            if (bandera) {
                var comprobante_retencion_id = $('#comprobante_retencion_id').val();
                data = {'_token': $('meta[name=csrf-token]').attr('content'), comprobante_retencion_id: comprobante_retencion_id};
                bandera = peticion(data, 'actualizarValorComprobanteRetencion');
                if (bandera) {
                    window.location.href = '/recaudacionescomprobantesretenciones/crear?id=' + cabecera_factura_id;
                } else {
                    alert('ERROR AL ACTUALIZAR EL VALOR DEL COMPROBANTE DE RETENCION')
                }

            } else {
                alert('ERROR AL CREAR LOS ITEMS DE COMPROBANTE DE RETENCION');
            }

        } else {
            alert('ERROR AL CREAR EL COMPROBANTE DE RETENCION');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para crear un item de comprobante de retencion">
    function crearItemComprobanteRetencion(item)
    {
        var retencion_id = $("#retencion_id_" + item + " option:selected").val();
        var base_imponible = $('#base_imponible_retencion_' + item).val();
        var valor = $('#valor_retencion_' + item).val();
        var comprobante_retencion_id = $('#comprobante_retencion_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), base_imponible: base_imponible, valor: valor, retencion_id: retencion_id, comprobante_retencion_id: comprobante_retencion_id};
        var resultado = peticion(data, 'crearItemComprobanteRetencion');
        return resultado;
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion que permite cargar un item de comprobante de retencion">
    function cargarItemComprobanteRetencion(item)
    {
        var ano_retencion = $('#ano_retencion').val();
        var cabecera_factura_retencion = $('#cabecera_factura_retencion').val();
        var serie_establecimiento_retencion = $('#serie_establecimiento_retencion').val();
        var serie_punto_venta_retencion = $('#serie_punto_venta_retencion').val();
        $('#ano_retencion_' + item).val(ano_retencion);
        $('#serie_establecimiento_retencion_' + item).val(serie_establecimiento_retencion);
        $('#serie_punto_venta_retencion_' + item).val(serie_punto_venta_retencion);
        $('#cabecera_factura_retencion_' + item).val(cabecera_factura_retencion);
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerRetenciones', '/retenciones/');
        if (resultado != null) {
            for (var i = 0; i < resultado.length; i++) {
                var opcion = $('<option>', {value: resultado[i].id, text: resultado[i].codigo_norma});
                $('#retencion_id_' + item).append(opcion);
            }
            $('#nombre_retencion_' + item).val(resultado[0].descripcion);
            $('#porcentaje_retencion_' + item).val(resultado[0].porcentaje);
            //ACA METO
            var suma_retencion_ir = 0;
            var suma_retencion_iva = 0;
            var cabecera_factura_id = $('#cabecera_factura_id').val();
            for (var i = 0; i < items_comprobante_retencion - 1; i++) {
                var retencion_id = $("#retencion_id_" + i + " option:selected").val();
                var data = {'_token': $('meta[name=csrf-token]').attr('content'), retencion_id: retencion_id};
                var resultado = peticion(data, 'verificarRetencionIR', '/retenciones/');
                if (resultado) {
                    suma_retencion_ir = suma_retencion_ir + parseFloat($('#base_imponible_retencion_' + i).val());
                } else {
                    suma_retencion_iva = suma_retencion_iva + parseFloat($('#base_imponible_retencion_' + i).val());
                }
            }
            retencion_id = $("#retencion_id_" + item + " option:selected").val();
            var data = {'_token': $('meta[name=csrf-token]').attr('content'), retencion_id: retencion_id};
            resultado = peticion(data, 'verificarRetencionIR', '/retenciones/');
            if (resultado) {
                data = {'_token': $('meta[name=csrf-token]').attr('content'), retencion_id: retencion_id, cabecera_factura_id: cabecera_factura_id};
                var resultado_ir = peticion(data, 'obtenerBaseImponible');
                if (resultado_ir != null) {
                    var base_imponible = resultado_ir - suma_retencion_ir;
                    $('#base_imponible_retencion_' + item).val(base_imponible);
                    var porcentaje = $('#porcentaje_retencion_' + item).val();
                    var valor = base_imponible * porcentaje / 100;
                    $('#valor_retencion_' + item).val(valor);
                } else {
                    alert('ERROR AL OBTENER LA BASE IMPONIBLE PARA IR');
                }
            } else {
                data = {'_token': $('meta[name=csrf-token]').attr('content'), retencion_id: retencion_id, cabecera_factura_id: cabecera_factura_id};
                var resultado_iva = peticion(data, 'obtenerBaseImponible');
                if (resultado_iva != null) {
                    var base_imponible = resultado_iva - suma_retencion_iva;
                    $('#base_imponible_retencion_' + item).val(base_imponible);
                    var porcentaje = $('#porcentaje_retencion_' + item).val();
                    var valor = base_imponible * porcentaje / 100;
                    $('#valor_retencion_' + item).val(valor);
                } else {
                    alert('ERROR AL OBTENER LA BASE IMPONIBLE PARA IVA');
                }
            }
            //ACA TERMINO
            var base_imponible = $('#base_imponible_retencion_' + item).val();
            var porcentaje_retencion = $('#porcentaje_retencion_' + item).val();
            var valor_retencion = (base_imponible * porcentaje_retencion) / 100;
            $('#valor_retencion_' + item).val(valor_retencion);
        } else {
            alert('ERROR AL OBTENER LAS RETENCIONES');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener un pad">
    function pad(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para codigo de retencion en el item de comprobante de retencion">
    $('#items_comprobante_retencion').on("change", '[name=retencion_id]', function ()
    {
        var retencion_id = $(this).val();
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var id = $(this).attr('id');
        var item = id.charAt(id.length - 1);
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerRetenciones', '/retenciones/');
        if (resultado != null) {
            for (var i = 0; i < resultado.length; i++) {
                if (retencion_id == resultado[i].id) {
                    $('#nombre_retencion_' + item).val(resultado[i].descripcion);
                    $('#porcentaje_retencion_' + item).val(resultado[i].porcentaje);
                }
            }
        } else {
            alert('ERROR AL OBTENER LAS RETENCIONES');
        }
        var suma_retencion_ir = 0;
        var suma_retencion_iva = 0;
        for (var i = 0; i < items_comprobante_retencion - 1; i++) {
            if (i != item) {
                retencion_id = $("#retencion_id_" + i + " option:selected").val();
                data = {'_token': $('meta[name=csrf-token]').attr('content'), retencion_id: retencion_id};
                var resultado = peticion(data, 'verificarRetencionIR', '/retenciones/');
                if (resultado) {
                    suma_retencion_ir = suma_retencion_ir + parseFloat($('#base_imponible_retencion_' + i).val());
                } else {
                    suma_retencion_iva = suma_retencion_iva + parseFloat($('#base_imponible_retencion_' + i).val());
                }
            }
        }
        retencion_id = $(this).val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), retencion_id: retencion_id};
        resultado = peticion(data, 'verificarRetencionIR', '/retenciones/');
        if (resultado) {
            data = {'_token': $('meta[name=csrf-token]').attr('content'), retencion_id: retencion_id, cabecera_factura_id: cabecera_factura_id};
            var resultado_ir = peticion(data, 'obtenerBaseImponible');
            if (resultado_ir != null) {
                var base_imponible = resultado_ir - suma_retencion_ir;
                $('#base_imponible_retencion_' + item).val(base_imponible);
                var porcentaje = $('#porcentaje_retencion_' + item).val();
                var valor = base_imponible * porcentaje / 100;
                $('#valor_retencion_' + item).val(valor);
            } else {
                alert('ERROR AL OBTENER LA BASE IMPONIBLE PARA IR');
            }
        } else {
            data = {'_token': $('meta[name=csrf-token]').attr('content'), retencion_id: retencion_id, cabecera_factura_id: cabecera_factura_id};
            var resultado_iva = peticion(data, 'obtenerBaseImponible');
            if (resultado_iva != null) {
                var base_imponible = resultado_iva - suma_retencion_iva;
                $('#base_imponible_retencion_' + item).val(base_imponible);
                var porcentaje = $('#porcentaje_retencion_' + item).val();
                var valor = base_imponible * porcentaje / 100;
                $('#valor_retencion_' + item).val(valor);
            } else {
                alert('ERROR AL OBTENER LA BASE IMPONIBLE PARA IVA');
            }
        }
        //ACA TERMINO
        var base_imponible = $('#base_imponible_retencion_' + item).val();
        var porcentaje_retencion = $('#porcentaje_retencion_' + item).val();
        var valor_retencion = (base_imponible * porcentaje_retencion) / 100;
        $('#valor_retencion_' + item).val(valor_retencion);
    });
    // </editor-fold>

    $('#comprobante_retencion_secuencia').on("change", function ()
    {
        var comprobante_retencion_secuencia = $('#comprobante_retencion_secuencia').val();
        var padsecuencia = pad(comprobante_retencion_secuencia, 13);
        $('#comprobante_retencion_secuencia').val(padsecuencia);
    });

    $('#items_comprobante_retencion').on("change", '[name=base_imponible_retencion]', function ()
    {
        var base_imponible = $(this).val();
        var id = $(this).attr('id');
        var item = id.charAt(id.length - 1);
        var porcentaje = $('#porcentaje_retencion_' + item).val();
        var valor = base_imponible * porcentaje / 100;
        $('#valor_retencion_' + item).val(valor);
    });


    $("#crearComprobanteRetencion").on("click", function ()
    {
        crearComprobanteRetencion();
    });

    $("#cargarXML").on("click", function ()
    {
        cargarComprobanteRetencion();
    });

    //<editor-fold defaultstate="collapsed" desc="Funcion para cargar comprobante de retencion">
    function cargarComprobanteRetencion() {
        var archivo = $("#archivo")[0].files[0];
        var data = new FormData();
        data.append('_token', $('[name="csrf-token"]').attr('content'));
        data.append("archivo", archivo);
        var resultado = peticionArchivo(data, 'cargarComprobanteRetencion');
        if (resultado != null) {
            var comprobante = resultado[0];
            var comprobante_retencion = comprobante.comprobante_retencion.split('-');
            var comprobante_retencion_establecimiento = comprobante_retencion[0];
            var comprobante_retencion_punto_venta = comprobante_retencion[1];
            var comprobante_retencion_secuencia = comprobante_retencion[2];
            var fecha_emision = comprobante.fecha_emision.split('/');
            var dia = fecha_emision[0];
            var mes = fecha_emision[1];
            var ano = fecha_emision[2];
            var fecha_emision = ano + '-' + mes + '-' + dia;
            var autorizacion = comprobante.autorizacion;
            $('#comprobante_retencion_establecimiento').val(comprobante_retencion_establecimiento);
            $('#comprobante_retencion_punto_venta').val(comprobante_retencion_punto_venta);
            $('#comprobante_retencion_secuencia').val(comprobante_retencion_secuencia);
            $('#fecha_emision').val(fecha_emision);
            $('#autorizacion').val(autorizacion);
            var comprobante_retencion_secuencia = $('#comprobante_retencion_secuencia').val();
            var padsecuencia = pad(comprobante_retencion_secuencia, 13);
            $('#comprobante_retencion_secuencia').val(padsecuencia);

            //AQUI LLENO PARA LOS ITEMS
            var items = resultado[1];
            $('#retencion_id_0').val(items[0].retencion.id);
            var retencion_id = items[0].retencion.id;
            data = {'_token': $('meta[name=csrf-token]').attr('content'), id: retencion_id};
            var retencion = peticion(data, 'obtenerRetencion', '/retenciones/');
            $('#nombre_retencion_0').val(retencion.descripcion);
            $('#porcentaje_retencion_0').val(retencion.porcentaje);
            $('#base_imponible_retencion_0').val(items[0].base_imponible);
            $('#valor_retencion_0').val(items[0].valor);
            for (var i = 1; i < items.length; i++) {
                var item = items_comprobante_retencion;
                nuevoItemComprobanteRetencion();
                cargarItemComprobanteRetencion(item);
                $('#retencion_id_' + item).val(items[i].retencion_id);
                var retencion_id = items[i].retencion_id;
                data = {'_token': $('meta[name=csrf-token]').attr('content'), id: retencion_id};
                retencion = peticion(data, 'obtenerRetencion', '/retenciones/');
                $('#nombre_retencion_'+item).val(retencion.descripcion);
                $('#porcentaje_retencion_'+item).val(retencion.porcentaje);
                $('#base_imponible_retencion_' + item).val(items[i].base_imponible);
                $('#valor_retencion_' + item).val(items[i].valor);
            }
        } else {
            alert('ERROR AL CARGAR EL COMPROBANTE DE RETENCION');
        }
    }
    // </editor-fold>


    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/recaudacionescomprobantesretenciones/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }

        });
        return respuesta;
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos con FormData">
    function peticionArchivo(data, funcion, ruta = '/recaudacionescomprobantesretenciones/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            processData: false,
            contentType: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>

});

