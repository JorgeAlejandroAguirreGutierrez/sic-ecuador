/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Fecha de cobro y efectivizacion en cheque ala vista es la misma de cuando se almacena la factura LISTO
 * Fecha de cobro la misma de la factura y fecha de efectivizacion variable y mayor a la fecha de cobro en cheque posfechado LISTO
 * Fecha de cobro es igual a la fecha de la factura en efectivo LISTO
 * Fecha de cobro es igual a la fecha de la factura en deposito LISTO
 * el input de banco debe ser editable LISTO
 * Codigo individual para los tipos de recaudo LISTO DEPOSITO= DP, TRANSFERENCIA=TR => LISTO
 * AGREGAR CASILLA SALDO: DIFERENTE ENTRE EL TOTAL DE LA FACTURA Y LO RECAUDADO LISTO ->TRABAJAR CON DOS DECIMALES Y SIGNO DE DOLAR AL PRINCIPIO => LISTO
 * PONER AUITOMATICAMENTE EL VALOR QUE FALTA POR RECAUDAR EN LA CASILLA VALOR PARA CADA TIPO DE RECAUDO LISTO => LISTO
 * LLENAR AUTOMATICAMENT EL COMENTARIO LISTO ->verificar si es venta de mercaderia o servicios, y completar comentario con campos, como banco, papeleta, numero de cheque. => LISTO
 * LLENAR SEGUN EL FORMATO DE TARJETAS (factura cliente b/s , tarjeta / ifis / operador / boucher / lote / fecha) ->PENDIENTE
 * EDITAR LOS RECAUDOS
 * FORMATEAR EL NUMERO DE CHEQUE CON PAD -> LISTO
 * 
 * LUEGO DE ESTAR ANULADA UNA FACTURA O SI ESTA EMITIDA Y SI SE VA A CREAR UNA NUEVA, TRAER LA CABECERA Y LAS LINEAS DE LA FACTURA -> PENDIENTE
 * 
 * FACTURACION
 * SI GUIAS DE REMISION = NO ENTONCES DESHABILITAR TRANSPORTISTA Y VEHICULO => PENDIENTE
 * 
 * CIERRE DE CAJA: UNA VREZ HECHO EL CIERRE DE CAJA NO ES POSIBLE CAMBIAR LAS FORMAS DE COBRO
 * 
 * DESCARGA DE LAS SERIES DE LOS PRODUCTOS EN FACTURACION ES NECESARIO EL MODULO DE COMPRAS.
 * 
 * CARGA DE PLANTILLAS POR CADA PANTALLA
 * 
 * Validar en las pantallas de recaudaciones que año y mes en el codigo de una recaudacion debe ser igual al de la factura -> PENDIENTE
 * Cuando se haga recaudacion directamente las transacciones de recaudo debe ser igual al monto total de la factura, si no no se debe cerrar la factura -> PENDIENTE 
 * 
 * 
 * 
 * SE UTILIZARA LOS NUMEROS EN DECIMAL DE DOS POSICIONES.
 */
$(function ()
{
    var banco_id = null;
    var banco = '';
    //<editor-fold defaultstate="collapsed" desc="Eventos al cargar la pantalla">
    //Obtener establecimiento, obtener puntos de venta por establecimiento, obtener bancos, obtener tipos de cheque
    //obtener tipo deposito o transferencia, obtener codigo de cheque, obtener codigo de efectivo y obtener codigo de deposito o transferencia
    $(document).ready(function ()
    {
        obtenerEstablecimientos();
        obtenerPuntosVentasEstablecimiento();
        obtenerBancos();
        obtenerTiposCheques();
        obtenerTiposDepositosTransferencias();

        ObtenerTipoLineas();

        obtenerCodigoChequeVista();
        obtenerCodigoChequePosfechado();
        obtenerCodigoEfectivo();
        obtenerCodigoDeposito();
        obtenerCodigoTransferencia();

        obtenerFechaCobroChequeVista();
        obtenerFechaEfectivizacionChequeVista();
        obtenerFechaCobroChequePosfechado();
        obtenerFechaCobroEfectivo();
        obtenerFechaCobroDeposito();

        actualizarComentario('cheque_vista');
        actualizarComentario('cheque_posfechado');
        actualizarComentario('efectivo');
        actualizarComentario('deposito');
        actualizarComentario('transferencia');

        $("#nav-cheque_vista-tab").addClass('active');
        $("#nav-cheque_posfechado-tab").removeClass('active');
        $("#nav-efectivo-tab").removeClass('active');
        $("#nav-deposito-tab").removeClass('active');
        $("#nav-transferencia-tab").removeClass('active');

        $("#nav-cheque_vista").addClass('active');
        $("#nav-cheque_vista").addClass('show');
        $("#nav-cheque_posfechado").removeClass('active');
        $("#nav-cheque_posfechado").removeClass('show');
        $("#nav-efectivo").removeClass('active');
        $("#nav-efectivo").removeClass('show');
        $("#nav-deposito").removeClass('active');
        $("#nav-deposito").removeClass('show');
        $("#nav-transferencia").removeClass('active');
        $("#nav-transferencia").removeClass('show');
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Eventos nav-tab y eventos crear cheque, efectivo, deposito, transferencia">
    $("#nav-cheque_vista-tab").on("click", function ()
    {
        $("#nav-cheque_vista-tab").addClass('active');
        $("#nav-cheque_posfechado-tab").removeClass('active');
        $("#nav-efectivo-tab").removeClass('active');
        $("#nav-deposito-tab").removeClass('active');
        $("#nav-transferencia-tab").removeClass('active');

        $("#nav-cheque_vista").addClass('active');
        $("#nav-cheque_vista").addClass('show');
        $("#nav-cheque_posfechado").removeClass('active');
        $("#nav-cheque_posfechado").removeClass('show');
        $("#nav-efectivo").removeClass('active');
        $("#nav-efectivo").removeClass('show');
        $("#nav-deposito").removeClass('active');
        $("#nav-deposito").removeClass('show');
        $("#nav-transferencia").removeClass('active');
        $("#nav-transferencia").removeClass('show');


    });
    $("#nav-cheque_posfechado-tab").on("click", function ()
    {
        $("#nav-cheque_vista-tab").removeClass('active');
        $("#nav-cheque_posfechado-tab").addClass('active');
        ;
        $("#nav-efectivo-tab").removeClass('active');
        $("#nav-deposito-tab").removeClass('active');
        $("#nav-transferencia-tab").removeClass('active');

        $("#nav-cheque_vista").removeClass('active');
        $("#nav-cheque_vista").removeClass('show');
        $("#nav-cheque_posfechado").addClass('active');
        $("#nav-cheque_posfechado").addClass('show');
        $("#nav-efectivo").removeClass('active');
        $("#nav-efectivo").removeClass('show');
        $("#nav-deposito").removeClass('active');
        $("#nav-deposito").removeClass('show');
        $("#nav-transferencia").removeClass('active');
        $("#nav-transferencia").removeClass('show');
    });
    $("#nav-efectivo-tab").on("click", function ()
    {
        $("#nav-cheque_vista-tab").removeClass('active');
        $("#nav-cheque_posfechado-tab").removeClass('active');
        ;
        $("#nav-efectivo-tab").addClass('active');
        $("#nav-deposito-tab").removeClass('active');
        $("#nav-transferencia-tab").removeClass('active');

        $("#nav-cheque_vista").removeClass('active');
        $("#nav-cheque_vista").removeClass('show');
        $("#nav-cheque_posfechado").removeClass('active');
        $("#nav-cheque_posfechado").removeClass('show');
        $("#nav-efectivo").addClass('active');
        $("#nav-efectivo").addClass('show');
        $("#nav-deposito").removeClass('active');
        $("#nav-deposito").removeClass('show');
        $("#nav-transferencia").removeClass('active');
        $("#nav-transferencia").removeClass('show');
    });

    $("#nav-deposito-tab").on("click", function ()
    {
        $("#nav-cheque_vista-tab").removeClass('active');
        $("#nav-cheque_posfechado-tab").removeClass('active');
        ;
        $("#nav-efectivo-tab").removeClass('active');
        $("#nav-deposito-tab").addClass('active');
        $("#nav-transferencia-tab").removeClass('active');

        $("#nav-cheque_vista").removeClass('active');
        $("#nav-cheque_vista").removeClass('show');
        $("#nav-cheque_posfechado").removeClass('active');
        $("#nav-cheque_posfechado").removeClass('show');
        $("#nav-efectivo").removeClass('active');
        $("#nav-efectivo").removeClass('show');
        $("#nav-deposito").addClass('active');
        $("#nav-deposito").addClass('show');
        $("#nav-transferencia").removeClass('active');
        $("#nav-transferencia").removeClass('show');
    });

    $("#nav-transferencia-tab").on("click", function ()
    {
        $("#nav-cheque_vista-tab").removeClass('active');
        $("#nav-cheque_posfechado-tab").removeClass('active');
        ;
        $("#nav-efectivo-tab").removeClass('active');
        $("#nav-deposito-tab").removeClass('active');
        $("#nav-transferencia-tab").addClass('active');

        $("#nav-cheque_vista").removeClass('active');
        $("#nav-cheque_vista").removeClass('show');
        $("#nav-cheque_posfechado").removeClass('active');
        $("#nav-cheque_posfechado").removeClass('show');
        $("#nav-efectivo").removeClass('active');
        $("#nav-efectivo").removeClass('show');
        $("#nav-deposito").removeClass('active');
        $("#nav-deposito").removeClass('show');
        $("#nav-transferencia").addClass('active');
        $("#nav-transferencia").addClass('show');
    });

    $('#crearChequeVista').on("click", function ()
    {
        crearChequeVista();
    });

    $('#crearChequePosfechado').on("click", function ()
    {
        crearChequePosfechado();
    });

    $('#crearEfectivo').on("click", function ()
    {
        crearEfectivo();
    });

    $('#crearDeposito').on("click", function ()
    {
        crearDeposito();
    });

    $('#crearTransferencia').on("click", function ()
    {
        crearTransferencia();
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion crear cheque a la vista">
    function crearChequeVista()
    {
        var codigo = $('#codigo_cheque_vista').val();
        var fecha_cobro = $('#fecha_cobro_cheque_vista').val();
        var fecha_efectivizacion = $('#fecha_efectivizacion_cheque_vista').val();
        var numero = $('#numero_cheque_vista').val();
        var comentario = $('#comentario_cheque_vista').val().toUpperCase();
        var valor = $('#valor_cheque_vista').val();
        var tipo_id = null;
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, fecha_cobro: fecha_cobro, fecha_efectivizacion: fecha_efectivizacion, numero: numero, comentario: comentario, valor: valor, banco_id: banco_id, tipo_id: tipo_id, cabecera_factura_id: cabecera_factura_id};
        var resultado = peticion(data, 'crearChequeVista');
        if (resultado) {
            window.location.href = '/recaudacionesefectivo/crear?id=' + cabecera_factura_id;
        } else {
            alert("ERROR AL CREAR EL CHEQUE A LA VISTA");
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion crear cheque posfechado">
    function crearChequePosfechado()
    {
        var codigo = $('#codigo_cheque_posfechado').val();
        var fecha_cobro = $('#fecha_cobro_cheque_posfechado').val();
        var fecha_efectivizacion = $('#fecha_efectivizacion_cheque_posfechado').val();
        var numero = $('#numero_cheque_posfechado').val();
        var comentario = $('#comentario_cheque_posfechado').val().toUpperCase();
        var valor = $('#valor_cheque_posfechado').val();
        var tipo_id = null;
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, fecha_cobro: fecha_cobro, fecha_efectivizacion: fecha_efectivizacion, numero: numero, comentario: comentario, valor: valor, banco_id: banco_id, tipo_id: tipo_id, cabecera_factura_id: cabecera_factura_id};
        var resultado = peticion(data, 'crearChequePosfechado');
        if (resultado) {
            window.location.href = '/recaudacionesefectivo/crear?id=' + cabecera_factura_id;
        } else {
            alert("ERROR AL CREAR EL CHEQUE POSFECHADO");
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion crear efectivo">
    function crearEfectivo()
    {
        var codigo = $('#codigo_efectivo').val();
        var fecha_cobro = $('#fecha_cobro_efectivo').val();
        var comentario = $('#comentario_efectivo').val().toUpperCase();
        var valor = $('#valor_efectivo').val();
        var punto_venta_id = $('select[name=punto_venta_id_efectivo]').val();
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, fecha_cobro: fecha_cobro, comentario: comentario, valor: valor, punto_venta_id: punto_venta_id, cabecera_factura_id: cabecera_factura_id};
        var resultado = peticion(data, 'crearEfectivo');
        if (resultado) {
            window.location.href = '/recaudacionesefectivo/crear?id=' + cabecera_factura_id;
        } else {
            alert("ERROR AL CREAR LA RECAUDACION EN EFECTIVO");
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion crear deposito">
    function crearDeposito()
    {
        var codigo = $('#codigo_deposito').val();
        var fecha_cobro = $('#fecha_cobro_deposito').val();
        var numero_soporte = $('#numero_soporte_deposito').val();
        var comentario = $('#comentario_deposito').val().toUpperCase();
        var valor = $('#valor_deposito').val();
        var tipo_id = null;
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, fecha_cobro: fecha_cobro, numero_soporte: numero_soporte, comentario: comentario, valor: valor, banco_id: banco_id, tipo_id: tipo_id, cabecera_factura_id: cabecera_factura_id};
        var resultado = peticion(data, 'crearDeposito');
        if (resultado) {
            window.location.href = '/recaudacionesefectivo/crear?id=' + cabecera_factura_id;
        } else {
            alert("ERROR AL CREAR EL DEPOSITO");
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion crear transferencia">
    function crearTransferencia()
    {
        var codigo = $('#codigo_transferencia').val();
        var fecha_cobro = $('#fecha_cobro_transferencia').val();
        var numero_soporte = $('#numero_soporte_transferencia').val();
        var comentario = $('#comentario_transferencia').val().toUpperCase();
        var valor = $('#valor_transferencia').val();
        var tipo_id = null;
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), codigo: codigo, fecha_cobro: fecha_cobro, numero_soporte: numero_soporte, comentario: comentario, valor: valor, banco_id: banco_id, tipo_id: tipo_id, cabecera_factura_id: cabecera_factura_id};
        var resultado = peticion(data, 'crearTransferencia');
        if (resultado) {
            window.location.href = '/recaudacionesefectivo/crear?id=' + cabecera_factura_id;
        } else {
            alert("ERROR AL CREAR LA TRANSFERENCIA");
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener los bancos">
    function obtenerBancos()
    {
        $('#banco_id_cheque_vista').editableSelect();
        $('#banco_id_cheque_posfechado').editableSelect();
        $('#banco_id_deposito').editableSelect();
        $('#banco_id_transferencia').editableSelect();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), tipo: 'BANCO'};
        var resultado = peticion(data, 'obtenerDatosAdicionales', '/datosadicionales/');
        if (resultado != null) {
            for (var i = 0; i < resultado.length; i++) {
                $('#banco_id_cheque_vista').editableSelect('add', function () {
                    $(this).val(resultado[i].id);
                    $(this).text(resultado[i].nombre);
                });
                $('#banco_id_cheque_posfechado').editableSelect('add', function () {
                    $(this).val(resultado[i].id);
                    $(this).text(resultado[i].nombre);
                });
                $('#banco_id_deposito').editableSelect('add', function () {
                    $(this).val(resultado[i].id);
                    $(this).text(resultado[i].nombre);
                });
                $('#banco_id_transferencia').editableSelect('add', function () {
                    $(this).val(resultado[i].id);
                    $(this).text(resultado[i].nombre);
                });
            }
        } else {
            alert('ERROR AL OBTENER LOS BANCOS');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion obtener establecimientos">
    function obtenerEstablecimientos()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerEstablecimientos', '/establecimientos/');
        if (resultado != null) {
            var establecimiento_id_efectivo = $('#establecimiento_id_efectivo');
            for (var i = 0; i < resultado.length; i++)
            {
                var opcion = $('<option>', {text: resultado[i].codigo, value: resultado[i].id});
                establecimiento_id_efectivo.append(opcion);
            }
        } else {
            alert('ERROR AL OBTENER LOS ESTABLECIMIENTOS');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion obtener la fecha de cobro para cheque a la vista">
    function obtenerFechaCobroChequeVista()
    {
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), cabecera_factura_id: cabecera_factura_id};
        var resultado = peticion(data, 'obtenerFecha');
        if (resultado != null) {
            $('#fecha_cobro_cheque_vista').val(resultado);
        } else {
            alert('ERROR AL OBTENER LA FECHA');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion obtener fecha de efectivizacion para cheque a la vista">
    function obtenerFechaEfectivizacionChequeVista()
    {
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), cabecera_factura_id: cabecera_factura_id};
        var resultado = peticion(data, 'obtenerFecha');
        if (resultado != null) {
            $('#fecha_efectivizacion_cheque_vista').val(resultado);
        } else {
            alert('ERROR AL OBTENER LA FECHA');
        }

    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion obtener fecha de cobro para cheque posfechado">
    function obtenerFechaCobroChequePosfechado()
    {
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), cabecera_factura_id: cabecera_factura_id};
        var resultado = peticion(data, 'obtenerFecha');
        if (resultado != null) {
            $('#fecha_cobro_cheque_posfechado').val(resultado);
        } else {
            alert('ERROR AL OBTENER LA FECHA');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion obtener fecha de cobro para efectivo">
    function obtenerFechaCobroEfectivo()
    {
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), cabecera_factura_id: cabecera_factura_id};
        var resultado = peticion(data, 'obtenerFecha');
        if (resultado != null) {
            $('#fecha_cobro_efectivo').val(resultado);
        } else {
            alert('ERROR AL OBTENER LA FECHA');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion obtener fecha de cobro para deposito">
    function obtenerFechaCobroDeposito()
    {
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), cabecera_factura_id: cabecera_factura_id};
        var resultado = peticion(data, 'obtenerFecha');
        if (resultado != null) {
            $('#fecha_cobro_deposito').val(resultado);
        } else {
            alert('ERROR AL OBTENER LA FECHA');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion obtener puntos de venta por establecimiento">
    function obtenerPuntosVentasEstablecimiento()
    {
        var establecimiento_id = $('select[name=establecimiento_id_efectivo]').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), establecimiento_id: establecimiento_id};
        var resultado = peticion(data, 'obtenerPuntosVentasEstablecimiento', '/puntosventas/');
        var punto_venta_id_efectivo = $('#punto_venta_id_efectivo');
        for (var i = 0; i < resultado.length; i++) {
            var opcion = $('<option>', {text: resultado[i].codigo, value: resultado[i].id});
            punto_venta_id_efectivo.append(opcion);
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener los tipos de cheque">
    function obtenerTiposCheques()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), tipo: 'CHEQUE'};
        var resultado = peticion(data, 'obtenerDatosAdicionales', '/datosadicionales/');
        for (var i = 0; i < resultado.length; i++) {
            $('#tipo_id' + resultado[i].nombre).val(resultado[i].id);
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener el tipo deposito o transferencia">
    function obtenerTiposDepositosTransferencias()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), tipo: 'DEPOSITO TRANSFERENCIA'};
        var resultado = peticion(data, 'obtenerDatosAdicionales', '/datosadicionales/');
        for (var i = 0; i < resultado.length; i++) {
            $('#tipo_id' + resultado[i].nombre).val(resultado[i].id);
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Eventos para eliminar un cheque">
    $('[name=eliminarCheque]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerCheque');
        if (resultado != null) {
            $('#id_chequeE').val(resultado.id);
            $('#codigo_chequeE').val(resultado.codigo);
            $('#fecha_cobro_chequeE').val(resultado.fecha_cobro);
            $('#numero_chequeE').val(resultado.numero);
            $('#comentario_chequeE').val(resultado.comentario);
            $('#valor_chequeE').val(resultado.valor);
            $('#banco_chequeE').val(resultado.banco.nombre);
        } else {
            alert('ERROR AL OBTENER EL CHEQUE');
        }
    });

    $('#cEliminarCheque').on("click", function ()
    {
        var id = $('#id_chequeE').val();
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarCheque');
        if (resultado) {
            window.location.href = "/recaudacionesefectivo/crear?id=" + cabecera_factura_id;
        } else {
            console.log('ERROR AL ELIMINAR UN CHEQUE');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Eventos para eliminar un deposito o transferencia">
    $('[name=eliminarDepositoTransferencia]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerDepositoTransferencia');
        if (resultado != null) {
            $('#id_deposito_transferenciaE').val(resultado.id);
            $('#codigo_deposito_transferenciaE').val(resultado.codigo);
            $('#fecha_cobro_deposito_transferenciaE').val(resultado.fecha_cobro);
            $('#numero_soporte_deposito_transferenciaE').val(resultado.numero_soporte);
            $('#comentario_deposito_transferenciaE').val(resultado.comentario);
            $('#valor_deposito_transferenciaE').val(resultado.valor);
            $('#banco_deposito_transferenciaE').val(resultado.banco.nombre);
        } else {
            alert('ERROR AL OBTENER EL DEPOSITO O TRANSFERENCIA');
        }
    });

    $('#cEliminarDepositoTransferencia').on("click", function ()
    {
        var id = $('#id_deposito_transferenciaE').val();
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarDepositoTransferencia');
        if (resultado) {
            window.location.href = "/recaudacionesefectivo/crear?id=" + cabecera_factura_id;
        } else {
            alert('ERROR AL ELIMINAR UN DEPOSITO O TRANSFERENCIA');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Eventos para eliminar efectivo">
    $('[name=eliminarEfectivo]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerEfectivo');
        if (resultado != null) {
            $('#id_efectivoE').val(resultado.id);
            $('#codigo_efectivoE').val(resultado.codigo);
            $('#fecha_cobro_efectivoE').val(resultado.fecha_cobro);
            $('#comentario_efectivoE').val(resultado.comentario);
            $('#valor_efectivoE').val(resultado.valor);
        } else {
            alert('ERROR AL OBTENER EL EFECTIVO');
        }
    });

    $('#cEliminarEfectivo').on("click", function ()
    {
        var id = $('#id_efectivoE').val();
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'eliminarEfectivo');
        if (resultado) {
            window.location.href = "/recaudacionesefectivo/crear?id=" + cabecera_factura_id;
        } else {
            alert('ERROR AL ELIMINAR EFECTIVO');
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener el codigo del cheque a la vista">
    function obtenerCodigoChequeVista()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCodigoChequeVista');
        if (resultado != null) {
            $('#codigo_cheque_vista').val(resultado);
        } else {
            alert('ERROR AL OBTENER EL CODIGO PARA EL CHEQUE A LA VISTA');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener el codigo del cheque posfechado">
    function obtenerCodigoChequePosfechado()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCodigoChequePosfechado');
        if (resultado != null) {
            $('#codigo_cheque_posfechado').val(resultado);
        } else {
            alert('ERROR AL OBTENER ')
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener el codigo de efectivo">
    function obtenerCodigoEfectivo()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCodigoEfectivo');
        if (resultado != null) {
            $('#codigo_efectivo').val(resultado);
        } else {
            alert('ERROR AL OBTENER EL CODIGO PARA EL EFECTIVO');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener el codigo de un deposito">
    function obtenerCodigoDeposito()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCodigoDeposito');
        if (resultado != null) {
            $('#codigo_deposito').val(resultado);
        } else {
            alert('ERROR AL OBTENER EL CODIGO DEL DEPOSITO');
        }

    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener el codigo para una transferencia">
    function obtenerCodigoTransferencia()
    {
        var data = {'_token': $('meta[name=csrf-token]').attr('content')};
        var resultado = peticion(data, 'obtenerCodigoTransferencia');
        if (resultado != null) {
            $('#codigo_transferencia').val(resultado);
        } else {
            alert('ERROR AL OBTENER EL CODIGO DE TRANSFERENCIA');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para obtener los tipos de linea de la factura">
    function ObtenerTipoLineas()
    {
        var cabecera_factura_id = $('#cabecera_factura_id').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), cabecera_factura_id: cabecera_factura_id};
        var resultado = peticion(data, 'obtenerTiposLineas');
        if (resultado != null) {
            $('#productos').val(resultado.productos);
            $('#servicios').val(resultado.servicios);
        } else {
            alert('ERROR AL OBTENER LOS TIPOS DE LINEAS');
        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento al cambiar de establecimiento">
    $('#establecimiento_id').on('change', function () {
        var establecimiento_id = $('select[name=establecimiento_id_efectivo]').val();
        obtenerPuntosVentasEstablecimiento(establecimiento_id);

    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento fecha de efectivizacion para cheque posfechado">
    $('#fecha_efectivizacion_cheque_posfechado').on('change', function () {
        var fecha_cobro_cheque_posfechado = $('#fecha_cobro_cheque_posfechado').val();
        var fecha_efectivizacion_cheque_posfechado = $('#fecha_efectivizacion_cheque_posfechado').val();

        var fecha_efectivizacion = new Date(fecha_efectivizacion_cheque_posfechado);
        var dia_efectivizacion = fecha_efectivizacion.getDay();
        var mes_efectivizacion = fecha_efectivizacion.getMonth();
        var año_efectivizacion = fecha_efectivizacion.getFullYear();

        var fecha_cobro = new Date(fecha_cobro_cheque_posfechado);
        var dia_cobro = fecha_cobro.getDay();
        var mes_cobro = fecha_cobro.getMonth();
        var año_cobro = fecha_cobro.getFullYear();
        if (dia_efectivizacion < dia_cobro && mes_efectivizacion < mes_cobro && año_efectivizacion < año_cobro)
        {
            alert('La fecha de efectivizacion tiene que ser mayor a la fecha de cobro');
            $('#fecha_efectivizacion_cheque_posfechado').val(fecha_cobro_cheque_posfechado);
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento select editable para bancos">
    $('#banco_id_cheque_vista').editableSelect().on('select.editable-select', function (e, li) {
        banco_id = li.val();
        banco = li.text();
        actualizarComentario('cheque_vista');
    });

    $('#banco_id_cheque_posfechado').editableSelect().on('select.editable-select', function (e, li) {
        banco_id = li.val();
        banco = li.text();
        actualizarComentario('cheque_posfechado');

    });

    $('#banco_id_deposito').editableSelect().on('select.editable-select', function (e, li) {
        banco_id = li.val();
        banco = li.text();
        actualizarComentario('deposito');
    });

    $('#banco_id_transferencia').editableSelect().on('select.editable-select', function (e, li) {
        banco_id = li.val();
        banco = li.text();
        actualizarComentario('transferencia');
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento fecha de cobro para comentario">
    $('#fecha_cobro_cheque_vista').on('change', function () {
        actualizarComentario('cheque_vista');
    });

    $('#fecha_cobro_cheque_posfechado').on('change', function () {
        actualizarComentario('cheque_posfechado');
    });

    $('#fecha_cobro_efectivo').on('change', function () {
        actualizarComentario('efectivo');
    });

    $('#fecha_cobro_deposito').on('change', function () {
        actualizarComentario('deposito');
    });

    $('#fecha_cobro_transferencia').on('change', function () {
        actualizarComentario('transferencia');
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento numero de soporte para recaudo para comentario">
    $('#numero_cheque_vista').on('change', function () {
        actualizarComentario('cheque_vista');
    });

    $('#numero_cheque_posfechado').on('change', function () {
        actualizarComentario('cheque_posfechado');
    });

    $('#numero_deposito').on('change', function () {
        actualizarComentario('deposito');
    });

    $('#numero_transferencia').on('change', function () {
        actualizarComentario('transferencia');
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion para actualizar comentario">
    function actualizarComentario(parametro)
    {
        var codigo_interno_factura = $('#codigo_interno_factura').val();
        var cliente = $('#cliente').val();
        var productos = $('#productos').val();
        var servicios = $('#servicios').val();
        var fecha_cobro = $('#fecha_cobro_' + parametro).val();
        var numero = '';
        if (parametro === 'efectivo') {
            numero = '';
        } else if (parametro !== 'efectivo' && parametro !== 'deposito' && parametro !== 'transferencia') {
            numero = $('#numero_' + parametro).val();
        } else {
            numero = $('#numero_soporte_' + parametro).val();
        }
        var tipo = '';
        if (parametro === 'cheque_vista' || parametro === 'cheque_posfechado') {
            tipo = 'cheque';
        } else {
            tipo = parametro;
        }
        $('#comentario_' + parametro).val(codigo_interno_factura + ' ' + cliente + ' ' + productos + ' ' + servicios + ' ' + tipo + ' ' + numero + ' ' + fecha_cobro + ' ' + banco);
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para modificar cheque">
    $('[name=modificarCheque]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerCheque');
        if (resultado != null) {
            $('#id_chequeM').val(resultado.id);
            $('#codigo_chequeM').val(resultado.codigo);
            $('#fecha_cobro_chequeM').val(resultado.fecha_cobro);
            $('#fecha_efectivizacion_chequeM').val(resultado.fecha_efectivizacion);
            $('#numero_chequeM').val(resultado.numero);
            $('#comentario_chequeM').val(resultado.comentario);
            $('#valor_chequeM').val(resultado.valor);
            $('#banco_id_chequeM').val(resultado.banco_id);
        } else {
            alert('ERROR AL OBTENER LOS DATOS PARA MODIFICAR EL CHEQUE');
        }
    });

    $('#cModificarCheque').on("click", function ()
    {
        var id = $('#id_chequeM').val();
        var codigo = $('#codigo_chequeM').val();
        var fecha_cobro = $('#fecha_cobro_chequeM').val();
        var fecha_efectivizacion = $('#fecha_efectivizacion_chequeM').val();
        var numero = $('#numero_chequeM').val();
        var comentario = $('#comentario_chequeM').val();
        var valor = $('#valor_chequeM').val();
        var banco_id = $('#banco_id_chequeM').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id, codigo: codigo, fecha_cobro: fecha_cobro, fecha_efectivizacion: fecha_efectivizacion, numero: numero, comentario: comentario, valor: valor, banco_id: banco_id};
        var resultado = peticion(data, 'modificarEfectivo');
        if (resultado) {
            var cabecera_factura_id = $('#cabecera_factura_id').val();
            window.location.href = '/recaudacionesefectivo/crear?id=' + cabecera_factura_id;
        } else {
            alert("No Registrado correctamente");
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para modificar efectivo">
    $('[name=modificarEfectivo]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerEfectivo');
        if (resultado != null) {
            $('#id_efectivoM').val(resultado.id);
            $('#codigo_efectivoM').val(resultado.codigo);
            $('#fecha_cobro_efectivoM').val(resultado.fecha_cobro);
            $('#comentario_efectivoM').val(resultado.comentario);
            $('#valor_efectivoM').val(resultado.valor);
            $('#punto_venta_id_efectivoM').val(resultado.punto_venta_id);
        } else {
            alert('ERROR AL OBTENER EFECTIVO');
        }
    });

    $('#cModificarEfectivo').on("click", function ()
    {
        var id = $('#id_efectivoM').val();
        var codigo = $('#codigo_efectivoM').val();
        var fecha_cobro = $('#fecha_cobro_efectivoM').val();
        var comentario = $('#comentario_efectivoM').val();
        var valor = $('#valor_efectivoM').val();
        var punto_venta_id = $('#punto_venta_idM').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id, codigo: codigo, fecha_cobro: fecha_cobro, comentario: comentario, valor: valor, punto_venta_id: punto_venta_id};
        var resultado = peticion(data, 'modificarEfectivo');
        if (resultado) {
            var cabecera_factura_id = $('#cabecera_factura_id').val();
            window.location.href = '/recaudacionesefectivo/crear?id=' + cabecera_factura_id;
        } else {
            alert("No Registrado correctamente");
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Evento para modificar una deposito o transferencia">
    $('[name=modificarDepositoTransferencia]').on("click", function ()
    {
        var id = $(this).attr('id');
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id};
        var resultado = peticion(data, 'obtenerDepositoTransferencia');
        if (resultado != null) {
            $('#id_deposito_transferenciaM').val(resultado.id);
            $('#codigo_deposito_transferenciaM').val(resultado.codigo);
            $('#fecha_cobro_deposito_transferenciaM').val(resultado.fecha_cobro);
            $('#numero_soporte_deposito_transferenciaM').val(resultado.numero_soporte);
            $('#comentario_deposito_transferenciaM').val(resultado.comentario);
            $('#valor_deposito_transferenciaM').val(resultado.valor);
            $('#banco_id_deposito_transferenciaM').val(resultado.banco_id);
        } else {
            alert('ERROR AL OBTENER LOS DATOS PARA MODIFICAR UN DEPOSITO O TRANSFERENCIA');
        }
    });

    $('#cModificarTransferencia').on("click", function ()
    {
        var id = $('#id_deposito_transferenciaM').val();
        var codigo = $('#codigo_deposito_transferenciaM').val();
        var fecha_cobro = $('#fecha_cobro_deposito_transferenciaM').val();
        var numero_soporte = $('#numero_soporte_deposito_transferenciaM').val();
        var comentario = $('#comentario_deposito_transferenciaM').val();
        var valor = $('#valor_deposito_transferenciaM').val();
        var data = {'_token': $('meta[name=csrf-token]').attr('content'), id: id, codigo: codigo, fecha_cobro: fecha_cobro, numero_soporte: numero_soporte, comentario: comentario, valor: valor};
        var resultado = peticion(data, 'modificarDepositoTransferencia');
        if (resultado) {
            var cabecera_factura_id = $('#cabecera_factura_id').val();
            window.location.href = '/recaudacionesefectivo/crear?id=' + cabecera_factura_id;
        } else {
            alert("ERROR AL CREAR MODIFICAR EL DEPOSITO O TRANSFERENCIA");
        }
    });
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Funcion peticion de datos">
    function peticion(data, funcion, ruta = '/recaudacionesefectivo/') {
        var respuesta;
        $.ajax({
            type: "POST",
            url: ruta + funcion,
            data: data,
            async: false,
            success: function (data)
            {
                var resultado = data.resultado;
                respuesta = resultado;
            },
            error: function ()
            {
                $('#alerta').addClass('alert alert-danger');
                $('#alerta').text('Error en la peticion');
                respuesta = null;
            }
        });
        return respuesta;
    }
    // </editor-fold>

});

