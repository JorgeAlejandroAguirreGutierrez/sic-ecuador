<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Retencion extends TestCase
{
    /**
     * @test
     */
    public function testObtenerRetenciones(){
        $response = $this->post('/retenciones/obtenerRetenciones');
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                ['id'=>1, 'porcentaje'=>100],
                ['id'=>2, 'porcentaje'=>15],
                ['id'=>3, 'porcentaje'=>10]
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testObtenerRetencion(){
        $response = $this->post('/retenciones/obtenerRetencion', ['id'=>1]);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                'id'=>1, 'porcentaje'=>100
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testCrearRetencion(){
        $response = $this->post('/retenciones/crearRetencion', 
            ['codigo'=>'RETENCION_IVA5',
            'codigo_norma'=>'105',
            'homologacion_f_e'=>'5',
            'descripcion'=>'IVA_5%',
            'porcentaje'=>5]);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                true
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testActualizarRetencion() {
        $response = $this->post('/retenciones/actualizarRetencion', 
            ['id'=>1,
            'codigo'=>'RETENCION_IVA50',
            'codigo_norma'=>'150',
            'homologacion_f_e'=>'50',
            'descripcion'=>'IVA_50%',
            'porcentaje'=>50]);
        $response
                ->assertStatus(200)
                ->assertJson([
                    'resultado' => true
        ]);
    }
    
    /**
     * @test
     */
    public function testEliminarRetencion() {
        $response = $this->post('/retenciones/eliminarRetencion', 
            ['id'=>1]);
        $response
                ->assertStatus(200)
                ->assertJson([
                    'resultado' => true
        ]);
    }
    
    public static function tearDownAfterClass() {
        exec('php artisan migrate:reset');
        exec('php artisan migrate');
        exec('php artisan db:seed');
    }
}
