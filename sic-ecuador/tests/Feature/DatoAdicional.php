<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DatoAdicional extends TestCase
{
    /**
     * @test
     */
    public function testObtenerDatosAdicionales(){
        $response = $this->post('/datosadicionales/obtenerDatosAdicionales', ['tipo'=>'ESTADO CIVIL']);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                ['id'=>1, 'codigo'=>'DA1'],
                ['id'=>2, 'codigo'=>'DA2'],
                ['id'=>3, 'codigo'=>'DA3']
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testObtenerDatoAdicional(){
        $response = $this->post('/datosadicionales/obtenerDatoAdicional', ['id'=>1]);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                'id'=>1, 'codigo'=>'DA1'
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testCrearDatoAdicional(){
        $response = $this->post('/datosadicionales/crearDatoAdicional', 
            ['codigo'=>'DA100',
            'tipo'=>'DATO ADICIONAL',
            'nombre'=>'DATO ADICIONAL 1',
            'abreviatura'=>'DA']);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                true
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testActualizarDatoAdicional() {
        $response = $this->post('/datosadicionales/actualizarDatoAdicional', 
            ['id'=>1,
            'codigo'=>'DA100',
            'tipo'=>'DATO ADICIONAL',
            'nombre'=>'DATO ADICIONAL 1',
            'abreviatura'=>'DA']);
        $response
                ->assertStatus(200)
                ->assertJson([
                    'resultado' => true
        ]);
    }
    
    /**
     * @test
     */
    public function testEliminarDatoAdicional() {
        $response = $this->post('/datosadicionales/eliminarDatoAdicional', 
            ['id'=>1]);
        $response
                ->assertStatus(200)
                ->assertJson([
                    'resultado' => true
        ]);
    }
    
    public static function tearDownAfterClass() {
        exec('php artisan migrate:reset');
        exec('php artisan migrate');
        exec('php artisan db:seed');
    }
}
