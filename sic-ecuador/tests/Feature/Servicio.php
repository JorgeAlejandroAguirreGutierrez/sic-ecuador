<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Servicio extends TestCase
{
    /**
     * @test
     */
    public function testObtenerServicios(){
        $response = $this->post('/servicios/obtenerServicios');
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                ['id'=>1, 'nombre'=>'CARGA PANELA'],
                ['id'=>2, 'nombre'=>'CARGA LENTEJA'],
                ['id'=>3, 'nombre'=>'LIMPIEZA'],
                ['id'=>4, 'nombre'=>'EMPAQUE']
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testObtenerServicio(){
        $response = $this->post('/servicios/obtenerServicio', ['id'=>1]);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                'id'=>1, 'nombre'=>'CARGA PANELA'
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testCrearServicio(){
        $response = $this->post('/servicios/crearServicio', 
            ['codigo'=>'S5',
            'nombre'=>'SELLADO',
            'precio1'=>22.12,
            'precio2'=>18.98,
            'precio3'=>20.01,
            'precio4'=>21.12,
            'activo'=>1,
            'grupo_contable_id'=>5,
            'impuesto_id'=>1,
            'tipo_id'=>17]);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                true
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testActualizarServicio() {
        $response = $this->post('/servicios/actualizarServicio', 
            ['id'=>1,
            'codigo'=>'S6',
            'nombre'=>'OTRO',
            'precio1'=>22.12,
            'precio2'=>18.98,
            'precio3'=>20.01,
            'precio4'=>21.12,
            'activo'=>1,
            'grupo_contable_id'=>5,
            'impuesto_id'=>1,
            'tipo_id'=>17]);
        $response
                ->assertStatus(200)
                ->assertJson([
                    'resultado' => true
        ]);
    }
    
    /**
     * @test
     */
    public function testEliminarServicio() {
        $response = $this->post('/servicios/eliminarServicio', 
            ['id'=>1]);
        $response
                ->assertStatus(200)
                ->assertJson([
                    'resultado' => true
        ]);
    }
    
    public static function tearDownAfterClass() {
        exec('php artisan migrate:reset');
        exec('php artisan migrate');
        exec('php artisan db:seed');
    }
}
