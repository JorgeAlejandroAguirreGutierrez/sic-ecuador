<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Ubicaciongeo extends TestCase
{
    /**
     * @test
     */
    public function testObtenerUbicacionesgeo(){
        $response = $this->post('/ubicacionesgeo/obtenerUbicacionesgeo');
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                ['id'=>1, 'codigo'=>'U1'],
                ['id'=>2, 'codigo'=>'U2'],
                ['id'=>3, 'codigo'=>'U3']
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testObtenerUbicaciongeo(){
        $response = $this->post('/ubicacionesgeo/obtenerUbicaciongeo', ['id'=>1]);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                'id'=>1, 'codigo'=>'U1'
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testCrearUbicaciongeo(){
        $response = $this->post('/ubicacionesgeo/crearUbicaciongeo', 
            ['codigo'=>'U6',
            'codigo_norma'=>'6',
            'provincia'=>'CALDAS',
            'canton'=>'MANIZALES',
            'parroquia'=>'SALAMINA']);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                true
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testActualizarUbicaciongeo() {
        $response = $this->post('/ubicacionesgeo/actualizarUbicaciongeo', 
            ['id'=>5,
            'codigo'=>'U7',
            'codigo_norma'=>'7',
            'provincia'=>'ANTIOQUIA',
            'canton'=>'MEDELLIN',
            'parroquia'=>'ENVIGADO']);
        $response
                ->assertStatus(200)
                ->assertJson([
                    'resultado' => true
        ]);
    }
    
    /**
     * @test
     */
    public function testEliminarUbicaciongeo() {
        $response = $this->post('/ubicacionesgeo/eliminarUbicaciongeo', 
            ['id'=>1]);
        $response
                ->assertStatus(200)
                ->assertJson([
                    'resultado' => true
        ]);
    }
    
    public static function tearDownAfterClass() {
        exec('php artisan migrate:reset');
        exec('php artisan migrate');
        exec('php artisan db:seed');
    }
}
