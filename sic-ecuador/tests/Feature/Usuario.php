<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Usuario extends TestCase
{
    /**
     * @test
     */
    public function testObtenerUsuarios(){
        $response = $this->post('/usuarios/obtenerUsuarios');
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                ['id'=>1, 'identificacion'=>'010225036'],
                ['id'=>2, 'identificacion'=>'030132225'],
                ['id'=>3, 'identificacion'=>'080212685']
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testObtenerUsuario(){
        $response = $this->post('/usuarios/obtenerUsuario', ['id'=>1]);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                'id'=>1, 'identificacion'=>'010225036'
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testCrearUsuario(){
        $response = $this->post('/usuarios/crearUsuario', 
            ['codigo'=>'U5',
            'nombre'=>'USUARIO PRUEBA',
            'correo'=>'USUARIOPRUEBA@gmail.com',
            'contrasena'=>'12345',
            'identificacion'=>'9999999998',
            'activo'=>true,
            'tipo_id'=>14,
            'punto_venta_id'=>1]);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                true
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testActualizarUsuario() {
        $response = $this->post('/usuarios/actualizarUsuario', 
            ['id'=>4,
            'codigo'=>'U5',
            'nombre'=>'USUARIO PRUEBA',
            'correo'=>'USUARIOPRUEBA@gmail.com',
            'contrasena'=>'12345',
            'identificacion'=>'9999999998',
            'activo'=>true,
            'tipo_id'=>14,
            'punto_venta_id'=>1]);
        $response
                ->assertStatus(200)
                ->assertJson([
                    'resultado' => true
        ]);
    }
    
    /**
     * @test
     */
    public function testEliminarUsuario() {
        $response = $this->post('/usuarios/eliminarUsuario', 
            ['id'=>1]);
        $response
                ->assertStatus(200)
                ->assertJson([
                    'resultado' => true
        ]);
    }
    
    public static function tearDownAfterClass() {
        exec('php artisan migrate:reset');
        exec('php artisan migrate');
        exec('php artisan db:seed');
    }
}
