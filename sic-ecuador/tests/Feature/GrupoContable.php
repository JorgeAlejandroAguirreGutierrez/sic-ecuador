<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GrupoContable extends TestCase
{
    /**
     * @test
     */
    public function testObtenerGruposContables(){
        $response = $this->post('/gruposcontables/obtenerGruposContables');
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                ['id'=>1, 'denominacion'=>'CLIENTES NACIONALES'],
                ['id'=>2, 'denominacion'=>'CLIENTES INTERNACIONALES'],
                ['id'=>3, 'denominacion'=>'HONORARIOS']
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testObtenerGrupoContable(){
        $response = $this->post('/gruposcontables/obtenerGrupoContable', ['id'=>1]);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                'id'=>1, 'denominacion'=>'CLIENTES NACIONALES'
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testCrearGrupoContable(){
        $response = $this->post('/gruposcontables/crearGrupoContable', 
            ['codigo'=>'GC5',
            'denominacion'=>'CLIENTES INTERNOS',
            'numero_cuenta'=>'1.1.1.5',
            'nombre_cuenta'=>'1.1.1.5',
            'cliente_relacionado'=>1]);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                true
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testActualizarGrupoContable() {
        $response = $this->post('/gruposcontables/actualizarGrupoContable', 
            ['id'=>1,
            'codigo'=>'GC5',
            'denominacion'=>'CLIENTES INTERNOS',
            'numero_cuenta'=>'1.1.1.5',
            'nombre_cuenta'=>'1.1.1.5',
            'cliente_relacionado'=>1]);
        $response
                ->assertStatus(200)
                ->assertJson([
                    'resultado' => true
        ]);
    }
    
    /**
     * @test
     */
    public function testEliminarGrupoContable() {
        $response = $this->post('/gruposcontables/eliminarGrupoContable', 
            ['id'=>1]);
        $response
                ->assertStatus(200)
                ->assertJson([
                    'resultado' => true
        ]);
    }
    
    public static function tearDownAfterClass() {
        exec('php artisan migrate:reset');
        exec('php artisan migrate');
        exec('php artisan db:seed');
    }
}
