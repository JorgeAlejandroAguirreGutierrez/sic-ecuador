<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Transportista extends TestCase
{
    /**
     * @test
     */
    public function testObtenerTransportistas(){
        $response = $this->post('/transportistas/obtenerTransportistas');
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                ['id'=>1, 'nombre'=>'JUAN PEREZ'],
                ['id'=>2, 'nombre'=>'JOSE LOPEZ'],
                ['id'=>3, 'nombre'=>'VICTOR AGUILERA']
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testObtenerTransportista(){
        $response = $this->post('/transportistas/obtenerTransportista', ['id'=>1]);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                'id'=>1, 'nombre'=>'JUAN PEREZ'
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testCrearTransportista(){
        $response = $this->post('/transportistas/crearTransportista', 
            ['codigo'=>'T4',
            'nombre'=>'USUARIO PRUEBA',
            'identificacion'=>'130375365']);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                true
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testActualizarTransportista() {
        $response = $this->post('/transportistas/actualizarTransportista', 
            ['id'=>3,
            'codigo'=>'T4',
            'nombre'=>'USUARIO PRUEBA',
            'identificacion'=>'130375365']);
        $response
                ->assertStatus(200)
                ->assertJson([
                    'resultado' => true
        ]);
    }
    
    /**
     * @test
     */
    public function testEliminarUsuario() {
        $response = $this->post('/transportistas/eliminarTransportista', 
            ['id'=>1]);
        $response
                ->assertStatus(200)
                ->assertJson([
                    'resultado' => true
        ]);
    }
    
    public static function tearDownAfterClass() {
        exec('php artisan migrate:reset');
        exec('php artisan migrate');
        exec('php artisan db:seed');
    }
}
