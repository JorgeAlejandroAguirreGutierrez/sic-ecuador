<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TipoContribuyente extends TestCase
{
    /**
     * @test
     */
    public function testObtenerTiposContribuyentes(){
        $response = $this->post('/tiposcontribuyentes/obtenerTiposContribuyentes');
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                ['id'=>1, 'subtipo'=>'NO OBLIGADO A CONTABILIDAD'],
                ['id'=>2, 'subtipo'=>'OBLIGADO A CONTABILIDAD'],
                ['id'=>3, 'subtipo'=>'PUBLICA'],
                ['id'=>4, 'subtipo'=>'PRIVADA']
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testObtenerTipoContribuyente(){
        $response = $this->post('/tiposcontribuyentes/obtenerTipoContribuyente', ['id'=>1]);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                'id'=>1, 'subtipo'=>'NO OBLIGADO A CONTABILIDAD'
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testCrearTipoContribuyente(){
        $response = $this->post('/tiposcontribuyentes/crearTipoContribuyente', 
            ['codigo'=>'TC5',
            'tipo'=>'TIPO PRUEBA',
            'subtipo'=>'SUBTIPO PRUEBA',
            'especial'=>0]);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                true
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testActualizarUsuario() {
        $response = $this->post('/tiposcontribuyentes/actualizarTipoContribuyente', 
            ['id'=>1,
            'codigo'=>'TC6',
            'tipo'=>'TIPO ACTUALIZAR',
            'subtipo'=>'SUBTIPO ACTUALIZAR',
            'especial'=>0]);
        $response
                ->assertStatus(200)
                ->assertJson([
                    'resultado' => true
        ]);
    }
    
    /**
     * @test
     */
    public function testEliminarTipoContribuyente() {
        $response = $this->post('/tiposcontribuyentes/eliminarTipoContribuyente', 
            ['id'=>1]);
        $response
                ->assertStatus(200)
                ->assertJson([
                    'resultado' => true
        ]);
    }
    
    public static function tearDownAfterClass() {
        exec('php artisan migrate:reset');
        exec('php artisan migrate');
        exec('php artisan db:seed');
    }
}
