<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PlazoCredito extends TestCase
{
    /**
     * @test
     */
    public function testObtenerPlazosCreditos(){
        $response = $this->post('/plazoscreditos/obtenerPlazosCreditos');
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                ['id'=>1, 'plazo'=>30],
                ['id'=>2, 'plazo'=>45],
                ['id'=>3, 'plazo'=>60]
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testObtenerPlazoCredito(){
        $response = $this->post('/plazoscreditos/obtenerPlazoCredito', ['id'=>1]);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                'id'=>1, 'plazo'=>30
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testCrearPlazoCredito(){
        $response = $this->post('/plazoscreditos/crearPlazoCredito', 
            ['codigo'=>'PC4',
            'plazo'=>80]);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                true
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testActualizarPlazoCredito() {
        $response = $this->post('/plazoscreditos/actualizarPlazoCredito', 
            ['id'=>1,
            'codigo'=>'PC6',
            'plazo'=>90]);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado' => true
        ]);
    }
    
    /**
     * @test
     */
    public function testEliminarPlazoCredito() {
        $response = $this->post('/plazoscreditos/eliminarPlazoCredito',['id'=>1]);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado' => true
        ]);
    }
    
    public static function tearDownAfterClass() {
        exec('php artisan migrate:reset');
        exec('php artisan migrate');
        exec('php artisan db:seed');
    }
}
