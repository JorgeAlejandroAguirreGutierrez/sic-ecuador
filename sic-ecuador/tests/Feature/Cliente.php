<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Cliente extends TestCase
{
    /**
     * @test
     */
    public function testObtenerClientes(){
        $response = $this->post('/clientes/obtenerClientes');
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                ['id'=>1, 'razon_social'=>'CLIENTE A'],
                ['id'=>2, 'razon_social'=>'CLIENTE B'],
                ['id'=>3, 'razon_social'=>'CLIENTE CF']
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testObtenerCliente(){
        $response = $this->post('/clientes/obtenerCliente', ['id'=>1]);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                'id'=>1, 'razon_social'=>'CLIENTE A'
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testCrearCliente(){
        $response = $this->post('/clientes/crearCliente', 
            ['codigo'=>'CL100',
            'identificacion'=>'1768038860002',
            'razon_social'=>'CLIENTE D',
            'direccion'=>'CARRERA 10 CALLE 5',
            'latitudgeo'=>0,
            'longitudgeo'=>0,
            'contribuyente_especial'=>0,
            'estado'=>1,
            'cliente_prepago'=>0,
            'valor_prepago'=>null,
            'cliente_pospago'=>0,
            'valor_pospago'=>null,
            'auxiliar'=>0,
            'eliminado'=>0,
            'grupo_contable_id'=>1,
            'tipo_contribuyente_id'=>1,
            'ubicaciongeo_id'=>1,
            'forma_pago_id'=>1,
            'plazo_credito_id'=>1,
            'categoria_cliente_id'=>6,
            'retencion_iva_b_id'=>1,
            'retencion_iva_s_id'=>1,
            'retencion_ir_b_id'=>3,
            'retencion_ir_s_id'=>4,
            'estado_civil_id'=>1,
            'sexo_id'=>11,
            'origen_ingreso_id'=>9]);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                true
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testActualizarCliente() {
        $response = $this->post('/clientes/actualizarCliente', 
            ['id'=>1,
            'codigo'=>'CL101',
            'identificacion'=>'1768038860002',
            'razon_social'=>'CLIENTE E',
            'direccion'=>'CARRERA 10 CALLE 5',
            'latitudgeo'=>0,
            'longitudgeo'=>0,
            'contribuyente_especial'=>0,
            'estado'=>1,
            'cliente_prepago'=>0,
            'valor_prepago'=>null,
            'cliente_pospago'=>0,
            'valor_pospago'=>null,
            'auxiliar'=>0,
            'eliminado'=>0,
            'grupo_contable_id'=>1,
            'tipo_contribuyente_id'=>1,
            'ubicaciongeo_id'=>1,
            'forma_pago_id'=>1,
            'plazo_credito_id'=>1,
            'categoria_cliente_id'=>6,
            'retencion_iva_b_id'=>1,
            'retencion_iva_s_id'=>1,
            'retencion_ir_b_id'=>3,
            'retencion_ir_s_id'=>4,
            'estado_civil_id'=>1,
            'sexo_id'=>11,
            'origen_ingreso_id'=>9]);
        $response
                ->assertStatus(200)
                ->assertJson([
                    'resultado' => true
        ]);
    }
    
    /**
     * @test
     */
    public function testEliminarCliente() {
        $response = $this->post('/clientes/eliminarCliente', 
            ['id'=>1]);
        $response
                ->assertStatus(200)
                ->assertJson([
                    'resultado' => true
        ]);
    }
    
    public function tearDown() {
        exec('php artisan migrate:reset');
        exec('php artisan migrate');
        exec('php artisan db:seed');
        parent::tearDown();
    }
}
