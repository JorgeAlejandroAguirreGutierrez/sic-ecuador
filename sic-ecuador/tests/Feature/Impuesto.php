<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Impuesto extends TestCase
{
    public function testObtenerImpuestos(){
        $response = $this->post('/impuestos/obtenerImpuestos');
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                ['id'=>1, 'porcentaje'=>12],
                ['id'=>2, 'porcentaje'=>0]
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testObtenerImpuesto(){
        $response = $this->post('/impuestos/obtenerImpuesto', ['id'=>1]);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                'id'=>1, 'porcentaje'=>12
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testCrearImpuesto(){
        $response = $this->post('/impuestos/crearImpuesto', 
            ['codigo'=>'IVA3',
            'codigo_norma'=>'IVA_20%',
            'porcentaje'=>20]);
        $response
        ->assertStatus(200)
        ->assertJson([
            'resultado'=>[
                true
            ]
        ]);
    }
    
    /**
     * @test
     */
    public function testActualizarImpuesto() {
        $response = $this->post('/impuestos/actualizarImpuesto', 
            ['id'=>1,
            'codigo'=>'IVA4',
            'codigo_norma'=>'IVA_80%',
            'porcentaje'=>80]);
        $response
                ->assertStatus(200)
                ->assertJson([
                    'resultado' => true
        ]);
    }
    
    /**
     * @test
     */
    public function testEliminarImpuesto() {
        $response = $this->post('/impuestos/eliminarImpuesto', 
            ['id'=>2]);
        $response
                ->assertStatus(200)
                ->assertJson([
                    'resultado' => true
        ]);
    }
    
    public static function tearDownAfterClass() {
        exec('php artisan migrate:reset');
        exec('php artisan migrate');
        exec('php artisan db:seed');
    }
}
