<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearDatosAdicionales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //ABARCA TABLA: CATEGORIAS CLIENTES, FORMAS PAGOS, SEXOS, ESTADOS CIVILES, ORIGENES INGRESOS
        Schema::create('datos_adicionales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('tipo');
            $table->string('nombre');
            $table->string('abreviatura');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datos_adicionales');
    }
}
