<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearSesiones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sesiones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('estado');
            $table->dateTime('fecha_ape');
            $table->dateTime('fecha_cie')->nullable();
            $table->string('nombre_pc');
            $table->string('sesion_ip');
            $table->integer('usuario_id')->unsigned();
            $table->integer('punto_venta_id')->unsigned();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sesiones');
    }
}
