<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearRetencionesCompras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retenciones_compras', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('numero');
            $table->date('fecha');
            $table->string('estado');
            $table->string('autorizacion');
            $table->double('total');
            $table->integer('factura_compra_id')->unsigned(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retenciones_compras');
    }
}
