<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearProductos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('nombre');
            $table->integer('cantidad');
            $table->double('precio');
            $table->boolean('consignacion');
            $table->double('costo_compra');
            $table->double('costo_kardex');
            $table->integer('medida_id')->unsigned();
            $table->integer('tipo_id')->unsigned();
            $table->integer('impuesto_id')->unsigned();
            $table->integer('bodega_id')->unsigned();
            $table->timestamps();
            
//            $table->foreign('tipo_id')->references('id')->on('datos_adicionales')->onDelete('cascade');
//            $table->foreign('medida_id')->references('id')->on('datos_adicionales')->onDelete('cascade');
//            $table->foreign('impuesto_id')->references('id')->on('impuestos')->onDelete('cascade');
//            $table->foreign('bodega_id')->references('id')->on('bodegas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
