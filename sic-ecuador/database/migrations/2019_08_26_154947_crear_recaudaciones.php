<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearRecaudaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recaudaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->date('fecha');
            $table->double('total');
            $table->string('comentario');
            $table->double('efectivo');
            $table->double('total_cheques');
            $table->double('total_depositos_transferencias');
            $table->double('total_retenciones_compras');
            $table->integer('tarjeta_credito_id')->nullable()->unsigned();
            $table->integer('tarjeta_debito_id')->nullable()->unsigned();
            $table->integer('credito_id')->nullable()->unsigned();
            $table->integer('tipo_comprobante_id')->unsigned();
            $table->integer('comprobante_id')->unsigned();            
            $table->integer('sesion_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recaudaciones');
    }
}
