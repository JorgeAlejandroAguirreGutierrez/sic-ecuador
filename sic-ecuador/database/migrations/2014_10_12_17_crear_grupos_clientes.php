<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearGruposClientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupos_clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('denominacion');
            $table->string('numero_cuenta');
            $table->string('nombre_cuenta');
            $table->boolean('cliente_relacionado')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupos_clientes');
    }
}
