<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearComprobantesDetalles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comprobantes_detalles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('posicion');
            $table->boolean('entregado');
            $table->double('cantidad');
            $table->double('descuento_porcentaje');
            $table->double('descuento');
            $table->double('total');                     
            $table->integer('tipo_comprobante_id')->unsigned();
            $table->integer('comprobante_id')->unsigned();
            $table->integer('tipo_producto_id')->unsigned();
            $table->integer('producto_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comprobantes_detalles');
    }
}
