<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearOperadoresTarjetas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operadores_tarjetas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('tipo');
            $table->string('nombre');
            $table->string('abreviatura');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operadores_tarjetas');
    }
}
