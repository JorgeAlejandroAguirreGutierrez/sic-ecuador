<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearRetencionesVentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retenciones_ventas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('numero');
            $table->date('fecha');
            $table->string('agente');
            $table->string('autorizacion');
            $table->boolean('compensado');
            $table->double('base_imponible');
            $table->double('valor');            
            $table->integer('tipo_retencion_id')->unsigned();            
            $table->integer('recaudacion_id')->unsigned(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retenciones_ventas');
    }
}
