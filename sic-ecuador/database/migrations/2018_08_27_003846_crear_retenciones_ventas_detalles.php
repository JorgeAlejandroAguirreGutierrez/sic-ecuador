<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearRetencionesVentasDetalles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retenciones_ventas_detalles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('posicion')->unsigned();
            $table->double('base_imponible');
            $table->double('valor');
            $table->integer('retencion_venta_id')->unsigned();
            $table->integer('tipo_retencion_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retenciones_ventas_detalles');
    }
}
