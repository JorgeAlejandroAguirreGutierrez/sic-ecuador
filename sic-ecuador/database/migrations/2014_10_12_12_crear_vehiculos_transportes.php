<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearVehiculosTransportes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehiculos_transportes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('placa');
            $table->integer('numero')->unsigned()->nullable();
            $table->string('marca');
            $table->string('modelo');
            $table->integer('anio');
            $table->integer('cilindraje');
            $table->string('clase');
            $table->string('color');
            $table->string('fabricacion');
            $table->boolean('activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehiculos_transportes');
    }
}
