<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearDepositosTransferencias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('depositos_transferencias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('tipo_transaccion');
            $table->string('numero_transaccion');
            $table->date('fecha_transaccion');
            $table->double('valor');
            $table->integer('recaudacion_id')->unsigned();
            $table->integer('banco_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('depositos_transferencias');
    }
}
