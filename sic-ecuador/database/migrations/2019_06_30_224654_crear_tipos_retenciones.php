<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTiposRetenciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos_retenciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('impuesto_retencion');
            $table->string('tipo_retencion');
            $table->string('codigo_norma');
            $table->string('homologacion_f_e')->nullable();
            $table->string('descripcion');
            $table->integer('porcentaje');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipos_retenciones');
    }
}
