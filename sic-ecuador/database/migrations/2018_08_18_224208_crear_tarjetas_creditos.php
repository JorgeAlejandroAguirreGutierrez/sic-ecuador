<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTarjetasCreditos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tarjetas_creditos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->boolean('diferido');
            $table->boolean('titular');           
            $table->string('identificacion');
            $table->string('nombre_titular');
            $table->string('lote');
            $table->double('valor');
            $table->integer('tarjeta_id')->unsigned();
            $table->integer('operador_tarjeta_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tarjetas_creditos');
    }
}
