<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearEstablecimientos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('establecimientos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('direccion');
            $table->integer('empresa_id')->unsigned();
            $table->integer('ubicaciongeo_id')->unsigned();
            $table->timestamps();
            
//            $table->foreign('ubicaciongeo_id')->references('id')->on('ubicacionesgeo')->onDelete('cascade');
//            $table->foreign('empresa_id')->references('id')->on('empresas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('establecimientos');
    }
}
