<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearCreditos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creditos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->double('saldo', 10, 2);
            $table->integer('plazo');
            $table->string('periodicidad');
            $table->string('modelo_tabla');
            $table->double('interes_periodo', 10, 2);
            $table->double('interes_anual', 10, 2);
            $table->date('primera_cuota');
            $table->date('vencimiento');
            $table->double('cuota', 10, 2);
            $table->double('intereses', 10, 2);
            $table->double('total', 10, 2);
            $table->string('estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creditos');
    }
}
