<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('nombre');
            $table->string('correo');
            $table->string('contrasena');
            $table->string('identificacion');
            $table->string('avatar');
            $table->boolean('activo');
            $table->integer('empresa_id')->unsigned();
            $table->integer('perfil_id')->unsigned();
            $table->timestamps();
            
//            $table->foreign('tipo_id')->references('id')->on('datos_adicionales')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
