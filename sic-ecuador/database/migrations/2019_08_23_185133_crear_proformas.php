<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearProformas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proformas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo')->nullable();
            $table->string('numero_interno')->nullable();
            $table->date('fecha');
            $table->date('fecha_caducidad');
            $table->string('estado');
            $table->double('subtotal', 10, 2);
            $table->double('subdescuento', 10, 2);
            $table->double('base_iva', 10, 2);
            $table->double('base_0', 10, 2);            
            $table->double('importe_iva', 10, 2);
            $table->double('total', 10, 2);
            $table->double('descuento_porcentaje', 10, 2);
            $table->double('descuento', 10, 2);
            $table->string('comentario')->nullable();            
            $table->integer('cliente_id')->unsigned();
            $table->integer('vendedor_id')->unsigned();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proformas');
    }
}
