<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearCompensaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compensaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('comprobante');
            $table->date('fecha_comprobante');
            $table->string('origen');
            $table->string('motivo')->nullable();
            $table->date('fecha_vencimiento')->nullable();
            $table->double('valor_origen');
            $table->double('saldo_anterior');
            $table->double('valor_compensado')->nullable();
            $table->double('saldo');            
            $table->boolean('compensado'); 
            $table->integer('recaudacion_id')->unsigned()->nullable();
            $table->integer('tipo_comprobante_id')->unsigned();
            $table->integer('cliente_id')->unsigned();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compensaciones');
    }
}
