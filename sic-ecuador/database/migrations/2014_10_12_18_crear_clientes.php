<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearClientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('identificacion');
            $table->string('razon_social');
            $table->boolean('estado');
            $table->boolean('eliminado');
            $table->integer('empresa_id')->unsigned();
            $table->integer('grupo_cliente_id')->unsigned();
            $table->integer('tipo_contribuyente_id')->unsigned();
            $table->integer('direccion_id')->unsigned();
            $table->integer('financiamiento_id')->unsigned();
            $table->integer('genero_id')->unsigned();            
            $table->integer('estado_civil_id')->unsigned();
            $table->integer('categoria_cliente_id')->unsigned();
            $table->integer('origen_ingreso_id')->unsigned();
            $table->timestamps();
            
//            $table->foreign('grupo_contable_id')->references('id')->on('grupos_contables')->onDelete('cascade');
//            $table->foreign('tipo_contribuyente_id')->references('id')->on('tipos_contribuyentes')->onDelete('cascade');
//            $table->foreign('ubicaciongeo_id')->references('id')->on('ubicacionesgeo')->onDelete('cascade');
//            $table->foreign('forma_pago_id')->references('id')->on('datos_adicionales')->onDelete('cascade');
//            $table->foreign('plazo_credito_id')->references('id')->on('plazos_creditos')->onDelete('cascade');
//            $table->foreign('categoria_cliente_id')->references('id')->on('datos_adicionales')->onDelete('cascade');
//            $table->foreign('retencion_iva_b_id')->references('id')->on('retenciones')->onDelete('cascade');
//            $table->foreign('retencion_iva_s_id')->references('id')->on('retenciones')->onDelete('cascade');
//            $table->foreign('retencion_ir_b_id')->references('id')->on('retenciones')->onDelete('cascade');
//            $table->foreign('retencion_ir_s_id')->references('id')->on('retenciones')->onDelete('cascade');
//            $table->foreign('estado_civil_id')->references('id')->on('datos_adicionales')->onDelete('cascade');
//            $table->foreign('sexo_id')->references('id')->on('datos_adicionales')->onDelete('cascade');
//            $table->foreign('origen_ingreso_id')->references('id')->on('datos_adicionales')->onDelete('cascade');

        });
    }
    
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
