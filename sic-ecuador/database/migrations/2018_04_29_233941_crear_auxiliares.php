<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearAuxiliares extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auxiliares', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('razon_social');
            $table->boolean('estado');
            $table->boolean('eliminado');
            $table->integer('cliente_id')->unsigned();
            $table->integer('direccion_id')->unsigned();
            $table->timestamps();
            
//            $table->foreign('ubicaciongeo_id')->references('id')->on('ubicacionesgeo')->onDelete('cascade');
//            $table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auxiliares');
    }
}
