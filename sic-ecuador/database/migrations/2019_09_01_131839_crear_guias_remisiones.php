<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearGuiasRemisiones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guias_remisiones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('numero');
            $table->date('fecha');
            $table->boolean('direccion_cliente');
            $table->string('n_direccion')->nullable();
            $table->string('n_telefono')->nullable();
            $table->string('n_celular')->nullable();
            $table->string('n_correo')->nullable();
            $table->string('n_referencia')->nullable();
            $table->string('n_longitudgeo')->nullable();
            $table->string('n_latitudgeo')->nullable();
            $table->string('estado');
            $table->integer('n_ubicacion_id')->unsigned()->nullable();
            $table->integer('cliente_id')->unsigned();
            $table->integer('tipo_comprobante_id')->unsigned();
            $table->integer('comprobante_id')->unsigned();
            $table->integer('transportista_id')->unsigned();
            $table->integer('vehiculo_transporte_id')->unsigned();            
            $table->integer('sesion_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guias_remisiones');
    }
}
