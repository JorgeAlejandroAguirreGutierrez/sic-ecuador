<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearServicios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('nombre');
            $table->double('precio1');
            $table->double('precio2');
            $table->double('precio3');
            $table->double('precio4');
            $table->boolean('activo');
            $table->integer('grupo_contable_id')->unsigned();
            $table->integer('impuesto_id')->unsigned();
            $table->integer('tipo_id')->unsigned();
            $table->timestamps();
            
            
//            $table->foreign('grupo_contable_id')->references('id')->on('grupos_contables')->onDelete('cascade');
//            $table->foreign('impuesto_id')->references('id')->on('impuestos')->onDelete('cascade');
//            $table->foreign('tipo_id')->references('id')->on('datos_adicionales')->onDelete('cascade');
        });
    }
    
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios');
    }
}
