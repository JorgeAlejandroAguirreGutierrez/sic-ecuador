<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearCreditosAmortizaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creditos_amortizaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('secuencia')->unsigned();
            $table->date('vencimiento');
            $table->double('saldo_capital')->unsigned();
            $table->integer('dias')->unsigned();
            $table->double('capital')->unsigned();
            $table->double('interes')->unsigned();
            $table->double('dividendo')->unsigned();
            $table->double('seguro')->unsigned();
            $table->double('otro')->unsigned();
            $table->double('cuota')->unsigned();
            $table->double('mora')->unsigned()->nullable();
            $table->date('fecha_abono')->nullable();
            $table->double('abono')->unsigned()->nullable();
            $table->date('fecha_pago')->nullable();
            $table->double('pago')->unsigned()->nullable();
            $table->double('capital_reducido')->unsigned();
            $table->string('estado');            
            $table->integer('credito_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creditos_amortizaciones');
    }
}
