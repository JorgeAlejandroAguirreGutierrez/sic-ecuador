<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearFacturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('numero');
            $table->date('fecha');
            $table->string('estado');
            $table->double('subtotal');
            $table->double('subdescuento');
            $table->double('base_iva');
            $table->double('base_0');
            $table->double('importe_iva');
            $table->double('total');
            $table->double('descuento_porcentaje');
            $table->double('descuento');
            $table->string('comentario')->nullable();
            $table->integer('cliente_id')->unsigned();
            $table->integer('cliente_factura_id')->nullable()->unsigned();
            $table->integer('auxiliar_id')->nullable()->unsigned();
            $table->integer('sesion_id')->unsigned();
            $table->integer('vendedor_id')->unsigned();
            $table->timestamps();
            
//            $table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
    }
}
