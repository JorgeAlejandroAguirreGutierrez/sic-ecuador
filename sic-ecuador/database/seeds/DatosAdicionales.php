<?php

use Illuminate\Database\Seeder;

class DatosAdicionales extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('datos_adicionales')->insert(array(
            'codigo'=>'DA13',
            'tipo'=>'TIPOS USUARIOS',
            'nombre'=>'VENDEDOR',
            'abreviatura'=>'V'
        ));
        
        DB::table('datos_adicionales')->insert(array(
            'codigo'=>'DA14',
            'tipo'=>'TIPOS USUARIOS',
            'nombre'=>'CAJERO',
            'abreviatura'=>'C'
        ));
        
        DB::table('datos_adicionales')->insert(array(
            'codigo'=>'DA15',
            'tipo'=>'TIPOS MEDIDAS',
            'nombre'=>'KILOGRAMO',
            'abreviatura'=>'KG'
        ));
        
        DB::table('datos_adicionales')->insert(array(
            'codigo'=>'DA22',
            'tipo'=>'CHEQUE',
            'nombre'=>'A LA VISTA',
            'abreviatura'=>'CHV'
        ));
        
        DB::table('datos_adicionales')->insert(array(
            'codigo'=>'DA23',
            'tipo'=>'CHEQUE',
            'nombre'=>'POSFECHADO',
            'abreviatura'=>'CHP'
        ));

        DB::table('datos_adicionales')->insert(array(
            'codigo'=>'DA24',
            'tipo'=>'TRANSACCION',
            'nombre'=>'DEPOSITO',
            'abreviatura'=>'DP'
        ));
        
        DB::table('datos_adicionales')->insert(array(
            'codigo'=>'DA25',
            'tipo'=>'TRANSACCION',
            'nombre'=>'TRANSFERENCIA',
            'abreviatura'=>'TF'
        ));
        
        DB::table('datos_adicionales')->insert(array(
            'codigo'=>'DA26',
            'tipo'=>'TARJETA',
            'nombre'=>'CREDITO',
            'abreviatura'=>'TCR'
        ));
        
        DB::table('datos_adicionales')->insert(array(
            'codigo'=>'DA27',
            'tipo'=>'TARJETA',
            'nombre'=>'DEBITO',
            'abreviatura'=>'TDB'
        ));
        
        DB::table('datos_adicionales')->insert(array(
            'codigo'=>'DA28',
            'tipo'=>'PERIODICIDAD',
            'nombre'=>'MENSUAL',
            'abreviatura'=>'M'
        ));

        DB::table('datos_adicionales')->insert(array(
            'codigo'=>'DA29',
            'tipo'=>'PERIODICIDAD',
            'nombre'=>'QUINCENAL',
            'abreviatura'=>'Q'
        ));

        DB::table('datos_adicionales')->insert(array(
            'codigo'=>'DA30',
            'tipo'=>'PERIODICIDAD',
            'nombre'=>'DIARIO',
            'abreviatura'=>'D'
        ));

        DB::table('datos_adicionales')->insert(array(
            'codigo'=>'DA31',
            'tipo'=>'TIPO AMORTIZACION',
            'nombre'=>'ALEMANA',
            'abreviatura'=>'AL'
        ));
        
        DB::table('datos_adicionales')->insert(array(
            'codigo'=>'DA32',
            'tipo'=>'TIPO AMORTIZACION',
            'nombre'=>'FRANCESA',
            'abreviatura'=>'FR'
        ));

        DB::table('datos_adicionales')->insert(array(
            'codigo'=>'DA33',
            'tipo'=>'FORMA COBRO',
            'nombre'=>'CONTADO',
            'abreviatura'=>'C'
        ));
        
        DB::table('datos_adicionales')->insert(array(
            'codigo'=>'DA34',
            'tipo'=>'FORMA COBRO',
            'nombre'=>'DIFERIDO',
            'abreviatura'=>'D'
        ));
        
        DB::table('datos_adicionales')->insert(array(
            'codigo'=>'DA35',
            'tipo'=>'TIPO COMPROBANTE RETENCION',
            'nombre'=>'FACTURA',
            'abreviatura'=>'FAC'
        ));
        
        DB::table('datos_adicionales')->insert(array(
            'codigo'=>'DA36',
            'tipo'=>'TIPOS MEDIDAS',
            'nombre'=>'UNIDAD',
            'abreviatura'=>'U'
        ));
        
        DB::table('datos_adicionales')->insert(array(
            'codigo'=>'DA37',
            'tipo'=>'TIPOS MEDIDAS',
            'nombre'=>'METRO',
            'abreviatura'=>'M'
        ));
        DB::table('datos_adicionales')->insert(array(
            'codigo'=>'DA38',
            'tipo'=>'FORMA TIEMPO',
            'nombre'=>'DIA',
            'abreviatura'=>'D'
        ));
        DB::table('datos_adicionales')->insert(array(
            'codigo'=>'DA39',
            'tipo'=>'FORMA TIEMPO',
            'nombre'=>'MES',
            'abreviatura'=>'M'
        ));
    }
}
