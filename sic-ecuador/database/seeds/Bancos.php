<?php

use Illuminate\Database\Seeder;

class Bancos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('bancos')->insert(array(
            'codigo'=>'DA19',
            'tipo'=>'BANCO',
            'nombre'=>'BANCO PRODUBANCO',
            'abreviatura'=>'PB'
        ));
        
        DB::table('bancos')->insert(array(
            'codigo'=>'DA20',
            'tipo'=>'BANCO',
            'nombre'=>'BANCO PICHINCHA',
            'abreviatura'=>'BPI'
        ));
        
        DB::table('bancos')->insert(array(
            'codigo'=>'DA21',
            'tipo'=>'BANCO',
            'nombre'=>'BANCO PACIFICO',
            'abreviatura'=>'BP'
        ));
        
        DB::table('bancos')->insert(array(
            'codigo'=>'DA22',
            'tipo'=>'COOPERATIVA',
            'nombre'=>'COOPROGRESO',
            'abreviatura'=>'CP'
        ));         
    }
}
