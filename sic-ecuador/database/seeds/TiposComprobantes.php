<?php

use Illuminate\Database\Seeder;

class TiposComprobantes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('tipos_comprobantes')->insert(array(
            'codigo'=>'TCO000001',
            'descripcion'=>'FACTURA',
            'nombre_tabla'=>'facturas'
        ));
        
        DB::table('tipos_comprobantes')->insert(array(
            'codigo'=>'TCO000002',
            'descripcion'=>'EGRESO',
            'nombre_tabla'=>'egresos'
        ));

        DB::table('tipos_comprobantes')->insert(array(
            'codigo'=>'TCO000003',
            'descripcion'=>'PEDIDO',
            'nombre_tabla'=>'pedidos'
        ));

        DB::table('tipos_comprobantes')->insert(array(
            'codigo'=>'TCO000004',
            'descripcion'=>'PROFORMA',
            'nombre_tabla'=>'proformas'
        ));   

        DB::table('tipos_comprobantes')->insert(array(
            'codigo'=>'TCO000005',
            'descripcion'=>'NOTA DE CREDITO',
            'nombre_tabla'=>'notas_creditos'
        )); 

        DB::table('tipos_comprobantes')->insert(array(
            'codigo'=>'TCO000006',
            'descripcion'=>'FACTURA DE COMPRA',
            'nombre_tabla'=>'facturas_compras'
        )); 

        DB::table('tipos_comprobantes')->insert(array(
            'codigo'=>'TCO000007',
            'descripcion'=>'ANTICIPOS',
            'nombre_tabla'=>'recaudaciones'
        ));         

        DB::table('tipos_comprobantes')->insert(array(
            'codigo'=>'TCO000008',
            'descripcion'=>'RETENCIONES EN VENTAS',
            'nombre_tabla'=>'retenciones_ventas'
        )); 
        
        DB::table('tipos_comprobantes')->insert(array(
            'codigo'=>'TCO000009',
            'descripcion'=>'RETENCIONES EN COMPRAS',
            'nombre_tabla'=>'retenciones_compras'
        )); 
    }
}
