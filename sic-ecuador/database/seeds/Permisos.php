<?php

use Illuminate\Database\Seeder;

class Permisos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('permisos')->insert(array(
            'codigo'=>'PER1',
            'modulo'=>'CLIENTES',
            'operacion'=>'CREAR',
            'habilitado'=>true,
            'perfil_id'=>1
        ));  
        
        DB::table('permisos')->insert(array(
            'codigo'=>'PER2',
            'modulo'=>'CLIENTES',
            'operacion'=>'OBTENER',
            'habilitado'=>true,
            'perfil_id'=>1
        )); 
        
        DB::table('permisos')->insert(array(
            'codigo'=>'PER3',
            'modulo'=>'CLIENTES',
            'operacion'=>'ACTUALIZAR',
            'habilitado'=>true,
            'perfil_id'=>1
        )); 

        DB::table('permisos')->insert(array(
            'codigo'=>'PER4',
            'modulo'=>'CLIENTES',
            'operacion'=>'ELIMINAR',
            'habilitado'=>true,
            'perfil_id'=>1
        ));         

        DB::table('permisos')->insert(array(
            'codigo'=>'PER5',
            'modulo'=>'CLIENTES',
            'operacion'=>'CREAR',
            'habilitado'=>true,
            'perfil_id'=>2
        ));  
        
        DB::table('permisos')->insert(array(
            'codigo'=>'PER6',
            'modulo'=>'CLIENTES',
            'operacion'=>'OBTENER',
            'habilitado'=>true,
            'perfil_id'=>2
        )); 
        
        DB::table('permisos')->insert(array(
            'codigo'=>'PER7',
            'modulo'=>'CLIENTES',
            'operacion'=>'ACTUALIZAR',
            'habilitado'=>true,
            'perfil_id'=>2
        )); 

        DB::table('permisos')->insert(array(
            'codigo'=>'PER8',
            'modulo'=>'CLIENTES',
            'operacion'=>'ELIMINAR',
            'habilitado'=>true,
            'perfil_id'=>2
        ));            
    }
}
