<?php

use Illuminate\Database\Seeder;

class Establecimientos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('establecimientos')->insert(array(
            'codigo'=>'EST001',
            'direccion'=>'CALLE 10 CARRERA 15 #27',
            'empresa_id'=>1,
            'ubicaciongeo_id'=>1
        ));
        
        DB::table('establecimientos')->insert(array(
            'codigo'=>'EST002',
            'direccion'=>'CALLE 5 CARRERA 60 #50',
            'empresa_id'=>1,            
            'ubicaciongeo_id'=>2
        ));

        DB::table('establecimientos')->insert(array(
            'codigo'=>'EST003',
            'direccion'=>'CALLE 8 Y LARREA #27',
            'empresa_id'=>1,            
            'ubicaciongeo_id'=>3
        ));
        
        DB::table('establecimientos')->insert(array(
            'codigo'=>'EST004',
            'direccion'=>'CALLE 19 Y OLMEDO #50',
            'empresa_id'=>2,            
            'ubicaciongeo_id'=>2
        ));        
    }
}
