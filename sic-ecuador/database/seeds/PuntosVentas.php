<?php

use Illuminate\Database\Seeder;

class PuntosVentas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('puntos_ventas')->insert(array(
            'codigo'=>'EM1ES1CA001',
            'descripcion'=>'CAJA1',
            'establecimiento_id'=>1,
        ));
        
        DB::table('puntos_ventas')->insert(array(
            'codigo'=>'EM1ES1CA002',
            'descripcion'=>'CAJA2',
            'establecimiento_id'=>1,
        ));
        
        DB::table('puntos_ventas')->insert(array(
            'codigo'=>'EM1ES2CA001',
            'descripcion'=>'CAJA1',
            'establecimiento_id'=>2,
        ));        
    }
}
