<?php

use Illuminate\Database\Seeder;

class ComprobantesDetalles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('comprobantes_detalles')->insert(array(
            'posicion'=>1,
            'entregado'=>1,
            'cantidad'=>3.5,
            'descuento_porcentaje'=>1.1,
            'descuento'=>1.07,
            'total'=>50.00,
            'tipo_comprobante_id'=>1,
            'comprobante_id'=>1,
            'tipo_producto_id'=>2,
            'producto_id'=>3
        ));        
        
        DB::table('comprobantes_detalles')->insert(array(
            'posicion'=>2,
            'entregado'=>1,
            'cantidad'=>1.0,
            'descuento_porcentaje'=>3.1,
            'descuento'=>2.00,
            'total'=>51.20,
            'tipo_comprobante_id'=>1,
            'comprobante_id'=>1,
            'tipo_producto_id'=>2,
            'producto_id'=>2
        ));
        
        DB::table('comprobantes_detalles')->insert(array(
            'posicion'=>1,
            'entregado'=>1,
            'cantidad'=>1.5,
            'descuento_porcentaje'=>1.1,
            'descuento'=>1.00,
            'total'=>100.00,
            'tipo_comprobante_id'=>1,
            'comprobante_id'=>2,
            'tipo_producto_id'=>2,
            'producto_id'=>3
        ));        
        
        DB::table('comprobantes_detalles')->insert(array(
            'posicion'=>2,
            'entregado'=>1,
            'cantidad'=>2.0,
            'descuento_porcentaje'=>2.1,
            'descuento'=>0.43,
            'total'=>78.57,
            'tipo_comprobante_id'=>1,
            'comprobante_id'=>2,
            'tipo_producto_id'=>2,
            'producto_id'=>2
        ));          
    }
}
