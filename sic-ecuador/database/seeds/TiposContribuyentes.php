<?php

use Illuminate\Database\Seeder;

class TiposContribuyentes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('tipos_contribuyentes')->insert(array(
            'codigo'=>'TC1',
            'tipo'=>'PERSONA NATURAL',
            'subtipo'=>'NO OBLIGADO A CONTABILIDAD',
            'especial'=>0
        ));
        
        DB::table('tipos_contribuyentes')->insert(array(
            'codigo'=>'TC2',
            'tipo'=>'PERSONA NATURAL',
            'subtipo'=>'OBLIGADO A CONTABILIDAD',
            'especial'=>1
        ));
        
        
        DB::table('tipos_contribuyentes')->insert(array(
            'codigo'=>'TC3',
            'tipo'=>'PERSONA JURIDICA',
            'subtipo'=>'PUBLICA',
            'especial'=>1
        ));
        
        DB::table('tipos_contribuyentes')->insert(array(
            'codigo'=>'TC4',
            'tipo'=>'PERSONA JURIDICA',
            'subtipo'=>'PRIVADA',
            'especial'=>1
        ));
    }
}
