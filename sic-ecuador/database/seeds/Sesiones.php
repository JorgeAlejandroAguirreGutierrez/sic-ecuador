<?php

use Illuminate\Database\Seeder;

class Sesiones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('sesiones')->insert(array(
            'codigo'=>'SES1',
            'estado'=>'CERRADA',
            'fecha_ape'=>'2019-08-16 08:01:01',
            'fecha_cie'=>'2019-08-16 08:05:01',            
            'nombre_pc'=>'JUANPC',
            'sesion_ip'=>'200.107.1.1',
            'usuario_id'=>2,
            'punto_venta_id'=>1
        ));  

        DB::table('sesiones')->insert(array(
            'codigo'=>'SES2',
            'estado'=>'ABIERTA',
            'fecha_ape'=>'2019-08-16 08:10:01',
            //'fecha_cie'=>'2019-08-16 08:10:01',   // Poner vacio          
            'nombre_pc'=>'JUANPC',
            'sesion_ip'=>'200.107.1.1',
            'usuario_id'=>2,
            'punto_venta_id'=>1
        ));  

        DB::table('sesiones')->insert(array(
            'codigo'=>'SES3',
            'estado'=>'ABIERTA',
            'fecha_ape'=>'2019-08-16 08:01:01',
            //'fecha_cie'=>'2019-08-16 08:01:01',  // Poner vacio         
            'nombre_pc'=>'JORGEPC',
            'sesion_ip'=>'200.107.1.2',
            'usuario_id'=>4,
            'punto_venta_id'=>1
        ));          
    }
}
