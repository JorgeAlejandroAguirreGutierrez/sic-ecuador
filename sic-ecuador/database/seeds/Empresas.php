<?php

use Illuminate\Database\Seeder;

class Empresas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('empresas')->insert(array(
            'codigo'=>'001',
            'razon_social'=>'SIC ECUADOR',
            'identificacion'=>'0660001840001',
            'logo'=>''
        ));
    }
}
