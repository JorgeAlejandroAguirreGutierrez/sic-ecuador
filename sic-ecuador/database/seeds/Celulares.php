<?php

use Illuminate\Database\Seeder;

class Celulares extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('celulares')->insert(array(
            'codigo'=>'C1',
            'numero'=>'0987654321',
            'cliente_id'=>1
        ));
        DB::table('celulares')->insert(array(
            'codigo'=>'C2',
            'numero'=>'0981234567',
            'cliente_id'=>1
        ));
        DB::table('celulares')->insert(array(
            'codigo'=>'C3',
            'numero'=>'0965431234',
            'cliente_id'=>2
        ));        
    }
}
