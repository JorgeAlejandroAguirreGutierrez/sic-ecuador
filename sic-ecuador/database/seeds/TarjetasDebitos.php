<?php

use Illuminate\Database\Seeder;

class TarjetasDebitos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('tarjetas_debitos')->insert(array(
            'codigo'=>'RTD000001',
            'identificacion'=>'0502685966',
            'nombre_titular'=>'JORGE AGUIRRE',
            'lote'=>'12314566',
            'valor'=>20.00,
            'tarjeta_id'=>1,
            'operador_tarjeta_id'=>1
        ));
        
        DB::table('tarjetas_debitos')->insert(array(
            'codigo'=>'RTD000002',
            'identificacion'=>'0502685966',
            'nombre_titular'=>'JORGE AGUIRRE',
            'lote'=>'12314566',
            'valor'=>30.00,
            'tarjeta_id'=>2,
            'operador_tarjeta_id'=>1
        ));
        
        DB::table('tarjetas_debitos')->insert(array(
            'codigo'=>'RTD000003',
            'identificacion'=>'0601234567',
            'nombre_titular'=>'MARIO DELGADO',
            'lote'=>'12314566',
            'valor'=>40.00,
            'tarjeta_id'=>1,
            'operador_tarjeta_id'=>2
        ));
        
        DB::table('tarjetas_debitos')->insert(array(
            'codigo'=>'RTD000004',
            'identificacion'=>'0601234567',
            'nombre_titular'=>'MARIO DELGADO',
            'lote'=>'12314566',
            'valor'=>50.00,
            'tarjeta_id'=>2,
            'operador_tarjeta_id'=>2
        ));         
    }
}
