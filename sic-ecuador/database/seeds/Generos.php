<?php

use Illuminate\Database\Seeder;

class Generos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('generos')->insert(array(
            'codigo'=>'GEN1',
            'sexo'=>'MASCULINO',
            'abreviatura'=>'M',
        ));    

        DB::table('generos')->insert(array(
            'codigo'=>'GEN2',
            'sexo'=>'FEMENINO',
            'abreviatura'=>'F',
        ));         
    }
}
