<?php

use Illuminate\Database\Seeder;

class Pedidos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('pedidos')->insert(array(
            'codigo'=>'PED000001',
            'numero_interno'=>'001-001-000001',
            'fecha'=>'2019-08-23',
            'fecha_entrega'=>'2019-08-24',
            'estado'=>'EMITIDA',
            'subtotal'=>91.43,
            'subdescuento'=>3.07,
            'base_iva'=>71.43,
            'base_0'=>20.00,
            'importe_iva'=>8.57,
            'total'=>100.00,
            'descuento_porcentaje'=>1.1857,
            'descuento'=>1.20,
            'comentario'=>'Pedido de mercaderia, falta despachar',
            'cliente_id'=>1,
            'vendedor_id'=>3
        ));

        DB::table('pedidos')->insert(array(
            'codigo'=>'PED000002',
            'numero_interno'=>'001-001-000002',
            'fecha'=>'2019-08-23',
            'fecha_entrega'=>'2019-08-25',
            'estado'=>'EMITIDA',
            'subtotal'=>178.57,
            'subdescuento'=>1.43,
            'base_iva'=>178.57,
            'base_0'=>0.00,
            'importe_iva'=>21.43,
            'total'=>200.00,
            'descuento_porcentaje'=>1.1,
            'descuento'=>2.20,
            'comentario'=>'Pedido de mercaderia, falta despachar',
            'cliente_id'=>2,
            'vendedor_id'=>2
        ));          
    }
}
