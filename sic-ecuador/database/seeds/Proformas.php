<?php

use Illuminate\Database\Seeder;

class Proformas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('proformas')->insert(array(
            'codigo'=>'PRO000001',
            'numero_interno'=>'001-001-000001',
            'fecha'=>'2019-08-23',
            'fecha_caducidad'=>'2019-09-23',
            'estado'=>'VIGENTE',
            'subtotal'=>91.43,
            'subdescuento'=>3.07,
            'base_iva'=>71.43,
            'base_0'=>20.00,
            'importe_iva'=>8.57,
            'total'=>100.00,
            'descuento_porcentaje'=>1.1857,
            'descuento'=>1.20,
            'comentario'=>'Proforma Vigente',
            'cliente_id'=>1,
            'vendedor_id'=>3
        ));

        DB::table('proformas')->insert(array(
            'codigo'=>'PRO000002',
            'numero_interno'=>'001-001-000002',
            'fecha'=>'2019-07-22',
            'fecha_caducidad'=>'2019-08-22',
            'estado'=>'CADUCADA',
            'subtotal'=>178.57,
            'subdescuento'=>1.43,
            'base_iva'=>178.57,
            'base_0'=>0.00,
            'importe_iva'=>21.43,
            'total'=>200.00,
            'descuento_porcentaje'=>1.1,
            'descuento'=>2.20,
            'comentario'=>'Proforma Caducada',
            'cliente_id'=>2,
            'vendedor_id'=>2
        ));          
    }
}
