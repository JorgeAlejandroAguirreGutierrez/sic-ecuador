<?php

use Illuminate\Database\Seeder;

class TiposProductos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('tipos_productos')->insert(array(
            'codigo'=>'TPR000001',
            'descripcion'=>'MERCADERIA',
            'nombre_tabla'=>'mercaderias'
        ));
        
        DB::table('tipos_productos')->insert(array(
            'codigo'=>'TPR000002',
            'descripcion'=>'SERVICIO',
            'nombre_tabla'=>'servicios'
        ));

        DB::table('tipos_productos')->insert(array(
            'codigo'=>'TPR000003',
            'descripcion'=>'ACTIVO FIJO',
            'nombre_tabla'=>'activos_fijos'
        ));
    }
}
