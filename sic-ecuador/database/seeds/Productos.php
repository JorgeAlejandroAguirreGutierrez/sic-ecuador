<?php

use Illuminate\Database\Seeder;

class Productos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('productos')->insert(array(
            'codigo'=>'P1',
            'nombre'=>'ARROZ',
            'consignacion'=>1,
            'cantidad'=>5,
            'precio'=>10.35,
            'costo_compra'=>8.45,
            'costo_kardex'=>9.45,
            'medida_id'=>15,
            'tipo_id'=>16,
            'impuesto_id'=>1,
            'bodega_id'=>1
        ));
        DB::table('productos')->insert(array(
            'codigo'=>'P2',
            'nombre'=>'HUEVOS',
            'consignacion'=>1,
            'cantidad'=>5,
            'precio'=>5.28,
            'costo_compra'=>4.18,
            'costo_kardex'=>3.45,
            'medida_id'=>15,
            'tipo_id'=>16,
            'impuesto_id'=>1,
            'bodega_id'=>1
        ));
        DB::table('productos')->insert(array(
            'codigo'=>'P3',
            'nombre'=>'GALLETAS',
            'consignacion'=>1,
            'cantidad'=>10,
            'precio'=>20.12,
            'costo_compra'=>18.12,
            'costo_kardex'=>17.45,
            'medida_id'=>15,
            'tipo_id'=>16,
            'impuesto_id'=>2,
            'bodega_id'=>2
        ));
        DB::table('productos')->insert(array(
            'codigo'=>'P4',
            'nombre'=>'CAFE',
            'consignacion'=>1,
            'cantidad'=>20,
            'precio'=>3.45,
            'costo_compra'=>1.12,
            'costo_kardex'=>1.01,
            'medida_id'=>15,
            'tipo_id'=>16,
            'impuesto_id'=>2,
            'bodega_id'=>3
        ));
        
        DB::table('productos')->insert(array(
            'codigo'=>'P5',
            'nombre'=>'FRIJOLES',
            'consignacion'=>1,
            'cantidad'=>10,
            'precio'=>5.35,
            'costo_compra'=>4.45,
            'costo_kardex'=>3.45,
            'medida_id'=>15,
            'tipo_id'=>16,
            'impuesto_id'=>2,
            'bodega_id'=>1
        ));
    }
}
