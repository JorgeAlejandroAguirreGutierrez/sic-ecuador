<?php

use Illuminate\Database\Seeder;

class Clientes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('clientes')->insert(array(
            'codigo'=>'CL1',
            'identificacion'=>'1768038860001',
            'razon_social'=>'CLIENTE A',
            'estado'=>1,
            'eliminado'=>0,
            'empresa_id'=>1,
            'grupo_cliente_id'=>1,
            'tipo_contribuyente_id'=>1,
            'direccion_id'=>1,
            'financiamiento_id'=>1,
            'genero_id'=>2,
            'estado_civil_id'=>2,
            'categoria_cliente_id'=>6,
            'origen_ingreso_id'=>9,
        ));
        
        DB::table('clientes')->insert(array(
            'codigo'=>'CL2',
            'identificacion'=>'1160000240001',
            'razon_social'=>'CLIENTE B',
            'estado'=>1,
            'eliminado'=>0,
            'empresa_id'=>1,
            'grupo_cliente_id'=>2,
            'tipo_contribuyente_id'=>2,
            'direccion_id'=>2,
            'financiamiento_id'=>1,
            'genero_id'=>1,
            'estado_civil_id'=>1,
            'categoria_cliente_id'=>6,
            'origen_ingreso_id'=>9,
        ));
        
        DB::table('clientes')->insert(array(
            'codigo'=>'CL3',
            'identificacion'=>'9999999999999',
            'razon_social'=>'CLIENTE CF',
            'estado'=>1,
            'eliminado'=>0,
            'empresa_id'=>1,
            'grupo_cliente_id'=>1,
            'tipo_contribuyente_id'=>2,
            'direccion_id'=>3,
            'financiamiento_id'=>2,
            'genero_id'=>2,
            'estado_civil_id'=>1,
            'categoria_cliente_id'=>6,
            'origen_ingreso_id'=>9,
        ));
    }
}
