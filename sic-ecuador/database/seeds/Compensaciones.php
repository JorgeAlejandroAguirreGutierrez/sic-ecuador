<?php

use Illuminate\Database\Seeder;

class Compensaciones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('compensaciones')->insert(array(
            'codigo'=>'CMP000001',
            'comprobante'=>'001-001-0000001',
            'fecha_comprobante'=>'2019-08-28',
            'origen'=>'001-001-0000234',
            'motivo'=>'DESCUENTO',
            //'fecha_vencimiento'=>'2019-08-28',
            'valor_origen'=>7.04,
            'saldo_anterior'=>7.04,
            'valor_compensado'=>7.04,
            'saldo'=>0.00,
            'compensado'=>TRUE,
            'recaudacion_id'=>1,
            'tipo_comprobante_id'=>5,
            'cliente_id'=>1
        ));

        DB::table('compensaciones')->insert(array(
            'codigo'=>'CMP000002',
            'comprobante'=>'001-001-0000123',
            'fecha_comprobante'=>'2019-08-28',
            'origen'=>'CREDITO',
            //'motivo'=>'DESCUENTO',
            'fecha_vencimiento'=>'2019-09-30',
            'valor_origen'=>9.04,
            'saldo_anterior'=>9.04,
            'valor_compensado'=>9.04,
            'saldo'=>0.00,
            'compensado'=>TRUE,
            'recaudacion_id'=>1,
            'tipo_comprobante_id'=>6,
            'cliente_id'=>1
        ));

        DB::table('compensaciones')->insert(array(
            'codigo'=>'CMP000003',
            'comprobante'=>'REC0000001',
            'fecha_comprobante'=>'2019-07-28',
            'origen'=>'PEDIDO',
            'motivo'=>'PREPAGO',
            //'fecha_vencimiento'=>'2019-08-28',
            'valor_origen'=>50.00,
            'saldo_anterior'=>50.00,
            'valor_compensado'=>50.00,
            'saldo'=>50.00,
            'compensado'=>FALSE,
            //'recaudacion_id'=>1,
            'tipo_comprobante_id'=>7,
            'cliente_id'=>1
        ));    
        
        DB::table('compensaciones')->insert(array(
            'codigo'=>'CMP000004',
            'comprobante'=>'001-001-0000001',
            'fecha_comprobante'=>'2019-08-28',
            'origen'=>'001-001-0000123',
            'motivo'=>'ENTREGA POSTERIOR',
            //'fecha_vencimiento'=>'2019-08-28',
            'valor_origen'=>60.00,
            'saldo_anterior'=>60.00,
            'valor_compensado'=>20.00,
            'saldo'=>40.00,
            'compensado'=>TRUE,
            'recaudacion_id'=>1,
            'tipo_comprobante_id'=>8,
            'cliente_id'=>1
        ));    
        
        DB::table('compensaciones')->insert(array(
            'codigo'=>'CMP000005',
            'comprobante'=>'001-001-0000001',
            'fecha_comprobante'=>'2019-08-28',
            'origen'=>'001-001-0000123',
            'motivo'=>'ENTREGA POSTERIOR',
            //'fecha_vencimiento'=>'2019-08-28',
            'valor_origen'=>60.00,
            'saldo_anterior'=>40.00,
            'valor_compensado'=>20.00,
            'saldo'=>20.00,
            'compensado'=>TRUE,
            'recaudacion_id'=>2,
            'tipo_comprobante_id'=>8,
            'cliente_id'=>1
        ));   

        DB::table('compensaciones')->insert(array(
            'codigo'=>'CMP000006',
            'comprobante'=>'001-001-0000001',
            'fecha_comprobante'=>'2019-08-28',
            'origen'=>'001-001-0000123',
            'motivo'=>'ENTREGA POSTERIOR',
            //'fecha_vencimiento'=>'2019-08-28',
            'valor_origen'=>60.00,
            'saldo_anterior'=>20.00,
            //'valor_compensado'=>0.00,
            'saldo'=>20.00,
            'compensado'=>FALSE,
            //'recaudacion_id'=>1,
            'tipo_comprobante_id'=>8,
            'cliente_id'=>1
        ));           
    }
}
 