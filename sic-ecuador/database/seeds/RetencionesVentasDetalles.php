<?php

use Illuminate\Database\Seeder;

class RetencionesVentasDetalles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('retenciones_ventas_detalles')->insert(array(
            'posicion'=>1,
            'base_imponible'=>10.00,
            'valor'=>7.00,
            'retencion_venta_id'=>1,
            'tipo_retencion_id'=>1
        )); 
        
        DB::table('retenciones_ventas_detalles')->insert(array(
            'posicion'=>2,
            'base_imponible'=>100.00,
            'valor'=>2.00,
            'retencion_venta_id'=>1,
            'tipo_retencion_id'=>4
        ));
        
        DB::table('retenciones_ventas_detalles')->insert(array(
            'posicion'=>1,
            'base_imponible'=>10.00,
            'valor'=>7.00,
            'retencion_venta_id'=>2,
            'tipo_retencion_id'=>2
        )); 
        
        DB::table('retenciones_ventas_detalles')->insert(array(
            'posicion'=>2,
            'base_imponible'=>100.00,
            'valor'=>2.00,
            'retencion_venta_id'=>2,
            'tipo_retencion_id'=>3
        ));                
    }
}
