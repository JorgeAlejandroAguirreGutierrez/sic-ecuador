<?php

use Illuminate\Database\Seeder;

class Telefonos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('telefonos')->insert(array(
            'codigo'=>'T1',
            'numero'=>'032964123',
            'cliente_id'=>1
        ));
        DB::table('telefonos')->insert(array(
            'codigo'=>'T2',
            'numero'=>'032424344',
            'cliente_id'=>2
        ));
        DB::table('telefonos')->insert(array(
            'codigo'=>'T3',
            'numero'=>'023987100',
            'cliente_id'=>1
        ));
    }
}
