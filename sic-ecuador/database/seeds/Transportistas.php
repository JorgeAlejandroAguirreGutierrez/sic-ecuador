<?php

use Illuminate\Database\Seeder;

class Transportistas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('transportistas')->insert(array(
            'codigo'=>'T1',
            'nombre'=>'JUAN PEREZ',
            'identificacion'=>'130375361',
            'vehiculo_propio'=>TRUE
        ));
        
        DB::table('transportistas')->insert(array(
            'codigo'=>'T2',
            'nombre'=>'JOSE LOPEZ',
            'identificacion'=>'010096765',
            'vehiculo_propio'=>FALSE            
        ));
        
        DB::table('transportistas')->insert(array(
            'codigo'=>'T3',
            'nombre'=>'VICTOR AGUILERA',
            'identificacion'=>'091738528',
            'vehiculo_propio'=>FALSE            
        ));
        
    }
}
