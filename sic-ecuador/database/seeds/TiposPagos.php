<?php

use Illuminate\Database\Seeder;

class TiposPagos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('tipos_pagos')->insert(array(
            'codigo'=>'TP1',
            'descripcion'=>'PREPAGO',
            'abreviatura'=>'PRE',
        )); 

        DB::table('tipos_pagos')->insert(array(
            'codigo'=>'TP2',
            'descripcion'=>'POSTPAGO',
            'abreviatura'=>'POST',
        ));         
    }
}
