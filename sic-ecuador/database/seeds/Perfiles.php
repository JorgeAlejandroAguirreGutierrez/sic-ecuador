<?php

use Illuminate\Database\Seeder;

class Perfiles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('perfiles')->insert(array(
            'codigo'=>'PE1',
            'descripcion'=>'ADMINISTRADOR',
            'abreviatura'=>'ADM',
        )); 

        DB::table('perfiles')->insert(array(
            'codigo'=>'PE2',
            'descripcion'=>'RECAUDADOR',
            'abreviatura'=>'REC',
        )); 

        DB::table('perfiles')->insert(array(
            'codigo'=>'PE3',
            'descripcion'=>'DESPACHADOR',
            'abreviatura'=>'DES',
        )); 

        DB::table('perfiles')->insert(array(
            'codigo'=>'PE4',
            'descripcion'=>'CONTADOR',
            'abreviatura'=>'CTD',
        )); 
    }
}
