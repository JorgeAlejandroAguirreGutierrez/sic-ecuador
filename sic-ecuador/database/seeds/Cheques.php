<?php

use Illuminate\Database\Seeder;

class Cheques extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('cheques')->insert(array(
            'codigo'=>'CHE000001',
            'numero'=>'123-345-00000123',
            'tipo_cheque'=>'A LA VISTA',
            'fecha_cheque'=>'2019-08-23',
            'fecha_efectivizacion'=>'2019-08-30',
            'valor'=>50.00,
            'recaudacion_id'=>1,
            'banco_id'=>2
        ));
        
        DB::table('cheques')->insert(array(
            'codigo'=>'CHE000002',
            'numero'=>'123-345-00000002',
            'tipo_cheque'=>'POSFECHADO',
            'fecha_cheque'=>'2019-08-24',
            'fecha_efectivizacion'=>'2019-09-30',
            'valor'=>10.00,
            'recaudacion_id'=>1,            
            'banco_id'=>7
        ));

        DB::table('cheques')->insert(array(
            'codigo'=>'CHE000003',
            'numero'=>'123-345-00000003',            
            'tipo_cheque'=>'A LA VISTA',
            'fecha_cheque'=>'2019-08-25',
            'fecha_efectivizacion'=>'2019-08-28',
            'valor'=>60.00,
            'recaudacion_id'=>2,            
            'banco_id'=>3
        ));

        DB::table('cheques')->insert(array(
            'codigo'=>'CHE000004',
            'numero'=>'123-888-000000004',            
            'tipo_cheque'=>'POSFECHADO',
            'fecha_cheque'=>'2019-08-26',
            'fecha_efectivizacion'=>'2019-09-15',
            'valor'=>20.00,
            'recaudacion_id'=>2,            
            'banco_id'=>2
        ));        
    }
}
