<?php

use Illuminate\Database\Seeder;

class Financiamientos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('financiamientos')->insert(array(
            'codigo'=>'FI1',
            'monto'=>100.20,
            'tipo_pago_id'=>1,
            'forma_pago_id'=>1,
            'plazo_credito_id'=>1,
        ));  
        
        DB::table('financiamientos')->insert(array(
            'codigo'=>'FI2',
            'monto'=>80.20,
            'tipo_pago_id'=>2,
            'forma_pago_id'=>2,
            'plazo_credito_id'=>2,
        ));     
        
        DB::table('financiamientos')->insert(array(
            'codigo'=>'FI3',
            'monto'=>90.20,
            'tipo_pago_id'=>3,
            'forma_pago_id'=>3,
            'plazo_credito_id'=>3,
        ));   
        
    }
}
