<?php

use Illuminate\Database\Seeder;

class Correos extends Seeder
{
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('correos')->insert(array(
            'codigo'=>'M1',
            'email'=>'je.hidalgob@hotmail.com',
            'cliente_id'=>1
        ));
        DB::table('correos')->insert(array(
            'codigo'=>'M2',
            'email'=>'alejoved@gmail.com',
            'cliente_id'=>2
        ));
        DB::table('correos')->insert(array(
            'codigo'=>'M3',
            'email'=>'gatosohidalgo@hotmail.com',
            'cliente_id'=>3
        ));          
    }
}
