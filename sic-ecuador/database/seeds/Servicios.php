<?php

use Illuminate\Database\Seeder;

class Servicios extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('servicios')->insert(array(
            'codigo'=>'S1',
            'nombre'=>'CARGA PANELA',
            'precio1'=>23.98,
            'precio2'=>15.78,
            'precio3'=>3.45,
            'precio4'=>1.12,
            'activo'=>1,
            'grupo_contable_id'=>5,
            'impuesto_id'=>1,
            'tipo_id'=>17
        ));
        
        DB::table('servicios')->insert(array(
            'codigo'=>'S2',
            'nombre'=>'CARGA LENTEJA',
            'precio1'=>40.98,
            'precio2'=>35.78,
            'precio3'=>39.45,
            'precio4'=>38.12,
            'activo'=>1,
            'grupo_contable_id'=>5,
            'impuesto_id'=>1,
            'tipo_id'=>17
        ));
        
        DB::table('servicios')->insert(array(
            'codigo'=>'S3',
            'nombre'=>'LIMPIEZA',
            'precio1'=>20.98,
            'precio2'=>13.67,
            'precio3'=>10.45,
            'precio4'=>20.14,
            'activo'=>1,
            'grupo_contable_id'=>5,
            'impuesto_id'=>1,
            'tipo_id'=>17
        ));
        DB::table('servicios')->insert(array(
            'codigo'=>'S4',
            'nombre'=>'EMPAQUE',
            'precio1'=>22.12,
            'precio2'=>18.98,
            'precio3'=>20.01,
            'precio4'=>21.12,
            'activo'=>1,
            'grupo_contable_id'=>5,
            'impuesto_id'=>1,
            'tipo_id'=>17
        ));
    }
}
