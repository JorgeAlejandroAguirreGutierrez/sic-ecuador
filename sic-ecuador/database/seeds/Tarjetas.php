<?php

use Illuminate\Database\Seeder;

class Tarjetas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('tarjetas')->insert(array(
            'codigo'=>'TAR000001',
            'tipo'=>'CREDITO',
            'nombre'=>'VISA',
            'banco_id'=>1
        ));
        
        DB::table('tarjetas')->insert(array(
            'codigo'=>'TAR000002',
            'tipo'=>'DEBITO',
            'nombre'=>'VISA',
            'banco_id'=>1
        ));  
        
        DB::table('tarjetas')->insert(array(
            'codigo'=>'TAR000003',
            'tipo'=>'CREDITO',
            'nombre'=>'MASTERCARD',
            'banco_id'=>2
        ));
        
        DB::table('tarjetas')->insert(array(
            'codigo'=>'TAR000004',
            'tipo'=>'DEBITO',
            'nombre'=>'MASTERCARD',
            'banco_id'=>2
        )); 
        
        DB::table('tarjetas')->insert(array(
            'codigo'=>'TAR000005',
            'tipo'=>'CREDITO',
            'nombre'=>'DINERS CLUB',
            'banco_id'=>3
        ));
        
        DB::table('tarjetas')->insert(array(
            'codigo'=>'TAR000006',
            'tipo'=>'DEBITO',
            'nombre'=>'VISA',
            'banco_id'=>4
        ));
    }
}
