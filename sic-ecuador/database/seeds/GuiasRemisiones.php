<?php

use Illuminate\Database\Seeder;

class GuiasRemisiones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('guias_remisiones')->insert(array(
            'codigo'=>'GUI000001',
            'numero'=>'001-001-0000001',
            'fecha'=>'2019-09-01',
            'direccion_cliente'=>FALSE,
            'n_direccion'=>'CALLE 1 Y LA 2',
            'n_telefono'=>'032967890',
            'n_celular'=>'0987654321',
            'n_correo'=>'pepito@hotmail.com',
            'n_referencia'=>'FRENTE A LA CASA DE DOÑA BERTHA',
            'n_latitudgeo'=>'40.75793',
            'n_longitudgeo'=>'-73.98552',
            'estado'=>'ENTREGADO',
            'n_ubicacion_id'=>2,
            'cliente_id'=>2,
            'tipo_comprobante_id'=>1,
            'comprobante_id'=>1,
            'transportista_id'=>1,
            'vehiculo_transporte_id'=>2,
            'sesion_id'=>1
        ));
        
        DB::table('guias_remisiones')->insert(array(
            'codigo'=>'GUI000002',
            'numero'=>'001-001-0000002',
            'fecha'=>'2019-09-01',
            'direccion_cliente'=>TRUE,
            //'n_direccion'=>'CALLE 1 Y LA 2',
            //'n_telefono'=>'032967890',
            //'n_celular'=>'0987654321',
            //'n_correo'=>'pepito@hotmail.com',
            //'n_referencia'=>'FRENTE A LA CASA DE DOÑA BERTHA',
            //'n_latitudgeo'=>'40.75793',
            //'n_longitudgeo'=>'-73.98552',
            'estado'=>'PENDIENTE',
            //'n_ubicacion_id'=>2,
            'cliente_id'=>1,
            'tipo_comprobante_id'=>2,
            'comprobante_id'=>1,
            'transportista_id'=>2,
            'vehiculo_transporte_id'=>1,
            'sesion_id'=>1
        ));        
    }
}
