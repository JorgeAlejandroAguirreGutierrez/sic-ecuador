<?php

use Illuminate\Database\Seeder;

class Ubicaciones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('ubicaciones')->insert(array(
            'codigo'=>'U1',
            'codigo_norma'=>'1',
            'provincia'=>'AZUAY',
            'canton'=>'CUENCA',
            'parroquia'=>'BELLAVISTA'
        ));
        
        DB::table('ubicaciones')->insert(array(
            'codigo'=>'U2',
            'codigo_norma'=>'2',
            'provincia'=>'AZUAY',
            'canton'=>'CUENCA',
            'parroquia'=>'CAÑARIBAMBA'
        ));
        
        DB::table('ubicaciones')->insert(array(
            'codigo'=>'U3',
            'codigo_norma'=>'3',
            'provincia'=>'CHIMBORAZO',
            'canton'=>'ALAUSI',
            'parroquia'=>'ALAUSI'
        ));
        
        DB::table('ubicaciones')->insert(array(
            'codigo'=>'U4',
            'codigo_norma'=>'4',
            'provincia'=>'CHIMBORAZO',
            'canton'=>'RIOBAMBA',
            'parroquia'=>'VELASCO'
        ));
        
        DB::table('ubicaciones')->insert(array(
            'codigo'=>'U5',
            'codigo_norma'=>'5',
            'provincia'=>'CHIMBORAZO',
            'canton'=>'RIOBAMBA',
            'parroquia'=>'RIOBAMBA'
        ));
    }
}
