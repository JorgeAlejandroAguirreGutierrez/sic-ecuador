<?php

use Illuminate\Database\Seeder;

class TiposRetenciones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('tipos_retenciones')->insert(array(
            'codigo'=>'RETENCION_IVA1',
            'impuesto_retencion'=>'IVA',
            'tipo_retencion'=>'BIEN',
            'codigo_norma'=>'103',
            'homologacion_f_e'=>'3',
            'descripcion'=>'IVA_100%',
            'porcentaje'=>100
        ));
        DB::table('tipos_retenciones')->insert(array(
            'codigo'=>'RETENCION_IVA2',
            'impuesto_retencion'=>'IVA',
            'tipo_retencion'=>'SERVICIO',
            'codigo_norma'=>'104',
            'homologacion_f_e'=>'4',
            'descripcion'=>'IVA_15%',
            'porcentaje'=>15
        ));
        DB::table('tipos_retenciones')->insert(array(
            'codigo'=>'RETENCION_IR1',
            'impuesto_retencion'=>'RENTA',
            'tipo_retencion'=>'BIEN',
            'codigo_norma'=>'303',
            'homologacion_f_e'=>null,
            'descripcion'=>'IR_10%',
            'porcentaje'=>10
        ));
        DB::table('tipos_retenciones')->insert(array(
            'codigo'=>'RETENCION_IR2',
            'impuesto_retencion'=>'RENTA',
            'tipo_retencion'=>'SERVICIO',            
            'codigo_norma'=>'304',
            'homologacion_f_e'=>null,
            'descripcion'=>'IR_5%',
            'porcentaje'=>5
        ));        
    }
}
