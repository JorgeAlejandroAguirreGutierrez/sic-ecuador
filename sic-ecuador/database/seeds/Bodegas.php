<?php

use Illuminate\Database\Seeder;

class Bodegas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('bodegas')->insert(array(
            'codigo'=>'001',
            'punto_venta_id'=>'1'
        ));
        DB::table('bodegas')->insert(array(
            'codigo'=>'002',
            'punto_venta_id'=>'1'
        ));
        DB::table('bodegas')->insert(array(
            'codigo'=>'003',
            'punto_venta_id'=>'2'
        ));
    }
}
