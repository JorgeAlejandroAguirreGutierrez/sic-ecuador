<?php

use Illuminate\Database\Seeder;

class FormasPagos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('formas_pagos')->insert(array(
            'codigo'=>'FP1',
            'descripcion'=>'EFECTIVO',
            'abreviatura'=>'EF',
        ));    
        
        DB::table('formas_pagos')->insert(array(
            'codigo'=>'FP2',
            'descripcion'=>'CREDITO',
            'abreviatura'=>'CR',
        ));           
    }
}
