<?php

use Illuminate\Database\Seeder;

class Auxiliares extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('auxiliares')->insert(array(
            'codigo'=>'AU1',
            'razon_social'=>'AUXILIAR 1 DE CLIENTE A',
            'estado'=>1,
            'eliminado'=>0,
            'cliente_id'=>1,
            'direccion_id'=>1,
        ));
        
        DB::table('auxiliares')->insert(array(
            'codigo'=>'AU2',
            'razon_social'=>'AUXILIAR 2 DE CLIENTE A',
            'estado'=>1,
            'eliminado'=>0,
            'cliente_id'=>1,
            'direccion_id'=>2,
        ));

        DB::table('auxiliares')->insert(array(
            'codigo'=>'AU3',
            'razon_social'=>'AUXILIAR 1 DE CLIENTE B',
            'estado'=>1,
            'eliminado'=>0,
            'cliente_id'=>2,
            'direccion_id'=>1,
        ));
        
        DB::table('auxiliares')->insert(array(
            'codigo'=>'AU4',
            'razon_social'=>'AUXILIAR 2 DE CLIENTE B',
            'estado'=>1,
            'eliminado'=>0,
            'cliente_id'=>2,
            'direccion_id'=>2,
        ));        
    }
}
