<?php

use Illuminate\Database\Seeder;

class VehiculosTransportes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('vehiculos_transportes')->insert(array(
            'codigo'=>'VEH000001',
            'placa'=>'XDF0067',
            'numero'=>1,
            'marca'=>'MERCEDEZ BENZ',
            'modelo'=>'C3',
            'anio'=>2010,
            'cilindraje'=>1500,
            'clase'=>'AUTOMOVIL',
            'color'=>'AMARRILLO',
            'fabricacion'=>'ALEMANA',
            'activo'=>1
        ));
        
        DB::table('vehiculos_transportes')->insert(array(
            'codigo'=>'VEH000002',
            'placa'=>'FRT0256',
            'numero'=>2,
            'marca'=>'FORD',
            'modelo'=>'RANGER',
            'anio'=>2014,
            'cilindraje'=>2000,
            'clase'=>'JEEP',
            'color'=>'NEGRO',
            'fabricacion'=>'AMERICANA',
            'activo'=>1
        ));
        
        DB::table('vehiculos_transportes')->insert(array(
            'codigo'=>'VEH000003',
            'placa'=>'DDA555',
            //'numero'=>1,
            'marca'=>'TOYOTA',
            'modelo'=>'HILUX',
            'anio'=>2008,
            'cilindraje'=>2200,
            'clase'=>'CAMIONETA',
            'color'=>'ROJO',
            'fabricacion'=>'COREANA',
            'activo'=>1
        ));
    }
}
