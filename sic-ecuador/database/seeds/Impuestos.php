<?php

use Illuminate\Database\Seeder;

class Impuestos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('impuestos')->insert(array(
            'codigo'=>'IVA1',
            'codigo_norma'=>'IVA_12%',
            'porcentaje'=>12
        ));
        DB::table('impuestos')->insert(array(
            'codigo'=>'IVA2',
            'codigo_norma'=>'IVA_0%',
            'porcentaje'=>0
        ));
    }
}
