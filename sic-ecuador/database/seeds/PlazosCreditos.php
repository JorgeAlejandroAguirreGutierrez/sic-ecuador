<?php

use Illuminate\Database\Seeder;

class PlazosCreditos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('plazos_creditos')->insert(array(
            'codigo'=>'PC1',
            'plazo'=>30
        ));
        
        DB::table('plazos_creditos')->insert(array(
            'codigo'=>'PC2',
            'plazo'=>45
        ));
        
        DB::table('plazos_creditos')->insert(array(
            'codigo'=>'PC3',
            'plazo'=>60
        ));
    }
}
