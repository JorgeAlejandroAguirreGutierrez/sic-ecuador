<?php

use Illuminate\Database\Seeder;

class TarjetasCreditos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('tarjetas_creditos')->insert(array(
            'codigo'=>'RTC000001',
            'diferido'=>FALSE,
            'titular'=>TRUE,
            'identificacion'=>'0502685966',
            'nombre_titular'=>'JORGE HIDALGO',
            'lote'=>'12314566',
            'valor'=>50.00,
            'tarjeta_id'=>1,
            'operador_tarjeta_id'=>1
        ));
        
        DB::table('tarjetas_creditos')->insert(array(
            'codigo'=>'RTC000002',
            'diferido'=>TRUE,
            'titular'=>TRUE,
            'identificacion'=>'0502685966',
            'nombre_titular'=>'JORGE HIDALGO',
            'lote'=>'12314566',
            'valor'=>30.00,
            'tarjeta_id'=>2,
            'operador_tarjeta_id'=>1
        ));
        
        DB::table('tarjetas_creditos')->insert(array(
            'codigo'=>'RTC000003',
            'diferido'=>FALSE,
            'titular'=>FALSE,
            'identificacion'=>'0601234567',
            'nombre_titular'=>'MARIO DELGADO',
            'lote'=>'12314566',
            'valor'=>90.00,
            'tarjeta_id'=>1,
            'operador_tarjeta_id'=>2
        ));
        
        DB::table('tarjetas_creditos')->insert(array(
            'codigo'=>'RTC000004',
            'diferido'=>TRUE,
            'titular'=>FALSE,
            'identificacion'=>'0601234567',
            'nombre_titular'=>'MARIO DELGADO',
            'lote'=>'12314566',
            'valor'=>70.00,
            'tarjeta_id'=>2,
            'operador_tarjeta_id'=>2
        ));          
    }
}
