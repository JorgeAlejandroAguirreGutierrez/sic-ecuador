<?php

use Illuminate\Database\Seeder;

class Egresos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('egresos')->insert(array(
            'codigo'=>'EGR000001',
            'numero_interno'=>'001-001-000001',
            'fecha'=>'2019-08-22',
            'fecha_entrega'=>'2019-08-23',
            'estado'=>'EMITIDA',
            'subtotal'=>91.43,
            'subdescuento'=>3.07,
            'base_iva'=>71.43,
            'base_0'=>20.00,
            'importe_iva'=>8.57,
            'total'=>100.00,
            'descuento_porcentaje'=>1.1857,
            'descuento'=>1.20,
            'abono'=>30.00,
            'comentario'=>'Egreso de mercaderia, abona 30.00 usd',
            'cliente_id'=>1,
            'sesion_id'=>2,
            'vendedor_id'=>3
        ));

        DB::table('egresos')->insert(array(
            'codigo'=>'EGR000002',
            'numero_interno'=>'001-001-000002',
            'fecha'=>'2019-08-22',
            'fecha_entrega'=>'2019-08-24',
            'estado'=>'EMITIDA',
            'subtotal'=>178.57,
            'subdescuento'=>1.43,
            'base_iva'=>178.57,
            'base_0'=>0.00,
            'importe_iva'=>21.43,
            'total'=>200.00,
            'descuento_porcentaje'=>1.1,
            'descuento'=>2.20,
            'abono'=>50.00,
            'comentario'=>'Egreso de mercaderia, abona 50.00 usd',
            'cliente_id'=>2,
            'sesion_id'=>2,
            'vendedor_id'=>3
        ));        
    }
}
