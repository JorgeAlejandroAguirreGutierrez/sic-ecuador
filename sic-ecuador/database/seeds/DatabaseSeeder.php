<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
         $this->call('Auxiliares');
         $this->call('Bancos');
         $this->call('Bodegas');
         $this->call('CategoriaClientes');
         $this->call('Celulares');
         $this->call('Cheques');
         $this->call('Clientes');
         $this->call('Compensaciones');         
         $this->call('ComprobantesDetalles');
         $this->call('Correos');
         $this->call('Creditos');
         $this->call('CreditosAmortizaciones');
         $this->call('DatosAdicionales');
         $this->call('DepositosTransferencias');         
         $this->call('Direcciones');
         $this->call('Egresos');
         $this->call('Empresas');
         $this->call('Establecimientos');
         $this->call('EstadosCiviles');
         $this->call('Facturas');
         $this->call('Financiamientos');
         $this->call('FormasPagos');
         $this->call('Generos');
         $this->call('GruposClientes');
         $this->call('GuiasRemisiones');
         $this->call('Impuestos');
         $this->call('OperadoresTarjetas');
         $this->call('OrigenIngresos');
         $this->call('Parametros');         
         $this->call('Pedidos');
         $this->call('Perfiles');
         $this->call('Permisos');
         $this->call('PlazosCreditos');
         $this->call('Productos');
         $this->call('Proformas');
         $this->call('PuntosVentas');
         $this->call('Recaudaciones');
         $this->call('RetencionesClientes');
         $this->call('RetencionesCompras');
         $this->call('RetencionesVentas');
         $this->call('RetencionesVentasDetalles');
         $this->call('Servicios');
         $this->call('Sesiones');
         $this->call('Tarjetas');         
         $this->call('TarjetasCreditos');
         $this->call('TarjetasDebitos');
         $this->call('Telefonos');
         $this->call('TiposComprobantes');
         $this->call('TiposContribuyentes');         
         $this->call('TiposPagos');
         $this->call('TiposProductos');
         $this->call('TiposRetenciones');
         $this->call('Transportistas');
         $this->call('Ubicaciones');
         $this->call('Usuarios');
         $this->call('VehiculosTransportes');
    }
}
