<?php

use Illuminate\Database\Seeder;

class Facturas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('facturas')->insert(array(
            'codigo'=>'FAC000001',
            'numero'=>'001-001-000001',
            'fecha'=>'2019-08-22 08:01:01',
            'estado'=>'EMITIDA',
            'subtotal'=>91.43,
            'subdescuento'=>3.07,
            'base_iva'=>71.43,
            'base_0'=>20.00,
            'importe_iva'=>8.57,
            'total'=>100.00,
            'descuento_porcentaje'=>1.1857,
            'descuento'=>1.20,
            'comentario'=>'El subdecuento viene del detalle y el descuento del total',
            'cliente_id'=>1,
            'cliente_factura_id'=>4,
            'auxiliar_id'=>2,
            'sesion_id'=>2,
            'vendedor_id'=>3
        ));
        
        DB::table('facturas')->insert(array(
            'codigo'=>'FAC000002',
            'numero'=>'001-001-000002',
            'fecha'=>'2019-08-22 08:03:01',
            'estado'=>'EMITIDA',
            'subtotal'=>178.57,
            'subdescuento'=>1.43,
            'base_iva'=>178.57,
            'base_0'=>0.00,
            'importe_iva'=>21.43,
            'total'=>200.00,
            'descuento_porcentaje'=>1.1,
            'descuento'=>2.20,
            'comentario'=>'El total salia 202.20, aplicando descuento de 1.1% sale 200.00',
            'cliente_id'=>1,
            'cliente_factura_id'=>4,
            'auxiliar_id'=>2,
            'sesion_id'=>2,
            'vendedor_id'=>3
        ));

        DB::table('facturas')->insert(array(
            'codigo'=>'FAC000003',
            'numero'=>'001-001-000003',
            'fecha'=>'2019-08-22 08:09:01',
            'estado'=>'ANULADA',
            'subtotal'=>91.43,
            'subdescuento'=>3.07,
            'base_iva'=>71.43,
            'base_0'=>20.00,
            'importe_iva'=>8.57,
            'total'=>100.00,
            'descuento_porcentaje'=>1.1857,
            'descuento'=>1.20,
            'comentario'=>'Se anula por solicitud del cliente',
            'cliente_id'=>1,
            'cliente_factura_id'=>4,
            'auxiliar_id'=>2,
            'sesion_id'=>2,
            'vendedor_id'=>3
        ));        
    }
}