<?php

use Illuminate\Database\Seeder;

class Direcciones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('direcciones')->insert(array(
            'codigo'=>'DIR1',
            'direccion'=>'CALLE 1 Y CALLE 2',
            'latitudgeo'=>'40.75793',
            'longitudgeo'=>'-73.98551',
            'ubicaciongeo_id'=>1,
        ));  
        
        DB::table('direcciones')->insert(array(
            'codigo'=>'DIR2',
            'direccion'=>'CALLE 3 Y CALLE 4',
            'latitudgeo'=>'-77.5000000',
            'longitudgeo'=>'-2.0000000',
            'ubicaciongeo_id'=>2,
        ));     
        
        DB::table('direcciones')->insert(array(
            'codigo'=>'DIR3',
            'direccion'=>'CALLE 5 Y CALLE 6',
            'latitudgeo'=>'-77.5000000',
            'longitudgeo'=>'-2.0000000',
            'ubicaciongeo_id'=>1,
        ));  

        DB::table('direcciones')->insert(array(
            'codigo'=>'DIR4',
            'direccion'=>'CALLE 7 Y CALLE 8',
            'latitudgeo'=>'-77.5000000',
            'longitudgeo'=>'-2.0000000',
            'ubicaciongeo_id'=>2,
        ));  
        
    }
}
