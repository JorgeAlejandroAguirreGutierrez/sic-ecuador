<?php

use Illuminate\Database\Seeder;

class EstadosCiviles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('estados_civiles')->insert(array(
            'codigo'=>'EC1',
            'estado_civil'=>'SOLTERO',
            'abreviatura'=>'S',
        ));
        
        DB::table('estados_civiles')->insert(array(
            'codigo'=>'EC2',
            'estado_civil'=>'CASADO',
            'abreviatura'=>'C',
        ));
        
        DB::table('estados_civiles')->insert(array(
            'codigo'=>'EC3',
            'estado_civil'=>'VIUDO',
            'abreviatura'=>'V',
        )); 

        DB::table('estados_civiles')->insert(array(
            'codigo'=>'EC4',
            'estado_civil'=>'DIVORCIADO',
            'abreviatura'=>'D',
        ));     
        
        DB::table('estados_civiles')->insert(array(
            'codigo'=>'EC5',
            'estado_civil'=>'UNION LIBRE',
            'abreviatura'=>'U',
        ));            
    }
}
