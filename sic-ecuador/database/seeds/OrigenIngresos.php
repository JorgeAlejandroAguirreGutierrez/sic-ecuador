<?php

use Illuminate\Database\Seeder;

class OrigenIngresos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('origen_ingresos')->insert(array(
            'codigo'=>'OI1',
            'origen_ingreso'=>'SALARIO',
            'abreviatura'=>'S',
        ));     
        
        DB::table('origen_ingresos')->insert(array(
            'codigo'=>'OI2',
            'origen_ingreso'=>'VENTAS',
            'abreviatura'=>'V',
        ));
        
        DB::table('origen_ingresos')->insert(array(
            'codigo'=>'OI3',
            'origen_ingreso'=>'INDEPENDIENTE',
            'abreviatura'=>'I',
        ));          
    }
}
