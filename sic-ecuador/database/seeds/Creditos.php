<?php

use Illuminate\Database\Seeder;

class Creditos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('creditos')->insert(array(
            'codigo'=>'CRE000001',
            'saldo'=>500,
            'plazo'=>12,
            'periodicidad'=>'MENSUAL',
            'modelo_tabla'=>'FRANCESA',
            'interes_periodo'=>16.00,
            'interes_anual'=>16.00,
            'primera_cuota'=>'2019-10-05',
            'vencimiento'=>'2020-09-05',
            'cuota'=>49.06,
            'intereses'=>44.39,
            'total'=>580.00,
            'estado'=>'VIGENTE'            
        ));
        
        DB::table('creditos')->insert(array(
            'codigo'=>'CRE000002',
            'saldo'=>120,
            'plazo'=>6,
            'periodicidad'=>'QUINCENAL',
            'modelo_tabla'=>'ALEMANA',
            'interes_periodo'=>4.00,
            'interes_anual'=>16.00,
            'primera_cuota'=>'2019-10-05',
            'vencimiento'=>'2019-12-20',
            'cuota'=>22.03,
            'intereses'=>4.03,
            'total'=>124.03,
            'estado'=>'CANCELADO'            
        ));
    }
}
