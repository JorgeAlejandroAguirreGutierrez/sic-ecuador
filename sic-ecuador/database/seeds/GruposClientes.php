<?php

use Illuminate\Database\Seeder;

class GruposClientes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('grupos_clientes')->insert(array(
            'codigo'=>'GC1',
            'denominacion'=>'CLIENTES NACIONALES',
            'numero_cuenta'=>'1.1.1.1',
            'nombre_cuenta'=>'1.1.1.1',
            'cliente_relacionado'=>1
        ));
        
        DB::table('grupos_clientes')->insert(array(
            'codigo'=>'GC2',
            'denominacion'=>'CLIENTES INTERNACIONALES',
            'numero_cuenta'=>'1.1.1.2',
            'nombre_cuenta'=>'1.1.1.2',
            'cliente_relacionado'=>1
        ));
        
        DB::table('grupos_clientes')->insert(array(
            'codigo'=>'GS1',
            'denominacion'=>'HONORARIOS',
            'numero_cuenta'=>'1.1.1.2',
            'nombre_cuenta'=>'1.1.1.2',
            'cliente_relacionado'=>2
        ));
        
        DB::table('grupos_clientes')->insert(array(
            'codigo'=>'GS2',
            'denominacion'=>'ASESORIAS',
            'numero_cuenta'=>'1.1.1.2',
            'nombre_cuenta'=>'1.1.1.2',
            'cliente_relacionado'=>2
        ));
        
        DB::table('grupos_clientes')->insert(array(
            'codigo'=>'GS3',
            'denominacion'=>'SERVICIOS',
            'numero_cuenta'=>'1.1.1.3',
            'nombre_cuenta'=>'1.1.1.3',
            'cliente_relacionado'=>2
        ));
    }
}
