<?php

use Illuminate\Database\Seeder;

class OperadoresTarjetas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('operadores_tarjetas')->insert(array(
            'codigo'=>'OTA000001',
            'tipo'=>'CREDITO',
            'nombre'=>'DATAFAST',
            'abreviatura'=>'DF'
        ));
        
        DB::table('operadores_tarjetas')->insert(array(
            'codigo'=>'OTA000002',
            'tipo'=>'DEBITO',
            'nombre'=>'DATAFAST',
            'abreviatura'=>'DF'
        ));
        
        DB::table('operadores_tarjetas')->insert(array(
            'codigo'=>'OTA000001',
            'tipo'=>'CREDITO',
            'nombre'=>'MEGADATOS',
            'abreviatura'=>'MD'
        ));
        
        DB::table('operadores_tarjetas')->insert(array(
            'codigo'=>'OTA000002',
            'tipo'=>'DEBITO',
            'nombre'=>'MEGADATOS',
            'abreviatura'=>'MD'
        ));        
    }
}
