<?php

use Illuminate\Database\Seeder;

class RetencionesCompras extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('retenciones_compras')->insert(array(
            'codigo'=>'RCO000001',
            'numero'=>'001-001-000001',
            'fecha'=>'2019-08-23',
            'estado'=>'EMITIDA',
            'autorizacion'=>'123948474618293917383029',
            'total'=>10.00,
            'factura_compra_id'=>1
        ));        
        
        DB::table('retenciones_compras')->insert(array(
            'codigo'=>'RCO000002',
            'numero'=>'001-001-000002',
            'fecha'=>'2019-08-23',
            'estado'=>'EMITIDA',
            'autorizacion'=>'1239484423424242',
            'total'=>25.00,
            'factura_compra_id'=>2
        ));          
        
        DB::table('retenciones_compras')->insert(array(
            'codigo'=>'RCO000003',
            'numero'=>'001-001-000003',
            'fecha'=>'2019-08-23',
            'estado'=>'ANULADA',
            'autorizacion'=>'123948474618293917383029',
            'total'=>10.00,
            'factura_compra_id'=>1
        ));          
    }
}
