<?php

use Illuminate\Database\Seeder;

class DepositosTransferencias extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('depositos_transferencias')->insert(array(
            'codigo'=>'DEP000001',
            'tipo_transaccion'=>'DEPOSITO',
            'numero_transaccion'=>'00012312312',
            'fecha_transaccion'=>'2019-08-28',
            'valor'=>5.00,
            'recaudacion_id'=>1,
            'banco_id'=>2
        ));
        
        DB::table('depositos_transferencias')->insert(array(
            'codigo'=>'TRA000001',
            'tipo_transaccion'=>'TRANSFERENCIA',
            'numero_transaccion'=>'0001231234242',
            'fecha_transaccion'=>'2019-08-28',
            'valor'=>5.00,
            'recaudacion_id'=>1,
            'banco_id'=>1
        ));
        
        DB::table('depositos_transferencias')->insert(array(
            'codigo'=>'DEP000002',
            'tipo_transaccion'=>'DEPOSITO',
            'numero_transaccion'=>'00012312312',
            'fecha_transaccion'=>'2019-08-28',
            'valor'=>5.00,
            'recaudacion_id'=>2,
            'banco_id'=>2
        ));
        
        DB::table('depositos_transferencias')->insert(array(
            'codigo'=>'DEP000003',
            'tipo_transaccion'=>'DEPOSITO',
            'numero_transaccion'=>'1312324236',
            'fecha_transaccion'=>'2019-08-28',
            'valor'=>15.00,
            'recaudacion_id'=>2,
            'banco_id'=>3
        ));        
    }
}
