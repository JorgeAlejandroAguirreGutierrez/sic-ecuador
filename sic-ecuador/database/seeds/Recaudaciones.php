<?php

use Illuminate\Database\Seeder;

class Recaudaciones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('recaudaciones')->insert(array(
            'codigo'=>'REC000001',
            'fecha'=>'2019-08-26',
            'total'=>100.00,
            'comentario'=>'Recaudación factura 1',
            'efectivo'=>0.00,
            'total_cheques'=>60.00,
            'total_depositos_transferencias'=>10.00,
            'total_retenciones_compras'=>10.00,
            'tarjeta_credito_id'=>1,
            'tarjeta_debito_id'=>1,
            'credito_id'=>1,
            'tipo_comprobante_id'=>1,
            'comprobante_id'=>1,            
            'sesion_id'=>2
        ));
        
        DB::table('recaudaciones')->insert(array(
            'codigo'=>'REC000002',
            'fecha'=>'2019-08-27',
            'total'=>300.00,
            'comentario'=>'Recaudación factura 2',
            'efectivo'=>100.00,
            'total_cheques'=>80.00,
            'total_depositos_transferencias'=>20.00,
            'total_retenciones_compras'=>00.00,
            'tarjeta_credito_id'=>2,
            'tarjeta_debito_id'=>2,
            'credito_id'=>2,
            'tipo_comprobante_id'=>1,
            'comprobante_id'=>1,            
            'sesion_id'=>2
        ));     
    }
}
