<?php

use Illuminate\Database\Seeder;

class Efectivos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('efectivos')->insert(array(
            'codigo'=>'EFE000001',
            'valor'=>50.00
        ));
        
        DB::table('efectivos')->insert(array(
            'codigo'=>'EFE000002',
            'valor'=>20.00
        ));

        DB::table('efectivos')->insert(array(
            'codigo'=>'EFE000003',
            'valor'=>10.00
        ));

        DB::table('efectivos')->insert(array(
            'codigo'=>'EFE000004',
            'valor'=>80.00
        ));        
    }
}
