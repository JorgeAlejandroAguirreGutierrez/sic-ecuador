<?php

use Illuminate\Database\Seeder;

class Parametros extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('parametros')->insert(array(
            'codigo'=>'CL',
            'tipo'=>'CREAR',
            'tabla'=>'clientes'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'DA',
            'tipo'=>'CREAR',
            'tabla'=>'datos_adicionales'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'GCLI',
            'tipo'=>'CREAR',
            'tabla'=>'grupos_clientes'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'GC',
            'tipo'=>'CREAR',
            'tabla'=>'grupos_contables'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'PC',
            'tipo'=>'CREAR',
            'tabla'=>'plazos_creditos'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'RETEIR',
            'tipo'=>'CREARIR',
            'tabla'=>'retenciones'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'RETEIVA',
            'tipo'=>'CREARIVA',
            'tabla'=>'retenciones'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'S',
            'tipo'=>'CREAR',
            'tabla'=>'servicios'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'TC',
            'tipo'=>'CREAR',
            'tabla'=>'tipos_contribuyentes'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'TR',
            'tipo'=>'CREAR',
            'tabla'=>'transportistas'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'VH',
            'tipo'=>'CREAR',
            'tabla'=>'vehiculos_transportes'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'UG',
            'tipo'=>'CREAR',
            'tabla'=>'ubicaciones'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'USU',
            'tipo'=>'CREAR',
            'tabla'=>'usuarios'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'B',
            'tipo'=>'CREAR',
            'tabla'=>'bodegas'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'PRO',
            'tipo'=>'CREAR',
            'tabla'=>'productos'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'FAC',
            'tipo'=>'CREAR',
            'tabla'=>'facturas'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'GR',
            'tipo'=>'CREAR',
            'tabla'=>'guias_remisiones'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'CHV',
            'tipo'=>'CREAR',
            'tabla'=>'cheques_vistas'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'CHP',
            'tipo'=>'CREAR',
            'tabla'=>'cheques_posfechados'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'EF',
            'tipo'=>'CREAR',
            'tabla'=>'efectivos'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'DP',
            'tipo'=>'CREAR',
            'tabla'=>'depositos'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'TR',
            'tipo'=>'CREAR',
            'tabla'=>'transferencias'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'TC',
            'tipo'=>'CREAR',
            'tabla'=>'tarjetas_creditos'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'TD',
            'tipo'=>'CREAR',
            'tabla'=>'tarjetas_debitos'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'CR',
            'tipo'=>'CREAR',
            'tabla'=>'comprobantes_retenciones'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'IMP',
            'tipo'=>'CREAR',
            'tabla'=>'impuestos'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'CRE',
            'tipo'=>'CREAR',
            'tabla'=>'creditos'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'EGI',
            'tipo'=>'CREAR',
            'tabla'=>'cabeceras_egresos_inventarios'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'TEL',
            'tipo'=>'CREAR',
            'tabla'=>'telefonos'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'CEL',
            'tipo'=>'CREAR',
            'tabla'=>'celulares'
        ));
        
        DB::table('parametros')->insert(array(
            'codigo'=>'COR',
            'tipo'=>'CREAR',
            'tabla'=>'correos'
        ));
        DB::table('parametros')->insert(array(
            'tabla'=>'origen_ingresos',
            'tipo'=>'CREAR',
            'codigo'=>'OI'
        ));

        DB::table('parametros')->insert(array(
            'tabla'=>'generos',
            'tipo'=>'CREAR',
            'codigo'=>'GEN'
        ));

        DB::table('parametros')->insert(array(
            'tabla'=>'estados_civiles',
            'tipo'=>'CREAR',
            'codigo'=>'ESC'
        ));

        DB::table('parametros')->insert(array(
            'tabla'=>'categoria_clientes',
            'tipo'=>'CREAR',
            'codigo'=>'CC'
        ));
        
    }
}
