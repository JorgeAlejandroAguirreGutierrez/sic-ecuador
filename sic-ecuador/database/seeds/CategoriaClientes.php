<?php

use Illuminate\Database\Seeder;

class CategoriaClientes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categoria_clientes')->insert(array(
            'codigo'=>'CAT1',
            'categoria'=>'EXELENTE',
            'abreviatura'=>'E',
        ));    

        DB::table('categoria_clientes')->insert(array(
            'codigo'=>'CAT2',
            'categoria'=>'MUY BUENO',
            'abreviatura'=>'M',
        ));  

        DB::table('categoria_clientes')->insert(array(
            'codigo'=>'CAT3',
            'categoria'=>'BUENO',
            'abreviatura'=>'B',
        ));  

        DB::table('categoria_clientes')->insert(array(
            'codigo'=>'CAT4',
            'categoria'=>'REGULAR',
            'abreviatura'=>'R',
        ));   
        
        DB::table('categoria_clientes')->insert(array(
            'codigo'=>'CAT5',
            'categoria'=>'MALO',
            'abreviatura'=>'MA',
        ));          
    }
}
