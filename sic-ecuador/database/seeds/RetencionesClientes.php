<?php

use Illuminate\Database\Seeder;

class RetencionesClientes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('retenciones_clientes')->insert(array(
            'codigo'=>'RET1',
            'cliente_id'=>1,
            'tipo_retencion_id'=>1,
        ));
        
        DB::table('retenciones_clientes')->insert(array(
            'codigo'=>'RET2',
            'cliente_id'=>1,
            'tipo_retencion_id'=>2,
        ));
        
        DB::table('retenciones_clientes')->insert(array(
            'codigo'=>'RET3',
            'cliente_id'=>2,
            'tipo_retencion_id'=>1,
        ));
        
        DB::table('retenciones_clientes')->insert(array(
            'codigo'=>'RET4',
            'cliente_id'=>2,
            'tipo_retencion_id'=>3,
        ));        
    }
}
