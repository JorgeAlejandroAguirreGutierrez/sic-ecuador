<?php

use Illuminate\Database\Seeder;

class Usuarios extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('usuarios')->insert(array(
            'codigo'=>'U1',
            'nombre'=>'JUAN ANTONIO',
            'correo'=>'juanantonio@gmail.com',
            'contrasena'=>'12345',
            'identificacion'=>'010225036',
            'avatar'=>'/storage/img/imagen1',
            'activo'=>false,
            'empresa_id'=>1,
            'perfil_id'=>1
        ));
        
        DB::table('usuarios')->insert(array(
            'codigo'=>'U2',
            'nombre'=>'CRISTINA ALEJANDRA',
            'correo'=>'cristinaalejandra@gmail.com',
            'contrasena'=>'12345',
            'identificacion'=>'030132225',
            'avatar'=>'/storage/img/imagen2',
            'activo'=>true,
            'empresa_id'=>1,
            'perfil_id'=>1
        ));
        
        DB::table('usuarios')->insert(array(
            'codigo'=>'U3',
            'nombre'=>'MARIO DELGADO',
            'correo'=>'mastermariodelgado@gmail.com',
            'contrasena'=>'12345',
            'identificacion'=>'0601234567',
            'avatar'=>'/storage/img/imagen3',
            'activo'=>true,
            'empresa_id'=>1,
            'perfil_id'=>1
        ));
        
        DB::table('usuarios')->insert(array(
            'codigo'=>'U4',
            'nombre'=>'JORGE HIDALGO',
            'correo'=>'gatosohidalgo@gmail.com',
            'contrasena'=>'12345',
            'identificacion'=>'0502685969',
            'avatar'=>'/storage/img/imagen4',
            'activo'=>true,
            'empresa_id'=>1,
            'perfil_id'=>1
        )); 
        
        DB::table('usuarios')->insert(array(
            'codigo'=>'U5',
            'nombre'=>'JORGE ALEJANDRO',
            'correo'=>'alejandro@gmail.com',
            'contrasena'=>'12345',
            'identificacion'=>'0123456789',
            'avatar'=>'/storage/img/imagen5',
            'activo'=>true,
            'empresa_id'=>1,
            'perfil_id'=>1
        ));        
        
        DB::table('usuarios')->insert(array(
            'codigo'=>'U6',
            'nombre'=>'MARIA JOSE',
            'correo'=>'mariajose@gmail.com',
            'contrasena'=>'12345',
            'identificacion'=>'080212685',
            'avatar'=>'/storage/img/imagen6',
            'activo'=>false,
            'empresa_id'=>1,
            'perfil_id'=>2
        ));
        
        DB::table('usuarios')->insert(array(
            'codigo'=>'U7',
            'nombre'=>'MARIA JULIANA',
            'correo'=>'nariajuliana@gmail.com',
            'contrasena'=>'12345',
            'identificacion'=>'130735366',
            'avatar'=>'/storage/img/imagen7',
            'activo'=>true,
            'empresa_id'=>1,
            'perfil_id'=>2
        ));
    }
}
