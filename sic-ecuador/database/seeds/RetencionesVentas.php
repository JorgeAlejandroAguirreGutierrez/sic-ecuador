<?php

use Illuminate\Database\Seeder;

class RetencionesVentas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('retenciones_ventas')->insert(array(
            'codigo'=>'RVE000001',
            'numero'=>'001-001-0000234',
            'fecha'=>'2019-08-28',
            'agente'=>'EMPRESA PROVEEDORA 1',
            'autorizacion'=>'123948474618293917383029',
            'compensado'=>TRUE,
            'base_imponible'=>100.00,
            'valor'=>12.00,
            'tipo_retencion_id'=>1,
            'recaudacion_id'=>1
        )); 

        DB::table('retenciones_ventas')->insert(array(
            'codigo'=>'RVE000002',
            'numero'=>'001-001-0000444',
            'fecha'=>'2019-08-28',
            'agente'=>'EMPRESA PROVEEDORA 1',
            'autorizacion'=>'123948474618243543689',
            'compensado'=>FALSE,
            'base_imponible'=>12.00,
            'valor'=>8.00,
            'tipo_retencion_id'=>2,
            'recaudacion_id'=>1
        )); 
    }
}
