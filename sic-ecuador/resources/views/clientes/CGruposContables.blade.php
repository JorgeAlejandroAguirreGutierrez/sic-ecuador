@extends('plantillaHEAD')
@section('title')
Crear Grupos de Contables
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/CGruposContables.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a title="Ver Grupos de Clientes" data-toggle="tooltip" data-placement="bottom" class="nav-link" href="javascript:window.open('{{action('clientes\ControladorGruposContables@vistaLeerActualizarEliminarGC')}}','','width=900px, height=600px toolbar=yes');void 0"><i class="fas fa-stream fa-lg"></i></a>
            </li>
            <li class="nav-item">
                <a title="Ver Grupos de Servicios" data-toggle="tooltip" data-placement="bottom" class="nav-link" href="javascript:window.open('{{action('clientes\ControladorGruposContables@vistaLeerActualizarEliminarGS')}}','','width=900px, height=600px toolbar=yes');void 0"><i class="fas fa-address-book fa-lg"></i></a>
            </li>
            <li class="nav-item">
                <a title="Crear Grupo" data-toggle="tooltip" data-placement="bottom" class="nav-link" href="javascript:window.open('{{action('clientes\ControladorGruposContables@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0"><i class="fas fa-file fa-lg"></i></a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>GRUPOS CONTABLES</h2>
        <hr>
        <h3>Crear Grupo Contables</h3>
        <hr>
        <div id="formulario">
            <div class="form-group form-row">
                <label for="codigo" class="col-4 col-form-label">Codigo</label>
                <div class="col-8">
                    <select name="codigo" id="codigo" class="form-control" required>
                    </select>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="denominacion" class="col-4 col-form-label">Denominación</label>
                <div class="col-8">
                    <input type="text" name="denominacion" id="denominacion" class="form-control" placeholder="Denominación del Grupo" required>
                    <div name="invalido" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="numero_cuenta" class="col-4 col-form-label">Numero de la Cuenta</label>
                <div class="col-8">
                    <input type="text" name="numero_cuenta" id="numero_cuenta" class="form-control" placeholder="Numero de Cuenta" required>
                    <div id="invalido_numero_cuenta" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="nombre_cuenta" class="col-4 col-form-label">Nombre de la Cuenta</label> 
                <div class="col-8">
                    <input type="text" name="nombre_cuenta" id="nombre_cuenta" class="form-control" placeholder="Nombre de la cuenta" required>
                    <div id="invalido_nombre_cuenta" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="cliente_relacionado" class="col-4 col-form-label">Cliente Relacionado</label> 
                <div class="col-8">
                    <input type="checkbox" name="cliente_relacionado" id="cliente_relacionado" class="form-control">
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-success" id="crearGrupoContable">Crear</button>
            </div>
            <div id="alerta" role="alert">
            </div>
        </div>
    </div>
</div>
@endsection