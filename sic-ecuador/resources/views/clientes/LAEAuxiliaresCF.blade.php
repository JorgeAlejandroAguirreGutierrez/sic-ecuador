@extends('plantillaHEAD')
@section('title')
Clientes Auxiliares Consumidor Final
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/LAEAuxiliaresCF.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorAuxiliaresCF@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorAuxiliaresCF@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>CLIENTES AUXILIARES CONSUMIDOR FINAL</h2>
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>CÓDIGO</th>
                    <th>NOMBRE</th>
                    <th>IDENTIFICACION</th>
                    <th>DIRECCION</th>
                    <th>PROVINCIA</th>
                    <th>CANTON</th>
                    <th>PARROQUIA</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach($auxiliaresCF as $auxiliarCF)
                <tr>
                    <td>
                        {{$auxiliarCF->cliente->codigo}}
                    </td>
                    <td>
                        {{$auxiliarCF->cliente->razon_social}}
                    </td>
                    <td>
                        {{$auxiliarCF->cliente->identificacion}}
                    </td>
                    <td>
                        {{$auxiliarCF->cliente->direccion}}
                    </td>
                    <td>
                        {{$auxiliarCF->cliente->ubicaciongeo->provincia}}
                    </td>
                    <td>
                        {{$auxiliarCF->cliente->ubicaciongeo->canton}}
                    </td>
                    <td>
                        {{$auxiliarCF->cliente->ubicaciongeo->parroquia}}
                    </td>
                    <td>
                        <div class="btn-group btn-group-justified">
                            <button type="button" class="btn btn-default" name="eliminarAuxiliarCF" id="{{$auxiliarCF->id}}" data-toggle="modal" data-target="#modalEliminarAuxiliarCF">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modalEliminarAuxiliarCF" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Cliente Auxiliar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar el cliente auxiliar de consumidor final?
                <input type="hidden" id="idE" value="-1">
                <form>
                    <div class="form-group form-row">
                        <label for="codigoE" class="col-2 col-form-label">Numero</label>
                        <div class="col-10">
                            <input type="text" name="codigoE" id="codigoE" class="form-control" placeholder="Numero del Cliente" required>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="razon_socialE" class="col-2 col-form-label">Cliente</label>
                        <div class="col-10">
                            <input type="text" name="nombreE" id="nombreE" class="form-control" placeholder="Nombre del Cliente" required>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="identificacionE" class="col-2 col-form-label">Identificacion</label>
                        <div class="col-10">
                            <input type="text" name="identificacionE" id="identificacionE" class="form-control" placeholder="Numero de Identificacion" required>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="direccionE" class="col-2 col-form-label">Direccion</label>
                        <div class="col-10">
                            <input type="text" name="direccionE" id="direccionE" class="form-control" placeholder="Direccion del Cliente" required>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="provinciaE" class="col-2 col-form-label">Provincia</label> 
                        <div class="col-10">
                            <input type="text" name="provinciaE" id="provinciaE" class="form-control" placeholder="Provincia" required>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="cantonE" class="col-2 col-form-label">Canton</label>
                        <div class="col-10">
                            <input type="text" name="cantonE" id="cantonE" class="form-control" placeholder="Canton" required>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="parroquiaE" class="col-2 col-form-label">Parroquia</label>
                        <div class="col-10">
                            <input type="text" name="parroquiaE" id="parroquiaE" class="form-control" placeholder="parroquia" required>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarAuxiliarCF">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection
