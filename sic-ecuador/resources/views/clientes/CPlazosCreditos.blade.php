@extends('plantillaHEAD')
@section('title')
Crear Plazo de Credito
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/CPlazosCreditos.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a title="Ver Plazos de Creditos" data-toggle="tooltip" data-placement="bottom" class="nav-link" href="javascript:window.open('{{action('clientes\ControladorPlazosCreditos@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0"><i class="fas fa-stream fa-lg"></i></a>
            </li>
            <li class="nav-item">
                <a title="Crear Plazos de Creditos" data-toggle="tooltip" data-placement="bottom" class="nav-link" href="javascript:window.open('{{action('clientes\ControladorPlazosCreditos@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0"><i class="fas fa-file fa-lg"></i></a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>PLAZO DE CREDITO</h2>
        <hr>
        <h3>Añadir Plazo de Credito</h3>
        <hr>
        <div id="formulario">
            <div class="form-group form-row">
                <label for="codigo" class="col-4 col-form-label">Codigo</label>
                <div class="col-8">
                    <input type="text" name="codigo" id="codigo" class="form-control" disabled="">
                    <div id="invalido">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="plazo" class="col-4 col-form-label">Plazo</label>
                <div class="col-8">
                    <input type="number" name="plazo" id="plazo" class="form-control" placeholder="Numero de Plazo" required>
                    <div id="invalido_plazo" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-success" id="crearPlazoCredito">Crear Plazo de Credito</button>
            </div>
            <div id="alerta" role="alert">
            </div>
        </div>
    </div>
</div>
@endsection
