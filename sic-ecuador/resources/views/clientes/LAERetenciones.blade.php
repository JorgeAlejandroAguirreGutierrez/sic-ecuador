@extends('plantillaHEAD')
@section('title')
Retenciones
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/LAERetenciones.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorRetenciones@vistaLeerActualizarEliminarIVAs')}}','','width=900px, height=600px toolbar=yes');void 0">Ver IVAs</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorRetenciones@vistaLeerActualizarEliminarIRs')}}','','width=900px, height=600px toolbar=yes');void 0">Ver IRs</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorRetenciones@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        @if ($tipoRetencion=='IVA')
        <h2>RETENCIONES DE IVAs</h2>
        @else
        <h2>RETENCIONES DE IRs</h2>
        @endif
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>CODIGO</th>
                    <th>DESCRIPCIÓN</th>
                    <th>PORCENTAJE</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach($retenciones as $retencion)
                <tr>
                    <td>
                        {{$retencion->codigo}}
                    </td>
                    <td>
                        {{$retencion->descripcion}}
                    </td>
                    <td>
                        {{$retencion->porcentaje}}
                    </td>
                    <td>
                        <div class="btn-group btn-group-justified">
                            <button type="button" class="btn btn-default" name="modificarRetencion" id="{{$retencion->id}}" data-toggle="modal" data-target="#modalModificarRetencion">
                                <i class="fas fa-edit"></i>
                            </button>
                            <button type="button" class="btn btn-default" name="eliminarRetencion" id="{{$retencion->id}}" data-toggle="modal" data-target="#modalEliminarRetencion">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="modalModificarRetencion" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <h2>RETENCIONES</h2>
                    <hr>
                    <h3>Modificar Rentención</h3>
                    <hr>
                    <input type="hidden" id="idM" value="-1">
                    <div>
                        <div class="form-group form-row">
                            <label for="codigoM" class="col-3 col-form-label">Codigo</label>
                            <div class="col-9">
                                <input type="text" name="codigoM" id="codigoM" class="form-control" disabled="">
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="descripcionM" class="col-3 col-form-label">Descripción</label>
                            <div class="col-9">
                                <input type="text" name="descripcionM" id="descripcionM" class="form-control" placeholder="Descripción de la Retención" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="porcentajeM" class="col-3 col-form-label">Porcentaje</label>
                            <div class="col-9">
                                <input type="number" name="porcentajeM" id="porcentajeM" class="form-control" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" id="cModificarRetencion">Modificar Retención</button>
            </div>
        </div>
        <div class="col-2"></div>
    </div>
</div>
<div class="modal fade" id="modalEliminarRetencion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Retencion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar la retencion?
                <br>
                <br>
                <input type="hidden" id="idE" value="-1">
                <div>
                    <div class="form-group form-row">
                        <label for="codigoE" class="col-3 col-form-label">Codigo</label>
                        <div class="col-9">
                            <input type="text" name="codigoE" id="codigoE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="descripcionE" class="col-3 col-form-label">Descripción</label>
                        <div class="col-9">
                            <input type="text" name="descripcionE" id="descripcionE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="porcentajeE" class="col-3 col-form-label">Porcentaje</label>
                        <div class="col-9">
                            <input type="number" name="porcentajeE" id="porcentajeE" class="form-control" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarRetencion">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection
