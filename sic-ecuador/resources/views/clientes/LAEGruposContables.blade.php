@extends('plantillaHEAD')
@section('title')
Grupos de Servicios
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/LAEGruposContables.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorGruposContables@vistaLeerActualizarEliminarGC')}}','','width=900px, height=600px toolbar=yes');void 0">Ver Grupos de Clientes</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorGruposContables@vistaLeerActualizarEliminarGS')}}','','width=900px, height=600px toolbar=yes');void 0">Ver Grupos de Servicios</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorGruposContables@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        @if ($tipoGrupoContable=='GC')
        <h2>GRUPOS DE CLIENTES</h2>
        @else
        <h2>GRUPOS DE SERVICIOS</h2>
        @endif
        <hr>
        <input type="hidden" id="tipoGrupoContable" value="{{$tipoGrupoContable}}">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>CÓDIGO</th>
                    <th>DENOMINACIÓN</th>
                    <th>NÚMERO DE CUENTA</th>
                    <th>NOMBRE DE LA CUENTA</th>
                    @if ($tipoGrupoContable=='GC')
                    <th>CLIENTE RELACIONADO</th>
                    @endif
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach($gruposContables as $grupoContable)
                <tr>
                    <td>
                        {{$grupoContable->codigo}}
                    </td>
                    <td>
                        {{$grupoContable->denominacion}}
                    </td>
                    <td>
                        {{$grupoContable->numero_cuenta}}
                    </td>
                    <td>
                        {{$grupoContable->nombre_cuenta}}
                    </td>
                    @if ($tipoGrupoContable=='GC')
                    <td>
                        @if ($grupoContable->cliente_relacionado==1)
                        {{"Sí"}}
                        @else
                        {{"No"}}
                        @endif                            
                    </td>
                    @endif
                    <td>
                        <div class="btn-group btn-group-justified">
                            <button type="button" class="btn btn-default" name="modificarGrupoContable" id="{{$grupoContable->id}}" data-toggle="modal" data-target="#modalModificarGrupoContable">
                                <i class="fas fa-edit"></i>
                            </button>
                            <button type="button" class="btn btn-default" name="eliminarGrupoContable" id="{{$grupoContable->id}}" data-toggle="modal" data-target="#modalEliminarGrupoContable">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modalModificarGrupoContable" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <h2>GRUPOS CONTABLES</h2>
                    <hr>
                    <h3>Modificar Grupo Contable</h3>
                    <hr>
                    <input type="hidden" id="idM" value="-1">
                    <div>
                        <div class="form-group form-row">
                            <label for="codigoM" class="col-4 col-form-label">Codigo</label>
                            <div class="col-8">
                                <input type="text" name="codigoM" id="codigoM" class="form-control" placeholder="Codigo del Grupo" required disabled="">  
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="denominacionM" class="col-4 col-form-label">Denominación</label>
                            <div class="col-8">
                                <input type="text" name="denominacionM" id="denominacionM" class="form-control" placeholder="Denominación del Grupo" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="numero_cuentaM" class="col-4 col-form-label">Numero de la Cuenta</label>
                            <div class="col-8">
                                <input type="text" name="numero_cuentaM" id="numero_cuentaM" class="form-control" placeholder="Numero de Cuenta" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="nombre_cuentaM" class="col-4 col-form-label">Nombre de la Cuenta</label> 
                            <div class="col-8">
                                <input type="text" name="nombre_cuentaM" id="nombre_cuentaM" class="form-control" placeholder="Nombre de la cuenta" required>
                            </div>
                        </div>
                        @if ($tipoGrupoContable=='GC')
                        <div class="form-group form-row">
                            <label for="cliente_relacionadoM" class="col-4 col-form-label">Cliente Relacionado</label> 
                            <div class="col-8">
                                <input type="checkbox" name="cliente_relacionadoM" id="cliente_relacionadoM" class="form-control">
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" id="cModificarGrupoContable">Modificar Grupo Contable</button>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalEliminarGrupoContable" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Grupo Contable</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar grupo contable?
                <input type="hidden" id="idE" value="-1">
                <br>
                <div>
                    <div class="form-group form-row">
                        <label for="codigoE" class="col-5 col-form-label">Codigo</label>
                        <div class="col-7">
                            <input type="text" name="codigoE" id="codigoE" class="form-control" disabled="">  
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="denominacionE" class="col-5 col-form-label">Denominación</label>
                        <div class="col-7">
                            <input type="text" name="denominacionE" id="denominacionE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="numero_cuentaE" class="col-5 col-form-label">Numero de la Cuenta</label>
                        <div class="col-7">
                            <input type="text" name="numero_cuentaE" id="numero_cuentaE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="nombre_cuentaE" class="col-5 col-form-label">Nombre de la Cuenta</label> 
                        <div class="col-7">
                            <input type="text" name="nombre_cuentaE" id="nombre_cuentaE" class="form-control" disabled="">
                        </div>
                    </div>
                    @if ($tipoGrupoContable=='GC')
                    <div class="form-group form-row">
                        <label for="cliente_relacionadoE" class="col-5 col-form-label">Cliente Relacionado</label> 
                        <div class="col-7">
                            <input type="checkbox" name="cliente_relacionadoE" id="cliente_relacionadoE" class="form-control" disabled="">
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarGrupoContable">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection