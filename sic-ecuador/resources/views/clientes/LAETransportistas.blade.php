@extends('plantillaHEAD')
@section('title')
Transportistas
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/LAETransportistas.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorTransportistas@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorTransportistas@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>TRANSPORTISTAS</h2>
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>CÓDIGO</th>
                    <th>NOMBRE</th>
                    <th>RUC</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach($transportistas as $transportista)
                <tr>
                    <td>
                        {{$transportista->codigo}}
                    </td>
                    <td>
                        {{$transportista->nombre}}
                    </td>
                    <td>
                        {{$transportista->identificacion}}
                    </td>
                    <td>
                        <div class="btn-group btn-group-justified">
                            <button type="button" class="btn btn-default" name="modificarTransportista" id="{{$transportista->id}}" data-toggle="modal" data-target="#modalModificarTransportista">
                                <i class="fas fa-edit"></i>
                            </button>
                            <button type="button" class="btn btn-default" name="eliminarTransportista" id="{{$transportista->id}}" data-toggle="modal" data-target="#modalEliminarTransportista">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="modalModificarTransportista" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <h2>TRANSPORTISTAS</h2>
                    <hr>
                    <h3>Modificar Transportista</h3>
                    <hr>
                    <input type="hidden" id="idM" value="-1">
                    <div>
                        <div class="form-group form-row">
                            <label for="codigoM" class="col-2 col-form-label">Código</label>
                            <div class="col-10">
                                <input type="text" name="codigoM" id="codigoM" class="form-control" placeholder="Codigo del Transportista" required disabled="">
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="nombreM" class="col-2 col-form-label">Nombre</label>
                            <div class="col-10">
                                <input type="text" name="nombreM" id="nombreM" class="form-control" placeholder="Nombre del Transportista" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="identificacionM" class="col-2 col-form-label">RUC</label>
                            <div class="col-10">
                                <input type="text" name="identificacionM" id="identificacionM" class="form-control" placeholder="Nombre del Transportista" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" id="cModificarTransportista">Modificar Transportista</button>
            </div>
        </div>
        <div class="col-2"></div>
    </div>
</div>
<div class="modal fade" id="modalEliminarTransportista" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Retencion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar la retencion?
                <input type="hidden" id="idE" value="-1">
                <div>
                    <div class="form-group form-row">
                        <label for="codigoE" class="col-2 col-form-label">Código</label>
                        <div class="col-10">
                            <input type="text" name="codigoE" id="codigoE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="nombreE" class="col-2 col-form-label">Nombre</label>
                        <div class="col-10">
                            <input type="text" name="nombreE" id="nombreE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="identificacionE" class="col-2 col-form-label">RUC</label>
                        <div class="col-10">
                            <input type="text" name="identificacionE" id="identificacionE" class="form-control" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarTransportista">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection
