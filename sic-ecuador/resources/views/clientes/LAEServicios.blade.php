@extends('plantillaHEAD')
@section('title')
Servicios a Facturar
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/LAEServicios.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorServicios@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorServicios@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>SERVICIOS</h2>
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>CODIGO</th>
                    <th>NOMBRE</th>
                    <th>GRUPO CONTABLE</th>
                    <th>IMPUESTO</th>
                    <th>TIPO DE SERVICIO</th>
                    <th>PRECIO 1</th>
                    <th>PRECIO 2</th>
                    <th>PRECIO 3</th>
                    <th>PRECIO 4</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach($servicios as $servicio)
                <tr>
                    <td>
                        {{$servicio->codigo}}
                    </td>
                    <td>
                        {{$servicio->nombre}}
                    </td>
                    <td>
                        {{$servicio->grupo_contable->denominacion}}
                    </td>
                    <td>
                        {{$servicio->impuesto->codigo_norma}}
                    </td>
                    <td>
                        {{$servicio->tipo->abreviatura}}
                    </td>
                    <td>
                        {{$servicio->precio1}}
                    </td>
                    <td>
                        {{$servicio->precio2}}
                    </td>
                    <td>
                        {{$servicio->precio3}}
                    </td>
                    <td>
                        {{$servicio->precio4}}
                    </td>
                    <td>
                        <div class="btn-group btn-group-justified">
                            <button type="button" class="btn btn-default" name="modificarServicio" id="{{$servicio->id}}" data-toggle="modal" data-target="#modalModificarServicio">
                                <i class="fas fa-edit"></i>
                            </button>
                            <button type="button" class="btn btn-default" name="eliminarServicio" id="{{$servicio->id}}" data-toggle="modal" data-target="#modalEliminarServicio">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modalModificarServicio" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <h2>SERVICIOS</h2>
                    <hr>
                    <h3>Modificar Servicio</h3>
                    <hr>
                    <input type="hidden" id="idM" value="-1">
                    <div>
                        <div class="form-group form-row">
                            <label for="codigoM" class="col-4 col-form-label">Codigo</label>
                            <div class="col-8">
                                <input type="text" name="codigoM" id="codigoM" class="form-control" placeholder="Codigo" required disabled="">
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="nombreM" class="col-4 col-form-label">Nombre</label>
                            <div class="col-8">
                                <input type="text" name="nombreM" id="nombreM" class="form-control" placeholder="Nombre del Servicio" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="grupo_contable_idM" class="col-4 col-form-label">Grupo Servicio</label>
                            <div class="col-8">
                                <select name="grupo_contable_idM" id="grupo_contable_idM" class="form-control" required>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="impuesto_idM" class="col-4 col-form-label">Tipo de IVA</label>
                            <div class="col-8">
                                <select name="impuesto_idM" id="impuesto_idM" class="form-control" required>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="tipo_idM" class="col-4 col-form-label">Tipo de Servicio</label>
                            <div class="col-8">
                                <select name="tipo_idM" id="tipo_idM" class="form-control" required>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-2">
                                <br><br><br>VALOR IVA
                                <br>TOTAL
                            </div>
                            <table class="col-10">
                                <tr>
                                    <td class="text-center">
                                        <label for="precio1M">Precio 1 </label>
                                        <input type="number" name="precio1M" id="precio1M" class="form-control" placeholder="Precio 1" required>
                                    </td>
                                    <td class="text-center">
                                        <label for="precio2M">Precio 2</label>
                                        <input type="number" name="precio2M" id="precio2M" class="form-control" placeholder="Precio 2" required>
                                    </td>
                                    <td class="text-center">
                                        <label for="precio3M">Precio 3</label>
                                        <input type="number" name="precio3M" id="precio3M" class="form-control" placeholder="Precio 3" required>
                                    </td>
                                    <td class="text-center">
                                        <label for="precio4M">Precio 4</label>
                                        <input type="number" name="precio4M" id="precio4M" class="form-control" placeholder="Precio 4" required>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <input type="number" name="iva1M" id="iva1M" class="form-control" disabled="">
                                    </td>
                                    <td class="text-center">
                                        <input type="number" name="iva2M" id="iva2M" class="form-control" disabled>
                                    </td>
                                    <td class="text-center">
                                        <input type="number" name="iva3M" id="iva3M" class="form-control" disabled>
                                    </td>
                                    <td class="text-center">
                                        <input type="text" name="iva4M" id="iva4M" class="form-control" disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <input type="number" name="total1M" id="total1M" class="form-control" disabled>
                                    </td>
                                    <td class="text-center">
                                        <input type="number" name="total2M" id="total2M" class="form-control" disabled>
                                    </td>
                                    <td class="text-center">
                                        <input type="number" name="total3M" id="total3M" class="form-control" disabled>
                                    </td>
                                    <td class="text-center">
                                        <input type="text" name="total4M" id="total4M" class="form-control" disabled>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" id="cModificarServicio">Modificar Servicio</button>
            </div>
        </div>
        <div class="col-2"></div>
    </div>
</div>
<div class="modal fade" id="modalEliminarServicio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Servicio</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar el servicio?
                <input type="hidden" id="idE" value="-1">
                <div>
                    <div class="form-group form-row">
                        <label for="codigoE" class="col-4">Codigo</label>
                        <div class="col-8">
                            <input type="text" name="codigoE" id="codigoE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="nombreE" class="col-4">Nombre</label>
                        <div class="col-8">
                            <input type="text" name="nombreE" id="nombreE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="grupo_contable_idE" class="col-4">Grupo Servicio</label>
                        <div class="col-8">
                            <input type="text" name="grupo_contable_idE" id="grupo_contable_idE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="impuesto_idE" class="col-4">Tipo de IVA</label>
                        <div class="col-8">
                            <input type="text" name="impuesto_idE" id="impuesto_idE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="tipo_idE" class="col-4">Tipo de Servicio</label>
                        <div class="col-8">
                            <input type="text" name="tipo_idE" id="tipo_idE" class="form-control" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarServicio">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection
