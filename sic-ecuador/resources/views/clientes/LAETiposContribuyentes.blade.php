@extends('plantillaHEAD')
@section('title')
Tipos de Contribuyentes
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/LAETiposContribuyentes.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorTiposContribuyentes@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorTiposContribuyentes@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>TIPOS DE CONTRIBUYENTES</h2>
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>CÓDIGO</th>
                    <th>TIPO</th>
                    <th>SUBTIPO</th>
                    <th>CONTRIBUYENTE ESPECIAL</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach($tiposContribuyentes as $tipoContribuyente)
                <tr>
                    <td>
                        {{$tipoContribuyente->codigo}}
                    </td>
                    <td>
                        {{$tipoContribuyente->tipo}}
                    </td>
                    <td>
                        {{$tipoContribuyente->subtipo}}
                    </td>
                    <td>
                        @if ($tipoContribuyente->especial==1)
                        {{"Sí"}}
                        @else
                        {{"No"}}
                        @endif  
                    </td>
                    <td>
                        <div class="btn-group btn-group-justified">
                            <button type="button" class="btn btn-default" name="modificarTipoContribuyente" id="{{$tipoContribuyente->id}}" data-toggle="modal" data-target="#modalModificarTipoContribuyente">
                                <i class="fas fa-edit"></i>
                            </button>
                            <button type="button" class="btn btn-default" name="eliminarTipoContribuyente" id="{{$tipoContribuyente->id}}" data-toggle="modal" data-target="#modalEliminarTipoContribuyente">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modalModificarTipoContribuyente" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <h2>TIPOS DE CONTRIBUYENTE</h2>
                    <hr>
                    <h3>Modificar Tipo de Contribuyente</h3>
                    <hr>
                    <input type="hidden" id="idM" value="-1">
                    <div>
                        <div class="form-group form-row">
                            <label for="codigoM" class="col-4 col-form-label">Codigo</label>
                            <div class="col-8">
                                <input type="text" name="codigoM" id="codigoM" class="form-control" placeholder="Codigo" required disabled="">  
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="tipoM" class="col-4 col-form-label">Tipo</label>
                            <div class="col-8">
                                <input type="text" name="tipoM" id="tipoM" class="form-control" placeholder="Tipo de Contribuyente" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="subtipoM" class="col-4 col-form-label">Subtipo</label>
                            <div class="col-8">
                                <input type="text" name="subtipoM" id="subtipoM" class="form-control" placeholder="Nombre del Dato Adicional" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="especialM" class="col-4 col-form-label">Contribuyente Especial</label> 
                            <div class="col-8">
                                <input type="checkbox" name="especialM" id="especialM" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" id="cModificarTipoContribuyente">Modificar Tipo de Contribuyente</button>
            </div>
        </div>
        <div class="col-2"></div>
    </div>
</div>
<div class="modal fade" id="modalEliminarTipoContribuyente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Tipo Contribuyente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar el tipo de contribuyente?
                <br>
                <br>
                <input type="hidden" id="idE" value="-1">
                <div>
                    <div class="form-group form-row">
                        <label for="codigoE" class="col-5 col-form-label">Codigo</label>
                        <div class="col-7">
                            <input type="text" name="codigoE" id="codigoE" class="form-control" disabled="">  
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="tipoE" class="col-5 col-form-label">Tipo</label>
                        <div class="col-7">
                            <input type="text" name="tipoE" id="tipoE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="subtipoE" class="col-5 col-form-label">Subtipo</label>
                        <div class="col-7">
                            <input type="text" name="subtipoE" id="subtipoE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="especialE" class="col-5 col-form-label">Contribuyente Especial</label> 
                        <div class="col-7">
                            <input type="checkbox" name="especialE" id="especialE" class="form-control" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarTipoContribuyente">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection

