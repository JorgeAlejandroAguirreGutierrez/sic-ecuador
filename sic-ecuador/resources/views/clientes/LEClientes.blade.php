@extends('plantillaHEAD')
@section('title')
Index
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/LEClientes.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorClientes@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0">Crear Cliente</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorAuxiliaresCF@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0">Crear Cliente Auxiliar CF</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorClientes@vistaAsignarClientesAuxiliares')}}','','width=900px, height=600px toolbar=yes');void 0">Asignar Clientes Auxiliares</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-10">
        <h2>CLIENTES</h2>
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">CÓDIGO</th>
                    <th scope="col">IDENTIFICACIÓN</th>
                    <th scope="col">RAZON SOCIAL</th>
                    <th scope="col">DIRECCIÓN</th>
                    <th scope="col">UBICACION</th>
                    <th scope="col">MAPA</th>
                    <th scope="col">TELEFONO</th>
                    <th scope="col">CELULAR</th>
                    <th scope="col">CORREO</th>
                    <th scope="col">OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach($clientes as $cliente)
                <tr>
                    <td>
                        {{$cliente->codigo}}
                    </td>
                    <td>
                        {{$cliente->identificacion}}
                    </td>
                    <td>
                        {{$cliente->razon_social}}
                    </td>
                    <td>
                        {{$cliente->direccion}}
                    </td>
                    <td>
                        {{$cliente->ubicaciongeo->canton}}
                    </td>
                    <td>
                    </td>
                    <td>
                        @foreach($cliente->telefonos as $telefono)
                        {{$telefono->numero}}
                        @endforeach
                    </td>
                    <td>
                        @foreach($cliente->celulares as $celular)
                        {{$celular->numero}}
                        @endforeach
                    </td>
                    <td>
                        @foreach($cliente->correos as $correo)
                        {{$correo->email}}
                        @endforeach
                    </td>
                    <td>
                        <div class="btn-group btn-group-justified">
                            <button type="button" class="btn btn-default" name="actualizar" id="{{$cliente->id}}">
                                <i class="fas fa-edit"></i>
                            </button>
                            <button type="button" class="btn btn-default" name="eliminar">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
