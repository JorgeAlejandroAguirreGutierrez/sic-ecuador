@extends('plantillaHEAD')
@section('title')
Auxiliares
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/CLEAuxiliares.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a title="Mostrar Auxiliares" data-toggle="tooltip" data-placement="bottom" class="nav-link" href="javascript:window.open('{{action('clientes\ControladorClientes@vistaLeerActualizarEliminarClientesAuxiliares')}}','','width=900px, height=600px toolbar=yes');void 0"><i class="fas fa-stream fa-lg"></i></a>
            </li>
            <li class="nav-item">
                <a title="Asignar un Cliente Auxiliar" data-toggle="tooltip" data-placement="bottom" class="nav-link" href="javascript:window.open('{{action('clientes\ControladorClientes@vistaAsignarClientesAuxiliares')}}','','width=900px, height=600px toolbar=yes');void 0"><i class="fas fa-arrow-right fa-lg"></i></a>
            </li>
            
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>CLIENTE AUXILIAR</h2>
        <hr>
        <form>
            <div class="form-group form-row">
                <label for="razon_socialB" class="col-2 col-form-label">Nombre</label>
                <div class="col-10">
                    <input type="text" name="razon_socialB" id="razon_socialB" class="form-control" disabled="" value="{{$auxiliares->razon_social}}">
                </div>
            </div>
            <div class="form-group form-row">
                <label for="identificacionB" class="col-2 col-form-label">Identificacion</label>
                <div class="col-10">
                    <input type="text" name="identificacionB" id="identificacionB" class="form-control" disabled="" value="{{$auxiliares->identificacion}}">
                </div>
            </div>
        </form>
        <h3>AUXILIARES</h3>
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>RAZON SOCIAL</th>
                    <th>DIRECCION</th>
                    <th>PROVINCIA</th>
                    <th>CANTON</th>
                    <th>PARROQUIA</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach($auxiliares->auxiliares as $auxiliar)
                <tr>
                    <td>
                        {{$auxiliar->razon_social}}
                    </td>
                    <td>
                        {{$auxiliar->direccion}}
                    </td>
                    <td>
                        {{$auxiliar->ubicaciongeo->provincia}}
                    </td>
                    <td>
                        {{$auxiliar->ubicaciongeo->canton}}
                    </td>
                    <td>
                        {{$auxiliar->ubicaciongeo->parroquia}}
                    </td>
                    <td>
                        <div class="btn-group btn-group-justified">
                            <button type="button" class="btn btn-default" name="eliminarAuxiliar" id="{{$auxiliar->id}}" data-toggle="modal" data-target="#modalEliminarAuxiliar">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="form-group text-center">
            <button class="btn btn-success" data-toggle="modal" data-target="#modalCrearAuxiliar">Crear Auxiliar</button>
        </div>
    </div>
</div>

<div class="modal fade" id="modalCrearAuxiliar" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <h2>CREAR AUXILIAR</h2>
                    <hr>
                    <input type="hidden" id="cliente_id" value="{{$auxiliares->id}}">
                    <div>
                        <div class="form-group form-row">
                            <label for="razon_social" class="col-2 col-form-label">Nombre</label>
                            <div class="col-10">
                                <input type="text" name="razon_social" id="razon_social" class="form-control" required>  
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="direccion" class="col-2 col-form-label">Direccion</label>
                            <div class="col-10">
                                <input type="text" name="direccion" id="direccion" class="form-control" required>  
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="correo" class="col-2 col-form-label">Correo</label>
                            <div class="col-8" id="correos">
                                <input type="email" name="correo" id="correo0" class="form-control" required="">
                            </div>
                            <div class="col-2" id="correos">
                                <button type="button" class="btn btn-default" name="masCorreo" id="masCorreo">
                                    <i class="fas fa-plus-circle"></i>
                                </button>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="telefono" class="col-2 col-form-label">Telefono</label>
                            <div class="col-8" id="telefonos">
                                <input type="tel" name="telefono" id="telefono0" class="form-control" required="">
                            </div>
                            <div class="col-2">
                                <button type="button" class="btn btn-default" name="masTelefono" id="masTelefono">
                                    <i class="fas fa-plus-circle"></i>
                                </button>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="celular" class="col-2 col-form-label">Celular</label>
                            <div class="col-8" id="celulares">
                                <input type="tel" name="celular" id="celular0" class="form-control" required="">
                            </div>
                            <div class="col-2">
                                <button type="button" class="btn btn-default" name="masCelular" id="masCelular">
                                    <i class="fas fa-plus-circle"></i>
                                </button>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="provincia" class="col-2 col-form-label">Provincia</label> 
                            <div class="col-10">
                                <select name="provincia" id="provincia" class="form-control" required>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="canton" class="col-2 col-form-label">Canton</label>
                            <div class="col-10">
                                <select name="canton" id="canton" class="form-control" required>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="parroquia" class="col-2 col-form-label">Parroquia</label>
                            <div class="col-10">
                                <select name="parroquia" id="parroquia" class="form-control" required>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" id="crearAuxiliar">Crear Auxiliar</button>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEliminarAuxiliar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Auxiliar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar el auxiliar?
                <input type="hidden" id="idE" value="-1">
                <div>
                    <div class="form-group form-row">
                        <label for="razon_socialE" class="col-2 col-form-label">Nombre</label>
                        <div class="col-10">
                            <input type="text" name="razon_socialE" id="razon_socialE" class="form-control" disabled="">  
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="direccionE" class="col-2 col-form-label">Direccion</label>
                        <div class="col-10">
                            <input type="text" name="direccionE" id="direccionE" class="form-control" disabled="">  
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="provinciaE" class="col-2 col-form-label">Provincia</label>
                        <div class="col-10">
                            <input type="text" name="provinciaE" id="provinciaE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="cantonE" class="col-2 col-form-label">Canton</label>
                        <div class="col-10">
                            <input type="text" name="cantonE" id="cantonE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="parroquiaE" class="col-2 col-form-label">Canton</label>
                        <div class="col-10">
                            <input type="text" name="parroquiaE" id="parroquiaE" class="form-control" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarAuxiliar">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection
