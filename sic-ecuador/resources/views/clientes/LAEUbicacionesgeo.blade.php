@extends('plantillaHEAD')
@section('title')
Ubicaciones Geograficas
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/LAEUbicacionesgeo.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorUbicacionesgeo@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorUbicacionesgeo@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>UBICACIONES GEOGRAFICAS</h2>
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>CODIGO</th>
                    <th>PROVINCIA</th>
                    <th>CANTON</th>
                    <th>PARROQUIA</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach($ubicacionesgeo as $ubicaciongeo)
                <tr>
                    <td>
                        {{$ubicaciongeo->codigo}}
                    </td>
                    <td>
                        {{$ubicaciongeo->provincia}}
                    </td>
                    <td>
                        {{$ubicaciongeo->canton}}
                    </td>
                    <td>
                        {{$ubicaciongeo->parroquia}}
                    </td>
                    <td>
                        <div class="btn-group btn-group-justified">
                            <button type="button" class="btn btn-default" name="modificarUbicaciongeo" id="{{$ubicaciongeo->id}}" data-toggle="modal" data-target="#modalModificarUbicaciongeo">
                                <i class="fas fa-edit"></i>
                            </button>
                            <button type="button" class="btn btn-default" name="eliminarUbicaciongeo" id="{{$ubicaciongeo->id}}" data-toggle="modal" data-target="#modalEliminarUbicaciongeo">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modalModificarUbicaciongeo" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <h2>UBICACIONES GEOGRAFICAS</h2>
                    <hr>
                    <h3>Modificar Ubicacion Geografica</h3>
                    <hr>
                    <input type="hidden" id="idM" value="-1">
                    <div>
                        <div class="form-group form-row">
                            <label for="codigoM" class="col-2 col-form-label">Código</label>
                            <div class="col-10">
                                <input type="text" name="codigoM" id="codigoM" class="form-control" placeholder="Codigo Ubicación Geografica" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="provinciaM" class="col-2 col-form-label">Provincia</label> 
                            <div class="col-10">
                                <input type="text" name="provinciaM" id="provinciaM" class="form-control" placeholder="Provincia" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="cantonM" class="col-2 col-form-label">Canton</label>
                            <div class="col-10">
                                <input type="text" name="cantonM" id="cantonM" class="form-control" placeholder="Canton" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="parroquiaM" class="col-2 col-form-label">Parroquia</label>
                            <div class="col-10">
                                <input type="text" name="parroquiaM" id="parroquiaM" class="form-control" placeholder="parroquia" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" id="cModificarUbicaciongeo">Modificar Ubicacion</button>
            </div>
        </div>
        <div class="col-2"></div>
    </div>
</div>
<div class="modal fade" id="modalEliminarUbicaciongeo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Ubicacion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar la ubicacion geografica?
                <input type="hidden" id="idE" value="-1">
                <br>
                <br>
                <div>
                    <div class="form-group form-row">
                        <label for="codigoE" class="col-2 col-form-label">Código</label>
                        <div class="col-10">
                            <input type="text" name="codigoE" id="codigoE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="provinciaE" class="col-2 col-form-label">Provincia</label> 
                        <div class="col-10">
                            <input type="text" name="provinciaE" id="provinciaE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="cantonE" class="col-2 col-form-label">Canton</label>
                        <div class="col-10">
                            <input type="text" name="cantonE" id="cantonE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="parroquiaE" class="col-2 col-form-label">Parroquia</label>
                        <div class="col-10">
                            <input type="text" name="parroquiaE" id="parroquiaE" class="form-control" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarUbicaciongeo">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection
