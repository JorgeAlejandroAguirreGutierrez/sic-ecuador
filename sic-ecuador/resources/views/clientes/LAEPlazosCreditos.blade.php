@extends('plantillaHEAD')
@section('title')
Plazos de Credito
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/LAEPlazosCreditos.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorPlazosCreditos@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorPlazosCreditos@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>PLAZOS DE CREDITOS</h2>
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>CODIGO</th>
                    <th>PLAZO</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach($plazosCreditos as $plazoCredito)
                <tr>
                    <td>
                        {{$plazoCredito->codigo}}
                    </td>
                    <td>
                        {{$plazoCredito->plazo}}
                    </td>
                    <td>
                        <div class="btn-group btn-group-justified">
                            <button type="button" class="btn btn-default" name="modificarPlazoCredito" id="{{$plazoCredito->id}}" data-toggle="modal" data-target="#modalModificarPlazoCredito">
                                <i class="fas fa-edit"></i>
                            </button>
                            <button type="button" class="btn btn-default" name="eliminarPlazoCredito" id="{{$plazoCredito->id}}" data-toggle="modal" data-target="#modalEliminarPlazoCredito">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modalModificarPlazoCredito" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <h2>PLAZOS DE CREDITOS</h2>
                    <hr>
                    <h3>Modificar Plazo de Credito</h3>
                    <hr>
                    <input type="hidden" id="idM" value="-1">
                    <div>
                        <div class="form-group form-row">
                            <label for="codigoM" class="col-2 col-form-label">Codigo</label>
                            <div class="col-10">
                                <input type="text" name="codigoM" id="codigoM" class="form-control" placeholder="Codigo del Plazo" required disabled="">
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="plazoM" class="col-2 col-form-label">Plazo</label>
                            <div class="col-10">
                                <input type="number" name="plazoM" id="plazoM" class="form-control" placeholder="Numero de Plazo" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" id="cModificarPlazoCredito">Modificar Plazo de Credito</button>
            </div>
        </div>
        <div class="col-2"></div>
    </div>
</div>
<div class="modal fade" id="modalEliminarPlazoCredito" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Plazo de Credito</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar el plazo de credito?
                <br>
                <input type="hidden" id="idE" value="-1">
                <div>
                    <div class="form-group form-row">
                        <label for="codigoE" class="col-2 col-form-label">Codigo</label>
                        <div class="col-10">
                            <input type="text" name="codigoE" id="codigoE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="plazoE" class="col-2 col-form-label">Plazo</label>
                        <div class="col-10">
                            <input type="number" name="plazoE" id="plazoE" class="form-control" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarPlazoCredito">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection
