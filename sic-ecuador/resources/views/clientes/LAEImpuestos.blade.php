@extends('plantillaHEAD')
@section('title')
Impuestos
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/LAEImpuestos.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorImpuestos@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0">Ver Impuestos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorImpuestos@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0">Crear Impuestos</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>CODIGO</th>
                    <th>CODIGO NORMA</th>
                    <th>PORCENTAJE</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach($impuestos as $impuesto)
                <tr>
                    <td>
                        {{$impuesto->codigo}}
                    </td>
                    <td>
                        {{$impuesto->codigo_norma}}
                    </td>
                    <td>
                        {{$impuesto->porcentaje}}
                    </td>
                    <td>
                        <div class="btn-group btn-group-justified">
                            <button type="button" class="btn btn-default" name="modificarImpuesto" id="{{$impuesto->id}}" data-toggle="modal" data-target="#modalModificarImpuesto">
                                <i class="fas fa-edit"></i>
                            </button>
                            <button type="button" class="btn btn-default" name="eliminarImpuesto" id="{{$impuesto->id}}" data-toggle="modal" data-target="#modalEliminarImpuesto">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="modalModificarImpuesto" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <h2>IMPUESTOS</h2>
                    <hr>
                    <h3>Modificar Impuesto</h3>
                    <hr>
                    <input type="hidden" id="idM" value="-1">
                    <div>
                        <div class="form-group form-row">
                            <label for="codigoM" class="col-3 col-form-label">Codigo</label>
                            <div class="col-9">
                                <input type="text" name="codigoM" id="codigoM" class="form-control" disabled="">
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="codigo_normaM" class="col-3 col-form-label">Codigo Norma</label>
                            <div class="col-9">
                                <input type="text" name="codigo_normaM" id="codigo_normaM" class="form-control" placeholder="Codigo de la norma" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="porcentajeM" class="col-3 col-form-label">Porcentaje</label>
                            <div class="col-9">
                                <input type="number" name="porcentajeM" id="porcentajeM" class="form-control" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" id="cModificarImpuesto">Modificar Impuesto</button>
            </div>
        </div>
        <div class="col-2"></div>
    </div>
</div>
<div class="modal fade" id="modalEliminarImpuesto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Impuesto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar el impuesto?
                <br>
                <br>
                <input type="hidden" id="idE" value="-1">
                <div>
                    <div class="form-group form-row">
                        <label for="codigoE" class="col-3 col-form-label">Codigo</label>
                        <div class="col-9">
                            <input type="text" name="codigoE" id="codigoE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="codigo_normaE" class="col-3 col-form-label">Codigo Norma</label>
                        <div class="col-9">
                            <input type="text" name="codigo_normaE" id="codigo_normaE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="porcentajeE" class="col-3 col-form-label">Porcentaje</label>
                        <div class="col-9">
                            <input type="number" name="porcentajeE" id="porcentajeE" class="form-control" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarImpuesto">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection
