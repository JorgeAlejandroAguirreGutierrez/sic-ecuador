@extends('plantillaHEAD')
@section('title')
Crear Vehiculos para Transportes
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/CVehiculosTransportes.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorVehiculosTransportes@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorVehiculosTransportes@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>VEHICULOS PARA TRANSPORTES</h2>
        <hr>
        <h3>Crear Vehiculo para Transporte</h3>
        <hr>
        <div>
            <div class="form-group form-row">
                <label for="codigo" class="col-3 col-form-label">Codigo</label>
                <div class="col-9">
                    <input type="text" name="codigo" id="codigo" class="form-control" placeholder="Codigo del vehiculo" required disabled="">
                </div>
            </div>
            <div class="form-group form-row">
                <label for="modelo" class="col-3 col-form-label">Modelo</label>
                <div class="col-9">
                    <input type="number" value="1900" name="modelo" id="modelo" class="form-control" placeholder="Modelo del Vehiculo" required>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="placa" class="col-3 col-form-label">Placa</label>
                <div class="col-9">
                    <input type="text" name="placa" id="placa" class="form-control" placeholder="Placa del Vehiculo" required> 
                </div>
            </div>
            <div class="form-group form-row">
                <label for="marca" class="col-3 col-form-label">Marca</label>
                <div class="col-9">
                    <input type="text" name="marca" id="marca" class="form-control" placeholder="Marca del carro" required>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="cilindraje" class="col-3 col-form-label">Cilindraje</label>
                <div class="col-9">
                    <input type="number" value="0" name="cilindraje" id="cilindraje" class="form-control" required>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="clase" class="col-3 col-form-label">Clase</label>
                <div class="col-9">
                    <input type="text" name="clase" id="clase" class="form-control" placeholder="Clase del Vehiculo">
                </div>
            </div>
            <div class="form-group form-row">
                <label for="color" class="col-3 col-form-label">Color</label>
                <div class="col-9">
                    <input type="text" name="color" id="color" class="form-control" placeholder="Color del Vehiculo" required="">
                </div>
            </div>
            <div class="form-group form-row">
                <label for="fabricacion" class="col-3 col-form-label">Año de Fabricación</label>
                <div class="col-9">
                    <input type="number" value="1900" name="fabricacion" id="fabricacion" class="form-control" placeholder="Año de Fabricacion" required="">
                </div>
            </div>
            <div class="form-group form-row">
                <label for="activo" class="col-3 col-form-label">Activo</label>
                <div class="col-9">
                    <input type="checkbox" name="activo" id="activo" class="form-control">
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-success" id="crearVehiculoTransporte">Crear Vehiculo de Transporte</button>
            </div>
            <div id="alerta" role="alert">
            </div>
        </div>
    </div>
</div>
@endsection
