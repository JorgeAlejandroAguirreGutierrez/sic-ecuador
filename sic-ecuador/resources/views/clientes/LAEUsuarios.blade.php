@extends('plantillaHEAD')
@section('title')
Mostrar Usuarios
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/LAEUsuarios.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorUsuarios@vistaMostrar')}}','','width=900px, height=600px toolbar=yes');void 0">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorUsuarios@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>USUARIOS</h2>
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>CÓDIGO</th>
                    <th>NOMBRE</th>
                    <th>IDENTIFICACION</th>
                    <th>TIPO</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach($usuarios as $usuario)
                <tr>
                    <td>
                        {{$usuario->codigo}}
                    </td>
                    <td>
                        {{$usuario->nombre}}
                    </td>
                    <td>
                        {{$usuario->identificacion}}
                    </td>
                    <td>
                        {{$usuario->tipo}}
                    </td>
                    <td>
                        <div class="btn-group btn-group-justified">
                            <button type="button" class="btn btn-default" name="eliminarUsuario" id="{{$usuario->id}}" data-toggle="modal" data-target="#modalEliminarUsuario">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modalEliminarDatoAdicional" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar el usuario?
                <input type="hidden" id="idE" value="-1">
                <br>
                <br>
                <div>
                    <div class="form-group form-row">
                        <label for="codigoE" class="col-3 col-form-label">Codigo</label>
                        <div class="col-9">
                            <input type="text" name="codigoE" id="codigoE" class="form-control" disabled="">  
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="nombreE" class="col-3 col-form-label">Nombre</label>
                        <div class="col-9">
                            <input type="text" name="nombreE" id="nombreE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="abreviaturaE" class="col-3 col-form-label">Identificacion</label> 
                        <div class="col-9">
                            <input type="text" name="abreviaturaE" id="abreviaturaE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="tipoE" class="col-3 col-form-label">Tipo</label> 
                        <div class="col-9">
                            <input type="text" name="tipoE" id="tipoE" class="form-control" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarUsuario">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection
