@extends('plantillaHEAD')
@section('title')
Datos Adicionales
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/LAEDatosAdicionales.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorDatosAdicionales@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorDatosAdicionales@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>DATOS ADICIONALES</h2>
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>CÓDIGO</th>
                    <th>TIPO</th>
                    <th>NOMBRE</th>
                    <th>ABREVIATURA</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach($datosAdicionales as $datoAdicional)
                <tr>
                    <td>
                        {{$datoAdicional->codigo}}
                    </td>
                    <td>
                        {{$datoAdicional->tipo}}
                    </td>
                    <td>
                        {{$datoAdicional->nombre}}
                    </td>
                    <td>
                        {{$datoAdicional->abreviatura}}
                    </td>
                    <td>
                        <div class="btn-group btn-group-justified">
                            <button type="button" class="btn btn-default" name="modificarDatoAdicional" id="{{$datoAdicional->id}}" data-toggle="modal" data-target="#modalModificarDatoAdicional">
                                <i class="fas fa-edit"></i>
                            </button>
                            <button type="button" class="btn btn-default" name="eliminarDatoAdicional" id="{{$datoAdicional->id}}" data-toggle="modal" data-target="#modalEliminarDatoAdicional">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modalModificarDatoAdicional" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <h2>DATOS ADICIONALES</h2>
                    <hr>
                    <h3>Modificar Dato Adicional</h3>
                    <hr>
                    <input type="hidden" id="idM" value="-1">
                    <div>
                        <div class="form-group form-row">
                            <label for="codigoM" class="col-2 col-form-label">Codigo</label>
                            <div class="col-10">
                                <input type="text" name="codigoM" id="codigoM" class="form-control" placeholder="Codigo" required disabled="">  
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="tipoM" class="col-2 col-form-label">Tipo</label>
                            <div class="col-10">
                                <input type="textM" name="tipo" id="tipoM" class="form-control" placeholder="Tipo de Dato Adicional" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="nombreM" class="col-2 col-form-label">Nombre</label>
                            <div class="col-10">
                                <input type="text" name="nombreM" id="nombreM" class="form-control" placeholder="Nombre del Dato Adicional" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="abreviaturaM" class="col-2 col-form-label">Abreviatura</label> 
                            <div class="col-10">
                                <input type="text" name="abreviaturaM" id="abreviaturaM" class="form-control" placeholder="Abreviatura para el Dato Adicional" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" id="cModificarDatoAdicional">Modificar Dato Adicional</button>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEliminarDatoAdicional" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Retencion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar el dato adicional?
                <input type="hidden" id="idE" value="-1">
                <br>
                <br>
                <div>
                    <div class="form-group form-row">
                        <label for="codigoE" class="col-3 col-form-label">Codigo</label>
                        <div class="col-9">
                            <input type="text" name="codigoE" id="codigoE" class="form-control" disabled="">  
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="tipoE" class="col-3 col-form-label">Tipo</label>
                        <div class="col-9">
                            <input type="text" name="tipoE" id="tipoE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="nombreE" class="col-3 col-form-label">Nombre</label>
                        <div class="col-9">
                            <input type="text" name="nombreE" id="nombreE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="abreviaturaE" class="col-3 col-form-label">Abreviatura</label> 
                        <div class="col-9">
                            <input type="text" name="abreviaturaE" id="abreviaturaE" class="form-control" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarDatoAdicional">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection
