@extends('plantillaHEAD')
@section('title')
Crear Transportistas
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/CTransportistas.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorTransportistas@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorTransportistas@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>TRANSPORTISTAS</h2>
        <hr>
        <h3>Crear Transportista</h3>
        <hr>
        <div>
            <div class="form-group form-row">
                <label for="codigo" class="col-3 col-form-label">Código</label>
                <div class="col-9">
                    <input type="text" name="codigo" id="codigo" class="form-control" placeholder="Codigo del Transportista" required disabled="">
                </div>
            </div>
            <div class="form-group form-row">
                <label for="nombre" class="col-3 col-form-label">Nombre</label>
                <div class="col-9">
                    <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre del Transportista" required>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="identificacion" class="col-3 col-form-label">RUC</label>
                <div class="col-9">
                    <input type="text" name="identificacion" id="identificacion" class="form-control" placeholder="Identificacion del Transportista" required>
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-success" id="crearTransportista">Crear Transportista</button>
            </div>
            <div id="alerta" role="alert">
            </div>
        </div>
    </div>
</div>
@endsection
