@extends('plantillaHEAD')
@section('title')
Vehiculos para Transportes
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/LAEVehiculosTransportes.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorVehiculosTransportes@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorVehiculosTransportes@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>VEHICULOS PARA TRANSPORTES</h2>
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>CÓDIGO</th>
                    <th>MODELO</th>
                    <th>PLACA</th>
                    <th>MARCA</th>
                    <th>CILINDRAJE</th>
                    <th>CLASE</th>
                    <th>ACTIVO</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach($vehiculosTransportes as $vehiculoTransporte)
                <tr>
                    <td>
                        {{$vehiculoTransporte->codigo}}
                    </td>
                    <td>
                        {{$vehiculoTransporte->modelo}}
                    </td>
                    <td>
                        {{$vehiculoTransporte->placa}}
                    </td>
                    <td>
                        {{$vehiculoTransporte->marca}}
                    </td>
                    <td>
                        {{$vehiculoTransporte->cilindraje}}
                    </td>
                    <td>
                        {{$vehiculoTransporte->clase}}
                    </td>
                    <td>
                        @if ($vehiculoTransporte->activo==1)
                        {{"Sí"}}
                        @else
                        {{"No"}}
                        @endif                            
                    </td>
                    <td>
                        <div class="btn-group btn-group-justified">
                            <button type="button" class="btn btn-default" name="modificarVehiculoTransporte" id="{{$vehiculoTransporte->id}}" data-toggle="modal" data-target="#modalModificarVehiculoTransporte">
                                <i class="fas fa-edit"></i>
                            </button>
                            <button type="button" class="btn btn-default" name="eliminarVehiculoTransporte" id="{{$vehiculoTransporte->id}}" data-toggle="modal" data-target="#modalEliminarVehiculoTransporte">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modalModificarVehiculoTransporte" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <h2>VEHICULOS DE TRRANSPORTE</h2>
                    <hr>
                    <h3>Modificar Vehiculo de Transporte</h3>
                    <hr>
                    <input type="hidden" id="idM" value="-1">
                    <div>
                        <div class="form-group form-row">
                            <label for="codigoM" class="col-4 col-form-label">Codigo</label>
                            <div class="col-8">
                                <input type="text" name="codigoM" id="codigoM" class="form-control" placeholder="Codigo del vehiculo" required disabled="">
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="modeloM" class="col-4 col-form-label">Modelo</label>
                            <div class="col-8">
                                <input type="number" value="1900" name="modeloM" id="modeloM" class="form-control" placeholder="Modelo del Vehiculo" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="placaM" class="col-4 col-form-label">Placa</label>
                            <div class="col-8">
                                <input type="text" name="placaM" id="placaM" class="form-control" placeholder="Placa del Vehiculo" required> 
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="marcaM" class="col-4 col-form-label">Marca</label>
                            <div class="col-8">
                                <input type="text" name="marcaM" id="marcaM" class="form-control" placeholder="Marca del carro" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="cilindrajeM" class="col-4 col-form-label">Cilindraje</label>
                            <div class="col-8">
                                <input type="number" value="0" name="cilindrajeM" id="cilindrajeM" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="claseM" class="col-4 col-form-label">Clase</label>
                            <div class="col-8">
                                <input type="text" name="claseM" id="claseM" class="form-control" placeholder="Clase del Vehiculo">
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="colorM" class="col-4 col-form-label">Color</label>
                            <div class="col-8">
                                <input type="text" name="colorM" id="colorM" class="form-control" placeholder="Color del Vehiculo" required="">
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="fabricacionM" class="col-4 col-form-label">Año de Fabricación</label>
                            <div class="col-8">
                                <input type="number" value="1900" name="fabricacionM" id="fabricacionM" class="form-control" placeholder="Año de Fabricacion" required="">
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="activoM" class="col-4 col-form-label">Activo</label>
                            <div class="col-8">
                                <input type="checkbox" name="activoM" id="activoM" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" id="cModificarVehiculoTransporte">Modificar Vehiculo de Transporte</button>
            </div>
        </div>
        <div class="col-2"></div>
    </div>
</div>
<div class="modal fade" id="modalEliminarVehiculoTransporte" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Vehiculo de Transporte</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar el vehiculo de transporte?
                <input type="hidden" id="idE" value="-1">
                <div>
                    <div class="form-group form-row">
                        <label for="codigoE" class="col-4 col-form-label">Codigo</label>
                        <div class="col-8">
                            <input type="text" name="codigoE" id="codigoE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="modeloE" class="col-4 col-form-label">Modelo</label>
                        <div class="col-8">
                            <input type="number" value="1900" name="modeloE" id="modeloE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="placaE" class="col-4 col-form-label">Placa</label>
                        <div class="col-8">
                            <input type="text" name="placaE" id="placaE" class="form-control" disabled=""> 
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="marcaE" class="col-4 col-form-label">Marca</label>
                        <div class="col-8">
                            <input type="text" name="marcaE" id="marcaE" class="form-control" placeholder="Marca del carro" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="cilindrajeE" class="col-4 col-form-label">Cilindraje</label>
                        <div class="col-8">
                            <input type="number" value="0" name="cilindrajeE" id="cilindrajeE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="claseE" class="col-4 col-form-label">Clase</label>
                        <div class="col-8">
                            <input type="text" name="claseE" id="claseE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="colorE" class="col-4 col-form-label">Color</label>
                        <div class="col-8">
                            <input type="text" name="colorE" id="colorE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="fabricacionE" class="col-4 col-form-label">Año de Fabricación</label>
                        <div class="col-8">
                            <input type="number" value="1900" name="fabricacionE" id="fabricacionE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="activoE" class="col-4 col-form-label">Activo</label>
                        <div class="col-8">
                            <input type="checkbox" name="activoE" id="activoE" class="form-control" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarVehiculoTransporte">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection
