@extends('plantillaHEAD')
@section('title')
Crear Clientes Auxiliares para Consumidor Final
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/asignarClientesAuxiliares.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorClientes@vistaAsignarClientesAuxiliares')}}','','width=900px, height=600px toolbar=yes');void 0" target="_blank">Asignar un cliente Auxiliar</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorClientes@vistaLeerActualizarEliminarClientesAuxiliares')}}','','width=900px, height=600px toolbar=yes');void 0" target="_blank">Mostrar Clientes Auxiliares</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>CLIENTES AUXILIARES</h2>
        <hr>
        <h3>Buscar Cliente Para asignar como Auxiliar</h3>
        <hr>
        <div>
            <div class="form-group form-row">
                <label for="razon_socialB" class="col-2 col-form-label">Cliente</label>
                <div class="col-10">
                    <select name="razon_socialB" id="razon_socialB" class="form-control">
                    </select>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="identificacionB" class="col-2 col-form-label">Identificacion</label>
                <div class="col-10">
                    <select name="identificacionB" id="identificacionB" class="form-control">
                    </select>
                </div>
            </div>
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>CODIGO</th>
                    <th>RAZON SOCIAL</th>
                    <th>IDENTIFICACION</th>
                    <th>DIRECCION</th>
                    <th>PROVINCIA</th>
                    <th>CANTON</th>
                    <th>PARROQUIA</th>
                </tr>
            </thead>
            <tbody id="clienteAuxiliar">
            </tbody>
        </table>
        <div class="form-group text-center">
            <button class="btn btn-success" id="asignarClienteAuxiliar">Asignar como Cliente Auxiliar</button>
        </div>
    </div>
</div>
@endsection
