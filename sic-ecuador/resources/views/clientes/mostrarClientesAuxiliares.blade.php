@extends('plantillaHEAD')
@section('title')
Clientes Auxiliares Consumidor Final
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/mostrarClientesAuxiliares.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorClientes@vistaAsignarClientesAuxiliares')}}','','width=900px, height=600px toolbar=yes');void 0" target="_blank">Asignar un Cliente Auxiliar</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorClientes@vistaLeerActualizarEliminarClientesAuxiliares')}}','','width=900px, height=600px toolbar=yes');void 0" target="_blank">Mostrar Clientes Auxiliares</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>CLIENTES AUXILIARES</h2>
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>CÓDIGO</th>
                    <th>NOMBRE</th>
                    <th>IDENTIFICACION</th>
                    <th>DIRECCION</th>
                    <th>PROVINCIA</th>
                    <th>CANTON</th>
                    <th>PARROQUIA</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach($clientesAuxiliares as $clienteAuxiliar)
                <tr>
                    <td>
                        {{$clienteAuxiliar->codigo}}
                    </td>
                    <td>
                        {{$clienteAuxiliar->razon_social}}
                    </td>
                    <td>
                        {{$clienteAuxiliar->identificacion}}
                    </td>
                    <td>
                        {{$clienteAuxiliar->direccion}}
                    </td>
                    <td>
                        {{$clienteAuxiliar->ubicaciongeo->provincia}}
                    </td>
                    <td>
                        {{$clienteAuxiliar->ubicaciongeo->canton}}
                    </td>
                    <td>
                        {{$clienteAuxiliar->ubicaciongeo->parroquia}}
                    </td>
                    <td>
                        <div class="btn-group btn-group-justified">
                            <a class="btn btn-default" type="button" href="{{action('clientes\ControladorAuxiliares@vistaMostrar', ['id'=>$clienteAuxiliar->id])}}">
                                <i class="fas fa-search-plus"></i>
                            </a>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
