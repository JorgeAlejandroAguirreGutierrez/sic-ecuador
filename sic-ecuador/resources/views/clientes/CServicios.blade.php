@extends('plantillaHEAD')
@section('title')
Crear Servicios a Facturar
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/CServicios.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorServicios@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorServicios@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>SERVICIOS</h2>
        <hr>
        <h3>Crear Servicio a Facturar</h3>
        <hr>
        <div>
            <div class="form-group form-row">
                <label for="codigo" class="col-3 col-form-label">Codigo</label>
                <div class="col-9">
                    <input type="text" name="codigo" id="codigo" class="form-control" disabled="">
                </div>
            </div>
            <div class="form-group form-row">
                <label for="nombre" class="col-3 col-form-label">Nombre</label>
                <div class="col-9">
                    <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre del Servicio" required>
                    <div id="invalido_nombre" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="grupo_contable_id" class="col-3 col-form-label">Grupo Servicio</label>
                <div class="col-9">
                    <select name="grupo_contable_id" id="grupo_contable_id" class="form-control" required>
                    </select>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="impuesto_id" class="col-3 col-form-label">Tipo de IVA</label>
                <div class="col-9">
                    <select name="impuesto_id" id="impuesto_id" class="form-control" required>
                    </select>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="tipo_id" class="col-3 col-form-label">Tipo de Servicio</label>
                <div class="col-9">
                    <select name="tipo_id" id="tipo_id" class="form-control" required>
                    </select>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="activo" class="col-3 col-form-label">Activo</label> 
                <div class="col-9">
                    <input type="checkbox" name="activo" id="activo" class="form-control form-control-sm">
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    <br><br><br>VALOR IVA
                    <br><br>TOTAL
                </div>
                <table class="col-10">
                    <tr>
                        <td class="text-center">
                            <label for="precio1">Precio 1 </label>
                            <input type="number" name="precio1" id="precio1" class="form-control" placeholder="Precio 1" required>
                            <div id="invalido_precio1" class="invalid-feedback">
                            </div>
                        </td>
                        <td class="text-center">
                            <label for="precio2">Precio 2</label>
                            <input type="number" name="precio2" id="precio2" class="form-control" placeholder="Precio 2" required>
                            <div id="invalido_precio2" class="invalid-feedback">
                            </div>
                        </td>
                        <td class="text-center">
                            <label for="precio3">Precio 3</label>
                            <input type="number" name="precio3" id="precio3" class="form-control" placeholder="Precio 3" required>
                            <div id="invalido_precio3" class="invalid-feedback">
                            </div>
                        </td>
                        <td class="text-center">
                            <label for="precio4">Precio 4</label>
                            <input type="number" name="precio4" id="precio4" class="form-control" placeholder="Precio 4" required>
                            <div id="invalido_precio4" class="invalid-feedback">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                            <input type="number" name="iva1" id="iva1" class="form-control" disabled="">
                        </td>
                        <td class="text-center">
                            <input type="number" name="iva2" id="iva2" class="form-control" disabled>
                        </td>
                        <td class="text-center">
                            <input type="number" name="iva3" id="iva3" class="form-control" disabled>
                        </td>
                        <td class="text-center">
                            <input type="text" name="iva4" id="iva4" class="form-control" disabled>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                            <input type="number" name="total1" id="total1" class="form-control" disabled>
                        </td>
                        <td class="text-center">
                            <input type="number" name="total2" id="total2" class="form-control" disabled>
                        </td>
                        <td class="text-center">
                            <input type="number" name="total3" id="total3" class="form-control" disabled>
                        </td>
                        <td class="text-center">
                            <input type="text" name="total4" id="total4" class="form-control" disabled>
                        </td>
                    </tr>
                </table>
            </div>
            <br>
            <div class="form-group text-center">
                <button class="btn btn-success" id="crearServicio">Crear Servicio a Facturar</button>
            </div>
            <div id="alerta" role="alert">
            </div>
        </div>
    </div>
</div>
@endsection
