@extends('plantillaHEAD')
@section('title')
Crear Tipos de Contribuyentes
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/CTiposContribuyentes.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorTiposContribuyentes@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorTiposContribuyentes@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>TIPOS DE CONTRIBUYENTES</h2>
        <hr>
        <h3>Crear Tipo de Contribuyente</h3>
        <hr>
        <div>
            <div class="form-group form-row">
                <label for="codigo" class="col-4 col-form-label">Codigo</label>
                <div class="col-8">
                    <input type="text" name="codigo" id="codigo" class="form-control" disabled="">  
                </div>
            </div>
            <div class="form-group form-row">
                <label for="tipo" class="col-4 col-form-label">Tipo</label>
                <div class="col-8">
                    <input type="text" name="tipo" id="tipo" class="form-control" placeholder="Tipo de Contribuyente" required>
                    <div id="invalido_tipo" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="subtipo" class="col-4 col-form-label">Subtipo</label>
                <div class="col-8">
                    <input type="text" name="subtipo" id="subtipo" class="form-control" placeholder="Subtipo de Contribuyente" required>
                    <div id="invalido_subtipo" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="especial" class="col-4 col-form-label">Contribuyente Especial</label> 
                <div class="col-8">
                    <input type="checkbox" name="especial" id="especial" class="form-control">
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-success" id="crearTipoContribuyente">Crear</button>
            </div>
            <div id="alerta" role="alert">
            </div>
        </div>
    </div>
</div>
@endsection
