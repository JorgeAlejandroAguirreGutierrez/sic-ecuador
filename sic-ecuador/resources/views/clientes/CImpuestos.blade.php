@extends('plantillaHEAD')
@section('title')
Crear Impuesto
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/CImpuestos.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a title="Ver Impuestos" data-toggle="tooltip" data-placement="bottom" class="nav-link" href="javascript:window.open('{{action('clientes\ControladorImpuestos@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0"><i class="fas fa-stream fa-lg"></i></a>
            </li>
            <li class="nav-item">
                <a title="Crear Impuestos" data-toggle="tooltip" data-placement="bottom" class="nav-link" href="javascript:window.open('{{action('clientes\ControladorImpuestos@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0"><i class="fas fa-file fa-lg"></i></a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>IMPUESTOS</h2>
        <hr>
        <h3>Crear Impuesto</h3>
        <hr>
        <div>
            <div class="form-group form-row">
                <label for="codigo" class="col-2 col-form-label">Codigo</label>
                <div class="col-10">
                    <input type="text" name="codigo" id="codigo" class="form-control" disabled="">
                </div>
            </div>
            <div class="form-group form-row">
                <label for="codigo_norma" class="col-2 col-form-label">Codigo Norma</label>
                <div class="col-10">
                    <input type="text" name="codigo_norma" id="codigo_norma" class="form-control" placeholder="Descripción de la Retención" required>
                    <div id="invalido_codigo_norma" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="porcentaje" class="col-2 col-form-label">Porcentaje</label>
                <div class="col-10">
                    <input type="number" name="porcentaje" id="porcentaje" class="form-control" placeholder="Procentaje de la retencion" required>
                    <div id="invalido_porcentaje" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-success" id="crearImpuesto">Crear Impuesto</button>
            </div>
            <div id="alerta" role="alert">
            </div>
        </div>
    </div>
</div>
@endsection
