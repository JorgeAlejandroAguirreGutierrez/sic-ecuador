@extends('plantillaHEAD')
@section('title')
Crear Usuarios
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/estructuras/CUsuarios.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorUsuarios@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorUsuarios@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>USUARIOS</h2>
        <hr>
        <h3>Crear Usuario</h3>
        <hr>
        <div>
            <div class="form-group form-row">
                <label for="codigo" class="col-2 col-form-label">Codigo</label>
                <div class="col-10">
                    <input type="text" name="codigo" id="codigo" class="form-control" disabled="">  
                </div>
            </div>
            <div class="form-group form-row">
                <label for="nombre" class="col-2 col-form-label">Nombre</label>
                <div class="col-10">
                    <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre del Dato Adicional" required>
                    <div id="invalido_nombre" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="contrasena" class="col-2 col-form-label">Contrasena</label>
                <div class="col-10">
                    <input type="password" name="contrasena" id="contrasena" class="form-control" placeholder="Nombre del Dato Adicional" required>
                    <div id="invalido_nombre" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="identificacion" class="col-2 col-form-label">Identificacion</label> 
                <div class="col-10">
                    <input type="text" name="identificacion" id="identificacion" class="form-control" placeholder="Identificacion" required>
                    <div id="invalido_identificacion" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="tipo" class="col-2 col-form-label">Tipo</label> 
                <div class="col-10">
                    <select name="tipo" id="tipo" class="form-control">
                    </select>
                    <div id="invalido_tipo" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-success" id="crearUsuario">Crear</button>
            </div>
            <div id="alerta" role="alert">
            </div>
        </div>
    </div>
</div>
@endsection
