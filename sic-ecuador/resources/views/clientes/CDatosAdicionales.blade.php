@extends('plantillaHEAD')
@section('title')
Crear Grupos de Contables
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/CDatosAdicionales.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a title="Ver Datos Adicionales" data-toggle="tooltip" data-placement="bottom" class="nav-link" href="javascript:window.open('{{action('clientes\ControladorDatosAdicionales@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0"><i class="fas fa-stream fa-lg"></i></a>
            </li>
            <li class="nav-item">
                <a title="Crear Datos Adicionales Clientes" data-toggle="tooltip" data-placement="bottom" class="nav-link" href="javascript:window.open('{{action('clientes\ControladorDatosAdicionales@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0"><i class="fas fa-file fa-lg"></i></a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>DATOS ADICIONALES</h2>
        <hr>
        <h3>Crear Dato Adicional</h3>
        <hr>
        <div>
            <div class="form-group form-row">
                <label for="codigo" class="col-2 col-form-label">Codigo</label>
                <div class="col-10">
                    <input type="text" name="codigo" id="codigo" class="form-control" disabled="">  
                </div>
            </div>
            <div class="form-group form-row">
                <label for="tipo" class="col-2 col-form-label">Tipo</label>
                <div class="col-10">
                    <input type="text" name="tipo" id="tipo" class="form-control" placeholder="Tipo de Dato Adicional" required>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="nombre" class="col-2 col-form-label">Nombre</label>
                <div class="col-10">
                    <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre del Dato Adicional" required>
                    <div id="invalido_nombre" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="abreviatura" class="col-2 col-form-label">Abreviatura</label> 
                <div class="col-10">
                    <input type="text" name="abreviatura" id="abreviatura" class="form-control" placeholder="Abreviatura para el Dato Adicional" required>
                    <div id="invalido_abreviatura" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-success" id="crearDatoAdicional">Crear</button>
            </div>
            <div id="alerta" role="alert">
            </div>
        </div>
    </div>
</div>
@endsection
