@extends('plantillaHEAD')
@section('title')
Crear Ubicación Geografica
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/CUbicacionesgeo.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorUbicacionesgeo@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('clientes\ControladorUbicacionesgeo@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>UBICACIONES GEOGRAFICAS</h2>
        <hr>
        <h3>Crear Ubicación Geografica</h3>
        <hr>
        <div>
            <div class="form-group form-row">
                <label for="codigo" class="col-3 col-form-label">Código</label>
                <div class="col-9">
                    <input type="text" name="codigo" id="codigo" class="form-control" disabled="">
                </div>
            </div>
            <div class="form-group form-row">
                <label for="codigo_norma" class="col-3 col-form-label">Código Norma</label>
                <div class="col-9">
                    <input type="text" name="codigo_norma" id="codigo_norma" class="form-control" placeholder="Codigo Norma">
                </div>
            </div>
            <div class="form-group form-row">
                <label for="provincia" class="col-3 col-form-label">Provincia</label> 
                <div class="col-9">
                    <input type="text" name="provincia" id="provincia" class="form-control" placeholder="Provincia" required>
                    <div name="invalido" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="canton" class="col-3 col-form-label">Canton</label>
                <div class="col-9">
                    <input type="text" name="canton" id="canton" class="form-control" placeholder="Canton" required>
                    <div name="invalido" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="parroquia" class="col-3 col-form-label">Parroquia</label>
                <div class="col-9">
                    <input type="text" name="parroquia" id="parroquia" class="form-control" placeholder="parroquia" required>
                    <div name="invalido" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-success" id="crearUbicaciongeo">Crear Ubicación Geografica</button>
            </div>
            <div id="alerta" role="alert">
            </div>
        </div>
    </div>
</div>
@endsection
