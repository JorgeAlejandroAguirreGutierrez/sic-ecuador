@extends('plantillaHEAD')
@section('title')
Crear Clientes
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/clientes/CAClientes.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a title="Ver Clientes" data-toggle="tooltip" data-placement="bottom" class="nav-link" href="javascript:window.open('{{action('clientes\ControladorClientes@vistaLeerEliminar')}}','','width=900px, height=600px toolbar=yes');void 0"><i class="fas fa-stream fa-lg"></i></a>
            </li>
            <li class="nav-item">
                <a title="Crear Cliente" data-toggle="tooltip" data-placement="bottom" class="nav-link" href="javascript:window.open('{{action('clientes\ControladorClientes@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0"><i class="fas fa-file fa-lg"></i></a>
            </li>
            <li class="nav-item">
                <a title="Ver Clientes Auxiliares CF" data-toggle="tooltip" data-placement="bottom" class="nav-link" href="javascript:window.open('{{action('clientes\ControladorAuxiliaresCF@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0"><i class="fas fa-users fa-lg"></i></a>
            </li>
            <li class="nav-item">
                <a title="Asignar Clientes Auxiliares" data-toggle="tooltip" data-placement="bottom" class="nav-link" href="javascript:window.open('{{action('clientes\ControladorClientes@vistaAsignarClientesAuxiliares')}}','','width=900px, height=600px toolbar=yes');void 0"><i class="fas fa-arrow-right fa-lg"></i></a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <input type="hidden" value="{{$operacion}}" id="operacion">
        <h2>CREAR CLIENTE</h2>
        <input type="hidden" value="{{$cliente->id}}" id="cliente_id">
        <hr>
        <h5>DATOS DEL CLIENTE</h5>
        <div id="formulario">
            <div class="form-group form-row">
                <label for="codigo" class="col-1 col-form-label">Codigo</label>
                <div class="col-3">
                    <input value="{{$cliente->codigo}}" type="text" name="codigo" id="codigo" class="form-control" disabled="">
                </div>
                <label for="identificacion" class="col-2 col-form-label">Identificación</label>
                <div class="col-1">
                    <input type="text" name="tipo_identificacion" id="tipo_identificacion" class="form-control" placeholder="Tipo" disabled="">
                </div>
                <div class="col-5">    
                    <input value="{{$cliente->identificacion}}" type="text" name="identificacion" id="identificacion" class="form-control" placeholder="Identificación del Cliente" required>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="razon_social" class="col-2 col-form-label">Razon Social</label>
                <div class="col-4">
                    <input value="{{$cliente->razon_social}}" type="text" name="razon_social" id="razon_social" class="form-control" placeholder="Razon Social del Cliente" required=""> 
                </div>
                <label for="grupo_contable_id" class="col-2 col-form-label">Grupo Clientes</label>
                <div class="col-4">
                    <input type="hidden" value="{{$cliente->grupo_contable_id}}" id="temp_grupo_contable_id">
                    <select name="grupo_contable_id" id="grupo_contable_id" class="form-control" required>
                    </select>
                </div>
            </div>
            <h5>DATOS DE DIRECCION</h5>
            <div class="form-group form-row">
                <label for="direccion" class="col-1 col-form-label">Dirección</label>
                <div class="col-11">
                    <input value="{{$cliente->direccion}}" type="text" name="direccion" id="direccion" class="form-control" placeholder="Direccion del Cliente" required>           
                </div>
            </div>
            <div class="form-group form-row">
                <input type="hidden" value="{{$cliente->ubicaciongeo_id}}" id="temp_ubicaciongeo_id">
                <label for="provincia" class="col-1 col-form-label">Provincia</label>
                <div class="col-3">
                    <input value="{{$cliente->ubicaciongeo->provincia}}" name="provincia" id="provincia" class="form-control" required>
                </div>
                <label for="canton" class="col-1 col-form-label">Canton</label>
                <div class="col-3">
                    <input value="{{$cliente->ubicaciongeo->canton}}" name="canton" id="canton" class="form-control" required>
                </div>
                <label for="parroquia" class="col-1 col-form-label">Parroquia</label>
                <div class="col-3">
                    <input value="{{$cliente->ubicaciongeo->parroquia}}" name="parroquia" id="parroquia" class="form-control" required>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="latitudgeo" class="col-2 col-form-label">Latitud Geografica</label>
                <div class="col-4">
                    <input value="{{$cliente->latitudgeo}}" type="text" value="0" name="latitudgeo" id="latitudgeo" class="form-control" disabled="">
                </div>
                <label for="longitudgeo" class="col-2 col-form-label">Longitud Geografica</label>
                <div class="col-4">
                    <input value="{{$cliente->longitudgeo}}" type="text" value="0" name="longitudgeo" id="longitudgeo" class="form-control" disabled="">
                </div>
            </div>
            <hr>
            <div class="form-group form-row">
                <label for="tipo_persona" class="col-3 col-form-label">Tipo de Contribuyente</label>
                <div class="col-2">
                    <input type="text" name="tipo_persona" id="tipo_persona" class="form-control" disabled="">
                </div>
                <div class="col-3" id="td_tipo_contribuyente_id">
                    <select value="{{$cliente->tipo_contribuyente_id}}" name="tipo_contribuyente_id" id="tipo_contribuyente_id" class="form-control" required>
                    </select>
                </div>
                <label for="contribuyente_especial" class="col-3 col-form-label">Contribuyente especial</label>
                <div class="col-1">
                    @if($cliente->contribuyente_especial)
                    <input type="checkbox" name="contribuyente_especial" id="contribuyente_especial" checked>
                    @else
                    <input type="checkbox" name="contribuyente_especial" id="contribuyente_especial">
                    @endif
                </div>   
            </div>
            <hr>
            <h5>DATOS DE CONTACTO</h5>
            <div class="form-group form-row"> 
                <label for="telefono" class="col-1 col-form-label">Telefono</label>
                <div class="col-4" id="telefonos">
                    @if(count($cliente->telefonos)==0)
                    <input type="tel" name="telefono" id="telefono0" class="form-control" placeholder="Telefono del Cliente">    
                    @else
                        @for($i=0; $i<(count($cliente->telefonos)); $i++)
                        <input type="tel" name="telefono" id="telefono{{$i}}" class="form-control" placeholder="Telefono del Cliente" value="{{$cliente->telefonos[$i]->numero}}">
                        @endfor
                    @endif
                </div>
                <div class="col-1">
                    <button type="button" class="btn btn-default" name="masTelefono" id="masTelefono">
                        <i class="fas fa-plus-circle"></i>
                    </button>
                </div>
                <label for="celular" class="col-1 col-form-label">Celular</label>
                <div class="col-4" id="celulares">
                    @if(count($cliente->celulares)==0)
                        <input type="tel" name="celular" id="celular0" class="form-control" placeholder="Celular del Cliente">
                    @else
                        @for($i=0; $i<(count($cliente->celulares)); $i++)
                        <input type="tel" name="celular" id="celular{{$i}}" class="form-control" placeholder="Celular del Cliente" value="{{$cliente->celulares[$i]->numero}}">
                        @endfor
                    @endif
                </div>
                <div class="col-1">
                    <button type="button" class="btn btn-default" name="masCelular" id="masCelular">
                        <i class="fas fa-plus-circle"></i>
                    </button>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="correo" class="col-1 col-form-label">Correo</label>
                <div class="col-10" id="correos">
                    @if(count($cliente->correos)==0)
                       <input type="email" name="correo" id="correo0" class="form-control" placeholder="Correo del Cliente"> 
                    @else
                        @for($i=0; $i<(count($cliente->correos)); $i++)
                        <input type="email" name="correo" id="correo{{$i}}" class="form-control" placeholder="Correo del Cliente" value="{{$cliente->correos[$i]->email}}">
                        @endfor
                    @endif
                    
                </div>
                <div class="col-1" id="correos">
                    <button type="button" class="btn btn-default" name="masCorreo" id="masCorreo">
                        <i class="fas fa-plus-circle"></i>
                    </button>
                </div>
            </div>
            <h5>DATOS DE COMERCIALIZACION</h5>
            <div class="form-group form-row">
                <label for="forma_pago_id" class="col-2 col-form-label">Forma de pago</label>
                <div class="col-4">
                    <input type="hidden" value="{{$cliente->forma_pago_id}}" id="temp_forma_pago_id">
                    <select name="forma_pago_id" id="forma_pago_id" class="form-control" required>
                    </select>
                </div>
                <label for="estado" class="col-2 col-form-label">Activo</label>
                <div class="col-4">
                    @if($cliente->estado)
                    <input type="checkbox" name="estado" id="estado" class="form-control" checked="">
                    @else
                    <input type="checkbox" name="estado" id="estado" class="form-control">
                    @endif
                </div>                       
            </div>
            <div class="form-group form-row">
                <label for="cliente_prepago" class="col-2 col-form-label">Cliente Prepago</label>
                <div class="col-4">
                    @if($cliente->cliente_prepago)
                    <input type="checkbox" name="cliente_prepago" id="cliente_prepago" class="form-control" checked>
                    @else
                    <input type="checkbox" name="cliente_prepago" id="cliente_prepago" class="form-control">
                    @endif
                </div>
                <label for="valor_prepago" class="col-2 col-form-label">Valor Prepago</label>
                <div class="col-4">
                    <input type="number" step="any" name="valor_prepago" id="valor_prepago" class="form-control" value="{{$cliente->valor_prepago}}" disabled="">
                </div>
            </div>
            <div class="form-group form-row">
                <label for="cliente_pospago" class="col-2 col-form-label">Cliente Pospago</label>
                <div class="col-4">
                    @if($cliente->cliente_prepago)
                    <input type="checkbox" name="cliente_pospago" id="cliente_pospago" class="form-control" checked>
                    @else
                    <input type="checkbox" name="cliente_pospago" id="cliente_pospago" class="form-control">
                    @endif
                </div>
                <label for="valor_pospago" class="col-2 col-form-label">Valor Pospago</label>
                <div class="col-4">
                    <input type="number" step="any" name="valor_pospago" id="valor_pospago" class="form-control" value="{{$cliente->valor_prepago}}" disabled="">
                </div>
            </div>
            <div class="form-group form-row">
                <label for="plazo_credito_id" class="col-2 col-form-label">Plazo de Credito</label>
                <div class="col-4">
                    <input type="hidden" value="{{$cliente->plazo_credito_id}}" id="temp_plazo_credito_id">
                    <select name="plazo_credito_id" id="plazo_credito_id" class="form-control" required>
                    </select>
                </div>
                <label for="categoria_cliente_id" class="col-3 col-form-label">Categoria del Cliente</label>
                <div class="col-3">
                    <input type="hidden" value="{{$cliente->categoria_cliente_id}}" id="temp_categoria_cliente_id">
                    <select name="categoria_cliente_id" id="categoria_cliente_id" class="form-control">
                    </select>
                </div>
            </div>
            <h5>DATOS DE RETENCION</h5>
            <div class="form-group form-row"> 
                <label for="retencion_iva_b_id" class="col-2 col-form-label">Retencion IVA (B)</label>
                <div class="col-4">
                    <input type="hidden" value="{{$cliente->retencion_iva_b_id}}" id="temp_retencion_iva_b_id">
                    <select name="retencion_iva_b_id" id="retencion_iva_b_id" class="form-control" required>
                    </select>
                </div>
                <label for="retencion_ir_b_id" class="col-2 col-form-label">Retencion IR (B)</label>
                <div class="col-4">
                    <input type="hidden" value="{{$cliente->retencion_ir_b_id}}" id="temp_retencion_ir_b_id">
                    <select name="retencion_ir_b_id" id="retencion_ir_b_id" class="form-control" required>
                    </select>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="retencion_iva_s_id" class="col-2 col-form-label">Retencion IVA (S)</label>
                <div class="col-4">
                    <input type="hidden" value="{{$cliente->retencion_iva_s_id}}" id="temp_retencion_iva_s_id">
                    <select name="retencion_iva_s_id" id="retencion_iva_s_id" class="form-control" required>
                    </select>
                </div>
                <label for="retencion_ir_s_id" class="col-2 col-form-label">Retencion IR (S)</label>
                <div class="col-4">
                    <input type="hidden" value="{{$cliente->retencion_ir_s_id}}" id="temp_retencion_ir_s_id">
                    <select name="retencion_ir_s_id" id="retencion_ir_s_id" class="form-control" required>
                    </select>
                </div>
            </div>
            <h5>DATOS DINARDAP</h5>
            <div class="form-group form-row">
                <label for="tipo_cliente" class="col-2 col-form-label">Tipo de Cliente</label>
                <div class="col-4">
                    <input type="text" name="tipo_cliente" id="tipo_cliente" class="form-control" disabled="">
                </div>
                <label for="estado_civil_id" class="col-2 col-form-label">Estado Civil</label>
                <div class="col-4">
                    <input type="hidden" value="{{$cliente->estado_civil_id}}" id="temp_estado_civil_id">
                    <select name="estado_civil_id" id="estado_civil_id" class="form-control" required value="">
                    </select>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="sexo_id" class="col-2 col-form-label">Sexo</label>
                <div class="col-4">
                    <input type="hidden" value="{{$cliente->sexo_id}}" id="temp_sexo_id">
                    <select name="sexo_id" id="sexo_id" class="form-control" required>
                    </select>
                </div>
                <label for="origen_ingreso_id" class="col-2 col-form-label">Origen de Ingresos</label>
                <div class="col-4">
                    <input type="hidden" value="{{$cliente->origen_ingreso_id}}" id="temp_origen_ingreso_id">
                    <select name="origen_ingreso_id" id="origen_ingreso_id" class="form-control" required>
                    </select>
                </div>
            </div>
            <br>
            <center>
                <div class="btn-group btn-group-justified">
                    <button type="button" class="btn btn-success" id="guardarCliente">Guardar Cliente</button>
                </div>
                <div id="alerta" role="alert">
                </div>
            </center>
        </div>
    </div>
</div>
@endsection
