@extends('plantillaHEAD')
@section('title')
Facturas
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/facturas/LEFacturas.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('facturas\ControladorFacturas@vistaLeerEliminar')}}','','width=900px, height=600px toolbar=yes');void 0">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('facturas\ControladorFacturas@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>FACTURAS</h2>
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>CÓDIGO</th>
                    <th>CLIENTE</th>
                    <th>TRANSPORTISTA</th>
                    <th>VEHICULO</th>
                    <th>VENDEDOR</th>
                    <th>CAJERO</th>
                    <th>TOTAL</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach($cabecerasFacturas as $cabeceraFactura)
                <tr>
                    <td>
                        {{$cabeceraFactura->codigo}}
                    </td>
                    <td>
                        {{$cabeceraFactura->cliente->razon_social}}
                    </td>
                    <td>
                        @if ($cabeceraFactura->transportista!=null)
                        {{$cabeceraFactura->transportista->nombre}}
                        @endif
                    </td>
                    <td>
                        @if ($cabeceraFactura->vehiculoTransporte!=null)
                        {{$cabeceraFactura->vehiculoTransporte->placa}}
                        @endif
                    </td>
                    <td>
                        {{$cabeceraFactura->vendedor->nombre}}
                    </td>
                    <td>
                        {{$cabeceraFactura->cajero->nombre}}
                    </td>
                    <td>
                        {{$cabeceraFactura->total}}
                    </td>
                    <td>
                        <div class="btn-group btn-group-justified">
                            <button type="button" class="btn btn-default" name="modificarFactura" id="{{$cabeceraFactura->id}}">
                                <i class="fas fa-edit"></i>
                            </button>
                            <button type="button" class="btn btn-default" name="eliminarFactura" id="{{$cabeceraFactura->id}}" data-toggle="modal" data-target="#modalEliminarFactura">
                                <i class="fas fa-trash"></i>
                            </button>
                            <button type="button" class="btn btn-default" name="cobroFactura" id="{{$cabeceraFactura->id}}">
                                <i class="fas fa-arrow-alt-circle-right"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="modalEliminarFactura" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Factura</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar la factura?
                <input type="hidden" id="idE" value="-1">
                <br>
                <br>
                <div>
                    <div class="form-group form-row">
                        <label for="codigoE" class="col-3 col-form-label">Numero de Factura</label>
                        <div class="col-9">
                            <input type="text" name="codigoE" id="codigoE" class="form-control" disabled="">  
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="codigo_internoE" class="col-3 col-form-label">Secuencia</label>
                        <div class="col-9">
                            <input type="text" name="codigo_internoE" id="codigo_internoE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="totalE" class="col-3 col-form-label">Total</label>
                        <div class="col-9">
                            <input type="text" name="totalE" id="totalE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="punto_ventaE" class="col-3 col-form-label">Punto de Venta</label> 
                        <div class="col-9">
                            <input type="text" name="punto_ventaE" id="punto_ventaE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="descarga_inventarioE" class="col-3 col-form-label">Descarga de Inventario</label> 
                        <div class="col-9">
                            <input type="text" name="descarga_inventarioE" id="descarga_inventarioE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="vendedorE" class="col-3 col-form-label">Vendedor</label> 
                        <div class="col-9">
                            <input type="text" name="vendedorE" id="vendedorE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="cajeroE" class="col-3 col-form-label">Cajero</label> 
                        <div class="col-9">
                            <input type="text" name="cajeroE" id="cajeroE" class="form-control" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarFactura">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection
