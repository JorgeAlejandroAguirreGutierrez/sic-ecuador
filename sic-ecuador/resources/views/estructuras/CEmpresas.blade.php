@extends('plantillaHEAD')
@section('title')
Crear Empresas
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/estructuras/CEmpresas.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('estructuras\ControladorEmpresas@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0" target="_blank">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('estructuras\ControladorEmpresas@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0" target="_blank">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>EMPRESAS</h2>
        <hr>
        <h3>Añadir una empresa</h3>
        <hr>
        <div>
            <div class="form-group form-row">
                <label for="codigo" class="col-4 col-form-label">Codigo</label>
                <div class="col-8">
                    <input type="text" name="codigo" id="codigo" class="form-control">
                    <div id="invalido_codigo" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="razon_social" class="col-4 col-form-label">Razon Social</label>
                <div class="col-8">
                    <input type="text" name="razon_social" id="razon_social" class="form-control" required>
                    <div id="invalido_direccion" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="identificacion" class="col-4 col-form-label">RUC</label>
                <div class="col-8">
                    <input type="text" name="identificacion" id="identificacion" class="form-control" required>
                    <div id="invalido_direccion" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-success" id="crearEmpresa">Crear Empresa</button>
            </div>
            <div id="alerta" role="alert">
            </div>
        </div>
    </div>
</div>
@endsection
