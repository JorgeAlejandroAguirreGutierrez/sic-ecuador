@extends('plantillaHEAD')
@section('title')
Crear Puntos Ventas
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/estructuras/CPuntosVentas.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('estructuras\ControladorPuntosVentas@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0" target="_blank">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('estructuras\ControladorPuntosVentas@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0" target="_blank">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>PUNTOS DE VENTA</h2>
        <hr>
        <h3>Añadir un punto de venta</h3>
        <hr>
        <div>
            <div class="form-group form-row">
                <label for="codigo" class="col-4 col-form-label">Codigo</label>
                <div class="col-8">
                    <input type="text" name="codigo" id="codigo" class="form-control">
                    <div id="invalido_codigo" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="empresa_id" class="col-4 col-form-label">Empresa</label>
                <div class="col-8">
                    <select name="empresa_id" id="empresa_id" class="form-control" required>
                    </select>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="establecimiento_id" class="col-4 col-form-label">Establecimiento</label>
                <div class="col-8">
                    <select name="establecimiento_id" id="establecimiento_id" class="form-control" required>
                    </select>
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-success" id="crearPuntoVenta">Crear Punto de Venta</button>
            </div>
            <div id="alerta" role="alert">
            </div>
        </div>
    </div>
</div>
@endsection
