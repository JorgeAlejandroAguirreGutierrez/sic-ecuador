@extends('plantillaHEAD')
@section('title')
Establecimientos
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/estructuras/LAEEstablecimientos.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('estructuras\ControladorEstablecimientos@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0" target="_blank">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('estructuras\ControladorEstablecimientos@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0" target="_blank">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>ESTABLECIMIENTOS</h2>
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>CÓDIGO</th>
                    <th>DIRECCION</th>
                    <th>PROVINCIA</th>
                    <th>CANTON</th>
                    <th>PARROQUIA</th>
                    <th>EMPRESA</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach($establecimientos as $establecimiento)
                <tr>
                    <td>
                        {{$establecimiento->codigo}}
                    </td>
                    <td>
                        {{$establecimiento->direccion}}
                    </td>
                    <td>
                        {{$establecimiento->ubicaciongeo->provincia}}
                    </td>
                    <td>
                        {{$establecimiento->ubicaciongeo->canton}}
                    </td>
                    <td>
                        {{$establecimiento->ubicaciongeo->parroquia}}
                    </td>
                    <td>
                        {{$establecimiento->empresa->razon_social}}
                    </td>
                    <td>
                        <div class="btn-group btn-group-justified">
                            <button type="button" class="btn btn-default" name="eliminarEstablecimiento" id="{{$establecimiento->id}}" data-toggle="modal" data-target="#modalEliminarEstablecimiento">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modalEliminarEstablecimiento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Establecimiento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar el establecimiento?
                <input type="hidden" id="idE" value="-1">
                <br>
                <br>
                <div>
                    <div class="form-group form-row">
                        <label for="codigoE" class="col-3 col-form-label">Codigo</label>
                        <div class="col-9">
                            <input type="text" name="codigoE" id="codigoE" class="form-control" disabled="">  
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="direccionE" class="col-3 col-form-label">direccion</label>
                        <div class="col-9">
                            <input type="text" name="direccionE" id="direccionE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="provinciaE" class="col-3 col-form-label">provincia</label>
                        <div class="col-9">
                            <input type="text" name="provinciaE" id="provinciaE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="cantonE" class="col-3 col-form-label">Canton</label> 
                        <div class="col-9">
                            <input type="text" name="cantonE" id="cantonE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="parroquiaE" class="col-3 col-form-label">Parroquia</label> 
                        <div class="col-9">
                            <input type="text" name="parroquiaE" id="parroquiaE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="empresaE" class="col-3 col-form-label">Empresa</label> 
                        <div class="col-9">
                            <input type="text" name="empresaE" id="empresaE" class="form-control" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarEstablecimiento">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection