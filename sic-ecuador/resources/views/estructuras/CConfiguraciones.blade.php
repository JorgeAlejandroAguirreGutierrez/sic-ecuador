@extends('plantillaHEAD')
@section('title')
Crear Configuraciones
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/estructuras/CConfiguraciones.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('estructuras\ControladorConfiguraciones@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0" target="_blank">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('estructuras\ControladorConfiguraciones@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0" target="_blank">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>CONFIGURACIONES</h2>
        <hr>
        <h3>Crear Configuracion</h3>
        <hr>
        <div>
            <div class="form-group form-row">
                <label for="tabla" class="col-2 col-form-label">Tabla</label>
                <div class="col-10">
                    <input type="text" name="tabla" id="tabla" class="form-control" placeholder="Tabla que referencia" required>  
                    <div id="invalido_tabla" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="tipo" class="col-2 col-form-label">Tipo</label>
                <div class="col-10">
                    <input type="text" name="tipo" id="tipo" class="form-control" placeholder="Tipo de configuracion" required>
                    <div id="invalido_tipo" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="codigo" class="col-2 col-form-label">Codigo</label>
                <div class="col-10">
                    <input type="text" name="codigo" id="codigo" class="form-control" placeholder="Codigo para la pantalla" required>
                    <div id="invalido_codigo" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-success" id="crearConfiguracion">Crear</button>
            </div>
        </div>
    </div>
</div>
@endsection