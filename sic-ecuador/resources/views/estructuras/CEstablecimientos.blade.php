@extends('plantillaHEAD')
@section('title')
Crear Establecimientos
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/estructuras/CEstablecimientos.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('estructuras\ControladorEstablecimientos@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0" target="_blank">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('estructuras\ControladorEstablecimientos@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0" target="_blank">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>ESTABLECIMIENTOS</h2>
        <hr>
        <h3>Añadir un Establecimiento</h3>
        <hr>
        <div>
            <div class="form-group form-row">
                <label for="codigo" class="col-4 col-form-label">Codigo</label>
                <div class="col-8">
                    <input type="text" name="codigo" id="codigo" class="form-control">
                    <div id="invalido_codigo" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="direccion" class="col-4 col-form-label">Direccion</label>
                <div class="col-8">
                    <input type="text" name="direccion" id="direccion" class="form-control" required>
                    <div id="invalido_direccion" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="provincia" class="col-4 col-form-label">Provincia</label>
                <div class="col-8">
                    <select name="provincia" id="provincia" class="form-control" required>
                    </select>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="canton" class="col-4 col-form-label">Canton</label>
                <div class="col-8">
                    <select name="canton" id="canton" class="form-control" required>
                    </select>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="parroquia" class="col-4 col-form-label">Parroquia</label>
                <div class="col-8">
                    <select name="parroquia" id="parroquia" class="form-control" required>
                    </select>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="empresa_id" class="col-4 col-form-label">Empresa</label>
                <div class="col-8">
                    <select name="empresa_id" id="empresa_id" class="form-control" required>
                    </select>
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-success" id="crearEstablecimiento">Crear Establecimiento</button>
            </div>
            <div id="alerta" role="alert">
            </div>
        </div>
    </div>
</div>
@endsection
