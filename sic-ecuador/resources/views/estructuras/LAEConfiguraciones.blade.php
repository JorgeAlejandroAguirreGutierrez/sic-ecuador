@extends('plantillaHEAD')
@section('title')
Configuraciones
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/estructuras/LAEConfiguraciones.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('estructuras\ControladorConfiguraciones@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0" target="_blank">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('estructuras\ControladorConfiguraciones@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0" target="_blank">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>CONFIGURACIONES</h2>
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>TABLA</th>
                    <th>TIPO</th>
                    <th>CÓDIGO</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach($configuraciones as $configuracion)
                <tr>
                    <td>
                        {{$configuracion->tabla}}
                    </td>
                    <td>
                        {{$configuracion->tipo}}
                    </td>
                    <td>
                        {{$configuracion->codigo}}
                    </td>
                    <td>
                        <div class="btn-group btn-group-justified">
                            <button type="button" class="btn btn-default" name="modificarConfiguracion" id="{{$configuracion->id}}" data-toggle="modal" data-target="#modalModificarConfiguracion">
                                <i class="fas fa-edit"></i>
                            </button>
                            <button type="button" class="btn btn-default" name="eliminarConfiguracion" id="{{$configuracion->id}}" data-toggle="modal" data-target="#modalEliminarConfiguracion">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modalModificarConfiguracion" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="col-2"></div>
                <div class="col-8">
                    <h2>CONFIGURACIONES</h2>
                    <hr>
                    <h3>Modificar Configuracion</h3>
                    <hr>
                    <input type="hidden" id="idM" value="-1">
                    <div>
                        <div class="form-group form-row">
                            <label for="tablaM" class="col-2 col-form-label">Tabla</label>
                            <div class="col-10">
                                <input type="text" name="tablaM" id="tablaM" class="form-control" placeholder="Codigo" required>  
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="tipoM" class="col-2 col-form-label">Tipo</label>
                            <div class="col-10">
                                <input type="textM" name="tipo" id="tipoM" class="form-control" placeholder="Tipo de Dato Adicional" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="codigoM" class="col-2 col-form-label">Codigo</label>
                            <div class="col-10">
                                <input type="text" name="codigoM" id="codigoM" class="form-control" placeholder="Nombre del Dato Adicional" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-success" id="cModificarConfiguracion">Modificar Condiguracion</button>
                </div>
            <div class="col-2"></div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalEliminarConfiguracion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Configuracion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar la configuracion?
                <input type="hidden" id="idE" value="-1">
                <div>
                    <div class="form-group form-row">
                        <label for="tablaE" class="col-2 col-form-label">Tabla</label>
                        <div class="col-10">
                            <input type="text" name="tablaE" id="tablaE" class="form-control" disabled="">  
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="tipoE" class="col-2 col-form-label">Tipo</label>
                        <div class="col-10">
                            <input type="text" name="tipoE" id="tipoE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="codigoE" class="col-2 col-form-label">Codigo</label>
                        <div class="col-10">
                            <input type="text" name="codigoE" id="codigoE" class="form-control" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarConfiguracion">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection