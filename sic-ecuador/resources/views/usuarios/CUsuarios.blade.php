@extends('plantillaHEAD')
@section('title')
Crear Usuarios
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/usuarios/CUsuarios.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('usuarios\ControladorUsuarios@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0" target="_blank">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('usuarios\ControladorUsuarios@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0" target="_blank">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>USUARIOS</h2>
        <hr>
        <h3>Añadir un usuario</h3>
        <hr>
        <div>
            <div class="form-group form-row">
                <label for="codigo" class="col-4 col-form-label">Codigo</label>
                <div class="col-8">
                    <input type="text" name="codigo" id="codigo" class="form-control" disabled="">
                    <div id="invalido_codigo" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="nombre" class="col-4 col-form-label">Nombre</label>
                <div class="col-8">
                    <input name="nombre" id="nombre" class="form-control" required>
                    <div id="invalido_nombre" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="correo" class="col-4 col-form-label">Correo</label>
                <div class="col-8">
                    <input type="email" name="correo" id="correo" class="form-control" required>
                    <div id="invalido_correo" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="contrasena" class="col-4 col-form-label">Contraseña</label>
                <div class="col-8">
                    <input type="password" name="contrasena" id="contrasena" class="form-control">
                    <div id="invalido_contrasena" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="identificacion" class="col-4 col-form-label">Identificacion</label>
                <div class="col-8">
                    <input type="text" name="identificacion" id="identificacion" class="form-control">
                    <div id="invalido_identificacion" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="tipo_id" class="col-4 col-form-label">Tipo</label>
                <div class="col-8">
                    <select name="tipo_id" id="tipo_id" class="form-control" required>
                    </select>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="activo" class="col-4 col-form-label">Activo</label>
                <div class="col-8">
                    <input type="checkbox" name="activo" id="activo" class="form-control">
                    </select>
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-success" id="crearUsuario">Crear Usuario</button>
            </div>
            <div id="alerta" role="alert">
            </div>
        </div>
    </div>
</div>
@endsection