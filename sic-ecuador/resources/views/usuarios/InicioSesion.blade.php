@extends('plantillaHEAD')
@section('title')
Inicio de Sesion
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/usuarios/inicioSesion.js')}}"></script>
@endsection
@section('content')
<div class="row">
    <div class="col-3">
    </div>
    <div class="col-6">
        <h2>INICIO DE SESION</h2>
        <hr>
        <div>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#iniciar_sesion-tab" role="tab" aria-controls="iniciarSesion" aria-selected="true">Iniciar Sesion</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#activar_sesion-tab" role="tab" aria-controls="activarSesion" aria-selected="false">Activar Usuario Sesion</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" role="tabpanel" aria-labelledby="iniciar_sesion-tab" id="iniciar_sesion-tab">
                    <br>
                    <div class="form-group form-row">
                        <img id="logo" src="" alt="logo" class="form-control col-12" style="width: 50%;height: auto;">
                    </div>
                    <div class="form-group form-row">
                        <label class="col-6 col-form-label">Empresa</label>
                        <label id="empresa" class="col-6 col-form-label"></label>
                    </div>
                    <div class="form-group form-row">
                        <label class="col-6 col-form-label">RUC</label>
                        <label id="ruc" class="col-6 col-form-label"></label>
                    </div>
                    <div class="form-group form-row">
                        <label for="identificacion" class="col-4 col-form-label">Identificacion</label>
                        <div class="col-8">
                            <input type="text" name="identificacion" id="identificacion" class="form-control">
                            <div id="invalido_identificacion" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="contrasena" class="col-4 col-form-label">Contraseña</label>
                        <div class="col-8">
                            <input type="password" name="contrasena" id="contrasena" class="form-control" required>
                            <div id="invalido_contrasena" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-success" id="iniciarSesion">Iniciar Sesion</button>
                    </div>
                    <div id="alerta" role="alert">
                    </div>
                </div>
                <div class="tab-pane fade" role="tabpanel" id="activar_sesion-tab" aria-labelledby="activar_sesion-tab">
                    <div class="form-group form-row">
                        <label for="identificacion_AS" class="col-4 col-form-label">Identificacion</label>
                        <div class="col-8">
                            <input type="text" name="identificacion_AS" id="identificacion_AS" class="form-control">
                            <div id="invalido_identificacion_AS" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="correo_AS" class="col-4 col-form-label">Correo</label>
                        <div class="col-8">
                            <input type="email" name="correo_AS" id="correo_AS" class="form-control">
                            <div id="invalido_correo_AS" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="contrasena_AS" class="col-4 col-form-label">Contraseña</label>
                        <div class="col-8">
                            <input type="password" name="contrasena_AS" id="contrasena_AS" class="form-control" required>
                            <div id="invalido_contrasena_AS" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="repetir_contrasena_AS" class="col-4 col-form-label">Repetir Contraseña</label>
                        <div class="col-8">
                            <input type="password" name="repetir_contrasena_AS" id="repetir_contrasena_AS" class="form-control" required>
                            <div id="invalido_repetir_contrasena_AS" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-success" id="activarSesion">Activar Sesion</button>
                    </div>
                    <div id="alerta" role="alert">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-3"></div>
</div>
@endsection