@extends('plantillaHEAD')
@section('title')
Menu
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/usuarios/menu.js')}}"></script>
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="form-group form-row">
            <img id="logo" src="" alt="logo" class="form-control" style="width: 50%;height: auto;">
        </div>
        <div class="form-group form-row">
            <label class="col-6 col-form-label">Empresa</label>
            <label id="empresa" class="col-6 col-form-label"></label>
        </div>
        <div class="form-group form-row">
            <label class="col-6 col-form-label">RUC</label>
            <label id="ruc" class="col-6 col-form-label"></label>
        </div>
        <section class="col-12">
            <div class="col-3">
            </div>
            <div class="col-3">
                <h3>FINANCIERO</h3>
                <nav class="nav flex-column">
                    <a class="nav-link" href="#">CONTABILIDAD</a>
                    <a class="nav-link" href="#">CONTROL PROVEEDORES</a>
                    <a class="nav-link" href="#">CONTROL CLIENTES</a>
                    <a class="nav-link" href="#">CONTROL FINANCIERO</a>
                    <a class="nav-link" href="#">CONTROL ACTIVOS FIJOS</a>
                    <a class="nav-link" href="#">CONTROL INVENTARIOS</a>
                    <a class="nav-link" href="#">CONTROL TALENTO HUMANO</a>
                </nav>
            </div>
            <div class="col-3">
                <h3>ADMINISTRATIVO</h3>
                <nav class="nav flex-column">
                    <a class="nav-link" href="#">CONTROL PRODUCCION</a>
                    <a class="nav-link" href="#">ORGANISMO DE CONTROL</a>
                    <a class="nav-link" href="#">INFORMES GERENCIALES</a>
                    <a class="nav-link" href="#">SEGURIDAD</a>
                    <a class="nav-link" href="#">TUTORIALES</a>
                </nav>
            </div>
            <div class="col-3"></div>
        </section>
        <footer id="footer" class="col-12">
            <div class="col-4">
                <center>
                    <h3><strong>Contáctenos</strong></h3>
                    <h6>
                        <strong>
                            SIC ECUADOR / RIOBAMBA ECUADOR <br>
                            sic-ecuador@gmail.com <br>
                            0368595212 / (311)3395601<br>
                        </strong>
                    </h6>
                </center>
            </div>
            <div class="col-8">
                <div class="row">
                    <a href="#" target="_blank">
                        <img src="" alt="facebook.png">
                    </a>
                    <a href="#" target="_blank">
                        <img src="" alt="instagram.png">
                    </a>
                    <a href="#" target="_blank">
                        <img src="" alt="youtube.png">
                    </a>
                    <h6>
                        <strong>
                            &copy; 2019. Todos los Derechos Reservados.
                        </strong>
                    </h6>
                </div>
            </div>
        </footer>
    </div>
</div>
@endsection