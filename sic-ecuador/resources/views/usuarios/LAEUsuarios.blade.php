@extends('plantillaHEAD')
@section('title')
Mostrar Usuarios
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/usuarios/LAEUsuarios.js')}}"></script>
@endsection
@section('content')
<div class="row">
    <div class="col-2">
        <nav class="nav flex-column">
            <a class="nav-link active" href="{{action('usuarios\ControladorUsuarios@vistaMostrar')}}">Ver</a>
            <a class="nav-link" href="{{action('usuarios\ControladorUsuarios@vistaCrear')}}">Crear</a>
        </nav>
    </div>
    <div class="col-10">
        <h2>USUARIOS</h2>
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>CÓDIGO</th>
                    <th>NOMBRE</th>
                    <th>CORREO</th>
                    <th>ID</th>
                    <th>TIPO</th>
                    <th>ACTIVO</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach($usuarios as $usuario)
                <tr>
                    <td>
                        {{$usuario->codigo}}
                    </td>
                    <td>
                        {{$usuario->nombre}}
                    </td>
                    <td>
                        {{$usuario->correo}}
                    </td>
                    <td>
                        {{$usuario->identificacion}}
                    </td>
                    <td>
                        {{$usuario->tipo->nombre}}
                    </td>
                    <td>
                        @if ($usuario->activo==1)
                        {{"Sí"}}
                        @else
                        {{"No"}}
                        @endif  
                    </td>
                    <td>
                        <div class="btn-group btn-group-justified">
                            <button type="button" class="btn btn-default" name="eliminarUsuario" id="{{$usuario->id}}" data-toggle="modal" data-target="#modalEliminarUsuario">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modalEliminarUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar el usuario?
                <input type="hidden" id="idE" value="-1">
                <br>
                <br>
                <div>
                    <div class="form-group form-row">
                        <label for="codigoE" class="col-3 col-form-label">Codigo</label>
                        <div class="col-9">
                            <input type="text" name="codigoE" id="codigoE" class="form-control" disabled="">  
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="nombreE" class="col-3 col-form-label">nombre</label>
                        <div class="col-9">
                            <input type="text" name="nombreE" id="nombreE" class="form-control" disabled="">  
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="identificacionE" class="col-3 col-form-label">Identificacion</label>
                        <div class="col-9">
                            <input type="text" name="identificacionE" id="identificacionE" class="form-control" disabled="">  
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarUsuario">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection
