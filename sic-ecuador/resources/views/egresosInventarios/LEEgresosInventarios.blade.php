@extends('plantillaHEAD')
@section('title')
Egresos Inventarios
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/egresosInventarios/LEEgresosInventarios.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('egresosInventarios\ControladorEgresosInventarios@vistaLeerEliminar')}}','','width=900px, height=600px toolbar=yes');void 0">Ver Egresos Inv</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('egresosInventarios\ControladorEgresosInventarios@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0">Crear Egreso Inv</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>EGRESOS INVENTARIOS</h2>
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>CÓDIGO</th>
                    <th>CLIENTE</th>
                    <th>TRANSPORTISTA</th>
                    <th>VEHICULO</th>
                    <th>VENDEDOR</th>
                    <th>CAJERO</th>
                    <th>TOTAL</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach($cabecerasEgresosInventarios as $cabeceraEgresoInventario)
                <tr>
                    <td>
                        {{$cabeceraEgresoInventario->codigo}}
                    </td>
                    <td>
                        {{$cabeceraEgresoInventario->cliente->razon_social}}
                    </td>
                    <td>
                        @if ($cabeceraEgresoInventario->transportista!=null)
                        {{$cabeceraEgresoInventario->transportista->nombre}}
                        @endif
                    </td>
                    <td>
                        @if ($cabeceraEgresoInventario->vehiculoTransporte!=null)
                        {{$cabeceraEgresoInventario->vehiculoTransporte->placa}}
                        @endif
                    </td>
                    <td>
                        {{$cabeceraEgresoInventario->vendedor->nombre}}
                    </td>
                    <td>
                        {{$cabeceraEgresoInventario->cajero->nombre}}
                    </td>
                    <td>
                        {{$cabeceraEgresoInventario->total}}
                    </td>
                    <td>
                        <div class="btn-group btn-group-justified">
                            <button type="button" class="btn btn-default" name="modificarEgresoInventario" id="{{$cabeceraEgresoInventario->id}}">
                                <i class="fas fa-edit"></i>
                            </button>
                            <button type="button" class="btn btn-default" name="eliminarEgresoInventario" id="{{$cabeceraEgresoInventario->id}}" data-toggle="modal" data-target="#modalEliminarEgresoInventario">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="modalEliminarFactura" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Egreso Inventario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar el egreso?
                <input type="hidden" id="id_E" value="-1">
                <br>
                <br>
                <div>
                    <div class="form-group form-row">
                        <label for="codigo_E" class="col-3 col-form-label">Codigo de Egreso</label>
                        <div class="col-9">
                            <input type="text" name="codigo_E" id="codigo_E" class="form-control" disabled="">  
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="codigo_interno_E" class="col-3 col-form-label">Secuencia del Egreso</label>
                        <div class="col-9">
                            <input type="text" name="codigo_interno_E" id="codigo_interno_E" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="total_E" class="col-3 col-form-label">Total</label>
                        <div class="col-9">
                            <input type="text" name="total_E" id="total_E" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="punto_venta_E" class="col-3 col-form-label">Punto de Venta</label> 
                        <div class="col-9">
                            <input type="text" name="punto_venta_E" id="punto_venta_E" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="vendedor_E" class="col-3 col-form-label">Vendedor</label> 
                        <div class="col-9">
                            <input type="text" name="vendedor_E" id="vendedor_E" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="cajero_E" class="col-3 col-form-label">Cajero</label> 
                        <div class="col-9">
                            <input type="text" name="cajero_E" id="cajero_E" class="form-control" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarEgresoInventario">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection
