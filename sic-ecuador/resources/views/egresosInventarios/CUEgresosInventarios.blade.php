@extends('plantillaHEAD')
@section('title')
Crear Egreso de Inventario
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/egresosInventarios/CAEgresosInventarios.js')}}"></script>
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <h2>CREAR EGRESO DE INVENTARIO</h2>
        <hr>
        <form id="formulario">
            <input type="hidden" id="id" value="-1">
            <div class="form-group form-row">
                <div class="col-12">  
                    <h6>DATOS DE EGRESO</h6>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="establecimiento_id" class="col-auto col-form-label">Est.</label>
                <div class="col-1">
                    <input type="text" name="establecimiento_id" id="establecimiento_id" class="form-control form-control-sm" disabled="">
                </div>
                <label for="punto_venta_id" class="col-auto col-form-label">P/Venta.</label>
                <div class="col-1">
                    <input type="text" name="punto_venta_id" id="punto_venta_id" class="form-control form-control-sm" disabled="">
                </div>
                <label for="codigo" class="col-auto col-form-label">No.</label>
                <div class="col-2">
                    <input type="text" name="codigo" id="codigo" class="form-control form-control-sm" disabled="">
                </div>
                <label for="numero_adicional" class="col-auto col-form-label">No. Adi</label>
                <div class="col-2">
                    <input type="number" name="numero_adicional" id="numero_adicional" class="form-control form-control-sm">
                </div>
                <label for="fecha" class="col-auto col-form-label">Fecha.</label>
                <div class="col">
                    <input type="text" name="fecha" id="fecha" class="form-control form-control-sm" disabled="">
                </div>
            </div>
            <div class="form-group form-row">
                <label for="codigo_cliente" class="col-auto col-form-label">Cod.</label>
                <div class="col-2">
                    <input type="text" name="codigo_cliente" id="codigo_cliente" class="form-control form-control-sm" disabled="">
                </div>
                <label for="razon_social_clienteB" class="col-auto col-form-label">Cliente.</label>
                <div class="col-5">
                    <select name="razon_social_clienteB" id="razon_social_clienteB" class="form-control form-control-sm">
                    </select>
                </div>
                <label for="identificacion_clienteB" class="col-auto col-form-label">RUC.</label>
                <div class="col">
                    <select name="identificacion_clienteB" id="identificacion_clienteB" class="form-control form-control-sm">
                    </select>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="direccion_cliente" class="col-auto col-form-label">Dir.</label>
                <div class="col">
                    <input type="text" name="direccion_cliente" id="direccion_cliente" class="form-control form-control-sm" disabled="">
                </div>
            </div>
            <div class="form-group form-row">
                <label for="auxiliar_cliente" class="col-auto col-form-label">Usar Aux.</label>
                <div class="col-1">
                    <select name="auxiliar_cliente" id="auxiliar_cliente" class="form-control form-control-sm">
                        <option value="0">No</option>
                        <option value="1">Si</option>
                    </select>
                </div>
                <label for="razon_social_auxiliar_clienteB" class="col-auto col-form-label">Aux.</label>
                <div class="col-2">
                    <select name="razon_social_auxiliar_clienteB" id="razon_social_auxiliar_clienteB" class="form-control form-control-sm" disabled="">
                    </select>
                </div>
                <label for="identificacion_auxiliar_cliente" class="col-auto col-form-label">RUC/CI Aux.</label>
                <div class="col-2">    
                    <input type="text" name="identificacion_auxiliar_cliente" id="identificacion_auxiliar_cliente" class="form-control form-control-sm" disabled="">
                </div>
                <label for="direccion_auxiliar_cliente" class="col-auto col-form-label">Dir. Aux.</label>
                <div class="col">
                    <input type="text" name="direccion_auxiliar_cliente" id="direccion_auxiliar_cliente" class="form-control form-control-sm" disabled=""> 
                </div>
            </div>
            <div class="form-group form-row">
                <label for="telefono_cliente" class="col-auto col-form-label">Telefono</label>
                <div class="col-3">
                    <input name="telefono_cliente" id="telefono_cliente" class="form-control form-control-sm" disabled="">
                </div>
                <label for="celular_cliente" class="col-auto col-form-label">Celular</label>
                <div class="col-3">
                    <input name="celular_cliente" id="celular_cliente" class="form-control form-control-sm" disabled="">
                </div>
                <label for="correo_cliente" class="col-auto col-form-label">Correo electronico</label>
                <div class="col">    
                    <input type="text" name="correo_cliente" id="correo_cliente" class="form-control form-control-sm" disabled="">
                </div>
            </div>
            <hr>
            <h5>TIPO DE CLIENTE</h5>
            <div class="form-group form-row" id="mostrar_cliente_prepago">
                <label for="cliente_prepago" class="col-auto col-form-label">Cliente Prepago</label>
                <div class="col-1">
                    <input type="text" name="cliente_prepago" id="cliente_prepago" class="form-control form-control-sm" disabled="">
                </div>
                <div class="col-3">
                    <input type="number" step="any" name="valor_prepago" id="valor_prepago" class="form-control form-control-sm" disabled="">
                </div>
                <label for="consumido_prepago" class="col-auto col-form-label">Cons. Prepago</label>
                <div class="col-2">
                    <input type="number" name="consumido_prepago" id="consumido_prepago" class="form-control form-control-sm" disabled="">
                </div>
                <label for="saldo_prepago" class="col-auto col-form-label">Saldo a Cons.</label>
                <div class="col">
                    <input type="number" name="saldo_prepago" id="saldo_prepago" class="form-control form-control-sm" disabled="">
                </div>
            </div>
            <div class="form-group form-row" id="mostrar_cliente_pospago">
                <label for="cliente_pospago" class="col-auto col-form-label">Cliente Pospago</label>
                <div class="col-1">
                    <input type="text" name="cliente_pospago" id="cliente_pospago" class="form-control form-control-sm" disabled="">
                </div>
                <div class="col-3">
                    <input type="number" step="any" name="valor_pospago" id="valor_pospago" class="form-control form-control-sm" disabled="">
                </div>
                <label for="consumido_pospago" class="col-auto col-form-label">Cons. Pospago</label>
                <div class="col-2">
                    <input type="number" name="consumido_pospago" id="consumido_pospago" class="form-control form-control-sm" disabled="">
                </div>
                <label for="saldo_pospago" class="col-auto col-form-label">Saldo a Cons.</label>
                <div class="col">
                    <input type="number" name="saldo_pospago" id="saldo_pospago" class="form-control form-control-sm" disabled="">
                </div>
            </div>
            <div class="form-group form-row">
                <label for="bodega_id" class="col-auto col-form-label">Bodega</label>
                <div class="col-1">
                    <select name="bodega_id" id="bodega_id" class="form-control form-control-sm">
                    </select>
                </div>
                <label for="referencia" class="col-auto col-form-label">Ref.</label>
                <div class="col-2">
                    <select name="referencia" id="referencia" class="form-control form-control-sm">
                        <option value="0">Pedido</option>
                        <option value="1">Proforma</option>
                    </select>
                </div>
                <label for="referencia_codigo" class="col-auto col-form-label">No.</label>
                <div class="col-2">
                    <select name="referencia_codigo" id="referencia_codigo" class="form-control form-control-sm">
                    </select>
                </div>
                <label for="factura_asignada" class="col-auto col-form-label">Fact. Asig</label>
                <div class="col-3">
                    <input name="factura_asignada" id="factura_asignada" class="form-control form-control-sm" disabled="">
                </div>
                <div class="col">
                    <button type="button" name="facturarEgreso" id="facturarEgreso" class="btn btn-sm btn-success" disabled="">Facturar</button>
                </div>
            </div>
            <h5>DATOS GUIA DE REMISION</h5>
            <div class="form-group form-row">
                <label for="guia_remision" class="col-auto col-form-label">Guia Remision.</label>
                <div class="col-1">
                    <select name="guia_remision" id="guia_remision" class="form-control form-control-sm">
                        <option value="1">Si</option>
                        <option value="0">No</option>
                    </select>
                </div>
                <label for="establecimiento_gr_id" class="col-auto  col-form-label">Estab. Remision.</label>
                <div class="col-1">
                    <select name="establecimiento_gr_id" id="establecimiento_gr_id" class="form-control form-control-sm">
                    </select>
                </div>
                <label for="punto_venta_gr_id" class="col-auto  col-form-label">P/Venta GR.</label>
                <div class="col-1">
                    <select name="punto_venta_gr_id" id="punto_venta_gr_id" class="form-control form-control-sm">
                    </select>
                </div>
                <label for="guia_remision_id" class="col-auto col-form-label">Sec.</label>
                <div class="col">
                    <input type="text" name="guia_remision_id" id="secuencia_guia_remision_id" class="form-control form-control-sm" disabled="">
                </div>
            </div>
            <div class="form-group form-row">
                <label for="transportistaB" class="col-auto col-form-label">Transportista</label>
                <div class="col-5">
                    <select name="transportistaB" id="transportistaB" class="form-control form-control-sm">
                    </select>
                </div>
                <label for="vehiculo_transporteB" class="col-auto col-form-label">Vehiculo</label>
                <div class="col">
                    <select name="vehiculo_transporteB" id="vehiculo_transporteB" class="form-control form-control-sm">
                    </select>
                </div>
            </div>
            <h5>DATOS ADICIONALES</h5>
            <div class="form-group form-row">
                <label for="vendedorB" class="col-auto col-form-label">Vendedor</label>
                <div class="col-3">
                    <select name="vendedorB" id="vendedorB" class="form-control form-control-sm">
                    </select>
                </div>
                <label for="cajeroB" class="col-auto col-form-label">Cajero</label>
                <div class="col-3">
                    <select name="cajeroB" id="cajeroB" class="form-control form-control-sm">
                    </select>
                </div>
                <label for="factura_anulada" class="col-auto col-form-label">Egreso. Anulada</label>
                <div class="col-1">
                    <select name="factura_anulada" id="factura_anulada" class="form-control form-control-sm">
                        <option value="0">No</option>
                        <option value="1">Si</option>
                    </select>
                </div>
                <div class="col">
                    LEYENDA EGRESO ANULADA
                </div>
            </div>
            <div class="form-row">
                <div class="col-auto">
                    <h4>PROCESO DE EGRESO</h4>
                </div>
                <div class="col-auto">
                    <button type="button" class="btn btn-default" id="nueva_linea_egreso_inventario">
                        <i class="fas fa-plus-square"></i>
                    </button>
                </div>
            </div>
            <div id="lineas_egreso_inventario">
                <div class="form-row">
                    <div class="form-group col-1">
                        <input type="hidden" id="linea_ps0" value="0">
                        <input type="hidden" id="id_ps0" value="0">
                        <label for="codigo_ps">Cod.</label>
                        <input type="text" name="codigo_ps" id="codigo_ps0" class="form-control form-control-sm" disabled="">
                    </div>
                    <div class="form-group col-4">
                        <label for="nombre_pB">Prod.</label>
                        <select name="nombre_pB" id="nombre_pB0" class="form-control form-control-sm">
                        </select>
                    </div>
                    <div class="form-group col-5">
                        <div class="form-row">
                            <label for="medida_ps">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Med.</label>
                            <label for="cantidad_ps">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cant.</label>
                            <label for="precio_ps">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P/U</label>
                            <label for="descuento_ps">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Des</label>
                            <label for="porcentaje_descuento_ps">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;%Des</label>
                            <label for="total_neto_ps">&nbsp;&nbsp&nbsp;&nbsp&nbsp;&nbsp;Total Neto</label>
                            <input type="text" name="medida_ps" id="medida_ps0" class="form-control form-control-sm col-2" disabled="">
                            <input type="number" min="1" name="cantidad_ps" id="cantidad_ps0" class="form-control form-control-sm col-2" value="1">
                            <input type="text" name="precio_ps" id="precio_ps0" class="form-control form-control-sm col-2" disabled="">
                            <input type="text" name="descuento_ps" id="descuento_ps0" class="form-control form-control-sm col-2" value="0">
                            <input type="text" name="porcentaje_descuento_ps" id="porcentaje_descuento_ps0" class="form-control form-control-sm col-2" value="0">
                            <input type="text" name="total_neto_ps" id="total_neto_ps0" class="form-control form-control-sm col-2" value="0">
                        </div>
                    </div>
                    <div class="form-group col-2">
                        <label for="total">Total.</label>
                        <input type="text" name="total_ps" id="total_ps0" class="form-control form-control-sm" disabled="">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="ultimo_costo" class="col-auto col-form-label">Ul Cost</label>
                <div class="col-1">
                    <input type="text" name="ultimo_costo" id="ultimo_costo" class="form-control form-control-sm" disabled="">
                </div>
                <label for="costo_promedio" class="col-auto col-form-label">Cost Prom</label>
                <div class="col-1">
                    <input type="text" name="costo_promedio" id="costo_promedio" class="form-control form-control-sm" disabled="">
                </div>
                <label for="total_unidades" class="col-auto col-form-label">Total. U</label>
                <div class="col-1">
                    <input type="text" name="total_unidades" id="total_unidades" class="form-control form-control-sm" disabled="">
                </div>
                <label for="existencias_producto" class="col-auto col-form-label">Ex. Prod</label>
                <div class="col-1">
                    <input type="text" name="existencias_producto" id="existencias_producto" class="form-control form-control-sm" disabled="">
                </div>
                <label for="total_descuento" class="col-auto col-form-label">Desc</label>
                <div class="col-1">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <div class="input-group-text">$</div>
                        </div>
                        <input type="text" name="total_descuento" id="total_descuento" class="form-control form-control-sm">
                    </div>
                </div>
                <div class="col-1">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <div class="input-group-text">%</div>
                        </div>
                        <input type="text" name="total_porcentaje_descuento" id="total_porcentaje_descuento" class="form-control form-control-sm">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-9"> 
                </div>
                <label for="subtotal" class="col-1 col-form-label">Subtotal</label>
                <div class="col-2">
                    <input type="text" name="subtotal" id="subtotal" class="form-control form-control-sm" disabled="">
                </div>
            </div>
            <div class="form-row">
                <div class="col-9"> 
                </div>
                <label for="descuento" class="col-1 col-form-label">Descuento</label>
                <div class="col-2">
                    <input type="text" name="descuento" id="descuento" class="form-control form-control-sm" disabled="">
                </div>
            </div>
            <div class="form-row">
                <div class="col-9"> 
                </div>
                <label for="base0" class="col-1 col-form-label">Base 0%</label>
                <div class="col-2">
                    <input type="text" name="base0" id="base0" class="form-control form-control-sm" disabled="">
                </div>
            </div>
            <div class="form-row">
                <div class="col-9"> 
                </div>
                <label for="base12" class="col-1 col-form-label">Base 12%</label>
                <div class="col-2">
                    <input type="text" name="base12" id="base12" class="form-control form-control-sm" disabled="">
                </div>
            </div>
            <div class="form-row">
                <div class="col-9"> 
                </div>
                <label for="importe_iva" class="col-1 col-form-label">Imp. IVA</label>
                <div class="col-2">
                    <input type="text" name="importe_iva" id="importe_iva" class="form-control form-control-sm" disabled="">
                </div>
            </div>
            <div class="form-row">
                <div class="col-9"> 
                </div>
                <label for="total" class="col-1 col-form-label">TOTAL EGRESO</label>
                <div class="col-2">
                    <input type="text" name="total" id="total" class="form-control form-control-sm" disabled="">
                </div>
            </div>
            <div class="form-group form-row">
                <div class="col-2">
                    <button type="button" class="btn btn-default" id="guardarEgresoInventario">Guardar Egreso</button>
                </div>
                <div class="col-10"></div>
            </div>
            <br>
            <br>
            <center>
                <div class="btn-group btn-group-justified">
                </div>
                <div id="alerta" role="alert">
                </div>
            </center>
        </form>
    </div>
</div>

<div class="modal fade" id="modalObtenerServicios" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <h2>SERVICIOS</h2>
                    <hr>
                    <input type="hidden" id="idS" value="-1">
                    <div>
                        <div class="form-group form-row">
                            <label for="nombre_sB" class="col-3 col-form-label">Nombre</label>
                            <div class="col-9">
                                <select name="nombre_sB" id="nombre_sB" class="form-control form-control-sm">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" id="cSeleccionServicio">Aceptar</button>
            </div>
        </div>
        <div class="col-2"></div>
    </div>
</div>
@endsection
