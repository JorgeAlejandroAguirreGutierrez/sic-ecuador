@extends('plantillaHEAD')
@section('title')
Index
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/index.js')}}"></script>
@endsection
@section('content')
<div class="wrapper">
    <nav id="sidebar">
        <div class="sidebar-header">
            <h3>SIC-ECUADOR</h3>
            <strong>SE</strong>
        </div>
        <ul class="list-unstyled components">
            <li>
                <a href="#" id="clientes">
                    <i class="fas fa-briefcase"></i>
                    Clientes
                </a>
            </li>
            <li>
                <a href="#" id="facturas">
                    <i class="fas fa-briefcase"></i>
                    Facturas
                </a>
            </li>
            <li>
                <a href="#" id="egresosinventarios">
                    <i class="fas fa-briefcase"></i>
                    Egresos Inv
                </a>
            </li>
            <li>
                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-copy"></i>
                    Admin
                </a>
                <ul class="collapse list-unstyled" id="pageSubmenu">
                    <li>
                        <a id="gruposcontables" href="#">Grupos Contables</a>
                    </li>
                    <li>
                        <a href="#" id="plazoscreditos">Plazos de Creditos</a>
                    </li>
                    <li>
                        <a href="#" id="tiposcontribuyentes">Tipos de Contribuyentes</a>
                    </li>
                    <li>
                        <a href="#" id="retenciones">Retenciones</a>
                    </li>
                    <li>
                        <a href="#" id="impuestos">Impuestos</a>
                    </li>
                    <li>
                        <a href="#" id="servicios">Servicios</a>
                    </li>
                    <li>
                        <a href="#" id="transportistas">Transportistas</a>
                    </li>
                    <li>
                        <a href="#" id="vehiculostransportes">Vehiculos</a>
                    </li>
                    <li>
                        <a href="#" id="ubicacionesgeo">Ubicaciones</a>
                    </li>
                    <li>
                        <a href="#" id="datosadicionales">Datos Adicionales</a>
                    </li>
                    <div class="dropdown-divider"></div>
                    <li>
                        <a href="#" id="empresas">Empresas</a>
                    </li>
                    <li>
                        <a href="#" id="establecimientos">Establecimientos</a>
                    </li>
                    <li>
                        <a href="#" id="puntosventas">Puntos de Venta</a>
                    </li>
                    <li>
                        <a href="#" id="usuarios">Usuarios</a>
                    </li>
                    <div class="dropdown-divider"></div>
                    <li>
                        <a href="#" id="bodegas">Bodegas</a>
                    </li> 
                    <li>
                        <a href="#" id="productos">Productos</a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
    <div id="content">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-justify"></i>
                    <span></span>
                </button>
                <button type="button" id="cerrarSesion" class="pull-right btn btn-primary">
                    Cerrar Sesion
                </button>
            </div>
        </nav>
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1" id="tabs-1">Nunc tincidunt</a> <span class="ui-icon ui-icon-close" role="presentation">Remove Tab</span></li>
            </ul>
            <div id="tabs-1">
                <p>Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.</p>
            </div>
        </div>        
    </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="respuesta">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Alerta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert" role="alert" id="mensaje">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
