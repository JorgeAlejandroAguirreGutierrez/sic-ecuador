@extends('plantillaHEAD')
@section('title')
Recaudaciones Comprobantes de Retencion
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/cobros/recaudacionesComprobantesRetenciones.js')}}"></script>
@endsection
@section('content')
<div class="row">
    <div class="col-2">
        <nav class="nav flex-column">
            <a class="nav-link active" href="">Ver</a>
            <a class="nav-link" href="/recaudacionesefectivo/crear?id={{$cabeceraFactura->id}}">Efectivo</a>
            <a class="nav-link" href="/recaudacionestarjetas/crear?id={{$cabeceraFactura->id}}">Tarjeta</a>
            <a class="nav-link" href="/recaudacionescomprobantesretenciones/crear?id={{$cabeceraFactura->id}}">Retencion</a>
            <a class="nav-link" href="/recaudacionescreditos/crear?id={{$cabeceraFactura->id}}">Credito</a>
        </nav>
    </div>
    <div class="col-10">
        <h2>RECAUDACIONES COMPROBANTES DE RETENCION</h2>
        <hr>
        <div class="form-group form-row">
            <label for="codigo" class="col-auto col-form-label">Cod Fac</label>
            <div class="col-3">
                <input type="hidden" id="servicios" value="">  
                <input type="hidden" id="productos" value="">  
                <input type="hidden" id="cabecera_factura_id" value="{{$cabeceraFactura->id}}">  
                <input type="hidden" id="cliente" value="{{$cabeceraFactura->cliente->razon_social}}">  
                <input type="text" name="codigo_interno_factura" id="codigo_interno_factura" class="form-control" value="{{$cabeceraFactura->codigo_interno}}" disabled="">  
            </div>
            <label for="total" class="col-auto col-form-label">Total</label>
            <div class="col-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">$</div>
                    </div>
                    <input type="text" name="total" id="total" class="form-control" value="{{$cabeceraFactura->total}}" disabled="">  
                </div>
            </div>
            <label for="recaudo" class="col-auto col-form-label">Rec</label>
            <div class="col-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">$</div>
                    </div>
                    <input type="text" name="recaudo" id="recaudo" class="form-control" value="{{$recaudo}}" disabled="">  
                </div>
            </div>
            <label for="saldo" class="col-auto col-form-label">Saldo</label>
            <div class="col">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">$</div>
                    </div>
                    <input type="text" name="saldo" id="saldo" class="form-control" value="{{$cabeceraFactura->total-$recaudo}}" disabled="">
                </div>
            </div>
        </div>
        <hr>
        <div class="form-group form-row">
            <input type="hidden" id="comprobante_retencion_id" value="-1">
            <label for="codigo" class="col-1 col-form-label">Codigo</label>
            <div class="col-4">
                <input type="text" name="codigo" id="codigo" class="form-control form-control-sm" disabled="">
                <div id="invalido_codigo" class="invalid-feedback">
                </div>
            </div>
            <label for="comprobante_retencion" class="col-auto col-form-label">Comp Ret</label>
            <div class="col-1">
                <input type="text" name="comprobante_retencion" id="comprobante_retencion_establecimiento" class="form-control form-control-sm">
                <div id="invalido_comprobante_retencion" class="invalid-feedback">
                </div>
            </div>
            <div class="col-1">
                <input type="text" name="comprobante_retencion" id="comprobante_retencion_punto_venta" class="form-control form-control-sm">
                <div id="invalido_comprobante_retencion" class="invalid-feedback">
                </div>
            </div>
            <div class="col">
                <input type="text" name="comprobante_retencion" id="comprobante_retencion_secuencia" class="form-control form-control-sm">
                <div id="invalido_comprobante_retencion" class="invalid-feedback">
                </div>
            </div>
        </div>
        <div class="form-group form-row">
            <label for="cliente_id" class="col-auto col-form-label">Cliente</label>
            <div class="col-2">
                <input type="text" name="cliente_id" id="cliente_id" class="form-control form-control-sm" disabled="" value="{{$cabeceraFactura->cliente->razon_social}}">
                <div id="invalido_cliente_id" class="invalid-feedback">
                </div>
            </div>
            <label for="ruc_id" class="col-auto col-form-label">RUC</label>
            <div class="col-2">
                <input type="text" name="ruc_id" id="ruc_id" class="form-control form-control-sm" disabled="" value="{{$cabeceraFactura->cliente->identificacion}}">
                <div id="invalido_ruc_id" class="invalid-feedback">
                </div>
            </div>
            <label for="telefono_id" class="col-auto col-form-label">Telefono</label>
            <div class="col-2">
                <input type="text" name="telefono_id" id="telefono_id" class="form-control form-control-sm" disabled="" value="{{$cabeceraFactura->cliente->telefono}}">
                <div id="invalido_telefono_id" class="invalid-feedback">
                </div>
            </div>
            <label for="direccion_id" class="col-auto col-form-label">Direccion</label>
            <div class="col">
                <input type="text" name="direccion_id" id="direccion_id" class="form-control form-control-sm" disabled="" value="{{$cabeceraFactura->cliente->direccion}}">
                <div id="invalido_direccion_id" class="invalid-feedback">
                </div>
            </div>
        </div>
        <div class="form-group form-row">
            <label for="fecha_registro" class="col-auto col-form-label">Fecha Registro</label>
            <div class="col-2">
                <input type="date" name="fecha_registro" id="fecha_registro" class="form-control form-control-sm" disabled="" value="{{$cabeceraFactura->fecha}}">
                <div id="invalido_fecha_registro" class="invalid-feedback">
                </div>
            </div>
            <label for="tipo_id" class="col-auto col-form-label">Tipo Comprobante</label>
            <div class="col-2">
                <select type="text" name="tipo_id" id="tipo_id" class="form-control form-control-sm" disabled="">
                    <option value="{{$tipo->id}}">{{$tipo->nombre}}</option>
                </select>
                <div id="invalido_tipo_id" class="invalid-feedback">
                </div>
            </div>
            <label for="fecha_emision" class="col-auto col-form-label">Fecha Emision</label>
            <div class="col">
                <input type="date" name="fecha_emision" id="fecha_emision" class="form-control form-control-sm" disabled="" value="{{$cabeceraFactura->fecha}}">
                <div id="invalido_fecha_emision" class="invalid-feedback">
                </div>
            </div>
        </div>
        <div class="form-group form-row">
            <label for="autorizacion" class="col-auto col-form-label">Autorizacion</label>
            <div class="col">
                <input type="text" name="autorizacion" id="autorizacion" class="form-control form-control-sm" required>
                <div id="invalido_autorizacion" class="invalid-feedback">
                </div>
            </div>
        </div>
        <div id="items_comprobante_retencion">
            <input type="hidden" id="ano_retencion" value="{{$anoRetencion}}">
            <input type="hidden" id="serie_establecimiento_retencion" value="{{$serieEstablecimientoRetencion}}">
            <input type="hidden" id="serie_punto_venta_retencion" value="{{$seriePuntoVentaRetencion}}">
            <input type="hidden" id="cabecera_factura_retencion" value="{{$cabeceraFacturaRetencion}}">
            @if (isset($itemsComprobanteRetencion))
            @foreach($itemsComprobanteRetencion as $itemComprobanteRetencion)
            <div class="form-group form-row">
                <label for="año_retencion">&nbsp;&nbsp;&nbsp;&nbsp;Año</label>
                <label for="serie_retencion">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Serie</label>
                <label for="cabecera_factura_id_retencion">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Factura</label>
                <label for="codigo_retencion">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cod. Ret</label>
                <label for="nombre_retencion">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nom. Ret</label>
                <label for="base_imponible_retencion">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B Impon</label>
                <label for="porcentaje_retencion">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;%Ret</label>
                <label for="valor_retencion">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Valor</label>
                <input type="text" name="ano_retencion" id="ano_retencion_0" class="form-control form-control-sm col-1" disabled="" value="{{$anoRetencion}}">
                <input type="text" name="serie_retencion" id="serie_establecimiento_retencion_0" class="form-control form-control-sm col-1" disabled="" value="{{$serieEstablecimientoRetencion}}">
                <input type="text" name="serie_retencion" id="serie_punto_venta_retencion_0" class="form-control form-control-sm col-1" disabled="" value="{{$seriePuntoVentaRetencion}}">
                <input type="text" name="cabecera_factura_retencion" id="cabecera_factura_retencion_0" class="form-control form-control-sm col-2" disabled="" value="{{$cabeceraFacturaRetencion}}">
                <select name="retencion_id" id="retencion_id_0" class="form-control form-control-sm col-2">
                    <option value="{{ $itemComprobanteRetencion->retencion->id }}" selected>{{ $itemComprobanteRetencion->retencion->codigo_norma }}</option>
                </select>
                <input type="text" name="nombre_retencion" id="nombre_retencion_0" class="form-control form-control-sm col-2" disabled="" value="{{ $itemComprobanteRetencion->retencion->descripcion }}">
                <input type="text" name="base_imponible_retencion" id="base_imponible_retencion_0" class="form-control form-control-sm col-1" value="{{ $itemComprobanteRetencion->base_imponible }}">
                <input type="text" name="porcentaje_retencion" id="porcentaje_retencion_0" class="form-control form-control-sm col-1" disabled="" value="{{ $itemComprobanteRetencion->retencion->porcentaje }}">
                <input type="text" name="valor_retencion" id="valor_retencion_0" class="form-control form-control-sm col-1" value="{{ $itemComprobanteRetencion->base_imponible }}">
            </div>
            @endforeach
            @else
            <div class="form-group form-row">
                <label for="año_retencion">&nbsp;&nbsp;&nbsp;&nbsp;Año</label>
                <label for="serie_retencion">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Serie</label>
                <label for="cabecera_factura_id_retencion">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Factura</label>
                <label for="codigo_retencion">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cod. Ret</label>
                <label for="nombre_retencion">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nom. Ret</label>
                <label for="base_imponible_retencion">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B Impon</label>
                <label for="porcentaje_retencion">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;%Ret</label>
                <label for="valor_retencion">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Valor</label>
                <input type="text" name="ano_retencion" id="ano_retencion_0" class="form-control form-control-sm col-1" disabled="">
                <input type="text" name="serie_retencion" id="serie_establecimiento_retencion_0" class="form-control form-control-sm col-1" disabled="">
                <input type="text" name="serie_retencion" id="serie_punto_venta_retencion_0" class="form-control form-control-sm col-1" disabled="">
                <input type="text" name="cabecera_factura_retencion" id="cabecera_factura_retencion_0" class="form-control form-control-sm col-2" disabled="">
                <select name="retencion_id" id="retencion_id_0" class="form-control form-control-sm col-2"></select>
                <input type="text" name="nombre_retencion" id="nombre_retencion_0" class="form-control form-control-sm col-2" disabled="">
                <input type="text" name="base_imponible_retencion" id="base_imponible_retencion_0" class="form-control form-control-sm col-1">
                <input type="text" name="porcentaje_retencion" id="porcentaje_retencion_0" class="form-control form-control-sm col-1" disabled="">
                <input type="text" name="valor_retencion" id="valor_retencion_0" class="form-control form-control-sm col-1">
            </div>
            @endif
        </div>
        <div class="form-group text-center">
            <button class="btn btn-success" id="crearComprobanteRetencion">Guardar</button>
            <button class="btn btn-secondary" id="cargarComprobanteRetencion" data-toggle="modal" data-target="#modalCargarComprobanteRetencion">Cargar</button>
        </div>
        @if (isset($comprobantesRetenciones))
        <div class="form-group form-row">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>COD</th>
                        <th>REGISTRO</th>
                        <th>EMISION</th>
                        <th>AUTORI</th>
                        <th>VALOR</th>
                        <th>OPCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    @if($comprobantesRetenciones!=null)
                    @foreach($comprobantesRetenciones as $comprobanteRetencion)
                    <tr>
                        <td>
                            {{$comprobanteRetencion->codigo}}
                        </td>
                        <td>
                            {{$comprobanteRetencion->fecha_registro}}
                        </td>
                        <td>
                            {{$comprobanteRetencion->fecha_emision}}
                        </td>
                        <td>
                            {{$comprobanteRetencion->autorizacion}}
                        </td>
                        <td>
                            {{$comprobanteRetencion->valor}}
                        </td>
                        <td>
                            <div class="btn-group btn-group-justified">
                                <button type="button" class="btn btn-default" name="modificarComprobanteRetencion" id="{{$comprobanteRetencion->id}}" data-toggle="modal" data-target="#modalModificarComprobanteRetencion">
                                    <i class="fas fa-edit"></i>
                                </button>
                                <button type="button" class="btn btn-default" name="eliminarComprobanteRetencion" id="{{$comprobanteRetencion->id}}" data-toggle="modal" data-target="#modalEliminarComprobanteRetencion">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        @endif
        <div id="alerta" role="alert">
        </div>
    </div>
    <div class="col-2"></div>
</div>
<div class="modal fade" id="modalEliminarComprobanteRetencion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar comprobante de retencion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar?
                <input type="hidden" id="idE" value="-1">
                <br>
                <br>
                <div>
                    <div class="form-group form-row">
                        <label for="codigoE" class="col-3 col-form-label">Codigo</label>
                        <div class="col-9">
                            <input type="text" name="codigoE" id="codigoE" class="form-control" disabled="">  
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="cliente_idE" class="col-3 col-form-label">Cliente</label>
                        <div class="col-9">
                            <input type="text" name="cliente_idE" id="cliente_idE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="fecha_registroE" class="col-3 col-form-label">Fecha de Registro</label>
                        <div class="col-9">
                            <input type="text" name="fecha_registroE" id="fecha_registroE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="tipo_comprobante_idE" class="col-3 col-form-label">Tipo de comprobante</label>
                        <div class="col-9">
                            <input type="text" name="tipo_comprobante_idE" id="tipo_comprobante_idE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="fecha_emisionE" class="col-3 col-form-label">Fecha Emision</label>
                        <div class="col-9">
                            <input type="text" name="fecha_emisionE" id="fecha_emisionE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="devolucionE" class="col-3 col-form-label">Dev. Ret</label>
                        <div class="col-9">
                            <input type="text" name="devolucionE" id="devolucionE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="afecta_contabilidadE" class="col-3 col-form-label">Af. Cont</label>
                        <div class="col-9">
                            <input type="text" name="afecta_contabilidadE" id="afecta_contabilidadE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="autorizacionE" class="col-3 col-form-label">Autorizacion</label> 
                        <div class="col-9">
                            <input type="text" name="autorizacionE" id="autorizacionE" class="form-control" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarComprobanteRetencion">Eliminar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEliminarItemRetencion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Registro de Tarjeta de Debito</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar?
                <input type="hidden" id="id_chequeE" value="-1">
                <br>
                <br>
                <div>
                    <div class="form-group form-row">
                        <label for="codigo_tarjeta_debitoE" class="col-3 col-form-label">Codigo</label>
                        <div class="col-9">
                            <input type="text" name="codigo_tarjeta_debitoE" id="codigo_tarjeta_debitoE" class="form-control" disabled="">  
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="anoE" class="col-3 col-form-label">Año</label>
                        <div class="col-9">
                            <input type="text" name="anoE" id="anoE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="serieE" class="col-3 col-form-label">Serie</label>
                        <div class="col-9">
                            <input type="text" name="ifis_tarjeta_debitoE" id="ifis_tarjeta_creditoE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="nombreE" class="col-3 col-form-label">Nombre</label>
                        <div class="col-9">
                            <input type="text" name="nombreE" id="nombreE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="base_imponibleE" class="col-3 col-form-label">Base Imponible</label>
                        <div class="col-9">
                            <input type="text" name="base_imponibleE" id="base_imponibleE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="porcentajeE" class="col-3 col-form-label">Porcentaje</label>
                        <div class="col-9">
                            <input type="text" name="porcentajeE" id="porcentajeE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="valorE" class="col-3 col-form-label">Valor</label> 
                        <div class="col-9">
                            <input type="text" name="valorE" id="valorE" class="form-control" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarTarjetaDebito">Eliminar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalCargarComprobanteRetencion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cargar Comprobante de Retencion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label>Subir Archivo XML del comprobante de retencion para la factura</label><br />
                <input type="file" id="archivo" name="archivo" /><br /><br />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" id="cargarXML">Cargar XML</button>
            </div>
        </div>
    </div>
</div>
@endsection
