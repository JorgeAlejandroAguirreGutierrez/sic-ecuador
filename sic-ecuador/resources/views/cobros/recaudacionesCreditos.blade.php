@extends('plantillaHEAD')
@section('title')
Recaudaciones Comprobantes de Retencion
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/cobros/recaudacionesCreditos.js')}}"></script>
@endsection
@section('content')
<div class="row">
    <div class="col-2">
        <nav class="nav flex-column">
            <a class="nav-link active" href="">Ver</a>
            <a class="nav-link" href="/recaudacionesefectivo/crear?id={{$cabeceraFactura->id}}">Efectivo</a>
            <a class="nav-link" href="/recaudacionestarjetas/crear?id={{$cabeceraFactura->id}}">Tarjeta</a>
            <a class="nav-link" href="/recaudacionescomprobantesretenciones/crear?id={{$cabeceraFactura->id}}">Retencion</a>
            <a class="nav-link" href="/recaudacionescreditos/crear?id={{$cabeceraFactura->id}}">Credito</a>
        </nav>
    </div>
    <div class="col-10">
        <h2>RECAUDACIONES CREDITOS</h2>
        <hr>
        <div class="form-group form-row">
            <label for="codigo" class="col-auto col-form-label">Cod Fac</label>
            <div class="col-3">
                <input type="hidden" id="servicios" value="">  
                <input type="hidden" id="productos" value="">  
                <input type="hidden" id="cabecera_factura_id" value="{{$cabeceraFactura->id}}">  
                <input type="hidden" id="cliente" value="{{$cabeceraFactura->cliente->razon_social}}">  
                <input type="hidden" id="fecha_factura" value="{{$cabeceraFactura->fecha}}">  
                <input type="text" name="codigo_interno_factura" id="codigo_interno_factura" class="form-control" value="{{$cabeceraFactura->codigo_interno}}" disabled="">  
            </div>
            <label for="total" class="col-auto col-form-label">Total</label>
            <div class="col-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">$</div>
                    </div>
                    <input type="text" name="total" id="total" class="form-control" value="{{$cabeceraFactura->total}}" disabled="">  
                </div>
            </div>
            <label for="recaudo" class="col-auto col-form-label">Rec</label>
            <div class="col-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">$</div>
                    </div>
                    <input type="text" name="recaudo" id="recaudo" class="form-control" value="{{$recaudo}}" disabled="">  
                </div>
            </div>
            <label for="saldo" class="col-auto col-form-label">Saldo</label>
            <div class="col">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">$</div>
                    </div>
                    <input type="text" name="saldo" id="saldo" class="form-control" value="{{$cabeceraFactura->total-$recaudo}}" disabled="">
                </div>
            </div>
        </div>
        <hr>
        <div class="form-group form-row">
            <input type="hidden" name="credito_id" id="credito_id" value="-1">
            <label for="codigo" class="col-1 col-form-label">Codigo</label>
            <div class="col-2">
                <input type="text" name="codigo" id="codigo" class="form-control form-control-sm" disabled="">
                <div id="invalido_codigo" class="invalid-feedback">
                </div>
            </div>
            <label for="cuotas" class="col-1 col-form-label">Cuotas.</label>
            <div class="col-1">
                <input type="number" name="cuotas" id="cuotas" class="form-control form-control-sm" value="0">
                <div id="invalido_cuotas" class="invalid-feedback">
                </div>
            </div>
            <label for="tiempo" class="col-1 col-form-label">Tiempo.</label>
            <div class="col-1">
                <input type="number" name="tiempo" id="tiempo" class="form-control form-control-sm">
                <div id="invalido_tiempo" class="invalid-feedback">
                </div>
            </div>
            <label for="forma_tiempo_id" class="col-1 col-form-label">Tipo.</label>
            <div class="col-1">
                <select name="forma_tiempo_id" id="forma_tiempo_id" class="form-control form-control-sm">
                </select>
                <div id="invalido_forma_tiempo_id" class="invalid-feedback">
                </div>
            </div>
            <label for="porcentaje_interes" class="col-1 col-form-label">Interes.</label>
            <div class="col-2">
                <div class="input-group input-group-sm">
                    <input type="number" step="any" name="porcentaje_interes" id="porcentaje_interes" class="form-control form-control-sm" value="0">
                    <div class="input-group-prepend">
                        <div class="input-group-text">%</div>
                    </div>
                    <div id="invalido_porcentaje_interes" class="invalid-feedback">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group text-center">
            <button class="btn btn-primary" id="generarCredito">Generar Credito</button>
        </div>
        <div class="form-group text-center">
            <button class="btn btn-outline-info" id="generarAmortizacionCredito" data-toggle="modal" data-target="#modalGenerarAmortizacionCredito">Generar Amortizacion</button>
        </div>
        <div class="form-group form-row">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Cuotas</th>
                        <th>Documento</th>
                        <th>Emision</th>
                        <th>Vencimientos</th>
                        <th>Concepto</th>
                        <th>Importe</th>
                    </tr>
                </thead>
                <tbody id="tabla_generar_credito">
                </tbody>
            </table>
        </div>
        <div class="form-group text-center">
            <button class="btn btn-success" id="crearCredito">Guardar</button>
        </div>
        <div class="form-group form-row">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>COD</th>
                        <th>INTERES</th>
                        <th>No. CUOTAS</th>
                        <th>VALOR</th>
                        <th>SALDO</th>
                        <th>OPCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    @if($creditos!=null)
                    @foreach($creditos as $credito)
                    <tr>
                        <td>
                            {{$credito->codigo}}
                        </td>
                        <td>
                            {{$credito->porcentaje_interes}}
                        </td>
                        <td>
                            {{$credito->cuotas}}
                        </td>
                        <td>
                            {{$credito->valor}}
                        </td>
                        <td>
                            {{$credito->saldo}}
                        </td>
                        <td>
                            <div class="btn-group btn-group-justified">
                                <button type="button" class="btn btn-default" name="modificarCredito" id="{{$credito->id}}" data-toggle="modal" data-target="#modalModificarCredito">
                                    <i class="fas fa-edit"></i>
                                </button>
                                <button type="button" class="btn btn-default" name="eliminarCredito" id="{{$credito->id}}" data-toggle="modal" data-target="#modalEliminarCredito">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        <div id="alerta" role="alert">
        </div>
    </div>
    <div class="col-2"></div>
</div>
<div class="modal fade" id="modalGenerarAmortizacionCredito" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <h2>TABLA DE AMORTIZACION</h2>
                    <hr>
                    <input type="hidden" id="id_A" value="-1">
                    <div class="form-group form-row">
                        <label for="pago_A" class="col-4 col-form-label">Pago</label>
                        <div class="col-8">
                            <input type="text" name="pago_A" id="pago_A" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Periodo</th>
                                    <th>Saldo Inicial</th>
                                    <th>Dividendo</th>
                                    <th>Pago Capital</th>
                                    <th>Pago Interes</th>
                                    <th>Saldo Final</th>
                                </tr>
                            </thead>
                            <tbody id="tabla_generar_amortizacion">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <div class="col-2"></div>
    </div>
</div>

<div class="modal fade" id="modalEliminarCredito" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Credito</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar el credito?
                <br>
                <br>
                <input type="hidden" id="id_E" value="-1">
                <div>
                    <div class="form-group form-row">
                        <label for="codigo_E" class="col-3 col-form-label">Codigo</label>
                        <div class="col-9">
                            <input type="text" name="codigo_E" id="codigo_E" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="cuotas_E" class="col-3 col-form-label">Cuotas</label>
                        <div class="col-9">
                            <input type="text" name="cuotas_E" id="cuotas_E" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="porcentaje_interes_E" class="col-3 col-form-label">Porcentaje Interes</label>
                        <div class="col-9">
                            <input type="number" name="porcentaje_interes_E" id="porcentaje_interes_E" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="saldo_E" class="col-3 col-form-label">Saldo</label>
                        <div class="col-9">
                            <input type="number" name="saldo_E" id="saldo_E" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="valor_E" class="col-3 col-form-label">Valor</label>
                        <div class="col-9">
                            <input type="number" name="valor_E" id="valor_E" class="form-control" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarCredito">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection
