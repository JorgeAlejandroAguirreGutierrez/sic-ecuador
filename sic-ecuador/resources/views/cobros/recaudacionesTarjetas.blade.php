@extends('plantillaHEAD')
@section('title')
Recaudaciones Efectivo
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/cobros/recaudacionesTarjetas.js')}}"></script>
@endsection
@section('content')
<div class="row">
    <div class="col-2">
        <nav class="nav flex-column">
            <a class="nav-link active" href="">Ver</a>
            <a class="nav-link" href="/recaudacionesefectivo/crear?id={{$cabeceraFactura->id}}">Efectivo</a>
            <a class="nav-link" href="/recaudacionestarjetas/crear?id={{$cabeceraFactura->id}}">Tarjeta</a>
            <a class="nav-link" href="/recaudacionescomprobantesretenciones/crear?id={{$cabeceraFactura->id}}">Retencion</a>
            <a class="nav-link" href="/recaudacionescreditos/crear?id={{$cabeceraFactura->id}}">Credito</a>
        </nav>
    </div>
    <div class="col-8">
        <h2>RECAUDACIONES TARJETA</h2>
        <hr>
        <div class="form-group form-row">
            <label for="codigo" class="col-auto col-form-label">Cod Fac</label>
            <div class="col-3">
                <input type="hidden" id="servicios" value="">  
                <input type="hidden" id="productos" value="">  
                <input type="hidden" id="cabecera_factura_id" value="{{$cabeceraFactura->id}}">  
                <input type="hidden" id="cliente" value="{{$cabeceraFactura->cliente->razon_social}}">  
                <input type="text" name="codigo_interno_factura" id="codigo_interno_factura" class="form-control" value="{{$cabeceraFactura->codigo_interno}}" disabled="">  
            </div>
            <label for="total" class="col-auto col-form-label">Total</label>
            <div class="col-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">$</div>
                    </div>
                    <input type="text" name="total" id="total" class="form-control" value="{{$cabeceraFactura->total}}" disabled="">  
                </div>
            </div>
            <label for="recaudo" class="col-auto col-form-label">Rec</label>
            <div class="col-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">$</div>
                    </div>
                    <input type="text" name="recaudo" id="recaudo" class="form-control" value="{{$recaudo}}" disabled="">  
                </div>
            </div>
            <label for="saldo" class="col-auto col-form-label">Saldo</label>
            <div class="col">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">$</div>
                    </div>
                    <input type="text" name="saldo" id="saldo" class="form-control" value="{{$cabeceraFactura->total-$recaudo}}" disabled="">
                </div>
            </div>
        </div>
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-tarjeta_credito-tab" data-toggle="tab" href="#nav-tarjeta_credito" role="tab" aria-controls="nav-tarjeta_credito" aria-selected="true">TARJETA CREDITO</a>
                <a class="nav-item nav-link" id="nav-tarjeta_debito-tab" data-toggle="tab" href="#nav-tarjeta_debito" role="tab" aria-controls="nav-tarjeta_debito" aria-selected="false">TARJETA DEBITO</a>
            </div>
        </nav>
        <br>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade" id="nav-tarjeta_credito" role="tabpanel" aria-labelledby="nav-tarjeta_credito-tab">
                <div>
                    <div class="form-group form-row">
                        <label for="codigo_tarjeta_credito" class="col-4 col-form-label">Codigo</label>
                        <div class="col-8">
                            <input type="text" name="codigo_tarjeta_credito" id="codigo_tarjeta_credito" class="form-control" disabled="">
                            <div id="invalido_codigo_tarjeta_credito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="tarjeta_id_tarjeta_credito" class="col-4 col-form-label">Tarjeta</label>
                        <div class="col-8">
                            <select name="tarjeta_id_tarjeta_credito" id="tarjeta_id_tarjeta_credito" class="form-control" required>
                            </select>
                            <div id="invalido_tarjeta_id_tarjeta_credito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="ifi_id_tarjeta_credito" class="col-4 col-form-label">Institucion Financiera</label>
                        <div class="col-8">
                            <select name="ifi_id_tarjeta_credito" id="ifi_id_tarjeta_credito" class="form-control" required>
                            </select>
                            <div id="invalido_ifi_id_tarjeta_credito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="operador_id_tarjeta_credito" class="col-4 col-form-label">Operador</label>
                        <div class="col-8">
                            <select name="operador_id_tarjeta_credito" id="operador_id_tarjeta_credito" class="form-control" required>
                            </select>
                            <div id="invalido_operador_id_tarjeta_credito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="boucher_tarjeta_credito" class="col-4 col-form-label">No. Boucher</label>
                        <div class="col-8">
                            <input type="text" name="boucher_tarjeta_credito" id="boucher_tarjeta_credito" class="form-control" required>
                            <div id="invalido_boucher_tarjeta_credito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="lote_tarjeta_credito" class="col-4 col-form-label">No. Lote</label>
                        <div class="col-8">
                            <input type="text" name="lote_tarjeta_credito" id="lote_tarjeta_credito" class="form-control" required>
                            <div id="invalido_lote_tarjeta_credito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="fecha_tarjeta_credito" class="col-4 col-form-label">Fecha</label>
                        <div class="col-8">
                            <input type="date" name="fecha_tarjeta_credito" id="fecha_tarjeta_credito" class="form-control" disabled="">
                            <div id="invalido_fecha_tarjeta_credito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="forma_cobro_id_tarjeta_credito" class="col-4 col-form-label">Forma Cobro</label>
                        <div class="col-8">
                            <select name="forma_cobro_id_tarjeta_credito" id="forma_cobro_id_tarjeta_credito" class="form-control">
                            </select>
                            <div id="invalido_forma_cobro_id_tarjeta_credito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="comentario_tarjeta_credito" class="col-4 col-form-label">Comentario</label>
                        <div class="col-8">
                            <textarea name="comentario_tarjeta_credito" id="comentario_tarjeta_credito" class="form-control" required></textarea>
                            <div id="invalido_comentario_tarjeta_credito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="valor_tarjeta_credito" class="col-4 col-form-label">Valor</label>
                        <div class="col-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">$</div>
                                </div>
                                <input type="number" step="any" name="valor_tarjeta_credito" id="valor_tarjeta_credito" class="form-control" required value="{{$cabeceraFactura->total-$recaudo}}" max="{{$cabeceraFactura->total-$recaudo}}">
                            </div>
                            <div id="invalido_valor_tarjeta_credito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-success" id="crearRegistroTarjetaCredito">Guardar</button>
                    </div>
                    <div id="alerta" role="alert">
                    </div>
                    <div class="form-group form-row">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>TARJETA</th>
                                    <th>BOUCHER</th>
                                    <th>LOTE</th>
                                    <th>COBRO</th>
                                    <th>COMENTARIO</th>
                                    <th>VALOR</th>
                                    <th>OPCIONES</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($recaudacionesTarjetas!=null)
                                @foreach($recaudacionesTarjetas['TARJETA CREDITO'] as $tarjetaCredito)
                                <tr>
                                    <td>
                                        {{$tarjetaCredito->tarjeta->nombre}}
                                    </td>
                                    <td>
                                        {{$tarjetaCredito->boucher}}
                                    </td>
                                    <td>
                                        {{$tarjetaCredito->lote}}
                                    </td>
                                    <td>
                                        {{$tarjetaCredito->formaCobro->nombre}}
                                    </td>
                                    <td>
                                        {{$tarjetaCredito->comentario}}
                                    </td>
                                    <td>
                                        ${{$tarjetaCredito->valor}}
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-justified">
                                            <button type="button" class="btn btn-default" name="modificarTarjetaCredito" id="{{$tarjetaCredito->id}}" data-toggle="modal" data-target="#modalModificarTarjetaCredito">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                            <button type="button" class="btn btn-default" name="eliminarTarjetaCredito" id="{{$tarjetaCredito->id}}" data-toggle="modal" data-target="#modalEliminarTarjetaCredito">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-tarjeta_debito" role="tabpanel" aria-labelledby="nav-tarjeta_debito-tab">
                <div>
                    <div class="form-group form-row">
                        <label for="codigo_tarjeta_debito" class="col-4 col-form-label">Codigo</label>
                        <div class="col-8">
                            <input type="text" name="codigo_tarjeta_debito" id="codigo_tarjeta_debito" class="form-control" disabled="">
                            <div id="invalido_codigo_tarjeta_debito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="tarjeta_id_tarjeta_debito" class="col-4 col-form-label">Tarjeta</label>
                        <div class="col-8">
                            <select name="tarjeta_id_tarjeta_debito" id="tarjeta_id_tarjeta_debito" class="form-control" required>
                            </select>
                            <div id="invalido_tarjeta_id_tarjeta_debito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="ifi_id_tarjeta_debito" class="col-4 col-form-label">Institucion Financiera</label>
                        <div class="col-8">
                            <select name="ifi_id_tarjeta_debito" id="ifi_id_tarjeta_debito" class="form-control" required>
                            </select>
                            <div id="invalido_ifi_id_tarjeta_debito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="operador_id_tarjeta_debito" class="col-4 col-form-label">Operador</label>
                        <div class="col-8">
                            <select name="operador_id_tarjeta_debito" id="operador_id_tarjeta_debito" class="form-control" required>
                            </select>
                            <div id="invalido_operador_id_tarjeta_debito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="boucher_tarjeta_debito" class="col-4 col-form-label">No. Boucher</label>
                        <div class="col-8">
                            <input type="text" name="boucher_tarjeta_debito" id="boucher_tarjeta_debito" class="form-control" required>
                            <div id="invalido_boucher_tarjeta_debito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="lote_tarjeta_debito" class="col-4 col-form-label">No. Lote</label>
                        <div class="col-8">
                            <input type="text" name="lote_tarjeta_debito" id="lote_tarjeta_debito" class="form-control" required>
                            <div id="invalido_lote_tarjeta_debito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="fecha_tarjeta_debito" class="col-4 col-form-label">Fecha</label>
                        <div class="col-8">
                            <input type="date" name="fecha_tarjeta_debito" id="fecha_tarjeta_debito" class="form-control" disabled="">
                            <div id="invalido_fecha_tarjeta_debito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="comentario_tarjeta_debito" class="col-4 col-form-label">Comentario</label>
                        <div class="col-8">
                            <textarea name="comentario_tarjeta_debito" id="comentario_tarjeta_debito" class="form-control" required></textarea>
                            <div id="invalido_comentario_tarjeta_debito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="valor_tarjeta_debito" class="col-4 col-form-label">Valor</label>
                        <div class="col-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">$</div>
                                </div>
                                <input type="number" step="any" name="valor_tarjeta_debito" id="valor_tarjeta_debito" class="form-control" required value="{{$cabeceraFactura->total-$recaudo}}" max="{{$cabeceraFactura->total-$recaudo}}" min="0">
                            </div>
                            <div id="invalido_valor_tarjeta_debito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-success" id="crearRegistroTarjetaDebito">Guardar</button>
                    </div>
                    <div id="alerta" role="alert">
                    </div>
                    <div class="form-group form-row">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>TARJETA</th>
                                    <th>BOUCHER</th>
                                    <th>LOTE</th>
                                    <th>FECHA</th>
                                    <th>COMENTARIO</th>
                                    <th>VALOR</th>
                                    <th>OPCIONES</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($recaudacionesTarjetas!=null)
                                @foreach($recaudacionesTarjetas['TARJETA DEBITO'] as $tarjetaDebito)
                                <tr>
                                    <td>
                                        {{$tarjetaDebito->tarjeta->nombre}}
                                    </td>
                                    <td>
                                        {{$tarjetaDebito->boucher}}
                                    </td>
                                    <td>
                                        {{$tarjetaDebito->lote}}
                                    </td>
                                    <td>
                                        {{$tarjetaDebito->fecha}}
                                    </td>
                                    <td>
                                        {{$tarjetaDebito->comentario}}
                                    </td>
                                    <td>
                                        ${{$tarjetaDebito->valor}}
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-justified">
                                            <button type="button" class="btn btn-default" name="modificarTarjetaDebito" id="{{$tarjetaDebito->id}}" data-toggle="modal" data-target="#modalModificarTarjetaDebito">
                                                <i class="fas fa-edit"></i>
                                            <button type="button" class="btn btn-default" name="eliminarTarjetaDebito" id="{{$tarjetaDebito->id}}" data-toggle="modal" data-target="#modalEliminarTarjetaDebito">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-2"></div>
</div>

<div class="modal fade" id="modalModificarTarjetaCredito" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <h2>TARJETAS DE CREDITO</h2>
                    <hr>
                    <h3>Modificar registro de recaudacion de tarjeta de credito</h3>
                    <hr>
                    <input type="hidden" id="id_tarjeta_credito_M" value="-1">
                    <div>
                        <div class="form-group form-row">
                            <label for="codigo_tarjeta_credito_M" class="col-3 col-form-label">Codigo</label>
                            <div class="col-9">
                                <input type="text" name="codigo_tarjeta_credito_M" id="codigo_tarjeta_credito_M" class="form-control" placeholder="Codigo" required disabled="">  
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="boucher_tarjeta_credito_M" class="col-3 col-form-label">Boucher</label>
                            <div class="col-9">
                                <input type="text" name="boucher_tarjeta_credito_M" id="boucher_tarjeta_credito_M" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="lote_tarjeta_credito_M" class="col-3 col-form-label">Lote</label>
                            <div class="col-9">
                                <input type="text" name="lote_tarjeta_credito_M" id="lote_tarjeta_credito_M" class="form-control" placeholder="Nombre del Dato Adicional" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="fecha_tarjeta_credito_M" class="col-3 col-form-label">Fecha</label> 
                            <div class="col-9">
                                <input type="date" name="fecha_tarjeta_credito_M" id="fecha_tarjeta_credito_M" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="comentario_tarjeta_credito_M" class="col-3 col-form-label">Comentario</label> 
                            <div class="col-9">
                                <textarea name="comentario_tarjeta_credito_M" id="comentario_tarjeta_credito_M" class="form-control" required></textarea>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="valor_tarjeta_credito_M" class="col-3 col-form-label">Valor</label> 
                            <div class="col-9">
                                <input type="number" step="any" name="valor_tarjeta_credito_M" id="valor_tarjeta_credito_M" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="tarjeta_id_tarjeta_credito_M" class="col-3 col-form-label">Tarjeta</label> 
                            <div class="col-9">
                                <select name="tarjeta_id_tarjeta_credito_M" id="tarjeta_id_tarjeta_credito_M" class="form-control" required></select>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="ifi_id_tarjeta_credito_M" class="col-3 col-form-label">IFI</label> 
                            <div class="col-9">
                                <select name="ifi_id_tarjeta_credito_M" id="ifi_id_tarjeta_credito_M" class="form-control" required></select>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="operador_id_tarjeta_credito_M" class="col-3 col-form-label">Operador</label> 
                            <div class="col-9">
                                <select name="operador_id_tarjeta_credito_M" id="operador_id_tarjeta_credito_M" class="form-control" required></select>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="forma_cobro_id_tarjeta_credito_M" class="col-3 col-form-label">Forma Cobro</label> 
                            <div class="col-9">
                                <select name="forma_cobro_id_tarjeta_credito_M" id="forma_cobro_id_tarjeta_credito_M" class="form-control" required></select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" id="cModificarCheque">Modificar Cheque</button>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEliminarTarjetaCredito" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Registro de Tarjeta de Credito</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar?
                <input type="hidden" id="id_cheque_E" value="-1">
                <br>
                <br>
                <div>
                    <div class="form-group form-row">
                        <label for="codigo_tarjeta_credito_E" class="col-3 col-form-label">Codigo</label>
                        <div class="col-9">
                            <input type="text" name="codigo_tarjeta_credito_E" id="codigo_tarjeta_credito_E" class="form-control" disabled="">  
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="tarjeta_id_tarjeta_credito_E" class="col-3 col-form-label">Tarjeta</label>
                        <div class="col-9">
                            <input type="text" name="tarjeta_id_tarjeta_credito_E" id="tarjeta_id_tarjeta_credito_E" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="ifi_id_tarjeta_credito_E" class="col-3 col-form-label">Institucion Financiera</label>
                        <div class="col-9">
                            <input type="text" name="ifi_id_tarjeta_credito_E" id="ifi_id_tarjeta_credito_E" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="boucher_tarjeta_credito_E" class="col-3 col-form-label">Boucher</label>
                        <div class="col-9">
                            <input type="text" name="boucher_tarjeta_credito_E" id="boucher_tarjeta_credito_E" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="lote_tarjeta_credito_E" class="col-3 col-form-label">Lote</label>
                        <div class="col-9">
                            <input type="text" name="lote_tarjeta_credito_E" id="lote_tarjeta_credito_E" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="fecha_tarjeta_credito_E" class="col-3 col-form-label">fecha</label>
                        <div class="col-9">
                            <input type="text" name="fecha_tarjeta_credito_E" id="fecha_tarjeta_credito_E" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="forma_cobro_id_tarjeta_credito_E" class="col-3 col-form-label">Forma Cobro</label>
                        <div class="col-9">
                            <input type="text" name="forma_cobro_id_tarjeta_credito_E" id="forma_cobro_id_tarjeta_credito_E" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="valor_tarjeta_credito_E" class="col-3 col-form-label">Valor</label> 
                        <div class="col-9">
                            <input type="text" name="valor_tarjeta_credito_E" id="valor_tarjeta_credito_E" class="form-control" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarTarjetaCredito">Eliminar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalModificarTarjetaDebito" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <h2>TARJETAS DEBITO</h2>
                    <hr>
                    <h3>Modificar registro de recaudacion de tarjeta de debito</h3>
                    <hr>
                    <input type="hidden" id="id_tarjeta_debitoM" value="-1">
                    <div>
                        <div class="form-group form-row">
                            <label for="codigo_tarjeta_debito_M" class="col-3 col-form-label">Codigo</label>
                            <div class="col-9">
                                <input type="text" name="codigo_tarjeta_debito_M" id="codigo_tarjeta_debito_M" class="form-control" placeholder="Codigo" required disabled="">  
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="boucher_tarjeta_debito_M" class="col-3 col-form-label">Boucher</label>
                            <div class="col-9">
                                <input type="text" name="boucher_tarjeta_debito_M" id="boucher_tarjeta_debito_M" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="lote_tarjeta_debito_M" class="col-3 col-form-label">Lote</label>
                            <div class="col-9">
                                <input type="text" name="lote_tarjeta_debito_M" id="lote_tarjeta_debito_M" class="form-control" placeholder="Nombre del Dato Adicional" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="fecha_tarjeta_debito_M" class="col-3 col-form-label">Fecha</label> 
                            <div class="col-9">
                                <input type="text" name="fecha_tarjeta_debito_M" id="fecha_tarjeta_debito_M" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="comentario_tarjeta_debito_M" class="col-3 col-form-label">Comentario</label> 
                            <div class="col-9">
                                <textarea name="comentario_tarjeta_debito_M" id="comentario_tarjeta_debito_M" class="form-control" required></textarea>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="valor_tarjeta_debito_M" class="col-3 col-form-label">Valor</label> 
                            <div class="col-9">
                                <input type="number" name="valor_tarjeta_debito_M" id="valor_tarjeta_debito_M" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="tarjeta_id_tarjeta_debito_M" class="col-3 col-form-label">Tarjeta</label> 
                            <div class="col-9">
                                <input type="text" name="tarjeta_id_tarjeta_debito_M" id="tarjeta_id_tarjeta_debito_M" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="ifi_id_tarjeta_debito_M" class="col-3 col-form-label">IFI</label> 
                            <div class="col-9">
                                <input type="text" name="ifi_id_tarjeta_debito_M" id="ifi_id_tarjeta_debito_M" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="operador_id_tarjeta_debito_M" class="col-3 col-form-label">Operador</label> 
                            <div class="col-9">
                                <input type="text" name="operador_id_tarjeta_debito_M" id="operador_id_tarjeta_debito_M" class="form-control" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" id="cModificarTarjetaDebito">Modificar Cheque</button>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEliminarTarjetaDebito" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Registro de Tarjeta de Debito</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar?
                <input type="hidden" id="id_chequeE" value="-1">
                <br>
                <br>
                <div>
                    <div class="form-group form-row">
                        <label for="codigo_tarjeta_debito_E" class="col-5 col-form-label">Codigo</label>
                        <div class="col-7">
                            <input type="text" name="codigo_tarjeta_debito_E" id="codigo_tarjeta_debito_E" class="form-control" disabled="">  
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="tarjeta_id_tarjeta_debito_E" class="col-5 col-form-label">Tarjeta</label>
                        <div class="col-7">
                            <input type="text" name="tarjeta_id_tarjeta_debito_E" id="tarjeta_id_tarjeta_debito_E" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="ifi_id_tarjeta_debito_E" class="col-5 col-form-label">Institucion Financiera</label>
                        <div class="col-7">
                            <input type="text" name="ifi_id_tarjeta_debito_E" id="ifi_id_tarjeta_debito_E" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="boucher_tarjeta_debito_E" class="col-5 col-form-label">Boucher</label>
                        <div class="col-7">
                            <input type="text" name="boucher_tarjeta_debito_E" id="boucher_tarjeta_debito_E" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="lote_tarjeta_debito_E" class="col-5 col-form-label">Lote</label>
                        <div class="col-7">
                            <input type="text" name="lote_tarjeta_debito_E" id="lote_tarjeta_debito_E" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="fecha_tarjeta_debito_E" class="col-5 col-form-label">fecha</label>
                        <div class="col-7">
                            <input type="text" name="fecha_tarjeta_debito_E" id="fecha_tarjeta_debito_E" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="valor_tarjeta_debito_E" class="col-5 col-form-label">Valor</label> 
                        <div class="col-7">
                            <input type="text" name="valor_tarjeta_debito_E" id="valor_tarjeta_debito_E" class="form-control" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarTarjetaDebito">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection
