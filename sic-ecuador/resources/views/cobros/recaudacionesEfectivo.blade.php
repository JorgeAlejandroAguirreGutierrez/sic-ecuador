@extends('plantillaHEAD')
@section('title')
Recaudaciones Efectivo
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/cobros/recaudacionesEfectivo.js')}}"></script>
@endsection
@section('content')
<div class="row">
    <div class="col-2">
        <nav class="nav flex-column">
            <a class="nav-link active" href="">Ver</a>
            <a class="nav-link" href="/recaudacionesefectivo/crear?id={{$cabeceraFactura->id}}">Efectivo</a>
            <a class="nav-link" href="/recaudacionestarjetas/crear?id={{$cabeceraFactura->id}}">Tarjeta</a>
            <a class="nav-link" href="/recaudacionescomprobantesretenciones/crear?id={{$cabeceraFactura->id}}">Retencion</a>
            <a class="nav-link" href="/recaudacionescreditos/crear?id={{$cabeceraFactura->id}}">Credito</a>
        </nav>
    </div>
    <div class="col-8">
        <h2>RECAUDACIONES EFECTIVO</h2>
        <hr>
        <div class="form-group form-row">
            <label for="codigo" class="col-auto col-form-label">Cod Fac</label>
            <div class="col-3">
                <input type="hidden" id="servicios" value="">  
                <input type="hidden" id="productos" value="">  
                <input type="hidden" id="cabecera_factura_id" value="{{$cabeceraFactura->id}}">  
                <input type="hidden" id="cliente" value="{{$cabeceraFactura->cliente->razon_social}}">  
                <input type="text" name="codigo_interno_factura" id="codigo_interno_factura" class="form-control" value="{{$cabeceraFactura->codigo_interno}}" disabled="">  
            </div>
            <label for="total" class="col-auto col-form-label">Total</label>
            <div class="col-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">$</div>
                    </div>
                    <input type="text" name="total" id="total" class="form-control" value="{{$cabeceraFactura->total}}" disabled="">  
                </div>
            </div>
            <label for="recaudo" class="col-auto col-form-label">Rec</label>
            <div class="col-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">$</div>
                    </div>
                    <input type="text" name="recaudo" id="recaudo" class="form-control" value="{{$recaudo}}" disabled="">  
                </div>
            </div>
            <label for="saldo" class="col-auto col-form-label">Saldo</label>
            <div class="col">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">$</div>
                    </div>
                    <input type="text" name="saldo" id="saldo" class="form-control" value="{{$cabeceraFactura->total-$recaudo}}" disabled="">
                </div>
            </div>
        </div>
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-cheque_vista-tab" data-toggle="tab" href="#nav-cheque_vista" role="tab" aria-controls="nav-cheque_vista" aria-selected="true">CHEQUE VISTA</a>
                <a class="nav-item nav-link" id="nav-cheque_posfechado-tab" data-toggle="tab" href="#nav-cheque_posfechado" role="tab" aria-controls="nav-cheque_posfechado" aria-selected="false">CHEQUE POSFECHADO</a>
                <a class="nav-item nav-link" id="nav-efectivo-tab" data-toggle="tab" href="#nav-efectivo" role="tab" aria-controls="nav-efectivo" aria-selected="false">EFECTIVO</a>
                <a class="nav-item nav-link" id="nav-deposito-tab" data-toggle="tab" href="#nav-deposito" role="tab" aria-controls="nav-deposito" aria-selected="false">DEPOSITO</a>
                <a class="nav-item nav-link" id="nav-transferencia-tab" data-toggle="tab" href="#nav-transferencia" role="tab" aria-controls="nav-transferencia" aria-selected="false">TRANSFERENCIA</a>
            </div>
        </nav>
        <br>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade" id="nav-cheque_vista" role="tabpanel" aria-labelledby="nav-cheque_vista-tab">
                <div>
                    <div class="form-group form-row">
                        <label for="codigo_cheque_vista" class="col-4 col-form-label">Codigo</label>
                        <div class="col-8">
                            <input type="text" name="codigo_cheque_vista" id="codigo_cheque_vista" class="form-control" disabled="">
                            <div id="invalido_codigo" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="fecha_cobro_cheque_vista" class="col-4 col-form-label">Fecha Cobro</label>
                        <div class="col-8">
                            <input type="date" name="fecha_cobro_cheque_vista" id="fecha_cobro_cheque_vista" class="form-control" disabled="">
                            <div id="invalido_fecha_cheque_vista" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="fecha_efectivizacion_cheque_vista" class="col-4 col-form-label">Fecha de Efectivizacion</label>
                        <div class="col-8">
                            <input type="date" name="fecha_efectivizacion_cheque_vista" id="fecha_efectivizacion_cheque_vista" class="form-control" disabled="">
                            <div id="invalido_fecha_cobro_cheque_vista" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="banco_id_cheque_vista" class="col-4 col-form-label">Banco</label>
                        <div class="col-8">
                            <select name="banco_id_cheque_vista" id="banco_id_cheque_vista" class="form-control" required>
                            </select>
                            <div id="invalido_banco_cheque_vista" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="numero_cheque_vista" class="col-4 col-form-label">No. Cheque</label>
                        <div class="col-8">
                            <input type="text" name="numero_cheque_vista" id="numero_cheque_vista" class="form-control" required>
                            <div id="invalido_numero_cheque_vista" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="comentario_cheque_vista" class="col-4 col-form-label">Comentario</label>
                        <div class="col-8">
                            <textarea name="comentario_cheque_vista" id="comentario_cheque_vista" class="form-control" required></textarea>
                            <div id="invalido_comentario_cheque_vista" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="valor_cheque_vista" class="col-4 col-form-label">Valor</label>
                        <div class="col-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">$</div>
                                </div>
                                <input type="number" step="any" name="valor_cheque_vista" id="valor_cheque_vista" class="form-control" required value="{{$cabeceraFactura->total-$recaudo}}" max="{{$cabeceraFactura->total-$recaudo}}">
                            </div>
                            <div id="invalido_valor_cheque_vista" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-success" id="crearChequeVista">Crear Cheque a la vista</button>
                    </div>
                    <div id="alerta" role="alert">
                    </div>
                    <div class="form-group form-row">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>FECHA COBRO</th>
                                    <th>FECHA EFECT</th>
                                    <th>NUMERO</th>
                                    <th>COMENTARIO</th>
                                    <th>VALOR</th>
                                    <th>OPCIONES</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($recaudacionesEfectivo!=null)
                                @foreach($recaudacionesEfectivo['CHEQUE A LA VISTA'] as $cheque)
                                <tr>
                                    <td>
                                        {{$cheque->fecha_cobro}}
                                    </td>
                                    <td>
                                        {{$cheque->fecha_efectivizacion}}
                                    </td>
                                    <td>
                                        {{$cheque->numero}}
                                    </td>
                                    <td>
                                        {{$cheque->comentario}}
                                    </td>
                                    <td>
                                        ${{$cheque->valor}}
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-justified">
                                            <button type="button" class="btn btn-default" name="modificarCheque" id="{{$cheque->id}}" data-toggle="modal" data-target="#modalModificarCheque">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                            <button type="button" class="btn btn-default" name="eliminarCheque" id="{{$cheque->id}}" data-toggle="modal" data-target="#modalEliminarCheque">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-cheque_posfechado" role="tabpanel" aria-labelledby="nav-cheque_posfechado-tab">
                <div>
                    <div class="form-group form-row">
                        <label for="codigo_cheque_posfechado" class="col-4 col-form-label">Codigo</label>
                        <div class="col-8">
                            <input type="text" name="codigo_cheque_posfechado" id="codigo_cheque_posfechado" class="form-control" disabled="">
                            <div id="invalido_codigo_cheque_posfechado" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="fecha_cobro_cheque_posfechado" class="col-4 col-form-label">Fecha Cobro</label>
                        <div class="col-8">
                            <input type="date" name="fecha_cobro_cheque_posfechado" id="fecha_cobro_cheque_posfechado" class="form-control" disabled="">
                            <div id="invalido_fecha_cobro_cheque_posfechado" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="fecha_efectivizacion_cheque_posfechado" class="col-4 col-form-label">Fecha de Efectivizacion</label>
                        <div class="col-8">
                            <input type="date" name="fecha_efectivizacion_cheque_posfechado" id="fecha_efectivizacion_cheque_posfechado" class="form-control" required>
                            <div id="invalido_fecha_efectivizacion_cheque_posfechado" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="banco_id_cheque_posfechado" class="col-4 col-form-label">Banco</label>
                        <div class="col-8">
                            <select name="banco_id_cheque_posfechado" id="banco_id_cheque_posfechado" class="form-control" required>
                            </select>
                            <div id="invalido_banco_id_cheque_posfechado" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="numero_cheque_posfechado" class="col-4 col-form-label">No. Cheque</label>
                        <div class="col-8">
                            <input type="text" name="numero_cheque_posfechado" id="numero_cheque_posfechado" class="form-control" required>
                            <div id="invalido_numero_cheque_posfechado" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="comentario_cheque_posfechado" class="col-4 col-form-label">Comentario</label>
                        <div class="col-8">
                            <textarea name="comentario_cheque_posfechado" id="comentario_cheque_posfechado" class="form-control" required></textarea>
                            <div id="invalido_comentario_cheque_posfechado" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="valor_cheque_posfechado" class="col-4 col-form-label">Valor</label>
                        <div class="col-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">$</div>
                                </div>
                                <input type="number" step="any" name="valor_cheque_posfechado" id="valor_cheque_posfechado" class="form-control" required value="{{$cabeceraFactura->total-$recaudo}}" max="{{$cabeceraFactura->total-$recaudo}}">
                            </div>
                            <div id="invalido_valor_cheque_posfechado" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-success" id="crearChequePosfechado">Crear Cheque Posfechado</button>
                    </div>
                    <div id="alerta" role="alert">
                    </div>
                    <div class="form-group form-row">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>FECHA COBRO</th>
                                    <th>FECHA EFECT</th>
                                    <th>NUMERO</th>
                                    <th>COMENTARIO</th>
                                    <th>VALOR</th>
                                    <th>OPCIONES</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($recaudacionesEfectivo!=null)
                                @foreach($recaudacionesEfectivo['CHEQUE POSFECHADO'] as $cheque)
                                <tr>
                                    <td>
                                        {{$cheque->fecha_cobro}}
                                    </td>
                                    <td>
                                        {{$cheque->fecha_efectivizacion}}
                                    </td>
                                    <td>
                                        {{$cheque->numero}}
                                    </td>
                                    <td>
                                        {{$cheque->comentario}}
                                    </td>
                                    <td>
                                        ${{$cheque->valor}}
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-justified">
                                            <button type="button" class="btn btn-default" name="modificarCheque" id="{{$cheque->id}}" data-toggle="modal" data-target="#modalModificarCheque">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                            <button type="button" class="btn btn-default" name="eliminarCheque" id="{{$cheque->id}}" data-toggle="modal" data-target="#modalEliminarCheque">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-efectivo" role="tabpanel" aria-labelledby="nav-efectivo-tab">
                <div>
                    <div class="form-group form-row">
                        <label for="codigo_efectivo" class="col-4 col-form-label">Codigo</label>
                        <div class="col-8">
                            <input type="text" name="codigo_efectivo" id="codigo_efectivo" class="form-control" disabled="">
                            <div id="invalido_codigo_efectivo" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="fecha_cobro_efectivo" class="col-4 col-form-label">Fecha de Cobro</label>
                        <div class="col-8">
                            <input type="date" name="fecha_cobro_efectivo" id="fecha_cobro_efectivo" class="form-control" disabled="">
                            <div id="invalido_fecha_cobro_efectivo" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="comentario_efectivo" class="col-4 col-form-label">Comentario</label>
                        <div class="col-8">
                            <textarea name="comentario_efectivo" id="comentario_efectivo" class="form-control" required></textarea>
                            <div id="invalido_comentario_efectivo" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="establecimiento_id_efectivo" class="col-4 col-form-label">Establecimiento</label>
                        <div class="col-8">
                            <select name="establecimiento_id_efectivo" id="establecimiento_id_efectivo" class="form-control" required>
                            </select>
                            <div id="invalido_cajero_id_efectivo" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="punto_venta_id_efectivo" class="col-4 col-form-label">Caja</label>
                        <div class="col-8">
                            <select name="punto_venta_id_efectivo" id="punto_venta_id_efectivo" class="form-control" required>
                            </select>
                            <div id="invalido_punto_venta_id_efectivo" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="valor_efectivo" class="col-4 col-form-label">Valor</label>
                        <div class="col-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">$</div>
                                </div>
                                <input type="number" step="any" name="valor_efectivo" id="valor_efectivo" class="form-control" required value="{{$cabeceraFactura->total-$recaudo}}" max="{{$cabeceraFactura->total-$recaudo}}">
                            </div>
                            <div id="invalido_valor_efectivo" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-success" id="crearEfectivo">Crear Efectivo</button>
                    </div>
                    <div id="alerta" role="alert">
                    </div>
                    <div class="form-group form-row">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>FECHA</th>
                                    <th>COMENTARIO</th>
                                    <th>VALOR</th>
                                    <th>PUNTO DE VENTA</th>
                                    <th>OPCIONES</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($recaudacionesEfectivo!=null)
                                @foreach($recaudacionesEfectivo['EFECTIVO'] as $efectivo)
                                <tr>
                                    <td>
                                        {{$efectivo->fecha_cobro}}
                                    </td>
                                    <td>
                                        {{$efectivo->comentario}}
                                    </td>
                                    <td>
                                        ${{$efectivo->valor}}
                                    </td>
                                    <td>
                                        {{$efectivo->puntoVenta->codigo}}
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-justified">
                                            <button type="button" class="btn btn-default" name="modificarEfectivo" id="{{$efectivo->id}}" data-toggle="modal" data-target="#modalModificarEfectivo">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                            <button type="button" class="btn btn-default" name="eliminarEfectivo" id="{{$efectivo->id}}" data-toggle="modal" data-target="#modalEliminarEfectivo">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-deposito" role="tabpanel" aria-labelledby="nav-deposito-tab">
                <div>
                    <div class="form-group form-row">
                        <label for="codigo_deposito" class="col-4 col-form-label">Codigo</label>
                        <div class="col-8">
                            <input type="text" name="codigo_deposito" id="codigo_deposito" class="form-control" disabled="">
                            <div id="invalido_codigo_deposito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="fecha_cobro_deposito" class="col-4 col-form-label">Fecha de Cobro</label>
                        <div class="col-8">
                            <input type="date" name="fecha_cobro_deposito" id="fecha_cobro_deposito" class="form-control" disabled="">
                            <div id="invalido_fecha_cobro_deposito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="numero_soporte_deposito" class="col-4 col-form-label">No. PAP DEP</label>
                        <div class="col-8">
                            <input type="text" name="numero_soporte_deposito" id="numero_soporte_deposito" class="form-control" required>
                            <div id="invalido_numero_soporte_deposito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="banco_id_deposito" class="col-4 col-form-label">Banco</label>
                        <div class="col-8">
                            <select name="banco_id_deposito" id="banco_id_deposito" class="form-control" required>
                            </select>
                            <div id="invalido_banco_id_deposito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="comentario_deposito" class="col-4 col-form-label">Comentario</label>
                        <div class="col-8">
                            <textarea name="comentario_deposito" id="comentario_deposito" class="form-control" required></textarea>
                            <div id="invalido_comentario_deposito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="valor_deposito" class="col-4 col-form-label">Valor</label>
                        <div class="col-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">$</div>
                                </div>
                                <input type="number" step="any" name="valor_deposito" id="valor_deposito" class="form-control" required value="{{$cabeceraFactura->total-$recaudo}}" max="{{$cabeceraFactura->total-$recaudo}}">
                            </div>
                            <div id="invalido_valor_deposito" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-success" id="crearDeposito">Crear Deposito</button>
                    </div>
                    <div id="alerta" role="alert">
                    </div>
                    <div class="form-group form-row">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>FECHA</th>
                                    <th>NUMERO SOPORTE</th>
                                    <th>COMENTARIO</th>
                                    <th>VALOR</th>
                                    <th>BANCO</th>
                                    <th>OPCIONES</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($recaudacionesEfectivo!=null)
                                @foreach($recaudacionesEfectivo['DEPOSITO'] as $deposito)
                                <tr>
                                    <td>
                                        {{$deposito->fecha_cobro}}
                                    </td>
                                    <td>
                                        {{$deposito->numero_soporte}}
                                    </td>
                                    <td>
                                        {{$deposito->comentario}}
                                    </td>
                                    <td>
                                        ${{$deposito->valor}}
                                    </td>
                                    <td>
                                        {{$deposito->banco->nombre}}
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-justified">
                                            <button type="button" class="btn btn-default" name="modificarDepositoTransferencia" id="{{$deposito->id}}" data-toggle="modal" data-target="#modalModificarDepositoTransferencia">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                            <button type="button" class="btn btn-default" name="eliminarDepositoTransferencia" id="{{$deposito->id}}" data-toggle="modal" data-target="#modalEliminarDepositoTransferencia">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-transferencia" role="tabpanel" aria-labelledby="nav-transferencia-tab">
                <div>
                    <div class="form-group form-row">
                        <label for="codigo_transferencia" class="col-4 col-form-label">Codigo</label>
                        <div class="col-8">
                            <input type="text" name="codigo_transferencia" id="codigo_transferencia" class="form-control" disabled="">
                            <div id="invalido_codigo_transferencia" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="fecha_cobro_transferencia" class="col-4 col-form-label">Fecha de Cobro</label>
                        <div class="col-8">
                            <input type="date" name="fecha_cobro_transferencia" id="fecha_cobro_transferencia" class="form-control" required>
                            <div id="invalido_fecha_cobro_transferencia" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="numero_soporte_transferencia" class="col-4 col-form-label">No. Transferencia</label>
                        <div class="col-8">
                            <input type="text" name="numero_soporte_transferencia" id="numero_soporte_transferencia" class="form-control" required>
                            <div id="invalido_numero_transferencia" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="banco_id_transferencia" class="col-4 col-form-label">Banco</label>
                        <div class="col-8">
                            <select name="banco_id_transferencia" id="banco_id_transferencia" class="form-control" required>
                            </select>
                            <div id="invalido_banco_id_transferencia" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="comentario_transferencia" class="col-4 col-form-label">Comentario</label>
                        <div class="col-8">
                            <textarea name="comentario_transferencia" id="comentario_transferencia" class="form-control" required></textarea>
                            <div id="invalido_comentario_transferencia" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="valor_transferencia" class="col-4 col-form-label">Valor</label>
                        <div class="col-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">$</div>
                                </div>
                                <input type="number" step="any" name="valor_transferencia" id="valor_transferencia" class="form-control" required value="{{$cabeceraFactura->total-$recaudo}}" max="{{$cabeceraFactura->total-$recaudo}}">
                            </div>
                            <div id="invalido_valor_transferencia" class="invalid-feedback">
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-success" id="crearTransferencia">Crear Transferencia</button>
                    </div>
                    <div id="alerta" role="alert">
                    </div>
                    <div class="form-group form-row">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>FECHA</th>
                                    <th>NUMERO PAPELETA</th>
                                    <th>COMENTARIO</th>
                                    <th>VALOR</th>
                                    <th>BANCO</th>
                                    <th>OPCIONES</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($recaudacionesEfectivo!=null)
                                @foreach($recaudacionesEfectivo['TRANSFERENCIA'] as $transferencia)
                                <tr>
                                    <td>
                                        {{$transferencia->fecha_cobro}}
                                    </td>
                                    <td>
                                        {{$transferencia->numero_soporte}}
                                    </td>
                                    <td>
                                        {{$transferencia->comentario}}
                                    </td>
                                    <td>
                                        ${{$transferencia->valor}}
                                    </td>
                                    <td>
                                        {{$transferencia->banco->nombre}}
                                    </td>
                                    <td>
                                        <div class="btn-group btn-group-justified">
                                            <button type="button" class="btn btn-default" name="modificarDepositoTransferencia" id="{{$transferencia->id}}" data-toggle="modal" data-target="#modalModificarDepositoTransferencia">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                            <button type="button" class="btn btn-default" name="eliminarDepositoTransferencia" id="{{$transferencia->id}}" data-toggle="modal" data-target="#modalEliminarDepositoTransferencia">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-2"></div>
</div>

<div class="modal fade" id="modalModificarCheque" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <h2>CHEQUES</h2>
                    <hr>
                    <h3>Modificar un cheque</h3>
                    <hr>
                    <input type="hidden" id="id_chequeM" value="-1">
                    <div>
                        <div class="form-group form-row">
                            <label for="codigo_chequeM" class="col-3 col-form-label">Codigo</label>
                            <div class="col">
                                <input type="text" name="codigo_chequeM" id="codigo_chequeM" class="form-control" disabled="">  
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="fecha_cobro_chequeM" class="col-3 col-form-label">Fecha Cobro</label>
                            <div class="col">
                                <input type="date" name="fecha_cobro_chequeM" id="fecha_cobro_chequeM" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="fecha_efectivizacion_chequeM" class="col-3 col-form-label">Fecha Efectivizacion</label>
                            <div class="col">
                                <input type="date" name="fecha_efectivizacion_cheque" id="fecha_efectivizacion_chequeM" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="numero_chequeM" class="col-3 col-form-label">Numero</label>
                            <div class="col">
                                <input type="text" name="numero_chequeM" id="numero_chequeM" class="form-control" placeholder="Nombre del Dato Adicional" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="comentario_chequeM" class="col-3 col-form-label">Comentario</label> 
                            <div class="col">
                                <textarea name="comentario_chequeM" id="comentario_chequeM" class="form-control" required></textarea>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="banco_id_chequeM" class="col-3 col-form-label">Banco</label> 
                            <div class="col">
                                <input type="text" name="banco_id_chequeM" id="banco_id_chequeM" class="form-control" required></textarea>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="valor_chequeM" class="col-3 col-form-label">Valor</label> 
                            <div class="col">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">$</div>
                                    </div>
                                    <input type="number" step="any" name="valor_chequeM" id="valor_chequeM" class="form-control" required></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" id="cModificarCheque">Modificar Cheque</button>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEliminarCheque" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Cheque</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar?
                <input type="hidden" id="id_chequeE" value="-1">
                <br>
                <br>
                <div>
                    <div class="form-group form-row">
                        <label for="codigo_chequeE" class="col-3 col-form-label">Codigo</label>
                        <div class="col">
                            <input type="text" name="codigo_chequeE" id="codigo_chequeE" class="form-control" disabled="">  
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="fecha_cobro_chequeE" class="col-3 col-form-label">Fecha de Cobro</label>
                        <div class="col">
                            <input type="text" name="fecha_cobro_chequeE" id="fecha_cobro_chequeE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="numero_chequeE" class="col-3 col-form-label">Numero del cheque</label>
                        <div class="col">
                            <input type="text" name="numero_chequeE" id="numero_chequeE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="comentario_chequeE" class="col-3 col-form-label">Comentario</label>
                        <div class="col">
                            <textarea name="comentario_chequeE" id="comentario_chequeE" class="form-control" disabled=""></textarea>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="valor_chequeE" class="col-3 col-form-label">Valor</label> 
                        <div class="col">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">$</div>
                                </div>
                                <input type="text" name="valor_chequeE" id="valor_chequeE" class="form-control" disabled="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="banco_chequeE" class="col-3 col-form-label">Banco</label> 
                        <div class="col">
                            <input type="text" name="banco_chequeE" id="banco_chequeE" class="form-control" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarCheque">Eliminar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalModificarEfectivo" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <h2>EFECTIVO</h2>
                    <hr>
                    <h3>Modificar una recaudacion por efectivo</h3>
                    <hr>
                    <input type="hidden" id="id_efectivoM" value="-1">
                    <div>
                        <div class="form-group form-row">
                            <label for="codigo_efectivoM" class="col-3 col-form-label">Codigo</label>
                            <div class="col">
                                <input type="text" name="codigo_efectivoM" id="codigo_efectivoM" class="form-control" placeholder="Codigo" required disabled="">  
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="fecha_cobro_efectivoM" class="col-3 col-form-label">Fecha Cobro</label>
                            <div class="col">
                                <input type="date" name="fecha_cobro_efectivoM" id="fecha_cobro_efectivoM" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="comentario_efectivoM" class="col-3 col-form-label">Comentario</label> 
                            <div class="col">
                                <textarea name="comentario_efectivoM" id="comentario_efectivoM" class="form-control" required></textarea>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="valor_efectivoM" class="col-3 col-form-label">Valor</label> 
                            <div class="col">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">$</div>
                                    </div>
                                    <input type="number" step="any" name="valor_efectivoM" id="valor_efectivoM" class="form-control" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="punto_venta_id_efectivoM" class="col-3 col-form-label">Punto de Venta</label> 
                            <div class="col">
                                <input type="text" name="punto_venta_id_efectivoM" id="punto_venta_id_efectivoM" class="form-control" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" id="cModificarEfectivo">Modificar Efectivo</button>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEliminarEfectivo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Efectivo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar?
                <input type="hidden" id="id_efectivoE" value="-1">
                <br>
                <br>
                <div>
                    <div class="form-group form-row">
                        <label for="codigo_efectivoE" class="col-3 col-form-label">Codigo</label>
                        <div class="col">
                            <input type="text" name="codigo_efectivoE" id="codigo_efectivoE" class="form-control" disabled="">  
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="fecha_cobro_efectivoE" class="col-3 col-form-label">Fecha de Cobro</label>
                        <div class="col">
                            <input type="text" name="fecha_cobro_efectivoE" id="fecha_cobro_efectivoE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="comentario_efectivoE" class="col-3 col-form-label">Comentario</label>
                        <div class="col">
                            <textarea name="comentario_efectivoE" id="comentario_efectivoE" class="form-control" disabled=""></textarea>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="valor_efectivoE" class="col-3 col-form-label">Valor</label> 
                        <div class="col">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">$</div>
                                </div>
                                <input type="text" name="valor_efectivoE" id="valor_efectivoE" class="form-control" disabled="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarEfectivo">Eliminar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalModificarDepositoTransferencia" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <h2>DEPOSITOS O TRANSFERENCIAS</h2>
                    <hr>
                    <h3>Modificar</h3>
                    <hr>
                    <input type="hidden" id="id_deposito_transferenciaM" value="-1">
                    <div>
                        <div class="form-group form-row">
                            <label for="codigo_deposito_transferenciaM" class="col-3 col-form-label">Codigo</label>
                            <div class="col">
                                <input type="text" name="codigo_deposito_transferenciaM" id="codigo_deposito_transferenciaM" class="form-control" required disabled="">  
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="fecha_cobro_deposito_transferenciaM" class="col-3 col-form-label">Fecha Cobro</label>
                            <div class="col">
                                <input type="date" name="fecha_cobro_deposito_transferenciaM" id="fecha_cobro_deposito_transferenciaM" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="numero_soporte_deposito_transferenciaM" class="col-3 col-form-label">Numero de Soporte</label>
                            <div class="col">
                                <input type="text" name="numero_soporte_deposito_transferenciaM" id="numero_soporte_deposito_transferenciaM" class="form-control" placeholder="Nombre del Dato Adicional" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="comentario_deposito_transferenciaM" class="col-3 col-form-label">Comentario</label> 
                            <div class="col">
                                <textarea name="comentario_deposito_transferenciaM" id="comentario_deposito_transferenciaM" class="form-control" required></textarea>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="banco_id_deposito_transferenciaM" class="col-3 col-form-label">Banco</label> 
                            <div class="col">
                                <input type="text" name="banco_id_deposito_transferenciaM" id="banco_id_deposito_transferenciaM" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label for="valor_deposito_transferenciaM" class="col-3 col-form-label">Valor</label> 
                            <div class="col">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">$</div>
                                    </div>
                                    <input type="number" step="any" name="valor_deposito_transferenciaM" id="valor_deposito_transferenciaM" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" id="cModificarCheque">Modificar Cheque</button>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEliminarDepositoTransferencia" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Deposito o Transferencia</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar?
                <input type="hidden" id="id_deposito_transferenciaE" value="-1">
                <br>
                <br>
                <div>
                    <div class="form-group form-row">
                        <label for="codigo_deposito_transferenciaE" class="col-3 col-form-label">Codigo</label>
                        <div class="col">
                            <input type="text" name="codigo_deposito_transferenciaE" id="codigo_deposito_transferenciaE" class="form-control" disabled="">  
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="fecha_cobro_deposito_transferenciaE" class="col-3 col-form-label">Fecha de Cobro</label>
                        <div class="col">
                            <input type="text" name="fecha_cobro_deposito_transferenciaE" id="fecha_cobro_deposito_transferenciaE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="numero_soporte_deposito_transferenciaE" class="col-3 col-form-label">Numero de Soporte</label>
                        <div class="col">
                            <input type="text" name="numero_soporte_deposito_transferenciaE" id="numero_soporte_deposito_transferenciaE" class="form-control" disabled="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="comentario_deposito_transferenciaE" class="col-3 col-form-label">Comentario</label>
                        <div class="col">
                            <textarea name="comentario_deposito_transferenciaE" id="comentario_deposito_transferenciaE" class="form-control" disabled=""></textarea>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="valor_deposito_transferenciaE" class="col-3 col-form-label">Valor</label> 
                        <div class="col">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">$</div>
                                </div>
                                <input type="text" name="valor_deposito_transferenciaE" id="valor_deposito_transferenciaE" class="form-control" disabled="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="banco_deposito_transferenciaE" class="col-3 col-form-label">Banco</label> 
                        <div class="col">
                            <input type="text" name="banco_deposito_transferenciaE" id="banco_deposito_transferenciaE" class="form-control" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarDepositoTransferencia">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection