<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="csrf-token" content="{{ Session::token() }}">
        <title>@yield('title', 'Default')</title>
        <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/jquery-ui.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/fontawesome.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/adjustments.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/navbar-vertical.css')}}">
        <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/fontawesome-all.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/solid.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery-ui.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/navbar-vertical.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/shortcut.js')}}"></script>
        @yield('scripts', '')
    </head>
    <body>
        @yield('content', '')
        <!--<div class="container-fluid">
            @yield('content', '')
        </div>-->
    </body>
</html>
