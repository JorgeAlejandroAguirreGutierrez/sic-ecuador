@extends('plantillaHEAD')
@section('title')
Productos
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/inventarios/LAEProductos.js')}}"></script>
@endsection
@section('content')
<div class="row">
    <div class="col-2">
        <nav class="nav flex-column">
            <a class="nav-link active" href="{{action('inventarios\ControladorProductos@vistaLeerActualizarEliminar')}}">Ver</a>
            <a class="nav-link" href="{{action('inventarios\ControladorProductos@vistaCrear')}}">Crear</a>
        </nav>
    </div>
    <div class="col-10">
        <h2>PRODUCTOS</h2>
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>CÓDIGO</th>
                    <th>NOMBRE</th>
                    <th>CONSIG</th>
                    <th>CANT</th>
                    <th>PRECIO/U</th>
                    <th>TIPO</th>
                    <th>MEDIDA</th>
                    <th>IVA</th>
                    <th>BODEGA</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach($productos as $producto)
                <tr>
                    <td>
                        {{$producto->codigo}}
                    </td>
                    <td>
                        {{$producto->nombre}}
                    </td>
                    <td>
                        @if ($producto->consignacion==1)
                        {{"Sí"}}
                        @else
                        {{"No"}}
                        @endif                            
                    </td>
                    <td>
                        {{$producto->cantidad}}
                    </td>
                    <td>
                        ${{$producto->precio}}
                    </td>
                    <td>
                        {{$producto->tipo->nombre}}
                    </td>
                    <td>
                        {{$producto->medida->nombre}}
                    </td>
                    <td>
                        {{$producto->impuesto->codigo_norma}}
                    </td>
                    <td>
                        {{$producto->bodega->codigo}}
                    </td>
                    <td>
                        <div class="btn-group btn-group-justified">
                            <button type="button" class="btn btn-default" name="eliminarProducto" id="{{$producto->id}}" data-toggle="modal" data-target="#modalEliminarProducto">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modalEliminarProducto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Producto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar el producto?
                <input type="hidden" id="idE" value="-1">
                <br>
                <br>
                <div>
                    <div class="form-group form-row">
                        <label for="codigoE" class="col-3 col-form-label">Codigo</label>
                        <div class="col-9">
                            <input type="text" name="codigoE" id="codigoE" class="form-control" disabled="">  
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="nombreE" class="col-3 col-form-label">Nombre</label>
                        <div class="col-9">
                            <input type="text" name="nombreE" id="nombreE" class="form-control" disabled="">  
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label for="cantidadE" class="col-3 col-form-label">Cantidad</label>
                        <div class="col-9">
                            <input type="text" name="cantidadE" id="cantidadE" class="form-control" disabled="">  
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarProducto">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection