@extends('plantillaHEAD')
@section('title')
Bodegas
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/inventarios/LAEBodegas.js')}}"></script>
@endsection
@section('content')
<div class="row">
    <div class="col-2">
        <nav class="nav flex-column">
            <a class="nav-link active" href="{{action('inventarios\ControladorBodegas@vistaLeerActualizarEliminar')}}">Ver</a>
            <a class="nav-link" href="{{action('inventarios\ControladorBodegas@vistaCrear')}}">Crear</a>
        </nav>
    </div>
    <div class="col-10">
        <h2>BODEGAS</h2>
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>CÓDIGO</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach($bodegas as $bodega)
                <tr>
                    <td>
                        {{$bodega->codigo}}
                    </td>
                    <td>
                        <div class="btn-group btn-group-justified">
                            <button type="button" class="btn btn-default" name="eliminarBodega" id="{{$bodega->id}}" data-toggle="modal" data-target="#modalEliminarBodega">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modalEliminarBodega" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Bodega</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Esta seguro de eliminar la bodega?
                <input type="hidden" id="idE" value="-1">
                <br>
                <br>
                <div>
                    <div class="form-group form-row">
                        <label for="codigoE" class="col-3 col-form-label">Codigo</label>
                        <div class="col-9">
                            <input type="text" name="codigoE" id="codigoE" class="form-control" disabled="">  
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" id="cEliminarBodega">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection
