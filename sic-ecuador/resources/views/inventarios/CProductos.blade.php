@extends('plantillaHEAD')
@section('title')
Crear Productos
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/inventarios/CProductos.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('inventarios\ControladorBodegas@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0" target="_blank">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('inventarios\ControladorBodegas@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0" target="_blank">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>PRODUCTOS</h2>
        <hr>
        <h3>Añadir un Producto</h3>
        <hr>
        <div>
            <div class="form-group form-row">
                <label for="codigo" class="col-4 col-form-label">Codigo</label>
                <div class="col-8">
                    <input type="text" name="codigo" id="codigo" class="form-control" disabled="">
                    <div id="invalido_codigo" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="nombre" class="col-4 col-form-label">Nombre</label>
                <div class="col-8">
                    <input type="text" name="nombre" id="nombre" class="form-control">
                    <div id="invalido_nombre" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="consignacion" class="col-4 col-form-label">En consignacion</label>
                <div class="col-8">
                    <select name="consignacion" id="consignacion" class="form-control" required>
                        <option value="0">No</option>
                        <option value="1">Si</option>
                    </select>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="costo_compra" class="col-4 col-form-label">Costo de Compra</label>
                <div class="col-8">
                    <input type="number" name="costo_compra" id="costo_compra" class="form-control">
                    <div id="invalido_costo_compra" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="costo_kardex" class="col-4 col-form-label">Costo en Kardex</label>
                <div class="col-8">
                    <input type="number" name="costo_kardex" id="costo_kardex" class="form-control">
                    <div id="invalido_costo_kardex" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="medida_id" class="col-4 col-form-label">Medida</label>
                <div class="col-8">
                    <select name="medida_id" id="medida_id" class="form-control" required>
                    </select>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="cantidad" class="col-4 col-form-label">Cantidad</label>
                <div class="col-8">
                    <input type="number" min="1" name="cantidad" id="cantidad" class="form-control">
                    <div id="invalido_cantidad" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="precio" class="col-4 col-form-label">Precio Unitario</label>
                <div class="col-8">
                    <input type="number" min="0" name="precio" id="precio" class="form-control" required>
                    <div id="invalido_precio_unitario" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="tipo_id" class="col-4 col-form-label">Tipo</label>
                <div class="col-8">
                    <select name="tipo_id" id="tipo_id" class="form-control" required>
                    </select>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="impuesto_id" class="col-4 col-form-label">IVA</label>
                <div class="col-8">
                    <select name="impuesto_id" id="impuesto_id" class="form-control" required>
                    </select>
                </div>
            </div>
            <div class="form-group form-row">
                <label for="bodega_id" class="col-4 col-form-label">Bodega</label>
                <div class="col-8">
                    <select name="bodega_id" id="bodega_id" class="form-control" required>
                    </select>
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-success" id="crearProducto">Crear Producto</button>
            </div>
            <div id="alerta" role="alert">
            </div>
        </div>
    </div>
</div>
@endsection
