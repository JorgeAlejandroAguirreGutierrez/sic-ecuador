@extends('plantillaHEAD')
@section('title')
Crear Empresas
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/inventarios/CBodegas.js')}}"></script>
@endsection
@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navegacion">
    <a class="navbar-brand" href="#">SICE</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('inventarios\ControladorBodegas@vistaLeerActualizarEliminar')}}','','width=900px, height=600px toolbar=yes');void 0" target="_blank">Ver</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="javascript:window.open('{{action('inventarios\ControladorBodegas@vistaCrear')}}','','width=900px, height=600px toolbar=yes');void 0" target="_blank">Crear</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-12">
        <h2>BODEGAS</h2>
        <hr>
        <h3>Añadir una Bodega</h3>
        <hr>
        <div>
            <div class="form-group form-row">
                <label for="codigo" class="col-4 col-form-label">Codigo</label>
                <div class="col-8">
                    <input type="text" name="codigo" id="codigo" class="form-control" disabled="">
                    <div id="invalido_codigo" class="invalid-feedback">
                    </div>
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-success" id="crearBodega">Crear Bodega</button>
            </div>
            <div id="alerta" role="alert">
            </div>
        </div>
    </div>
</div>
@endsection
